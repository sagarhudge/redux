import AsyncStorage from '@react-native-community/async-storage';
// import firebase from 'react-native-firebase';
import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
 import React, { Component } from 'react';
import { Alert, AppState, SafeAreaView, View, DeviceEventEmitter } from 'react-native';
import AlertForeground from '../components/AlertForeground';
import Snakebar from '../components/Snakebar'
import NetInformation from '../components/NoInternetScreen'

//https://apoorv487.medium.com/testing-fcm-push-notification-through-postman-terminal-part-1-5c2df94e6c8d
//https://stackoverflow.com/questions/58461255/how-to-detect-an-application-in-the-kill-app-on-react-native

export default class FcmNotification extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      notificationClick: true,
      foregroundData: {}
    }
  }
  componentDidMount() {
    // this.getAppState();
    //let _self = this;


    console.log('deepraj')

    if (Platform.OS == 'android') {
      this.checkPermission();

    } else {
      console.log('else')

      this.authStatus();
    }
    this.createNotificationListeners(); //add this line
  }
  async authStatus() {
    const authorizationStatus = await messaging().requestPermission();
    console.log('authorizationStatus ',authorizationStatus)

    if (authorizationStatus) {
       this.getToken()
    }
  }
  componentWillUnmount() {
    //  this.notificationListener();
    // this.notificationOpenedListener();
  }
  async getAppState() {
    const nextAppState = await AsyncStorage.getItem('nextAppState');
    if (nextAppState !== AppState.currentState && nextAppState === "background") {
      //to check app killed from background -initial state wont change so compare on the basis
      AsyncStorage.setItem('login', 'false');
    }
  }
  async deepLink(notifyData) {

    console.log("deepLink_inside", notifyData.urlPath, notifyData.notification0Id);
    var isLogin = await AsyncStorage.getItem("login");

    if (notifyData && notifyData != null && notifyData.urlPath != null && notifyData.notificationId != null) {
      await AsyncStorage.setItem("notifyUrl", notifyData.urlPath);
      await AsyncStorage.setItem("cardTitle", notifyData.cardTitle);
      await AsyncStorage.setItem("notification_id", notifyData.notificationId);

    }

    // await AsyncStorage.setItem("notify", isLogin === null ? "false" : "true");
    // var toScreen = "";
    // if (isLogin == "true") {
    //   toScreen = "Home";
    // } else {
    //   toScreen = "SignInCognito";
    //   // this.props.navigation.replace('SignInCognito');
    // }
    // this.props.navigation.dispatch(
    //   CommonActions.reset({
    //     index: 0,
    //     routes: [{ name: toScreen }]
    //   }));
  }


  async createNotificationListeners() {

    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log("setBackgroundMessageHandler", remoteMessage);
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log('^^^^^^^^getInitialNotification^^^^^^^^^');
          // if (this.state.notificationClick)
          //DeviceEventEmitter.emit('customName', { data: remoteMessage.data })


          this.deepLink(remoteMessage.data);
        }
        //  setLoading(false);
      });

    messaging().onNotificationOpenedApp(remoteMessage => {

      // if (this.state.notificationClick && remoteMessage) {

      //   this.setState({ notificationClick: false }, () => {
      //     if (!this.state.notificationClick)
      //       this.deepLink(remoteMessage.data).then(
      //         () => { this.setState({ notificationClick: true }); }
      //       );
      //   });
      // }
      console.log('^^^^^^^^onNotificationOpenedApp^^^^^^^^^');
      DeviceEventEmitter.emit('customName', { data: remoteMessage.data, type: 'View' })

    });


    this.messageListener = messaging().onMessage((message) => {
      //process data message foreground data

      this.setState({ loading: true, foregroundData: message.data }, () => {
        console.log("11---------------------------------------");
        DeviceEventEmitter.emit('customName', { data: message.data, type: 'Received' })

      });

    });
  }

  showAlert(title, body) {
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }

  //1
  async checkPermission() {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('raj')
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
    console.log("fcmToken---------\n", fcmToken);

  }

  //2
  async requestPermission() {
    try {
      await messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  callbackClicks = (type) => {
    console.log("+++++++++++++++++++++-notification_callback*********************", type);

    DeviceEventEmitter.emit('customName', { data: this.state.foregroundData, type: type })

    this.setState({ loading: false, foregroundData: null }, () => { });

    // this.deepLink(this.state.foregroundData);

  }
   
  render() {
    return (
      <SafeAreaView >

        {
          this.state.loading ? <AlertForeground loading={this.state.loading}
            foregroundData={this.state.foregroundData}
            callbackClick={this.callbackClicks.bind(this)}
          /> : <View>
            <Snakebar></Snakebar>
          </View>
        }
 
      </SafeAreaView>
    );
  }
}
import React, { Component } from "react";
import { StatusBar, StyleSheet, Text, View, Image, DeviceEventEmitter, Linking, AppState } from "react-native";
 import AsyncStorage from '@react-native-community/async-storage';
 import { AppImagesSplash } from '../components/AppImages'
export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.navigate = this.props.navigation.navigate;
    this.state = {
      message: 'Something went wrong. Try again',
      isConnected: true,
      connect: true,
      deeplinkUsername: null,
      deeplinkPassword: null,
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          hidden={true}
          translucent={true}
        />

        <AppImagesSplash />

      </View>
    )
  }


  componentDidMount() {
    // this.getAppState();
    this.nav()
  }

  nav() {
    setTimeout(() => this.props.navigation.replace('Auth'), 4000);
    // setTimeout(() => this.props.navigation.replace('AppNavigator'), 4000);

  }


  async getAppState() {
    const nextAppState = await AsyncStorage.getItem('nextAppState');
    if (nextAppState !== AppState.currentState && nextAppState === "background") {
      //to check app killed from background -initial state wont change so compare on the basis
      AsyncStorage.setItem('login', 'false');
    }
  }


}


let styles = StyleSheet.create({
  container: {
    flex: 1, justifyContent: 'center', backgroundColor: '#FFE8E3'
  },

  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch',
    justifyContent: 'center',
  },

  loginForm: {
    backgroundColor: 'transparent',
    color: 'white'
  },

  text: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: 'black',
    textAlign: 'center'
  }
});

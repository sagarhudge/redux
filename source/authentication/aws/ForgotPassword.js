"use strict";

import React, { PureComponent } from "react";
import { SafeAreaView, StatusBar, View, Image, ScrollView, TouchableOpacity, Modal, BackHandler } from "react-native";
import { Card, Text } from "react-native-elements";
import styles from "../../constants/StyleSheet";
import TextInput from "react-native-material-textinput";
import { Auth } from "aws-amplify";
import { ButtonSmall } from "../../components/Buttons";
import AppStrings from '../../constants/AppStrings'
import COLORS from "../../constants/COLORS";
import Loader from '../../components/Loader';
import { Icon } from "native-base";
import { passwordCheck } from '../../constants/utils';
// import OfflineNotice from '../components/OfflineNotice'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { AppImages } from "../../components/AppImages"
import { testFont, FontsApp } from '../../constants/FontsApp'

let regular = FontsApp.regular;
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading;
let smallText = FontsApp.smallText;
let inputTextHeight = FontsApp.inputTextHeight;
let lineHeight = hp("0.4%");

export default class ForgotPassword extends PureComponent {

    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        const { username, sentMail } = this.props.route.params;
        console.log("username recived " + username);

        const errorMessage = "";
        this.state = {
            error: '',
            password: '',
            code: '',
            newPasswordInput: '',
            loading: false,
            user: {},
            sentMail: sentMail,
            username: username,
            title: AppStrings.loginTo,
            changePasswordResponse: {},
            iconFirst: "eye-off",
            showPasswordFirst: true,
            showPasswordSecond: true,
            iconSecond: "eye-off",

            isLowerCase: false,
            isUpperCase: false,
            isNumber: false,
            hasSymbol: false,
            pwdLength: false,
        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);

    }
    handleBackPress = async () => {
        this.props.navigation.goBack();
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1, justifyContent: 'center', backgroundColor: '#d4d4d4', height: hp('100%'), width: wp('100%') }}>

                <StatusBar barStyle="light-content" backgroundColor='#d4d4d4' />
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <Card containerStyle={{ flex: 1, borderRadius: 15, justifyContent: 'center', alignItems: 'center', padding: "5%" }}>

                        <AppImages />

                        <View>
                            {this.state.error ? <View style={{ marginTop: hp("1%") }}>
                                <Text style={styles.errorContainer}>{this.state.error}</Text>
                            </View> : null}
                            <View style={{ marginTop: hp("2%") }}>

                                <Text style={styles.sendEmail}>We have sent a password reset code by email to  {this.state.sentMail}. Enter it below to reset your password.</Text>

                                <TextInput
                                    labelActiveTop={-30}
                                    autoFocus={true}
                                    value={this.state.code}
                                    fontSize={inputTextHeight}
                                    fontFamily={FontsApp.fontFamily}

                                    labelActiveColor={'#533278'}
                                    label={AppStrings.code}
                                    underlineColor={'#b9b9b9'}
                                    underlineActiveColor={'#773ba5'}
                                    underlineHeight={lineHeight}
                                    onChangeText={this.onChangeTextCode}
                                />

                                <View>
                                    <TextInput
                                        labelActiveTop={-30}
                                        value={this.state.password}
                                        fontFamily={FontsApp.fontFamily}
                                        fontSize={inputTextHeight}
                                        labelActiveColor={'#533278'}
                                        label={AppStrings.enter_new_password}
                                        secureTextEntry={this.state.showPasswordFirst}
                                        underlineColor={'#b9b9b9'}
                                        underlineActiveColor={'#773ba5'}
                                        underlineHeight={lineHeight}
                                        onChangeText={this.onChangeTextpassword}
                                    />
                                    <Icon onPress={() => this.changeIconF()} name={this.state.iconFirst} style={{
                                        right: 5, position: 'absolute', marginTop: hp("2%"),
                                        fontSize: heading
                                    }} />
                                </View>
                                <View>
                                    <TextInput
                                        marginTop={10}
                                        labelActiveTop={-30}
                                        value={this.state.newPasswordInput}
                                        fontSize={inputTextHeight}
                                        label={AppStrings.confirmPassword}
                                        fontFamily={FontsApp.fontFamily}
                                        labelActiveColor={'#533278'}
                                        secureTextEntry={this.state.showPasswordSecond}
                                        underlineColor={'#b9b9b9'}
                                        underlineActiveColor={'#773ba5'}
                                        underlineHeight={lineHeight}
                                        onChangeText={this.onChangeTextConfirmPassword}
                                    />
                                    <Icon onPress={() => this.changeIconS()} name={this.state.iconSecond} style={{
                                        right: 5, position: 'absolute',
                                        marginTop: hp("2%"), ...testFont,
                                        fontSize: heading
                                    }} />

                                </View>
                                <View style={{ marginTop: hp("1.5%"), alignItems: 'center' }}>
                                    <ButtonSmall title={AppStrings.changePassword} isDisabled={false}
                                        callback={this.buttonPress.bind(this)} /></View>
                                {
                                    //password validation view
                                    this.state.password !== "" ?
                                        <View style={{ marginTop: hp("2%"), alignSelf: 'center' }}>
                                            <Text style={{ ...testFont, fontSize: smallText, color: this.state.isLowerCase ? '#28a745' : '#dc3545' }}>{this.state.isLowerCase ? '✓' : '✖'} Password must contain a lower case letter</Text>
                                            <Text style={{ ...testFont, fontSize: smallText, color: this.state.isUpperCase ? '#28a745' : '#dc3545' }}>{this.state.isUpperCase ? '✓' : '✖'} Password must contain an upper case letter</Text>
                                            <Text style={{ ...testFont, fontSize: smallText, color: this.state.hasSymbol ? '#28a745' : '#dc3545' }}>{this.state.hasSymbol ? '✓' : '✖'} Password must contain a special character</Text>
                                            <Text style={{ ...testFont, fontSize: smallText, color: this.state.isNumber ? '#28a745' : '#dc3545' }}>{this.state.isNumber ? '✓' : '✖'} Password must contain a number</Text>
                                            <Text style={{ ...testFont, fontSize: smallText, color: this.state.pwdLength ? '#28a745' : '#dc3545' }}>{this.state.pwdLength ? '✓' : '✖'} Password must contain at least 8 characters</Text>

                                        </View>
                                        : null
                                }

                                <View style={{ marginTop: hp("5%") }}><TouchableOpacity activeOpacity={.7} hitSlop={{ top: hp("5%") }} style={{ alignItems: 'center', justifyContent: 'center' }} onPress={() => this.backToSignIn()}>
                                    <View style={{ height: hp("5%"), }}>
                                        <Text style={{ ...testFont, fontSize: subHeading }}>Go to <Text style={{ fontWeight: 'bold', color: '#e9716f' }}>Sign In</Text></Text>
                                    </View>
                                </TouchableOpacity></View>


                            </View>
                        </View>
                    </Card>
                </ScrollView>
                <Loader loading={this.state.loading} />

            </SafeAreaView>
        )
    }
    changeIconS() {
        this.setState(prev => ({
            iconSecond: prev.iconSecond === "eye-off" ? "eye" : "eye-off",
            showPasswordSecond: !prev.showPasswordSecond,
        }))
    }
    changeIconF() {
        this.setState(prev => ({
            iconFirst: prev.iconFirst === "eye-off" ? "eye" : "eye-off",
            showPasswordFirst: !prev.showPasswordFirst,
        }))
    }
    backToSignIn = () => {
        this.props.navigation.goBack();
    }

    onChangeTextCode = (code) => {
        if (code !== '') {
            this.setState({ code: code, error: '' });
        } else {
            this.setState({ code: '', error: "Confirmation code cannot be empty" });
        }

    };

    buttonPress() {
        this.setState({ loading: true, error: '' });

        if (this.state.code !== '' && this.state.password !== '' && this.state.newPasswordInput !== '' && this.state.password === this.state.newPasswordInput) {
            Auth.forgotPasswordSubmit(this.state.username, this.state.code, this.state.password)
                .then(data => {
                    console.log('password reset success' + JSON.stringify(data));
                    this.backToSignIn();

                })
                .catch(err => {
                    console.log("Error " + err.message);
                    this.setState({ loading: false, error: err.message });
                })

        } else {

            if (this.state.code == '') {
                this.setState({ loading: false, error: "Confirmation code cannot be empty" });
            }
            else if (this.state.password == '') {
                this.setState({ loading: false, error: "Password cannot be empty" });
            }
            else if (this.state.newPasswordInput == '') {
                this.setState({ loading: false, error: "Please enter same password" });
            }
            else {
                this.setState({ loading: false, error: "Enter password and confirmed password must be same!" });
            }
        }

    }


    onChangeTextpassword = (password) => {

        if (password !== '') {
            this.setState({
                password: password,
                error: "",
                isUpperCase: passwordCheck.checkForUpperCase(password),
                isLowerCase: passwordCheck.checkForLowerCase(password),
                hasSymbol: passwordCheck.checkForSymbols(password),
                isNumber: passwordCheck.checkForSymbols(password),
                pwdLength: passwordCheck.checkPwdLength(password),
            })
        } else {
            this.setState({ password: '' })
        }

    };
    onChangeTextConfirmPassword = (pass) => {

        if (pass !== '') {
            this.setState({ newPasswordInput: pass, error: '' });
        } else {
            this.setState({ newPasswordInput: '', error: "Password cannot be empty" });
        }

    };



    passwordReset = async () => {
        // https://aws-amplify.github.io/docs/js/authentication
        this.setState({ loading: true, error: '' });
        if (this.state.newPasswordInput === '') {
            this.setState({ loading: false, error: '' });
        } else {
            await Auth.completeNewPassword(
                this.state.user,
                this.state.newPasswordInput,
            ).then((resp) => {
                this.setState({
                    changePasswordResponse: resp,
                    newPasswordInput: '',
                    newPassword: false,
                    loading: false,
                    title: AppStrings.login_with_new_password
                });
            }).catch((error) => {
                this.setState({ loading: false, error: error.message });
            });
        }
    };

};



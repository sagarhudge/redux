"use strict";
import React, { PureComponent, Component } from "react";
import {
    SafeAreaView, Alert, StatusBar, View, Image, ScrollView, TouchableOpacity,
    AppState, DeviceEventEmitter, BackHandler, KeyboardAvoidingView
} from "react-native";
import { Card, Text } from "react-native-elements";
import styles from "../../constants/StyleSheet";
import TextInput from "react-native-material-textinput";
import AsyncStorage from '@react-native-community/async-storage';
import { Auth } from "aws-amplify";
import { ButtonSmall } from "../../components/Buttons";
import AppStrings from '../../constants/AppStrings'
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken } from '../../constants/CommonServices'
import awsConfig from "../awsConfig";
import BaseUrl from '../../constants/BaseUrl'
import AwesomeAlert from 'react-native-awesome-alerts';
import Loader from '../../components/Loader';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Item, Icon } from "native-base";
import { isAndroid } from '../../constants/PlatformCheck';
import Biometrics from 'react-native-biometric-identification';
import { ConfirmDialog, Dialog } from 'react-native-simple-dialogs';
import { passwordCheck } from '../../constants/utils';
import CheckBox from '@react-native-community/checkbox';
import NetInformation from "../../components/NetInfo"
import { AppImages } from "../../components/AppImages"
import RNExitApp from 'react-native-exit-app';
import COLORS from '../../constants/COLORS';
import PrivacyPolicy from '../../components/PrivacyPolicy';
// const optionalConfigObject = {
//     title: 'Fingerprint Authentication',
//     unifiedErrors: true, // use unified error messages (default false)

// };
import { testFont, FontsApp } from '../../constants/FontsApp'
import ReleaseManagement from "../../constants/Dev_Production";

let regular = FontsApp.regular;
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading;
let smallText = FontsApp.smallText;

const optionalConfigObject = {
    title: 'Authentication Required', // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    authFailDescription: 'Authentication Failed', // Android
    cancelText: 'CANCEL', // Android
    fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
    unifiedErrors: true, // use unified error messages (default false)
    passcodeFallback: true, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};
let inputTextHeight = FontsApp.inputTextHeight;
let lineHeight = hp("0.4%");
export default class SignInCognito extends PureComponent {
    // https://aws-amplify.github.io/docs/js/authentication
    //cognito implementation
    checkBioLogin = false;
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.email;
        this.password;
        this.state = {
            mobile: '',
            show: false,
            confirmShow: true,
            verificationCode: '',
            codeView: false,
            inputCode: '',
            error: '',
            appState: AppState.currentState,
            username: '',
            password: '',
            newUsername: '',
            checkRegistration: false,
            newPasswordInput: '',
            loading: false,
            newPassword: false,
            display: false,
            displayPrivacyCheck: false,
            user: {},
            accessToken: "",
            forgot: false,
            isConnected: null,
            title: AppStrings.loginTo,
            changePasswordResponse: {},
            confirmNewPassword: '',
            alertTitle: '',
            alertMessage: '',
            alertFrom: '',
            sentNumberCode: '',
            icon: "eye-off",
            iconConfirm: "eye-off",
            showPassword: true,
            showConfirmPassword: true,
            dialog: false,
            remebrShow: false,
            isSelected: false,
            setSelection: '',
            faceIdIcon: false
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <ConfirmDialog
                    title=""
                    message={"Do you want to continue with " + (isAndroid ? "Fingerprint Authentication?" : "Touch-Id or Face-Id ?")}
                    visible={this.state.dialog}
                    positiveButton={{
                        title: "YES",
                        onPress: () => this.setState({ dialog: false }, () => {
                            this.checkBioLogin = true;
                            this.pressHandler()
                        })
                    }}
                    negativeButton={{
                        title: "NO",
                        onPress: () => this.setState({ dialog: false }, () => this.verifyAndSignIn())
                    }}
                />
                <StatusBar barStyle="light-content" backgroundColor={COLORS.background} />

                <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior={isAndroid ? null : 'padding'}   >

                    <ScrollView
                        contentContainerStyle={{
                            flexGrow: 1, justifyContent: 'center', alignItems: 'center',
                            backgroundColor: COLORS.background,
                            paddingLeft: wp('5%'),
                            paddingRight: wp('5%')
                        }}>
                        <View>
                            <View style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: COLORS.white,
                                borderRadius: 15,
                                borderWidth: 1,
                                borderColor: '#d5d5d5',
                                padding: wp('4%')
                            }}>

                                <AppImages />

                                {this.state.error ?
                                    <Text style={styles.errorContainer}>{this.state.error}</Text>
                                    : null}
                                {
                                    this.state.newPassword === true
                                        ?
                                        this.state.forgot === true || this.state.checkRegistration
                                            ?
                                            //Forgot password email view
                                            <View style={{ top: hp("2.5%") }}>

                                                <TextInput
                                                    labelActiveTop={hp("-4%")}
                                                    autoFocus={false}
                                                    value={this.state.username}
                                                    fontSize={inputTextHeight}
                                                    fontFamily={FontsApp.fontFamily}
                                                    label={AppStrings.enter_your_email}
                                                    labelActiveColor={COLORS.labelActiveColor}
                                                    underlineColor={COLORS.underlineColor}
                                                    underlineActiveColor={COLORS.underlineActiveColor}
                                                    underlineHeight={lineHeight}
                                                    onChangeText={this.onChangeTextEmail}
                                                />

                                                <View style={{ marginTop: hp("1.5%"), alignItems: 'center' }}>

                                                    {
                                                        this.state.checkRegistration
                                                            ?
                                                            <ButtonSmall title={this.state.title} isDisabled={false}
                                                                callback={this.verifyUserRegiastration.bind(this, "Verify")} />
                                                            :
                                                            <ButtonSmall title={AppStrings.chnage_password} isDisabled={false}
                                                                callback={this.verifyUserRegiastration.bind(this, "Forgot")} />
                                                    }
                                                </View>

                                                <View style={{ marginTop: hp("2%") }}>
                                                    <Text style={{
                                                        color: '#5f2161',
                                                        fontWeight: 'bold',
                                                        textAlign: 'center', ...testFont,
                                                        fontSize: regular
                                                    }}>{this.state.forgot ? AppStrings.forgotText : AppStrings.havingtrouble} </Text>

                                                    <Text style={{
                                                        textAlign: 'center',
                                                        top: hp("1%"), ...testFont, fontWeight: 'bold',
                                                        fontSize: regular
                                                    }}>{this.state.forgot ? AppStrings.bottomForgot : AppStrings.lets_Check}</Text>
                                                </View>

                                                {this.buttonBackSignin()}

                                                <Text style={{
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    color: 'white'
                                                }}>
                                                </Text>
                                            </View>
                                            :
                                            //reset temp password
                                            <View style={{ top: hp("3%") }}>

                                                <Text style={{
                                                    fontSize: subHeading, ...testFont,
                                                    alignSelf: 'center',
                                                    color: '#6c757d'
                                                }}>Change Password</Text>
                                                <Text style={{
                                                    fontSize: regular, ...testFont,
                                                    alignSelf: 'center',
                                                    marginBottom: hp('2%'),
                                                    color: '#6c757d'
                                                }}>Please enter your new passsword below.</Text>
                                                <View>
                                                    <TextInput
                                                        labelActiveTop={hp("-4%")}
                                                        autoFocus={true}
                                                        fontFamily={FontsApp.fontFamily}
                                                        value={this.state.newPasswordInput}
                                                        fontSize={inputTextHeight}
                                                        label={AppStrings.enter_new_password}
                                                        secureTextEntry={this.state.showPassword}
                                                        labelActiveColor={COLORS.labelActiveColor}
                                                        underlineColor={COLORS.underlineColor}
                                                        underlineActiveColor={COLORS.underlineActiveColor}
                                                        underlineHeight={lineHeight}
                                                        onChangeText={this.onChangeTextNewPassword}
                                                    />
                                                    <Icon onPress={() => this.changeIcon()} name={this.state.icon} style={{
                                                        right: 5, position: 'absolute', marginTop: hp("3%"), ...testFont,
                                                        fontSize: heading
                                                    }} />

                                                </View>

                                                <View>
                                                    <TextInput
                                                        marginTop={10}
                                                        labelActiveTop={hp("-4%")}
                                                        value={this.state.confirmNewPassword}
                                                        fontSize={inputTextHeight}
                                                        label={AppStrings.confirmPassword}
                                                        labelActiveColor={COLORS.labelActiveColor}
                                                        underlineColor={COLORS.underlineColor}
                                                        underlineActiveColor={COLORS.underlineActiveColor} secureTextEntry={this.state.showConfirmPassword}
                                                        fontFamily={FontsApp.fontFamily}

                                                        underlineHeight={lineHeight}
                                                        onChangeText={this.onChangeTextConfirmPassword}
                                                    />
                                                    <Icon onPress={() => this.changeConfirmIcon()} name={this.state.iconConfirm} style={{
                                                        right: 5, position: 'absolute', marginTop: hp("3%"),
                                                        ...testFont, fontSize: heading
                                                    }} />

                                                </View>

                                                <ButtonSmall title={"Send"} isDisabled={false}
                                                    callback={this.passwordReset.bind(this)} />

                                                {this.buttonBackSignin()}
                                            </View>
                                        :
                                        this.state.codeView === false
                                            ?
                                            <View>
                                                {/* main login view */}
                                                <View style={{ top: hp("2%") }}>
                                                    <TextInput
                                                        labelActiveTop={hp("-3%")}
                                                        autoFocus={false}
                                                        value={this.state.username}
                                                        fontSize={inputTextHeight}
                                                        fontFamily={FontsApp.fontFamily}
                                                        label={AppStrings.enter_name}
                                                        labelActiveColor={COLORS.labelActiveColor}
                                                        underlineColor={COLORS.underlineColor}
                                                        underlineActiveColor={COLORS.underlineActiveColor}
                                                        underlineHeight={lineHeight}
                                                        onChangeText={this.onChangeTextEmail} />

                                                    <View style={{ justifyContent: 'center' }}>
                                                        <TextInput
                                                            marginTop={wp('1%')}
                                                            labelActiveTop={hp("-3%")}
                                                            value={this.state.password}
                                                            fontSize={inputTextHeight}
                                                            label={AppStrings.enter_password}
                                                            fontFamily={FontsApp.fontFamily}
                                                            labelActiveColor={COLORS.labelActiveColor}
                                                            underlineColor={COLORS.underlineColor}
                                                            underlineActiveColor={COLORS.underlineActiveColor}
                                                            secureTextEntry={this.state.showPassword}
                                                            underlineHeight={lineHeight}
                                                            onSubmitEditing={() => {
                                                                this.buttonPress();
                                                            }}
                                                            onKeyPress={(event) => {
                                                                //called when multi line is true
                                                                // if(event.nativeEvent.key == "Enter"){
                                                                //     this.buttonPress();
                                                                // }
                                                               // console.log("key event-", event.nativeEvent.key);

                                                            }}
                                                            onChangeText={this.onChangeTextpassword}
                                                        />
                                                        <Icon onPress={() => this.changeIcon()} name={this.state.icon} style={{
                                                            right: 5, position: 'absolute', marginTop: hp("4%"),
                                                            ...testFont, fontSize: heading
                                                        }} />
                                                    </View>

                                                    {/* Remember me layout */}
                                                    <View style={{ marginBottom: hp("1%"), flexDirection: 'row', alignItems: 'center', marginRight: 10 }}>
                                                        <CheckBox
                                                            boxType={'square'}
                                                            value={this.state.isSelected}
                                                            tintColors={{ true: COLORS.primaryColor, false: COLORS.graya1 }}
                                                            onValueChange={(resp) => {
                                                                this.setState({ isSelected: resp }, () => { });
                                                            }}
                                                            style={{ width: 20, height: 20 }}
                                                        />
                                                        <Text style={{
                                                            ...testFont,
                                                            marginLeft: hp("1%"),
                                                            fontSize: regular
                                                        }}>  Remember me</Text>
                                                    </View>
                                                    <View style={{
                                                        marginBottom: hp("1%"),
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        marginTop: hp('1%')
                                                    }}>
                                                        <CheckBox
                                                            boxType={'square'}
                                                            tintColors={{ true: COLORS.primaryColor, false: COLORS.graya1 }}
                                                            value={this.state.displayPrivacyCheck}
                                                            onValueChange={(resp) => {
                                                                this.setState({ display: true, displayPrivacyCheck: resp }, () => { });
                                                            }}
                                                            style={{ width: 20, height: 20 }}
                                                        />
                                                        <TouchableOpacity activeOpacity={.7} style={{ alignItems: 'center', justifyContent: 'center' }} onPress={() => this.ShowPrivacypolicy()}>

                                                            <Text style={{ fontSize: regular, ...testFont, marginLeft: hp('2%') }}>*I agree with the <Text style={{ fontWeight: 'bold', color: COLORS.primaryColor }}>privacy policy.</Text></Text>

                                                        </TouchableOpacity>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.forgot()}>
                                                        <Text style={{ color: '#e9716f', ...testFont, fontSize: regular }}>Forgot Your Password?</Text>
                                                    </TouchableOpacity>
                                                    <Text>{null}</Text>
                                                    <View style={{ alignItems: 'center' }}>
                                                        <ButtonSmall title={AppStrings.login} isDisabled={false}
                                                            callback={this.buttonPress.bind(this)} />
                                                    </View>
                                                </View>

                                                {/* main login screen */}
                                                <View style={{ marginTop: hp("3%"), marginBottom: hp("1%"), alignItems: 'center', justifyContent: 'center' }}>
                                                    <Text style={{
                                                        fontWeight: 'bold', color: '#000000',
                                                        alignItems: 'center', justifyContent: 'center', ...testFont,
                                                        fontSize: regular
                                                    }}>
                                                        {AppStrings.havingtrouble}</Text>
                                                    <Text style={{ textAlign: 'center', ...testFont, fontSize: regular, fontWeight: 'bold' }}>
                                                        <Text style={{ color: '#e9716f', ...testFont, fontSize: regular, fontWeight: 'bold' }} onPress={() => this.pleaseCheckEmail()}>
                                                            {AppStrings.pleaseCheck} </Text>{AppStrings.emailIsRegistered}
                                                    </Text>

                                                </View>
                                            </View>
                                            : //OTP layout
                                            <View>

                                                <View style={{ top: 10 }}>
                                                    {/* display textview with number */}

                                                    <Text style={styles.sendEmail}> We have delivered the authentication code by SMS to {this.state.sentNumberCode}. Please enter the code to complete authentication.</Text>
                                                    <TextInput
                                                        marginTop={10}
                                                        marginBottom={20}
                                                        labelActiveTop={hp("-4%")}
                                                        value={this.state.verificationCode}
                                                        fontSize={inputTextHeight}
                                                        fontFamily={FontsApp.fontFamily}

                                                        label={AppStrings.enterCode}
                                                        labelActiveColor={COLORS.labelActiveColor}
                                                        underlineColor={COLORS.underlineColor}
                                                        underlineActiveColor={COLORS.underlineActiveColor}
                                                        secureTextEntry={true}

                                                        underlineHeight={lineHeight}
                                                        onChangeText={this.onChangeCode}
                                                    />

                                                    <ButtonSmall title={AppStrings.login} isDisabled={false}
                                                        callback={this.confirmSignInMFA.bind(this)} />
                                                </View>

                                                <View style={{ marginTop: hp("5%") }}><TouchableOpacity activeOpacity={.7} hitSlop={{ top: hp("5%") }} style={{ alignItems: 'center', justifyContent: 'center' }} onPress={() => this.ResendCode()}>
                                                    <View style={{ height: hp("5%"), }}>
                                                        <Text style={{ ...testFont, fontSize: subHeading }}>Didn't receive? <Text style={{ fontWeight: 'bold', color: '#00f' }}>Resend Code</Text></Text>
                                                    </View>
                                                </TouchableOpacity></View>
                                                {this.buttonBackSignin()}

                                            </View>
                                }

                            </View>
                            <Text style={{
                                fontSize: hp("1.2%"),
                                alignItems: 'center',
                                position: 'relative',
                                textAlign: 'center',
                                color: COLORS.textColor,
                                margin: wp('2%')

                            }}>{AppStrings.copyright}</Text>
                        </View>
                    </ScrollView>


                </KeyboardAvoidingView>

                {this.displayAlert()}
                <Loader loading={this.state.loading} />
                <NetInformation abc={this.verifyConnection.bind(this)} />
                <PrivacyPolicy
                    viewMode={false}
                    display={this.state.display} privacyPolicy={this.privacyPolicyClick.bind(this)} />

            </SafeAreaView>
        )
    }
    onKeyPressPass(action) {

    }
    ResendCode() {
        this.signIn().then(() => {
            console.log("Success Login " + this.state.username)
        });
    }
    privacyPolicyClick(result) {
        this.setState({ display: false, displayPrivacyCheck: result, error: '' });
        console.log("privacyCheck result", result);

        AsyncStorage.setItem("PrivacyCheck", "" + result);

    }
    ShowPrivacypolicy() {
        this.setState({ display: true });
    }
    changeIcon() {
        this.setState(prev => ({
            icon: prev.icon === "eye-off" ? "eye" : "eye-off",
            showPassword: !prev.showPassword,
        }))
    }
    changeConfirmIcon() {
        this.setState(prev => ({
            iconConfirm: prev.iconConfirm === "eye-off" ? "eye" : "eye-off",
            showConfirmPassword: !prev.showConfirmPassword,
        }))
    }

    pressHandler() {
        Biometrics.authenticate('Scan your fingerprint on the device scanner to Continue.', optionalConfigObject)
            .then(successOptions => {
                console.log("successOptions -success", successOptions);
                this.checkBioLogin = false;
                this.setBioMetric(true);
            })
            .catch(error => {
                console.log("successOptions cancel", error.message);

                if (error.message === "User canceled authentication") {
                    if (this.checkBioLogin === true) {
                        this.setBioMetric(false);
                    } else {
                        this.setState({ password: '' }, () => { });
                    };
                    this.checkBioLogin = false;
                } else {
                    this.checkBioLogin = false;
                    this.setState({ password: '', error: "Authentication Failed" }, () => { });
                }
            });

    }
    async setBioMetric(val) {
        if (val) {

            AsyncStorage.setItem("biometric", "true");

            if (this.email && this.password) {
                this.setState({ username: this.email, password: this.password }, () => this.verifyAndSignIn());

            } else {
                this.verifyAndSignIn();
            }

        } else {
            this.setState({ password: '' }, () => { });
            AsyncStorage.setItem("biometric", "false")
            this.verifyAndSignIn();
        }
    }

    verifyConnection = (connection) => {
        if (this.state.isConnected != connection) {
            this.setState({ isConnected: connection });
            if (connection) {

                this.getAccessToken();
                // this.getCredentials()
            } else {
                this.getCredentials()
            }

            DeviceEventEmitter.emit('connectionCheck', connection);


        } else {
            console.log("verifyConnection")

        }



    }

    checkOfflineAndDataSaved() {

    }

    onChangeTextConfirmPassword = (confirmNewPassword) => {
        if (confirmNewPassword !== "") {
            this.setState({ confirmNewPassword: confirmNewPassword, error: '' }, () => { });
        } else {
            this.setState({ confirmNewPassword: "", error: AppStrings.enterValidPassword }, () => { });
        }

    }
    onChangeCode = (code) => {

        if (code !== "") {
            this.setState({ verificationCode: code, error: '' }, () => { });
        } else {
            this.setState({ verificationCode: "", error: "The code entered is invalid" }, () => { });
        }

    }

    displayAlert() {
        return <AwesomeAlert
            show={this.state.show}
            showProgress={false}
            title={this.state.alertTitle}
            message={this.state.alertMessage}
            closeOnTouchOutside={false}
            closeOnHardwareBackPress={false}
            showCancelButton={this.state.confirmShow}
            titleStyle={{ color: COLORS.textColor }}
            messageStyle={{ color: COLORS.textColor }}
            showConfirmButton={true}
            cancelText={AppStrings.cancel}
            confirmText={this.state.alertFrom === "" ? AppStrings.cancel : this.state.alertFrom}
            confirmButtonColor={COLORS.primaryColor}
            cancelButtonColor={COLORS.cancelButton}
            customView={

                this.state.faceIdIcon ? <Image
                    resizeMode="contain"
                    style={{
                        height: wp("15%"), width: wp("15%"),
                        margin: wp('3%')
                    }}
                    source={require('../../assets/images/face_id.png')}
                /> : null
            }
            onCancelPressed={() => {
                this.hideAlert();
            }}
            onConfirmPressed={() => {
                this.alertOkClick(this.state.alertFrom);
            }}
        />
    }

    showAlert = (title, message, from) => {
        this.setState({
            show: true, alertTitle: title, alertFrom: from, alertMessage: message
        }, () => { });
    };

    hideAlert = () => {
        this.setState({
            show: false, loading: false, error: '', faceIdIcon: false
        }, () => { });
    }

    pleaseCheckEmail() {
        this.setState({ username: '', newPassword: true, title: 'CHECK REGISTRATION', checkRegistration: true, error: '' }, () => { });
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.notificationListener = DeviceEventEmitter.addListener('customName', this.deepLink.bind(this));
    }

    async getPrivacyCheck() {
        var privacyCheck = await AsyncStorage.getItem('PrivacyCheck');
        // console.log("privacyCheck", privacyCheck);
        this.setState({
            isplayPrivacyCheck: JSON.parse(rememberMe) == null || !JSON.parse(privacyCheck) ? false : true
        });
    }
    async deepLink(notifyData) {
        // console.log("SIGNIN-----------------------------------------------", notifyData.data);
        if (notifyData && notifyData != null && notifyData.data.urlPath != null && notifyData.data.notificationId != null) {
            await AsyncStorage.setItem("notifyUrl", notifyData.data.urlPath);
            await AsyncStorage.setItem("cardTitle", notifyData.data.cardTitle);
            await AsyncStorage.setItem("notification_id", notifyData.data.notificationId);
        } else {
            console.log("deepLink " + "no value for notification path");
        }


    }
    async getCredentials() {
        this.email = await AsyncStorage.getItem('email');
        this.password = await AsyncStorage.getItem('password');
        let rememberMe = await AsyncStorage.getItem('rememberMe');
        //  let accesstoken = await AsyncStorage.getItem('accesstoken');
        var privacyCheck = await AsyncStorage.getItem('PrivacyCheck');

        this.setState({
            displayPrivacyCheck: JSON.parse(rememberMe) != null ? JSON.parse(privacyCheck) : false,
            isSelected: JSON.parse(rememberMe) != null ? JSON.parse(rememberMe) : false
        }, () => { });

        if (this.email) {
            this.setState({ username: this.state.isSelected ? this.email : '' }, () => { });
        }

        if (this.email !== undefined && this.email !== null && this.password !== undefined && this.password !== null) {
            let biometric = await AsyncStorage.getItem('biometric');
            if (biometric === "true") {
                this.touchIdSupported().then(biometryType => {
                    // console.log("biometryType------", biometryType);
                    // if (!(biometryType === 'FaceID')) {
                    this.checkBioMetric();
                    // } else {
                    //     this.setState({ faceIdIcon: true }, () => { });

                    //     this.showAlert("Face ID", "Do you want to continue with Face ID?", "SCAN");
                    // }

                }).catch(e => {
                    console.log("biometryType", e.message)
                })
            }
        }
    }

    async checkBioMetric() {
        this.pressHandler();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.notificationListener.remove();
    }

    handleConnectionChange = (isConnected) => {
        this.setState({ isConnected: isConnected }, () => { });
        // console.log(`is connected: ${this.state.status}`);
    }

    getAccessToken() {
        this.setState({ loading: true }, () => { });
        // console.log("getAccessToken ",);

        getGateWayToken().then(success => {
            if (success != null && success.headers != null && success.headers.authorization != null) {
                const token = success.headers.authorization;
                console.log("getAccessToken", token);
                AsyncStorage.setItem("accesstoken", token);
                this.setState({ accessToken: token, loading: false, error: '' }, () => { });
            } else {
                this.setState({
                    loading: false, error: success && success.message && success.message == 'Network Error' ?
                        AppStrings.noInternet : 'Something went wrong, Try again'
                }, () => { });
            }

            this.getCredentials();
        }).catch(e => {
            console.log("access token error", e.message)
            this.getCredentials()
        });
    }


    backToSignIn = () => {
        this.setState({
            username: this.email && this.state.isSelected ? this.email : '',
            password: '', password: '', newPassword: false, forgot: false,
            loading: false, error: '', codeView: false
        }, () => { });
    }

    alertOkClick = (from) => {
        this.setState({ show: false, error: '' }, () => { });

        if (from === AppStrings.forgot_your_password) {
            this.setState({ loading: true, error: '' }, () => { })
            Auth.forgotPassword(this.state.username)
                .then(data => {
                    console.log(data.CodeDeliveryDetails.Destination)
                    //navigate to forgot_password screen
                    this.props.navigation.navigate('ForgotPassword', { username: this.state.username, sentMail: data.CodeDeliveryDetails.Destination })
                    this.setState({ username: '', newPassword: false, loading: false }, () => { });
                })
                .catch(err => {
                    this.setState({ loading: false, confirmShow: false, error: err.message }), () => { };
                }

                )
        } else if (from === "Retry") {
            //retry to get access token
            this.setState({ loading: false }, () => { });

            this.getAccessToken();

        } else if (from === "Resend Email") {
            //this.setState({ forgot: false, newPassword: true, checkRegistration: false });
            let request = {
                "cognitoUserPool": awsConfig.userPoolId,
                "username": this.state.username
            }
            this.setState({ loading: true, error: '' }, () => { });

            postAxiosWithToken(BaseUrl.FABRIC_V1 + 'memberportal/resetuser', request, this.state.accessToken)
                .then((resp) => {
                    if (resp != null && resp.status === 200) {
                        var message = "Great news! We found your registration! We have sent a next step email with your temporary password to your email address.\n" + "Email sent succesfully";
                        this.setState({ loading: false, error: '', confirmShow: true }, () => { });
                        this.showAlert("", message, "Resend Email");
                    } else {
                        this.setState({ loading: false, confirmShow: false, error: resp != null && resp.message != null ? resp.message : 'Server Error' }, () => { });
                    }

                }).catch((err) => {
                    console.log(err)
                    let msg = 'Please try again!'
                    if (err.message) {
                        msg = err.message;
                    }
                    this.setState({ loading: false, confirmShow: false, error: '' }, () => { });

                    this.showAlert("Error", msg, "");

                })

        }
        else if (from === "Exit Application") {
            RNExitApp.exitApp();
        }
        else if (from === "SCAN") {
            this.checkBioMetric();
        }
        else {
            this.setState({ loading: false, error: '' }, () => { });

        }
    }

    forgot = () => {
        this.setState({ newPassword: true, forgot: true, error: '', checkRegistration: false, username: '' }, () => { });
    }

    buttonBackSignin() {
        return <View style={{ marginTop: hp("5%") }}>
            <TouchableOpacity activeOpacity={.7} hitSlop={{ top: hp("5%") }}
                style={{ alignItems: 'center', justifyContent: 'center' }}
                onPress={() => this.backToSignIn()}>

                <View style={{ height: hp("5%"), }}>
                    <Text style={{ ...testFont, fontSize: subHeading }}>Go to <Text style={{ fontWeight: 'bold', color: '#e9716f' }}>Sign In</Text></Text>
                </View>
            </TouchableOpacity>
        </View>
    }

    onChangeTextEmail = (email) => {
        if (email !== '') {
            this.setState({ username: email.trim(), error: '' }, () => { });
        } else {
            this.setState({ username: '', error: AppStrings.enterValidEmail }, () => { });
        }
    };

    onChangeTextpassword = (password) => {

        if (password !== '') {
            this.setState({ password: password, error: '' }, () => { });
        } else {
            this.setState({ password: '', error: AppStrings.enterValidPassword }, () => { });
        }
    };

    onChangeTextNewUsername = (newUsername) => {
        if (newUsername !== '') {
            this.setState({ newUsername: newUsername, error: '' }, () => { });
        } else {
            this.setState({ newUsername: '', error: AppStrings.enterValidEmail }, () => { });
        }
    };

    onChangeTextNewPassword = (NewPssword) => {
        if (NewPssword !== '') {
            this.setState({ newPasswordInput: NewPssword, error: '' }, () => { });
        } else {
            this.setState({ newPasswordInput: '', error: AppStrings.enterValidPassword }, () => { });
        }
    };

    buttonPress = async () => {
        let biometric = await AsyncStorage.getItem('biometric');

        if (this.state.username === '' || this.state.password === '') {
            const errorMsg = this.state.username === '' ? AppStrings.enterValidEmail : AppStrings.enterValidPassword
            this.setState({ loading: false, error: errorMsg }, () => { });
        } else {
            //check biometric is supported or not 
            this.touchIdSupported().then(biometryType => {
                console.log("biometryType------", biometryType);
                if (biometryType === 'FaceID' || biometryType === "Fingerprint" || biometryType === "TouchID") {
                    if (biometric === undefined || biometric === null || biometric === "false") {
                        this.setState({ dialog: true }, () => { });
                    } else {
                        this.verifyAndSignIn();
                    }
                } else {
                    this.verifyAndSignIn();
                }
            }).catch(e => {
                console.log("biometryType", e.message)
                this.verifyAndSignIn();

            })
        }
    }

    touchIdSupported = async () => {

        return Biometrics.isSupported(optionalConfigObject);
    }

    getUserData = async () => {
        var validUserName = passwordCheck.validateEmail(this.state.username);
        if (this.state.isConnected && this.state.username !== "" && validUserName) {
            this.setState({ loading: true }, () => { });
            let request = {
                "cognitoUserPool": awsConfig.userPoolId,
                "username": this.state.username
            }
            const getUser = BaseUrl.FABRIC_V1 + 'memberportal/getuser';

            console.log("getSource Id " + getUser + "\n" + this.state.accessToken);
            return postAxiosWithToken(getUser, request, this.state.accessToken)

        }
    }

    verifyAndSignIn() {

        if (!passwordCheck.validateEmail(this.state.username.trim())) {
            this.setState({ error: "Incorrect username or password." + this.state.username, loading: false }, () => { })
            return;
        }

        if (!this.state.displayPrivacyCheck) {
            this.setState({ error: "Please agree with the privacy policy." });
            return;
        }

        //Offline check

        if (this.state.isConnected) {
            const verifyUrl = BaseUrl.FABRIC_V1 + "memberportal/verifyemail/" + this.state.username.trim();
            console.log("verifyUrl " + verifyUrl + " " + this.state.accessToken);
            this.setState({ loading: true }, () => { });
            getAxiosWithToken(verifyUrl, this.state.accessToken)
                .then(response => {
                    if (response != null && response.status === 200) {
                        this.setState({ username: response.data, loading: true }, () => { });
                        this.signIn().then(() => {
                            console.log("Success Login " + this.state.username);
                        });

                    } else {
                        if (response != null && response.message != null && response.message !== "Network Error") {
                            this.setState({
                                loading: false, confirmShow: false,
                                error: response.message.includes("No valid Member found") ? "Incorrect username or password." : response.message
                            }, () => { });
                            if (response != null && response.message != null && response.message.includes("The Token has expired")) {

                                this.getAccessToken();
                            }
                        } else {
                            this.setState({ isConnected: false, loading: false, error: response && response.message == "Network Error" ? AppStrings.noInternet : response.message }, () => { });
                            console.log("Exception_Login_code_error-------------------------", response)

                        }

                    }

                }).catch = (e => {
                    console.log("Exception 6")

                    this.setState({ loading: false, error: e.message }, () => { });
                    this.getAccessToken();
                });
        } else {
            //offline verification
            console.log("**********LOGIN inside offline******");

            this.offLineLogin();
        }
    }

    offLineLogin() {
        //save in encrypt format
        if (this.email && this.password && this.email === this.state.username && this.password === this.state.password) {
            this.props.navigation.replace('AppNavigator')
            console.log("**********LOGIN Success******");
        } else {
            this.setState({ error: AppStrings.noInternet, loading: false }, () => { })
            console.log("**********LOGIN Fail******");
        }
    }
    verifyUserRegiastration = (from) => {

        var validUserName = passwordCheck.validateEmail(this.state.username);
        if (this.state.isConnected && this.state.username !== "" && validUserName) {

            const verifyUrl = BaseUrl.FABRIC_V1 + "memberportal/verifyemail/" + this.state.username.trim();
            console.log("verifyUrl " + verifyUrl + " " + this.state.accessToken);
            this.setState({ loading: true }, () => { });
            getAxiosWithToken(verifyUrl, this.state.accessToken).then(responseVerify => {

                if (responseVerify != null && responseVerify.status === 200) {
                    this.setState({ username: responseVerify.data }, () => {
                        // console.log("from " + from);
                        //forgot password and verify user

                        this.setState({ loading: false }, () => { });
                        let request = {
                            "cognitoUserPool": awsConfig.userPoolId,
                            "username": this.state.username
                        }
                        const getUser = BaseUrl.FABRIC_V1 + 'memberportal/getuser';
                        console.log("getSource Id " + getUser + "\n" + this.state.accessToken);
                        //First check if user is registered then process
                        postAxiosWithToken(getUser, request, this.state.accessToken)
                            .then(resp => {
                                //check response error code and add condition
                                let userstatus = 'NotFound'
                                if (resp != null && resp.status && resp.status === 200 && resp.data && resp.data.userStatus) {

                                    userstatus = resp.data.userStatus;
                                    console.log("respresp" + userstatus);

                                    if (userstatus == "CONFIRMED") {
                                        console.log("ForgotPassword " + this.state.username + " " + userstatus)
                                        if (from === "Forgot") {
                                            this.ForgotPassword();
                                        } else {
                                            this.setState({ confirmShow: true }, () => { });
                                            this.showAlert(AppStrings.allset,
                                                AppStrings.remember_password,
                                                AppStrings.forgot_your_password);
                                        }

                                    } else if (userstatus == "FORCE_CHANGE_PASSWORD") {
                                        let request = {
                                            "cognitoUserPool": awsConfig.userPoolId,
                                            "username": this.state.username
                                        }

                                        postAxiosWithToken(BaseUrl.FABRIC_V1 + 'memberportal/resetuser', request, this.state.accessToken)
                                            .then((resp) => {
                                                console.log(resp.data + " resp");
                                                if (resp != null && resp.status === 200) {
                                                    //Discuss flow and implement navigation need to add one otp input text in reset passsword section
                                                    //Same ceck for RESET password during signin according to display UI
                                                    this.setState({ loading: false, error: '', confirmShow: true }, () => { });
                                                    this.showAlert("", "Great news! We found your registration! We have sent a next step email with your temporary password to your email address.", "Resend Email");
                                                } else {
                                                    this.setState({ loading: false, confirmShow: false, error: response.message }, () => { });

                                                }

                                            }).catch((err) => {
                                                console.log(err)
                                                let msg = 'Please try again!'
                                                if (err.message) {
                                                    msg = err.message;
                                                }
                                                this.setState({ loading: false, confirmShow: false, error: msg }, () => { });
                                            })

                                    } else {
                                        //User not found

                                        let message = `Sorry! We could not find an active registration for ${this.state.username}.` +
                                            `If it’s been more than one business day since you signed up, please contact our Account Activation line: (888) 308 0024`;
                                        this.setState({ loading: false, error: '', confirmShow: false }, () => { });

                                        this.showAlert("", message, "");

                                    }
                                }
                                else {

                                    if (resp != null && resp.message != null && resp.message.includes("The Token has expired")) {

                                        this.getAccessToken();
                                    } else {
                                        let message = `Sorry! We could not find an active registration for ${this.state.username}.` +
                                            `If it’s been more than one business day since you signed up, please contact our Account Activation line: (888) 308 0024`;
                                        this.setState({ loading: false, error: '', confirmShow: false }, () => { });

                                        this.showAlert("", message, "");
                                    }

                                }

                            })
                            .catch(err => {
                                let msg = 'Please try again!'
                                if (err.message) {
                                    msg = err.message;
                                }
                                this.setState({ loading: false, confirmShow: false, error: msg }, () => { });
                            });


                    });
                } else {

                }

            })
        } else {
            if (!this.state.isConnected && this.state.username !== "" && validUserName) {
                this.setState({ error: AppStrings.noInternet, loading: false }, () => { });

            } else {
                this.setState({ error: AppStrings.enterValidEmail, loading: false }, () => { });
            }
        }

    }


    ForgotPassword() {
        console.log("ForgotPassword " + this.state.username);
        this.setState({ loading: true, error: '' })
        if (this.state.username !== "") {
            Auth.forgotPassword(this.state.username)
                .then(data => {
                    console.log(data.message + " \n " + data.name)
                    this.props.navigation.navigate('ForgotPassword', { username: this.state.username, sentMail: data.CodeDeliveryDetails.Destination })
                    this.setState({ username: '', newPassword: false, loading: false }, () => { })
                })
                .catch(err => {
                    this.setState({ loading: false, confirmShow: false, error: err.message });
                    // this.showAlert("Error", err.message, "");
                })

        } else {
            this.setState({ error: AppStrings.enterValidEmail }, () => { });
        }
    }

    passwordReset = async () => {
        this.setState({ loading: true }, () => { });
        if (this.state.newPasswordInput === "" || this.state.confirmNewPassword === "") {
            this.setState({ loading: false, error: 'Please enter valid email or password' }, () => { });
        } else {
            await Auth.completeNewPassword(
                this.state.user,
                this.state.newPasswordInput,
            ).then((resp) => {
                this.setState({
                    changePasswordResponse: resp,
                    newPasswordInput: '',
                    confirmNewPassword: '',
                    newPassword: false,
                    loading: false,
                    title: AppStrings.login_with_new_password
                }, () => { });
            }).catch((error) => {
                this.setState({ loading: false, error: error.message }, () => { });
            });
        }
    };

    signIn = async () => {

        AsyncStorage.setItem("rememberMe", JSON.stringify(this.state.isSelected));

        await Auth.signIn(this.state.username, this.state.password)
            .then(success => {
                //verify user email here take email from response
                const name = this.state.username;
                console.log("Login_Status_challengeParam", success.challengeParam)
                //To make visible change password component
                if (success.challengeName == "NEW_PASSWORD_REQUIRED") {
                    this.setState({
                        title: AppStrings.send,
                        newPassword: true,
                        user: success,
                        error: '',
                        forgot: false,
                        username: '',
                        password: '',
                        checkRegistration: false,
                        loading: false
                    }, () => { });
                } else if (success.challengeName === 'SMS_MFA' ||
                    success.challengeName === 'SOFTWARE_TOKEN_MFA') {
                    this.setState({
                        user: success,
                        codeView: true, newPassword: false, error: '', loading: false,
                        sentNumberCode: success.challengeParam.CODE_DELIVERY_DESTINATION
                    });

                }
                else {
                    console.log("elses--------")
                    this.state.user = success;
                    //get client id for notification
                    this.getOurClientId();
                }
            })
            .catch((error) => {
                this.setState({ loading: false, error: error.message }, () => { });
            });

    };

    getOurClientId() {
        var getOurClientId = BaseUrl.FABRIC_V2 + "memberportal/idcard";
        var emailObject = { "email": this.state.username }
        postAxiosWithToken(getOurClientId, emailObject, this.state.accessToken)
            .then((response) => {
                if (response != null && response.status === 200) {

                    let data = response.data.memberIdCardList;

                    this.setContactandCardID(data[0]);
                    console.log("clientId-------------", this.state.user.pool.clientId,
                        data[0].clientId);
                    // console.log("source----------------------------------------------------------", data)
                    AsyncStorage.setItem("client_id_locale", data[0].clientId);
                    AsyncStorage.setItem("username", data[0].firstName + " " + data[0].lastName);
                    AsyncStorage.setItem("email", data[0].email);
                    AsyncStorage.setItem("phone", data[0].phone);
                    AsyncStorage.setItem("source", data[0].source);
                    AsyncStorage.setItem("GetIdCardData", JSON.stringify(data));//encrypt
                    AsyncStorage.setItem("password", this.state.password);//encrypt
                    AsyncStorage.setItem("client_id", this.state.user.pool.clientId);

                    this.setState({ error: '', username: '', password: '' }, () => { });
                    this.getSourceID(data[0].email);


                    // this.saveUser();
                } else {
                    this.setState({ loading: false, error: response.message, confirmShow: false }, () => { });

                }
            }).catch(e => {
                this.setState({ loading: false, error: e.message }, () => { });

            });

    }

    setContactandCardID(data) {
        //C0ntact number to display on disabled card
        data.planInfo.map((data, index) => {
            if (data.idcardField == "contact number") {
                AsyncStorage.setItem("ContactNumber", data.fieldValue);
            }

        })

    }
    async saveUser() {
        let userName = await AsyncStorage.getItem('email');
        let client_id_locale = await AsyncStorage.getItem("client_id_locale");
        let fcmToken = await AsyncStorage.getItem("fcmToken");
        let name = await AsyncStorage.getItem("username");
        let phone = await AsyncStorage.getItem("phone");
        let type = isAndroid ? "android" : "ios";
        let userObject = {
            "email": userName,
            "phone": phone,
            "username": name,
            "token": fcmToken,
            "type": type,
            "clientId": client_id_locale,
            "activeflag": true
        }

        var saveUser = BaseUrl.V2_API_PATH + "Twillio/saveUser";
        console.log("saveUser1 ------------------------", saveUser, userObject);
        postAxiosWithToken(saveUser, userObject, "").then((response) => {

            if (response != null && response.status && response.status === 200) {
                this.setState({ loading: false, error: '' }, () => {
                    this.props.navigation.replace('AppNavigator')
                });
            } else {
                this.setState({ loading: false, error: response.message }, () => { });
            }
        });
    }

    getSourceID = (email) => {
        // console.log("getSourceID of-" + email);
        const urlSourceID = BaseUrl.FABRIC_V7 + `memberportal/getSourceId/` + email;
        this.setState({ loading: true }, () => { });
        console.log("getSource Id----------------------", urlSourceID, email)
        getAxiosWithToken(urlSourceID, "")
            .then(res => {
                // console.log("getSource Id----------------------", res)

                if (res != null && res.status === 200) {
                    console.log("getSource Id------memberIdSource----------------", res.data.memberIdSource)

                    if (res.data.memberIdSource) {
                        AsyncStorage.setItem("sourceID", res.data.memberIdSource);
                    } else {
                        AsyncStorage.setItem("sourceID", '');
                    }

                    this.setState({ error: '', username: '', password: '', }, () => { });
                    this.saveUser();

                } else {
                    this.setState({ loading: false, confirmShow: false, error: 'Fail to get source id' });
                }
            })
            .catch(error => {
                this.setState({ error: error.message, loading: false }, () => { });
            });
    }
    handleBackPress = async () => {

        if (this.state.forgot === true || this.state.checkRegistration === true) {
            this.setState({ forgot: false, checkRegistration: false, newPassword: false, username: this.email && this.state.isSelected ? this.email : '', password: '' }, () => { });
        } else {
            this.showAlert("", AppStrings.exitApplication, "Exit Application");

        }
    }
    confirmSignInMFA() {
        const { verificationCode, user } = this.state;
        this.setState({ loading: true });

        Auth.confirmSignIn(
            user,   // Return object from Auth.signIn()
            verificationCode, // Confirmation code
            "SMS_MFA"
            // MFA Type e.g. SMS_MFA, SOFTWARE_TOKEN_MFA
            //The code entered is invalid, please try again.
        ).then((resp) => {
            console.log("confirmSignInMFA", resp);
            this.getOurClientId();

        }).catch(err => {
            console.log("confirmSignInMFA-error", err.message);

            this.setState({
                loading: false,
                error: err.message.includes('Invalid code') ? "The code entered is invalid, please try again." : err.message
            }, () => { });

        });

    }
};
 
SignInCognito.navigationOptions = () => {
    return {
        header: null,
    };
};


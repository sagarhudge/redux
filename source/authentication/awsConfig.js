import ReleaseManagement from '../constants/Dev_Production'

 
const awsConfig = {
    region: ReleaseManagement.region,
    userPoolId: ReleaseManagement.userPoolId,
    userPoolWebClientId: ReleaseManagement.userPoolWebClientId,
    identityPoolId: ReleaseManagement.identityPoolId,

}



export default awsConfig;
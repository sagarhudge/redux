
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, Image } from "react-native";
import { getGateWayToken, getAxiosUser, getAxiosWithToken, postAxiosWithToken, getData } from '../constants/CommonServices'

import styles from "../constants/StyleSheet";
import Loader from "../components/Loader";
import COLORS from "../constants/COLORS"
import BaseUrl from '../constants/BaseUrl'
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import { passwordCheck } from '../constants/utils';
import AppStrings from "../constants/AppStrings";
import { SafeAreaView } from "react-native";
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let errorMessage = "You currently have no new notifications.";
import AwesomeAlertDialog from '../components/AwesomeAlertDialog';
import AnnouncementDialog from '../components/AnnouncementDialog';
import { FabButton } from "./botchat/FabButton";

export default class NotificationDetails extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.accessToken = '';
        this.offset = 0;
        this.previosPage = 0;
        this.email = '';
        this.notification = this.props.route.params.notification;
        console.log("notification", this.props.route.params)
        this.state = {
            data: [],
            AllCards: [],
            loading: false,
            error: ' ',
            loadMore: false,
            Connection: true,
            roeCount: 0,


            title: "",
            message: '',
            alertCancel: AppStrings.cancel,
            alertConfirm: '',
            alertCLose: AppStrings.cancel,
            canBeClosed: false,
            okHide: false,
            alertType: '',
            show: false,
            noticeData: null,
            showAlert: false,
        }

    }

    componentDidMount() {

        this.getData();

        if (this.navigation) {
            this.navigation.setOptions({
                title: this.notification ? AppStrings.notifications : AppStrings.announcementsNotices, //Set Header Title

                headerStyle: headerStyleContainer,
                headerTintColor: COLORS.white,
                headerTitleStyle: headerStyle,
            });
        }
    }
    async getData() {
        this.getOffline();
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.email = await AsyncStorage.getItem('email');

        this.getNotificationDetails();
    }

    showAlert(title, message, buttonType) {
        this.setState({
            alertType: '', show: true,
            title: title, message: message == "Network Error" ? AppStrings.noInternet : message, alertConfirm: AppStrings.ok,
            canBeClosed: true, alertCancel: AppStrings.cancel, okHide: false
        });
    }

    showAlertNotice(result) {
        this.setState({
            showAlert: true, noticeData: result
        });
    }

    render() {
        return (

            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
                {this.state.error ?
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: hp("2%") }}>
                        <Text style={{ alignSelf: 'center', fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.error}</Text>
                    </View>
                    : <FlatList
                        showsVerticalScrollIndicator={false}
                        numColumns={1}
                        data={this.state.data}
                        renderItem={this.renderItem.bind(this)}

                        onEndReachedThreshold={1}
                        ListFooterComponent={
                            this.state.loadMore ? <View style={{
                                flexDirection: 'row',

                                justifyContent: 'space-around'
                            }}>

                                <Text
                                    onPress={() =>
                                        this.goToNext()
                                    }
                                    style={styles.footerActive}>
                                    {`Load More`}</Text>

                            </View> : null}

                        keyExtractor={(item, index) => index.toString()}
                    />

                }

                <Loader loading={this.state.loading} />
                <AwesomeAlertDialog
                    callbackClick={this.hideAlert.bind(this)}
                    show={this.state.show}
                    title={this.state.title}
                    message={this.state.message}
                    alertCancel={this.state.alertCancel}
                    alertConfirm={this.state.alertConfirm}
                    canBeClosed={this.state.canBeClosed}
                    okHide={this.state.okHide}
                    type={this.state.alertType} />

                <AnnouncementDialog
                    display={this.state.showAlert}
                    announcementResult={this.announcementResult.bind(this)}

                    noticeData={this.state.noticeData}
                />
                <FabButton
                    style={{ margin: hp('3%') }}
                    canGoBack={this.state.canGoBack}
                    navigation={this.props.navigation}
                />
            </SafeAreaView >

        );
    }


    goToPrevios() {
        if (this.previosPage > 0) {
            this.offset = this.previosPage - 1;
            this.getNotificationDetails();
        } else {
            this.offset = 0;
        }
    }

    goToNext() {
        this.previosPage = this.offset + 1;

        this.getNotificationDetails();
    }
    renderItem({ item }) {

        return (
            <TouchableOpacity
                style={{
                    backgroundColor: item.status === 'sent' && this.notification ? '#decdf1' : COLORS.white,
                    flexDirection: 'row', flex: 1,
                    alignItems: 'center', padding: wp('4%'), borderBottomColor: COLORS.graylight, borderBottomWidth: 1.0
                }}
                onPress={() => {
                    // console.log("item",item)
                    if (!this.notification) {
                        // this.showAlert("", item.message)
                        this.showAlertNotice(item)
                    } else {
                        this.updateNotificationStatus(item);

                    }
                }}>
                <Image
                    style={{ height: wp("10%"), width: wp("10%"), resizeMode: 'contain' }}
                    source={passwordCheck.notificationIcon(this.notification ? item.title : item.type)} />
                <View>
                    <View style={{ flexDirection: 'row', width: wp('82%'), justifyContent: 'space-between' }}>

                        <Text
                            numberOfLines={1}
                            ellipsizeMode='tail'
                            style={{
                                color: COLORS.subText, flex: 1,
                                fontSize: regular, ...testFont,
                                marginLeft: wp('2%'),
                            }}>{this.notification ? item.title : item.type}</Text>

                        <Text style={{
                            color: COLORS.subText,
                            fontSize: regular, ...testFont,
                        }}>{passwordCheck.dateformat(item.created_date ? item.created_date : item.createdDate)}
                        </Text>
                    </View>
                    <Text numberOfLines={2} ellipsizeMode='tail'
                        style={{
                            color: 'black',
                            fontSize: regular, ...testFont,
                            marginLeft: wp('2%'), width: wp('82%')
                        }}>{this.notification ? item.message.trim() : item.title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    getNotificationDetails() {
        var notificatioGet = "";
        if (this.notification) {
            notificatioGet = BaseUrl.V2_API_PATH + "Twillio/getNotificationDetails/" + this.email + '?page=' + this.offset + '&size=20'

        } else {
            notificatioGet = BaseUrl.V2_API_PATH + "Twillio/getAnnouncemetDetails/" + '?page=' + this.offset + '&size=20'

        }

        this.setState({ loading: true, loadMore: false }, () => { });
        console.log("NotificationDetails", notificatioGet);
        getAxiosUser(notificatioGet)
            .then(success => {
                if (success != null && success.data && success.data.code === 200) {
                    this.previosPage = this.offset;
                    var response = success.data.response.notificationDetails;
                    let pageList = success.data.response.pageList;

                    // var filterArray = response.filter(
                    //     data => this.notification ? data.title != "ANNOUNCEMENT" : data.title == "ANNOUNCEMENT"
                    // );


                    this.setState({
                        // data: [...this.state.data, ...response],
                        data: [...response],
                        loading: false, error: ''
                    }, () => {
                        this.setState({ loadMore: pageList > 20 && this.state.data.length != pageList });
                        this.offset = this.offset + 1;
                    });

                } else {

                    let message = passwordCheck.responseError(success);
                    console.log(message);

                    this.setState({ loading: false, error: message == "Error" || message == "Notification details not available" ? errorMessage : message, loadMore: false }, () => { });
                }

            }).catch(error => {
                console.log("error", error);

                this.setState({ loading: false, error: error.message }, () => { });

            });
    }

    hideAlert() {
        this.setState({ show: false, showAlert: false }, () => { });
    }

    announcementResult(result, data) {
        this.hideAlert();
        if (result) {

            let cardTitle;
            if (data.cardTitle) {
                cardTitle = data.cardTitle;
            } else {
                cardTitle = passwordCheck.getCardTypeByPath(data.urlPath)
            }
            console.log("cardTitle--------------------------", cardTitle, data.urlPath)
            if (cardTitle == AppStrings.notices) {
                Linking.openURL(AppStrings.noticeUrl)
            } else {
                this.checkDisableAndNavigate(cardTitle, data.urlPath)
            }
        }

    }


    updateNotificationStatus(data) {

        console.log("cardTitle-1-", data,);

        let notId = data.notificationID ? data.notificationID : data.notificationId;
        this.setState({ loading: true });

        let cardTitle;
        if (data.cardTitle) {
            cardTitle = data.cardTitle;
        } else {
            cardTitle = passwordCheck.getCardTypeByPath(data.urlPath)
        }


        if (notId) {
            var statusUrl = BaseUrl.V2_API_PATH + "Twillio/updateNotificationStatus";
            var postObject = {
                "notificationId": notId
            }
            postAxiosWithToken(statusUrl, postObject, "").then(resp => {
                this.setState({ loading: false });

                if (resp && resp.data && resp.data.code === 200) {
                    this.checkDisableAndNavigate(cardTitle, data.urlPath);
                    this.getNotificationDetails(false);
                } else {

                    let message = passwordCheck.responseError(resp);
                    this.setState({ error: message == "Error" ? errorMessage : message }, () => { });
                    this.showAlert("", message, "")
                }

            }).catch(error => {
                console.log("error", error);
                this.setState({ loading: false });
            });

        } else {
            console.log("notificationId-is- ", notId);
            this.setState({ loading: false });
        }

    }
    async getOffline() {
        let AllCards = JSON.parse(await AsyncStorage.getItem('AllCards'));
        this.setState({ AllCards: AllCards });
    }

    checkDisableAndNavigate(cardTitle, urlPath) {
        console.log("checkDisableAndNavigate---------------", cardTitle, urlPath,);

        let newArray = passwordCheck.ArrayFilterFunction(this.state.AllCards, cardTitle);

        if (newArray && newArray.enable)
            passwordCheck.navigationUtil(newArray.cardtitle, this.props, this.state.Connection, urlPath);
        else {
            let message = `We’re facing some technical difficulties, due to which this feature is currently unavailable. For support, call Member Services at ${this.ContactNumber}, Monday through Friday, 8.00am to 8.00pm CST.`
            this.showAlert("", message, "");
        }

    }
};

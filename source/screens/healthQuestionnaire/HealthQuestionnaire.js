
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, StyleSheet } from "react-native";

import { isAndroid } from "../../constants/PlatformCheck";
import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import * as Progress from 'react-native-progress';
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken, getAxiosUser } from '../../constants/CommonServices'

import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import BaseUrl from "../../constants/BaseUrl";
import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
import AppStrings from "../../constants/AppStrings";
import AboutYou from "./AboutYou";
import LifeStyleQuestion from "./LifeStyleQuestion";
import HealthQuestionData from "./HealthQuestionData";
import CurrentQuestionData from "./CurrentQuestionData";
import AgreementChange from "./AgreementChange";
import HomeFooter from '../HomeFooter';
import { SafeAreaView } from "react-native";
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
import { passwordCheck } from "../../constants/utils";
import { FabButton } from "../botchat/FabButton";
let heading = FontsApp.heading;
let inputTextHeight = FontsApp.inputTextHeight;
let subHeading = FontsApp.subHeading
let subHeadingCard = FontsApp.subHeadingCard
let regular = FontsApp.regular
let errorMessage = AppStrings.tryAgain;
export default class HealthQuestionnaire extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.accessToken = '';

        this.email = '';
        this.state = {
            data: [],
            HealthInfo: [],
            instructionSet: [],
            questionList: [],

            count: 0,
            totalSteps: 4,
            progress: 0,

            disablePrev: false,
            disableNext: true,

            disableFinish: true,
            checkedB: true,
            firstName: '',
            lastName: '',
            socialNumber: '',
            relationship: '',
            showEdit: false,
            membersData: [],
            id: '',
            subId: '',
            isAllDataFilled: true,
            enrollFamilyData: [{
                birthDate: '',
                gender: '',

            }],
            houseHoldData: {},
            lifeStyleQuestionData: [],
            healthQuestionData: [],
            currentQuestionData: [],
            instructionData: [],
            authorize: false,
            privacyPolicy: false,
            age: null,
            currentUser: '',
            fullName: '',
            userName: '',
            totalQueLength: 0,

            loading: false,
            error: '',
            questionStatus: false,

        }

    }

    componentDidMount() {
        passwordCheck.applyHeader(this.navigation, AppStrings.healthQuestionnaire)
        this.getAsync().then(() => {
            this.getHealthInfo();
        });

    }

    getAsync = async () => {
        this.accessToken = await AsyncStorage.getItem("accessToken");
        this.email = await AsyncStorage.getItem("email");
    }

    validateFieldHandler(value, data) {
        let enrollFamilyData = this.state.enrollFamilyData;
        enrollFamilyData[this.state.count] = data;
        this.setState({
            disableNext: value,
            enrollFamilyData: enrollFamilyData,
            disableFinish: value
        })
    }
    render() {
        return (

            <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>

                {
                    !this.state.showEdit
                        ?
                        this.getMainView()
                        :
                        this.state.showEdit ?

                            <View style={{ flex: 1, }}>

                                <ScrollView>
                                    <View style={{ flex: 1, paddingBottom: hp('6%') }}>
                                        <View style={{
                                            paddingHorizontal: hp('3%'),
                                            paddingBottom: hp('1%')
                                        }}>
                                            <Text style={{
                                                fontSize: regular, ...testFont,
                                                marginTop: wp('2%'), color: COLORS.primaryColor,
                                                fontWeight: 'bold',
                                            }}>{this.state.userName}</Text>

                                            <Progress.Bar
                                                color={COLORS.yellow}
                                                unfilledColor={COLORS.graylight}
                                                borderWidth={0}
                                                height={hp('2%')}
                                                marginTop={hp("2%")}
                                                progress={this.state.progress}
                                                width={wp('90%')} />

                                        </View>

                                        {
                                            this.state.count === 0
                                                ?
                                                <AboutYou onClick={this.validateFieldHandler.bind(this)} familyData={this.state.enrollFamilyData[this.state.count]} instData={this.state.instructionData[this.state.count]} age={this.state.age} viewMode={this.state.viewMode} />
                                                :
                                                this.state.count === 1
                                                    ?
                                                    <LifeStyleQuestion onClick={this.saveQuestionData.bind(this)} familyData={this.state.lifeStyleQuestionData} instData={this.state.instructionData[this.state.count]} viewMode={this.state.viewMode} />
                                                    :
                                                    this.state.count === 2
                                                        ?
                                                        <HealthQuestionData onClick={this.saveQuestionData.bind(this)} familyData={this.state.healthQuestionData} instData={this.state.instructionData[this.state.count]} viewMode={this.state.viewMode} />
                                                        :
                                                        this.state.count === 3
                                                            ?
                                                            <CurrentQuestionData onClick={this.saveQuestionData.bind(this)} familyData={this.state.currentQuestionData} instData={this.state.instructionData[this.state.count]} viewMode={this.state.viewMode} />
                                                            :
                                                            this.state.count === 4
                                                                ?
                                                                <AgreementChange onClick={this.agreementChangeHandler.bind(this)} authorize={this.state.authorize} privacyPolicy={this.state.privacyPolicy} fullName={this.state.fullName} viewMode={this.state.viewMode} />
                                                                : null
                                        }
                                    </View>
                                </ScrollView>

                                <View style={{ flex: 1, flexDirection: 'row', position: 'absolute', bottom: 0, backgroundColor: COLORS.blueL }}>
                                    <Text
                                        onPress={(event) => { this.reduceProgress() }}
                                        style={styles.footerButtons}>BACK</Text>
                                    {
                                        this.state.viewMode ?
                                            <Text
                                                onPress={(event) => {
                                                    this.state.count === this.state.totalSteps ? this.gotoMemberList() : this.increaseProgress()
                                                }}
                                                style={styles.footerButtons}>
                                                {this.state.count === this.state.totalSteps ? 'FINISH' : 'NEXT'}

                                            </Text>
                                            :
                                            <Text
                                                onPress={(event) => { !this.state.disableNext ? this.increaseProgress() : null }}
                                                style={!this.state.disableNext ? styles.footerButtons : styles.footerDisableButtons}>
                                                NEXT
                                            </Text>
                                    }

                                    {
                                        !this.state.viewMode ?
                                            <Text onPress={(event) => { !this.state.disableFinish ? this.finishButtonHandler() : null }}
                                                style={!this.state.disableFinish ? styles.footerButtons : styles.footerDisableButtons}>
                                                {this.state.count === 4 ? "FINISH" : "FINISH LATER"}
                                            </Text> : null
                                    }

                                </View>
                            </View>

                            :
                            null

                }

                <Loader loading={this.state.loading} />

            </SafeAreaView>


        );
    }
    getMainView() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ flex: 1, paddingHorizontal: wp('6%'), paddingVertical: wp('3%') }}>
                        {
                            this.state.membersData.length > 0 ?
                                <View>
                                    <Text style={{ fontSize: regular, ...testFont, color: COLORS.textColor }}>Please select a family member and fill in their medical questionnaire.
                                        This information will help us serve you better, hence please update this
                                        regularly to reflect your current health.
                                    </Text>

                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        numColumns={1}
                                        style={{ marginBottom: hp('7%') }}
                                        data={this.state.membersData}
                                        renderItem={(item, index) => this.renderItem(item, index)}
                                        keyExtractor={(item, index) => index.toString()}
                                    />

                                </View>
                                :
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: hp("2%") }}>
                                    <Text style={{ alignSelf: 'center', fontSize: heading, ...testFont, fontWeight: 'bold' }}>{this.state.error}</Text>
                                </View>
                        }
                    </View>
                </ScrollView>
                <FabButton
                    style={{ margin: hp('8%') }}

                    canGoBack={this.state.canGoBack}
                    navigation={this.props.navigation}
                />

                <HomeFooter from="HQ" isDashboard={false} isOtherScreen={true} callbackFooter={(from, isHome) => {
                    if (from === AppStrings.dashboard) {
                        this.props.navigation.goBack(null);
                    }
                    if (from === AppStrings.documents) {
                        this.props.navigation.navigate(AppStrings.documents);
                    }

                    if (from === AppStrings.MyNeedsScreen) {
                        this.props.navigation.navigate(AppStrings.MyNeedsScreen);
                    }

                }} />
            </View>

        )
    }

    renderItem({ item, index }) {
        let progress = (item.totalQuestionLength) / this.state.totalQueLength * 1.0;
        return (
            <View style={{ marginTop: hp('3%') }}>

                <Progress.Bar
                    color={progress == 1 ? COLORS.green : '#420045'}
                    unfilledColor={COLORS.graylight}
                    borderWidth={0}
                    height={hp('2%')}
                    progress={progress}
                    width={wp('88%')} />

                <View style={{ flexDirection: 'row', flex: 1 }}>
                    <View style={{ flex: 0.5 }}>
                        <Text style={styles.textHeading}>NAME</Text>
                        <Text style={{ fontSize: regular, marginTop: wp('1%'), ...testFont }}>{item.firstName + " " + item.lastName}</Text>
                        {item.lastUpdatedDate ? <Text style={styles.textHeading}>LAST UPDATED</Text> : null}

                        <Text style={{ fontSize: regular, marginTop: wp('1%'), ...testFont }}>
                            {
                                item.lastUpdatedDate ? moment(item.lastUpdatedDate).format("MMMM DD, YYYY") : ''
                            }
                        </Text>

                    </View>
                    <View style={{ flex: 0.5, alignItems: 'flex-end', }}>
                        {
                            item.completionStatus == 0
                                ?
                                <Text onPress={(event) => { this.editButtonHandler(item, 'START', index) }}
                                    style={styles.buttonBackground}> {item.lastUpdatedDate ? 'RESUME' : 'START'}
                                </Text>
                                :
                                item.completionStatus === 4
                                    ?
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }} >

                                        <Text
                                            onPress={(event) => this.editButtonHandler(item, 'START', index, "")}
                                            style={styles.buttonBackground}>EDIT
                                        </Text>

                                        <Text
                                            onPress={(event) => this.editButtonHandler(item, 'START', index, "VIEWMODE")}
                                            style={styles.buttonBackground}>VIEW
                                        </Text>

                                    </View>
                                    :
                                    <Text
                                        onPress={(event) => this.editButtonHandler(item, 'RESUME', index, "")}
                                        style={styles.buttonBackground}>RESUME
                                    </Text>
                        }

                    </View>

                </View>
            </View>
        )
    }

    reduceProgress = () => {
        console.log(this.state.count, " membersData ", this.state.membersData.length)
        if (this.state.count > 0) {
            this.setState({
                count: this.state.count - 1,
                progress: (this.state.count - 1) / this.state.totalSteps * 1.0
            });
        } else {
            this.setState({
                showEdit: false
            });
        }
    }

    increaseProgress = () => {
        if (this.state.count < this.state.totalSteps) {
            this.setState({
                count: this.state.count + 1,
                progress: (this.state.count + 1) / this.state.totalSteps * 1.0
            });
        }
    }

    gotoMemberList = () => {

        this.setState({
            showEdit: false
        });

    }

    getHealthInfo() {
        let url = BaseUrl.FABRIC_V_02 + "questionbank/healthinfo";
        let object = {
            "email": this.email
        }
        this.setState({ loading: true });

        postAxiosWithToken(url, object, this.accessToken).then(membersResult => {

            if (membersResult && membersResult.data && membersResult.data.response) {
                let membersData = membersResult.data.response;
                this.getQuestions(membersData);
            } else {
                let message = passwordCheck.responseError(membersResult);
                this.setState({ loading: false, error: message == "Error" ? errorMessage : message }, () => { });

            }
        })

    }

    getQuestions(membersData) {
        let url = BaseUrl.FABRIC_V_02 + "questionbank/getQuestions";

        getAxiosUser(url).then(response => {
            let membersResult = response.data;
            let queResult = membersResult.response.questionList;
            let completionStatus = [];
            for (let i = 0; i < membersData.length; i++) {
                let answredCount = 0;
                for (let j = 0; j < membersData[i].healthQuestions.length; j++) {
                    let data = membersData[i].healthQuestions[j].response;
                    let relatedQueAns = membersData[i].healthQuestions[j].reltdQstnID;

                    if (data === 'Yes' && relatedQueAns.length > 0) {
                        if (relatedQueAns[0].response) {
                            answredCount++;
                        }
                    } else {
                        if (data) {
                            answredCount++;
                        }
                    }
                }
                completionStatus.push(membersData[i].completionStatus)
                membersData[i].totalQuestionLength = answredCount;
            }
            console.log("completionStatus", completionStatus)
            this.setState({
                membersData: membersData,
                loading: false, error: '',
                totalQueLength: queResult.length
            }, () => {
                AsyncStorage.setItem("completionStatus", JSON.stringify(completionStatus));

            });
        });
    }

    saveQuestionData(value, data, type) {
        if (type === 'LIFESTYLE') {
            this.setState({
                disableNext: value,
                lifeStyleQuestionData: data,
                disableFinish: value
            })
        } else if (type === 'HEALTH') {
            this.setState({
                disableNext: value,
                healthQuestionData: data,
                disableFinish: value
            })
        } else if (type === 'CURRENT') {
            this.setState({
                disableNext: value,
                currentQuestionData: data,
                disableFinish: value
            })
        }
    }

    agreementChangeHandler = (value, authorize, privacyPolicy, fullName) => {
        this.setState({
            authorize: authorize,
            privacyPolicy: privacyPolicy,
            fullName: fullName,
            disableFinish: value,
            disableNext: true
        });
    }
    showLastUpdate = () => {
        console.log("showLastUpdate", this.state.membersData)
        let showLastUpdateTitle = this.state.membersData.some((item) => item.completionStatus != 0 || item.lastUpdatedDate);
        if (showLastUpdateTitle) {
            return <View><Text>ENROLL_FAMILY.LAST_UPDATE</Text></View>
        }
        return null;
    }

    finishButtonHandler = (event) => {
        this.setState({
            loading: true
        });

        let gender = '';
        if (this.state.enrollFamilyData[0].gender === 'MALE') {
            gender = 'M';
        } else if (this.state.enrollFamilyData[0].gender === 'FEMALE') {
            gender = 'F';
        } else if (this.state.enrollFamilyData[0].gender === 'NEUTRAL') {
            gender = 'U';
        }

        let user = this.state.membersData.find(obj => obj.memberUniqueId === this.state.id);
        user.authorize = this.state.authorize;
        user.privacyPolicy = this.state.privacyPolicy;
        user.dob = new Date(this.state.enrollFamilyData[0].dob).getTime();
        user.gender = gender;
        user.completionStatus = this.state.count;
        user.email = this.state.enrollFamilyData[0].email;
        user.fullName = this.state.fullName;

        for (let i = 0; i < this.state.lifeStyleQuestionData.length; i++) {
            let findQue = user.healthQuestions.find(obj => obj.questionID.toString() === this.state.lifeStyleQuestionData[i].questionID.toString());
            findQue.response = this.state.lifeStyleQuestionData[i].answer.toString();
        }

        for (let i = 0; i < this.state.healthQuestionData.length; i++) {
            let findQue = user.healthQuestions.find(obj => obj.questionID.toString() === this.state.healthQuestionData[i].questionID.toString());
            findQue.response = this.state.healthQuestionData[i].answer.toString();
        }

        for (let i = 0; i < this.state.currentQuestionData.length; i++) {
            let findQue = user.healthQuestions.find(obj => obj.questionID.toString() === this.state.currentQuestionData[i].questionID.toString());
            findQue.response = this.state.currentQuestionData[i].answer.toString();
        }

        let arr = [];
        arr.push(user);

        postAxiosWithToken(BaseUrl.FABRIC_V_02 + '/questionbank/saveHealthQues', arr, this.accessToken)
            .then(response => {
                this.getHealthInfo();
                this.setState({
                    showEdit: false
                })
            })
            .catch(error => {
                console.log("error----", error.message);
            })
    }

    editButtonHandler = (key, flag, index, type) => {
        this.setState({
            loading: true,
            currentUser: key.firstName + ' ' + key.lastName
        });

        let gender = '';
        if (key.gender === 'M') {
            gender = 'MALE';
        } else if (key.gender === 'F') {
            gender = 'FEMALE';
        } else if (key.gender === 'U') {
            gender = 'NEUTRAL';
        }

        let data = [{
            dob: key.dob ? this.getDateInUTC(parseInt(key.dob, false)) : new Date(),
            gender: key.gender ? gender : '',
            email: key.email ? key.email : '',
            isPrimary: key.memberUniqueId == key.subscriberUniqueId
        }];

        getAxiosUser(BaseUrl.FABRIC_V_02 + '/questionbank/getQuestions')
            .then(resp => {
                let membersResult = resp.data;
                // console.log("membersResult", membersResult);
                let queResult = membersResult.response.questionList;
                let lifeStyleQuestionData = [];
                let healthQuestionData = [];
                let currentQuestionData = [];
                let healthQuestions = [];
                for (let i = 0; i < queResult.length; i++) {
                    if (key.healthQuestions && key.healthQuestions.length > 0) {
                        let found = key.healthQuestions.find(obj => obj.questionID.toString() === queResult[i].question.id.toString());
                        let optionId = '';
                        let response = '';

                        if (queResult[i].question.type === 'radio') {
                            if (found.response) {
                                let arr = queResult[i].options.find(obj => obj.option === found.response);
                                optionId = arr.id;
                                response = found.response;
                            } else {
                                optionId = '';
                                response = '';
                            }

                        } else if (queResult[i].question.type === "textbox") {
                            response = found.response;
                            optionId = '';
                        } else if (queResult[i].question.type === "dropdown") {
                            if (found.response) {
                                response = found.response.split(',');
                            } else {
                                response = [];
                            }
                            optionId = '';

                        }

                        if (queResult[i].question.questionTypeCode === "LIFESTYLE") {
                            lifeStyleQuestionData.push({
                                questionID: queResult[i].question.id,
                                type: queResult[i].question.type,
                                question: queResult[i].question.question,
                                questionTypeCode: queResult[i].question.questionTypeCode,
                                answer: response,
                                optionId: optionId,
                                options: queResult[i].options,
                                relatedQue: found.reltdQstnID
                            })
                        } else if (queResult[i].question.questionTypeCode === "HEALTH") {
                            healthQuestionData.push({
                                questionID: queResult[i].question.id,
                                type: queResult[i].question.type,
                                question: queResult[i].question.question,
                                questionTypeCode: queResult[i].question.questionTypeCode,
                                answer: response,
                                optionId: optionId,
                                options: queResult[i].options,
                                relatedQue: found.reltdQstnID
                            })
                        } else if (queResult[i].question.questionTypeCode === "CURRENT") {
                            currentQuestionData.push({
                                questionID: queResult[i].question.id,
                                type: queResult[i].question.type,
                                question: queResult[i].question.question,
                                questionTypeCode: queResult[i].question.questionTypeCode,
                                answer: response,
                                optionId: optionId,
                                options: queResult[i].options,
                                relatedQue: found.reltdQstnID
                            });
                        }
                    } else {

                        let relatedQ = [];
                        let question = queResult[i].question;

                        if (question.relatedQuestions.length > 0) {
                            for (let j = 0; j < question.relatedQuestions.length; j++) {
                                let obj = {
                                    "id": null,
                                    "questionID": question.relatedQuestions[j].id,
                                    "responseTypCode": question.relatedQuestions[j].responseTypCode,
                                    "response": "",
                                    "questionTypCode": question.questionTypeCode,
                                    "maintTypCode": null,
                                    "questionDesc": question.relatedQuestions[j].question
                                };

                                relatedQ.push(obj);
                            }
                        }

                        let obj = {
                            "id": null,
                            "questionID": question.id,
                            "responseTypCode": question.responseTypCode,
                            "response": "",
                            "questionTypCode": question.questionTypeCode,
                            "questionDesc": question.question,
                            "reltdQstnID": relatedQ
                        };

                        healthQuestions.push(obj);

                        if (queResult[i].question.questionTypeCode === "LIFESTYLE") {
                            lifeStyleQuestionData.push({
                                questionID: queResult[i].question.id,
                                type: queResult[i].question.type,
                                question: queResult[i].question.question,
                                questionTypeCode: queResult[i].question.questionTypeCode,
                                answer: '',
                                optionId: '',
                                options: queResult[i].options,
                                relatedQue: relatedQ
                            });
                        } else if (queResult[i].question.questionTypeCode === "HEALTH") {
                            healthQuestionData.push({
                                questionID: queResult[i].question.id,
                                type: queResult[i].question.type,
                                question: queResult[i].question.question,
                                questionTypeCode: queResult[i].question.questionTypeCode,
                                answer: '',
                                optionId: '',
                                options: queResult[i].options,
                                relatedQue: relatedQ
                            })
                        } else if (queResult[i].question.questionTypeCode === "CURRENT") {
                            currentQuestionData.push({
                                questionID: queResult[i].question.id,
                                type: queResult[i].question.type,
                                question: queResult[i].question.question,
                                questionTypeCode: queResult[i].question.questionTypeCode,
                                answer: '',
                                optionId: '',
                                options: queResult[i].options,
                                relatedQue: relatedQ
                            });
                        }
                    }
                }

                if (key.healthQuestions && key.healthQuestions.length === 0) {
                    this.state.membersData[index].healthQuestions = healthQuestions;
                }

                let count = key.completionStatus ? key.completionStatus : 0;
                var progressVal = 0;
                if (flag === 'RESUME') {
                    progressVal = (count) / this.state.totalSteps * 1.0;
                }
                let isViewMode = type == 'VIEWMODE' ? true : false;
                // this.props.toggleHealthQnEditMode(!isViewMode);
                this.setState({
                    error: '',
                    enrollFamilyData: data,
                    showEdit: true,
                    viewMode: isViewMode,
                    count: count === 4 ? 0 : count,
                    progress: progressVal,
                    id: key.memberUniqueId,
                    subId: key.subscriberUniqueId,
                    authorize: type == 'VIEWMODE' ? key.authorize : false,
                    privacyPolicy: type == 'VIEWMODE' ? key.privacyPolicy : false,
                    // authorize: key.authorize,
                    // privacyPolicy: key.privacyPolicy,
                    fullName: key.fullName,
                    userName: key.firstName + " " + key.lastName,
                    loading: false,
                    lifeStyleQuestionData: lifeStyleQuestionData,
                    healthQuestionData: healthQuestionData,
                    currentQuestionData: currentQuestionData,
                    instructionData: membersResult.response.instructionSet
                });
            }).catch(e => {
                console.log("error messagwe" + e.message)
                this.setState({ loading: false, error: e.message })
            });
    }
    getDateInUTC(date, getInMillisecs) {
        let newDateTime = date + new Date(date).getTimezoneOffset() * 60 * 1000;
        return new Date(newDateTime)
    }
};

const styles = StyleSheet.create({

    buttonBackground: {
        textAlign: 'center',
        color: COLORS.blueL,
        fontSize: regular, ...testFont,
        fontWeight: '700',
        marginTop: wp('3%'),
        borderRadius: FontsApp.borderRadiusRound,
        paddingVertical: hp('1%'),
        borderColor: COLORS.blueL,
        borderWidth: 1.8,
        minWidth: wp('20%'),
        height: wp('10%'),
        paddingHorizontal: wp('2%'),
        marginHorizontal: wp('1%'),
    },

    textHeading: {
        fontSize: regular,
        ...testFont,
        marginTop: wp('2%'),
        color: COLORS.subText,
        fontWeight: 'bold',
        marginTop: wp('3%'),
    },

    footerButtons: {
        flex: 1,
        margin: hp('1%'),
        textAlign: 'center',
        alignSelf: 'center',
        color: COLORS.white,
        ...testFont,
        fontSize: regular,
        fontWeight: '700',
        borderRadius: FontsApp.borderRadiusRound,
        padding: wp('2%'),
        borderColor: COLORS.white,
        borderWidth: 1.5,
        minHeight: wp('10%'),
    },

    footerDisableButtons: {
        flex: 1,
        margin: hp('1%'),
        textAlign: 'center',
        alignSelf: 'center',
        color: COLORS.graylight,
        ...testFont,
        fontSize: regular,
        fontWeight: 'bold',
        borderRadius: FontsApp.borderRadiusRound,
        minHeight: wp('10%'),
        padding: hp('1%'),
        borderColor: COLORS.graylight,
        borderWidth: 2,
    }
});
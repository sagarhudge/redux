
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, SafeAreaView } from "react-native";


import Loader from "../../components/Loader";

import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import COLORS from "../../constants/COLORS";
import TextInput from "react-native-material-textinput";
import RadioButtons from "../../components/RadioButtons"
import { FontsApp, testFont } from '../../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let inputTextHeight = FontsApp.inputTextHeight;

export default class HealthQuestionData extends Component {
  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;
    this.state = {
      data: [],
      loading: false,
      error: '',
      questionData: this.props.familyData,
      viewMode: this.props.viewMode,
      instData: this.props.instData,
      errorId: null,
    }

  }

  componentDidMount() {
    console.log('=================== this.state.questionData ================', this.state.questionData.length)
    this.answerChangeHandler();
  }

  textChangeHandler = (val, isValid, parentObj) => {
    let questionData = this.state.questionData;

    if (parentObj.type === 'Text') {
      if (isValid) {
        this.setState({ errorId: null, });

        if (parentObj.name === 'medications' || parentObj.name === 'medical_equipment') {
          questionData[parentObj.index].relatedQue[0].response = val;
        } else {
          questionData[parentObj.index].answer = val;
        }
      } else {
        if (parentObj.name === 'medications' || parentObj.name === 'medical_equipment') {
          questionData[parentObj.index].relatedQue[0].response = '';
        } else {
          questionData[parentObj.index].answer = '';
        }
        this.setState({ errorId: parentObj.index });
      }

      this.setState({
        questionData: questionData
      });
    }

    let validOne = questionData[2].optionId ? (questionData[2].answer === 'Yes' ? (questionData[2].relatedQue[0].response !== '' ? true : false) : true) : false;
    let validTwo = questionData[3].optionId ? (questionData[3].answer === 'Yes' ? (questionData[3].relatedQue[0].response !== '' ? true : false) : true) : false;
    if (questionData[0].answer !== '' && questionData[1].answer !== '' && validOne && validTwo) {
      this.props.onClick(false, this.state.questionData, 'HEALTH');
    } else {
      this.props.onClick(true, this.state.questionData, 'HEALTH');
    }
  };

  answerChangeHandler = (event, index, type) => {
    let questionData = this.state.questionData;
    if (event) {

      console.log('============== uestionData[index] ================');
      console.log(questionData[index]);


      let value = event;
      if (type === 'Radio') {
        questionData[index].optionId = value;
        let exists = questionData[index].options.find(obj => obj.id.toString() == value.toString())
        questionData[index].answer = exists ? exists.option : 'No';
        this.setState({
          questionData: questionData
        });
      } else if (type === 'Text') {
        questionData[index].answer = value;
        this.setState({
          questionData: questionData
        });
      }
    }
    let validOne = questionData[2].optionId ? (questionData[2].answer === 'Yes' ? (questionData[2].relatedQue[0].response !== '' ? true : false) : true) : false;
    let validTwo = questionData[3].optionId ? (questionData[3].answer === 'Yes' ? (questionData[3].relatedQue[0].response !== '' ? true : false) : true) : false;
    if (questionData[0].answer !== '' && questionData[1].answer !== '' && validOne && validTwo) {
      this.props.onClick(false, this.state.questionData, 'HEALTH');
    } else {
      this.props.onClick(true, this.state.questionData, 'HEALTH');
    }
  };
  render() {
    return (

      <View pointerEvents={this.state.viewMode ? "none" : null} style={{ backgroundColor: COLORS.white, flex: 1, padding: hp('3%') }}>

        <Text style={{ fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.instData.title}</Text>
        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[0].question}</Text>

        <TextInput
          marginTop={-10}
          labelActiveTop={-30}
          value={this.state.questionData[0].answer}
          fontSize={inputTextHeight}
          fontFamily={FontsApp.fontFamily}
          error={this.state.errorId == 0 ? "No. Required" : ''}
          errorColor={COLORS.red}
          maxLength={2}
          labelActiveColor={COLORS.labelActiveColor}
          underlineColor={COLORS.underlineColor}
          underlineActiveColor={COLORS.underlineActiveColor}
          underlineHeight={hp("0.4%")}

          onChangeText={result => this.textChangeHandler(result, (result != ""), { index: 0, type: 'Text', name: 'ERInPast' })}
        />
        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[1].question}</Text>

        <TextInput
          marginTop={-10}
          labelActiveTop={-30}
          value={this.state.questionData[1].answer}
          maxLength={2}
          fontSize={inputTextHeight}
          fontFamily={FontsApp.fontFamily}
          error={this.state.errorId == 1 ? "No. Required" : ''}
          errorColor={COLORS.red}
          maxLength={2}
          labelActiveColor={COLORS.labelActiveColor}
          underlineColor={COLORS.underlineColor}
          underlineActiveColor={COLORS.underlineActiveColor}
          underlineHeight={hp("0.4%")}

          onChangeText={result => this.textChangeHandler(result, (result != ""), { index: 1, type: 'Text', name: 'hospitalizedPast' })}
        />

        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[2].question}</Text>

        <RadioButtons
          viewMode={this.state.viewMode}

          type={this.state.questionData[2].optionId}
          options={this.state.questionData[2].options}
          onPressButton={(result) => {
            this.answerChangeHandler(result, 2, 'Radio')
          }} />
        {
          this.state.questionData[2].answer === 'Yes' ? <TextInput
            marginTop={10}
            labelActiveTop={-30}
            value={this.state.questionData[2].relatedQue[0].response}
            fontSize={inputTextHeight}
            label={"List medication*"}
            fontFamily={FontsApp.fontFamily}
            error={this.state.errorId == 2 ? "Medication Required" : ''}
            errorColor={COLORS.red}
            labelActiveColor={COLORS.labelActiveColor}
            underlineColor={COLORS.underlineColor}
            underlineActiveColor={COLORS.underlineActiveColor}
            underlineHeight={hp("0.4%")}
            height={150}
            multiline
            onChangeText={result => this.textChangeHandler(result, (result != ""), { index: 2, type: 'Text', name: 'medications' })}
          /> : null
        }

        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[3].question}</Text>

        <RadioButtons

          viewMode={this.state.viewMode}
          type={this.state.questionData[3].optionId}
          options={this.state.questionData[3].options}
          onPressButton={(result) => {
            this.answerChangeHandler(result, 3, 'Radio')
          }} />
        {
          this.state.questionData[3].answer === 'Yes' ?
            <TextInput
              marginTop={10}
              labelActiveTop={-30}
              value={this.state.questionData[3].relatedQue[0].response}
              fontSize={inputTextHeight}
              label={"List Medical Equipment*"}
              error={this.state.errorId == 3 ? "Medical Equipment Required" : ''}
              fontFamily={FontsApp.fontFamily}
              errorColor={COLORS.red}
              labelActiveColor={COLORS.labelActiveColor}
              underlineColor={COLORS.underlineColor}
              underlineActiveColor={COLORS.underlineActiveColor}
              underlineHeight={hp("0.4%")}
              height={150}
              multiline
              onChangeText={result => this.textChangeHandler(result, (result != ""), { index: 3, type: 'Text', name: 'medications' })}
            /> : null
        }
        {
          this.state.instData.description !== '' &&
          <View>
            <Text style={{ fontSize: regular, ...testFont, marginTop: hp('3%'), marginBottom: hp('1%'), fontWeight: 'bold' }}>Why we need this?</Text>
            <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%'), marginBottom: hp('1%') }}>{this.state.instData.description}</Text>

          </View>

        }

        <Loader loading={this.state.loading} />

      </View>

    );
  }
};

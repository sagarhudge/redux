
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, SafeAreaView } from "react-native";
import Loader from "../../components/Loader";
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import COLORS from "../../constants/COLORS";
import RadioButtons from "../../components/RadioButtons"
import TextInput from "react-native-material-textinput";
import CheckBox from '@react-native-community/checkbox';

import AppStrings from "../../constants/AppStrings";

import { FontsApp, testFont } from '../../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let inputTextHeight = FontsApp.inputTextHeight;

export default class CurrentQuestionData extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.state = {
            val: [],
            count: 0,
            loading: false,
            questionData: this.props.familyData,
            instData: this.props.instData,
            viewMode: this.props.viewMode,
            error: '',
            checked: new Map(),
            phoneErr: false,
            phoneVal: '',
            errorId: null,
        }
    }

    componentDidMount() {
        this.textChangeHandler('', '', '');
        this.answerChangeHandler('', '', '');
        let phoneValue = this.state.questionData[2].relatedQue[1].response;
        if (phoneValue) {
            phoneValue = phoneValue.split('+1')[1].trim()
        }
        this.setValue(phoneValue);
    }
    answerChangeHandler = (value, index, type) => {
        let questionData = this.state.questionData;
        if (value) {

            if (type === 'Radio') {
                questionData[index].optionId = value;
                let exists = questionData[index].options.find(obj => obj.id.toString() === value.toString());
                questionData[index].answer = exists ? exists.option : 'No';
                this.setState({
                    questionData: questionData
                });
            } else if (type === 'Multiple') {
                console.log('---Checked---' + questionData)

            } else if (type === 'dropdown') {
                const { options } = value.target;
                const value = [];
                for (let i = 0, l = options.length; i < l; i += 1) {
                    if (options[i].selected) {
                        value.push(options[i].value);
                    }
                }
                questionData[index].answer = value;
                this.setState({
                    questionData: questionData
                });
            }
        }

        let validOne = questionData[0].optionId ? (questionData[0].answer === 'Yes' ? (questionData[0].relatedQue[0].response !== '') : true) : false;
        let validTwo = questionData[2].optionId ? (questionData[2].answer === 'Yes' ? ((questionData[2].relatedQue[0].response !== '')) : true) : false;

        if (validOne && validTwo && questionData[1].answer.length > 0) {
            this.props.onClick(false, this.state.questionData, 'CURRENT');
        } else {
            this.props.onClick(true, this.state.questionData, 'CURRENT');
        }
    }

    textChangeHandler = (val, isValid, parentObj) => {
        console.log(val, isValid, parentObj);
        let questionData = this.state.questionData;
        if (parentObj.type === 'Question') {
            if (isValid) {
                questionData[parentObj.index].relatedQue[0].response = val;
            } else {
                questionData[parentObj.index].relatedQue[0].response = '';
            }
        } else if (parentObj.type === 'physician') {
            if (isValid) {
                questionData[parentObj.index].relatedQue[0].response = val;
                questionData[parentObj.index].relatedQue[2].response = '';
            } else {
                questionData[parentObj.index].relatedQue[0].response = '';
                questionData[parentObj.index].relatedQue[2].response = '';
            }
        } else if (parentObj.type === 'otherPhysician') {
            if (isValid) {
                questionData[parentObj.index].relatedQue[2].response = val;
                questionData[parentObj.index].relatedQue[0].response = '';
                questionData[parentObj.index].relatedQue[1].response = '';
            } else {
                questionData[parentObj.index].relatedQue[2].response = '';
                questionData[parentObj.index].relatedQue[0].response = '';
                questionData[parentObj.index].relatedQue[1].response = '';
            }
        }
        this.setState({
            questionData: questionData, errorId: parentObj == "" ? null : !isValid ? parentObj.index : null
        });

        let validOne = questionData[0].optionId ? (questionData[0].answer === 'Yes' ? (questionData[0].relatedQue[0].response !== '') : true) : false;
        let validTwo = questionData[2].optionId ? (questionData[2].answer === 'Yes' ? questionData[2].relatedQue[0].response !== '' : true) : false;

        if (validOne && validTwo && questionData[1].answer.length > 0) {
            this.props.onClick(false, this.state.questionData, 'CURRENT');
        } else {
            this.props.onClick(true, this.state.questionData, 'CURRENT');
        }
    }

    setValue(value) {
        console.log(value)
        let valueWithCode;
        // if (!value.includes('+1')) {
        // }
        valueWithCode = '+1 ' + value;
        if (value != "") {
            let isvalid = value.length > 10;

            this.setState({
                phoneErr: !isvalid,
                phoneVal: value
            })
            console.log('isvalid::', isvalid)
            if (isvalid) {
                this.state.questionData[2].relatedQue[1].response = valueWithCode;
            } else {
                this.state.questionData[2].relatedQue[1].response = '';
            }
            this.setState({ refresh: true }, () => {
                this.validateForm()
            });
        } else {
            this.state.questionData[2].relatedQue[1].response = '';
            this.setState({ phoneErr: false, phoneVal: "" }, () => {
                this.validateForm()
            });
        }
    }

    validateForm() {
        if (this.state.questionData[1].answer.length > 0 && this.state.questionData[2].answer.toLowerCase() === 'no') {
            this.props.onClick(false, this.state.questionData, 'CURRENT');
        } else if (this.state.questionData[1].answer.length > 0 && this.state.questionData[2].relatedQue[0].response !== '') {
            this.props.onClick(false, this.state.questionData, 'CURRENT');
        } else {
            this.props.onClick(true, this.state.questionData, 'CURRENT');
        }
    }

    onToggle = (key) => {
        console.log(key)
        console.log(this.state.questionData[1].answer)

        if (this.state.questionData[1].answer.indexOf(key) === -1) {
            if (key.toLowerCase() === 'none') {
                this.state.questionData[1].answer = [];
                this.state.questionData[1].answer.push(key)
            } else {
                if (this.state.questionData[1].answer.indexOf('None') !== -1) {
                    this.state.questionData[1].answer.splice(this.state.questionData[1].answer.indexOf('None'), 1)
                }

                if (Array.isArray(this.state.questionData[1].answer)) {
                    this.state.questionData[1].answer.push(key)
                } else {
                    let arr = [];
                    arr.push(key);
                    this.state.questionData[1].answer = arr;
                }
            }

        } else {
            this.state.questionData[1].answer.splice(this.state.questionData[1].answer.indexOf(key), 1)
        }

        this.setState({ refresh: true });
        let validOne = this.state.questionData[0].optionId ? (this.state.questionData[0].answer === 'Yes' ? (this.state.questionData[0].relatedQue[0].response !== '') : true) : false;
        let validTwo = this.state.questionData[2].optionId ? (this.state.questionData[2].answer === 'Yes' ? ((this.state.questionData[2].relatedQue[0].response !== '')) : true) : false;
       
        console.log(this.state.questionData[1].answer.length)

        if (validOne && validTwo && this.state.questionData[1].answer.length > 0) {
            this.props.onClick(false, this.state.questionData, 'CURRENT');
        } else {
            this.props.onClick(true, this.state.questionData, 'CURRENT');
        }
        console.log("------------------------------------------------------");

    }
    render() {
        return (

            <View style={{ flex: 1, backgroundColor: COLORS.white, padding: hp('3%') }}>

                <Text style={{ fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.instData.title}</Text>
                <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[2].question}</Text>

                <RadioButtons
                    type={this.state.questionData[2].optionId}
                    viewMode={this.state.viewMode}
                    options={this.state.questionData[2].options}
                    onPressButton={(result) => {
                        this.answerChangeHandler(result, 2, 'Radio')
                    }} />
                {
                    this.state.questionData[2].answer === 'Yes'
                        ? <View>
                            <TextInput
                                editable={!this.state.viewMode}
                                marginTop={10}
                                labelActiveTop={-30}
                                value={this.state.questionData[2].relatedQue[0].response}
                                fontSize={inputTextHeight}
                                fontFamily={FontsApp.fontFamily}
                                label={"Primary Care Physician*"}
                                error={this.state.errorId == 2 ? "Primary Physician required" : ''}
                                labelActiveColor={COLORS.labelActiveColor}
                                underlineColor={COLORS.underlineColor}
                                underlineActiveColor={COLORS.underlineActiveColor}
                                underlineHeight={hp("0.4%")}
                                onChangeText={result => this.textChangeHandler(result, (result != ""), { index: 2, type: 'physician' })}
                            />

                            <TextInput
                                editable={!this.state.viewMode}
                                marginTop={10}
                                labelActiveTop={-30}
                                value={this.state.phoneVal}
                                fontSize={inputTextHeight}
                                fontFamily={FontsApp.fontFamily}
                                label={"Phone No."}
                                maxLength={10}
                                labelActiveColor={COLORS.labelActiveColor}
                                underlineColor={COLORS.underlineColor}
                                underlineActiveColor={COLORS.underlineActiveColor}
                                underlineHeight={hp("0.4%")}
                                onChangeText={this.setValue.bind(this)}
                            />
                        </View>
                        : (this.state.questionData[2].answer === 'No' && this.state.questionData[2].optionId !== '')
                            ?
                            <View>
                                <TextInput
                                    editable={!this.state.viewMode}
                                    marginTop={10}
                                    labelActiveTop={-30}
                                    value={this.state.questionData[2].relatedQue[2].response}
                                    fontSize={inputTextHeight}
                                    fontFamily={FontsApp.fontFamily}
                                    label={"Other physicians you see regularly"}
                                    labelActiveColor={COLORS.labelActiveColor}
                                    underlineColor={COLORS.underlineColor}
                                    underlineActiveColor={COLORS.underlineActiveColor}
                                    underlineHeight={hp("0.4%")}
                                    height={120}
                                    multiline
                                    onChangeText={result => this.textChangeHandler(result, (result != ""), { index: 2, type: 'otherPhysician' })}
                                />
                            </View>
                            :
                            null
                }
                <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[0].question}</Text>
                <RadioButtons
                    viewMode={this.state.viewMode}
                    type={this.state.questionData[0].optionId}
                    options={this.state.questionData[0].options}
                    onPressButton={(result) => {
                        this.answerChangeHandler(result, 0, 'Radio')
                    }} />

                {
                    this.state.questionData[0].answer === 'Yes' ?
                        <View>
                            <TextInput
                                editable={!this.state.viewMode}

                                multiline
                                marginTop={10}
                                labelActiveTop={-30}
                                value={this.state.questionData[0].relatedQue[0].response}
                                fontSize={inputTextHeight}
                                fontFamily={FontsApp.fontFamily}

                                label={"List conditions or diseases*"}
                                error={this.state.errorId == 0 ? "List conditions or diseases" : ''}

                                labelActiveColor={COLORS.labelActiveColor}
                                underlineColor={COLORS.underlineColor}
                                underlineActiveColor={COLORS.underlineActiveColor}
                                underlineHeight={hp("0.4%")}
                                height={120}
                                onChangeText={result => this.textChangeHandler(result, (result != ""), { index: 0, type: 'Question' })}
                            />
                            <Text style={{ fontSize: smallText, ...testFont }}>List of Condition/Diseases required</Text>
                        </View> : null
                }

                <View pointerEvents={null}>
                    <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%'), marginBottom: hp('1%') }}>{this.state.questionData[1].question}</Text>

                    <FlatList
                        numColumns={1}
                        nestedScrollEnabled
                        persistentScrollbar
                        scrollEnabled={true}
                        data={this.state.questionData[1].options}
                        renderItem={({ item }) => {
                            return (<TouchableOpacity
                                onPress={() => { !this.state.viewMode ? this.onToggle(item.option) : null }}
                                style={{ marginBottom: hp("1%"), flexDirection: 'row', alignItems: 'center', marginRight: 10 }}>

                                <CheckBox
                                    disabled={true} 
                                    tintColors={{ true: COLORS.primaryColor, false: COLORS.graya1 }}
                                    boxType={'square'}
                                    value={this.state.questionData[1].answer.indexOf(item.option) !== -1}
                                    // onValueChange={
                                    //     resp => { !this.state.viewMode ? this.onToggle(item.option) : null }
                                    // }
                                    style={{ width: hp('2%'), height: hp('2%') }}
                                />
                                <Text style={{ marginLeft: hp("3%"), fontSize: regular, ...testFont }}>{item.option}</Text>
                            </TouchableOpacity>)
                        }}
                        style={{ maxHeight: hp('15%'), backgroundColor: COLORS.background, paddingHorizontal: wp('2%') }}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                {
                    this.state.instData.description !== '' &&
                    <View>
                        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('3%'), marginBottom: hp('1%'), fontWeight: 'bold' }}>Why we need this?</Text>
                        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%'), marginBottom: hp('1%') }}>{this.state.instData.description}</Text>
                    </View>
                }

                <Loader loading={this.state.loading} />

            </View>


        );
    }
};


import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, SafeAreaView } from "react-native";


import Loader from "../../components/Loader";
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import COLORS from "../../constants/COLORS";
import RadioButtons from "../../components/RadioButtons"
import { FontsApp, testFont } from '../../constants/FontsApp'
let regular = FontsApp.regular
let inputTextHeight = FontsApp.inputTextHeight;
export default class LifeStyleQuestion extends Component {
  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;

    this.state = {
      data: [],
      loading: false,
      error: '',
      questionData: this.props.familyData,
      instData: this.props.instData,
      viewMode: this.props.viewMode
    }

  }
  componentDidMount() {
    this.answerChangeHandler('', '');
  }

  answerChangeHandler = (event, index) => {
    let questionData = this.state.questionData;
    if (event) {
      let value = event;
      let answer = questionData[index].options.find(obj => obj.id.toString() === value.toString());
      questionData[index].optionId = value;
      questionData[index].answer = answer.option;
      this.setState({
        questionData: questionData
      });
    }
    if (questionData[0].optionId !== '' && questionData[1].optionId !== '' && questionData[2].optionId !== '') { // && questionData[3].optionId !== ''
      this.props.onClick(false, this.state.questionData, 'LIFESTYLE');
    } else {
      this.props.onClick(true, this.state.questionData, 'LIFESTYLE');
    }
  }
  handlerCopy(e) {
    e.preventDefault();

  }
  render() {
    return (
      <View pointerEvents={this.state.viewMode ? "none" : null}
        style={{ backgroundColor: COLORS.white, flex: 1, padding: hp('3%') }}>


        <Text style={{ fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.instData.title}</Text>
        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[0].question}</Text>

        <RadioButtons
          viewMode={this.state.viewMode}

          type={this.state.questionData[0].optionId}
          options={this.state.questionData[0].options}
          onPressButton={(result) => {
            this.answerChangeHandler(result, 0, 'Radio')
          }} />


        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[1].question}</Text>

        <RadioButtons
          viewMode={this.state.viewMode}

          type={this.state.questionData[1].optionId}
          options={this.state.questionData[1].options}
          onPressButton={(result) => {
            this.answerChangeHandler(result, 1, 'Radio')
          }} />
        <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%') }}>{this.state.questionData[2].question}</Text>

        <RadioButtons
          viewMode={this.state.viewMode}
          type={this.state.questionData[2].optionId}
          options={this.state.questionData[2].options}
          onPressButton={(result) => {
            this.answerChangeHandler(result, 2, 'Radio')
          }} />

        {
          this.state.instData.description !== '' &&
          <View>
            <Text style={{ fontSize: regular, ...testFont, marginTop: hp('3%'), marginBottom: hp('1%'), fontWeight: 'bold' }}>Why we need this?</Text>
            <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%'), marginBottom: hp('1%') }}>{this.state.instData.description}</Text>

          </View>

        }

        <Loader loading={this.state.loading} />

      </View>
    );
  }
};

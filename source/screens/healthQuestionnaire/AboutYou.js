
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, Image, View, FlatList, SafeAreaView } from "react-native";

import Loader from "../../components/Loader";
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import COLORS from "../../constants/COLORS";
import moment from "moment";
import DateTimePicker from '@react-native-community/datetimepicker';
import TextInput from "react-native-material-textinput";
import { FontsApp, testFont } from '../../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
import { Icon, Picker } from "native-base";
import stylesCommon from "../../constants/StyleSheet";

export default class AboutYou extends Component {
  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;
    this.state = {
      data: [],
      error: '',
      count: 0,
      selectedDate: new Date(),
      setSelectedDate: '',
      refresh: false,
      progress: 0,
      disablePrev: false,
      checkedB: true,
      dob: this.props.familyData.dob,
      gender: this.props.familyData.gender,
      email: this.props.familyData.email,
      isPrimary: this.props.familyData.isPrimary,

      showEdit: false,
      genderList: ["MALE", "FEMALE"],
      loaderShow: false,
      instData: this.props.instData,
      ageValid: false,
      age_latest: '',
      dateErr: false,
      todayDateValid: false,
      birthDtFocus: false,
      birthDt: false,
      x1: '',
      heightValid: true,
      date: new Date()

    }

  }

  componentDidMount() {
    let age = this.handleDateChange(this.state.dob, true);
    this.setState({
      refresh: true,
      loaderShow: false,
      ageValid: age
    });
  }

  enableNext() {
    let val;
    console.log('============== enableNext ============', this.state.dob,
      this.state.gender,
      this.state.weight,
      this.state.inches,
      this.state.heightValid,
      this.state.dateErr,
      this.state.feet
    );

    if (this.state.dob !== '' && this.state.gender !== '' && this.state.feet !== '' && this.state.weight !== '' && this.state.inches !== '' && !this.state.dateErr && this.state.heightValid) {
      val = false;
    } else {
      val = true;
    }

    let obj = {
      dob: this.state.dob,
      gender: this.state.gender,
      email: this.state.email
    };

    this.props.onClick(val, obj);
  }

  textChangeHandler = (val, type) => {

    if (val != "") {
      if (type == "email") {
        this.setState({ email: val })
      } else {
        this.setState({ gender: val })
      }

    } else {
      if (type == "email") {
        this.setState({ email: "" })
      } else {
        this.setState({ gender: "" })
      }
    }

    this.enableNext();
  }

  calculate_age = (dob1) => {
    var today = new Date();
    var birthDate = new Date(dob1);
    var age_now = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age_now--;
    }
    if (this.props.age !== null && this.props.age !== undefined && age_now !== this.props.age) {
      return false;
    }
    else if (age_now > 85) {
      return false;
    } else {
      return true;
    }

  }
  handleDateChange = (date, didMount) => {
    const today = new Date()
    const valDate = new Date(date);
    let validAge = false;
    console.log("didMount", didMount, " date ", date)
    this.state.dob = moment(date).format('YYYY-MM-DD');
    if (this.props.age > 0 && valDate.getDate() === today.getDate() && valDate.getMonth() === today.getMonth() && valDate.getFullYear() === today.getFullYear()) {
      if (didMount === true) {
        this.state.todayDateValid = true;
      } else {
        this.state.todayDateValid = false;
      }
      validAge = false;
    } else {
      if (this.props.age === 0 && valDate.getDate() === today.getDate() && valDate.getMonth() === today.getMonth() && valDate.getFullYear() === today.getFullYear()) {
        this.state.todayDateValid = false;
        validAge = true;
      } else {
        if (didMount === true && valDate === today) {
          this.state.todayDateValid = true;
        } else {
          this.state.todayDateValid = false;
        }
        validAge = this.calculate_age(this.state.dob);
      }
    }

    console.log("validAge", validAge);
    this.setState({
      ageValid: validAge
    })

    if (!validAge) {
      this.setState({ dateErr: true }, () => {
        this.enableNext();
      })

    } else {
      this.setState({ dateErr: false }, () => {
        this.enableNext()
      })
    }
  }

  render() {
    let myDate = moment(this.state.dob).format('MM') + '/' + moment(this.state.dob).format('DD') + '/' + moment(this.state.dob).format('YYYY');
    return (

      <View style={{ backgroundColor: COLORS.white, flex: 1, padding: hp('3%') }}>

        <Text style={{ fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.instData.title + ""}</Text>
        {/* <DateTimePicker
          testID="dateTimePicker"
          value={this.state.todayDateValid ? null : myDate}
          mode={'datetime'}
          is24Hour={true}
          display="default"
          onChange={this.handleDateChange}
        /> */}
        <View style={{ justifyContent: 'center' }}>
          <TextInput
            marginTop={10}
            editable={false}
            labelActiveTop={-25}
            value={this.state.todayDateValid ? null : myDate}
            fontSize={subHeading}
            fontFamily={FontsApp.fontFamily}
            label={"Birth Date*"}
            labelActiveColor={COLORS.labelActiveColor}
            underlineColor={COLORS.underlineColor}
            underlineActiveColor={COLORS.underlineActiveColor}
            underlineHeight={hp("0.4%")}
            onChangeText={this.handleDateChange.bind(this)}
          />
          <Image
            style={{
              position: 'absolute', end: 0, marginRight: wp('5%'),
              height: wp("6%"), width: wp("6%"),
            }}
            resizeMode='contain'
            source={require('../../assets/images/event.png')}
          />
        </View>
        {/* <TextInput
          editable={false}
          marginTop={10}
          labelActiveTop={-30}
          value={this.state.gender}
          fontSize={subHeading}
          label={"Birth Gender*"}
          labelActiveColor={COLORS.labelActiveColor}
          underlineColor={COLORS.underlineColor}
          underlineActiveColor={COLORS.underlineActiveColor}
          underlineHeight={hp("0.4%")}
          onChangeText={result => this.textChangeHandler(result, "gender")}
        /> */}
        <Text style={{
          color: COLORS.graya1, fontSize: regular, ...testFont,
        }}>Birth Gender*</Text>

        <View style={{ borderColor: COLORS.divider, borderBottomWidth: 3, marginBottom: hp('1.5%') }}>
          <Picker
            selectedValue={this.state.gender}
            mode="dropdown"
            iosHeader="Select Gender"
            iosIcon={<Icon name="ios-arrow-down" />}
            enabled={false}
            style={{ width: wp('90%'), marginLeft: -15 }}
            onValueChange={(itemValue, itemIndex) => {
              this.setState({ error: '', gender: itemValue, })
            }}>

            <Picker.Item color={COLORS.primaryColor} style={{ fontSize: regular, ...testFont }} label={'Select Gender'} value="first" />

            <Picker.Item style={{ fontSize: regular, ...testFont }} label="MALE" value="MALE" />
            <Picker.Item style={{ fontSize: regular, ...testFont }} label="FEMALE" value="FEMALE" />
          </Picker>
          {
            this.state.errorId == 4 ? <Text style={stylesCommon.errorRegular}>{this.state.error}</Text> : null
          }

        </View>
        <TextInput
          editable={false}
          marginTop={10}
          labelActiveTop={-30}
          value={this.state.email}
          fontSize={subHeading}
          fontFamily={FontsApp.fontFamily}
          label={"Enter Email"}
          labelActiveColor={COLORS.labelActiveColor}
          underlineColor={COLORS.underlineColor}
          underlineActiveColor={COLORS.underlineActiveColor}
          underlineHeight={hp("0.4%")}
          onChangeText={result => this.textChangeHandler(result, "email")}
        />


        <Text style={{
          fontSize: regular, ...testFont, marginTop: hp('1%'),
          marginBottom: hp('1%'), fontWeight: 'bold'
        }}>Why we need this?</Text>
        <Text style={{
          fontSize: regular, ...testFont,
          marginTop: hp('1%'), marginBottom: hp('1%')
        }}>We need your email address for
          any personal health related communication.</Text>

        <Loader loading={this.state.loaderShow} />

      </View>

    );
  }
  onChange = (selectedDate) => {
    console.log("selectedDate", selectedDate.nativeEvent.timestamp)
    this.setState({ date: new Date(selectedDate.nativeEvent.timestamp) })
  };

};

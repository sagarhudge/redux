
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, SafeAreaView } from "react-native";

import styles from "../../constants/StyleSheet";
import SendIntentAndroid from "react-native-send-intent";
import { isAndroid } from "../../constants/PlatformCheck";
import { Image } from "react-native-elements";
import Loader from "../../components/Loader";
import { Card } from "react-native-elements";
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import COLORS from "../../constants/COLORS";
import CheckBox from '@react-native-community/checkbox';
import PrivacyPolicy from '../../components/PrivacyPolicy';
import AuthorizationModal from '../../components/AuthorizationModal';
import TextInput from "react-native-material-textinput";
import { FontsApp, testFont } from '../../constants/FontsApp'
import { CustomTextInput } from "../../components/CustomTextInput";
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let inputTextHeight = FontsApp.inputTextHeight;

export default class AgreementChange extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.state = {
            modalShow: false,
            modalShowPrivacy: false,
            displayPrivacyCheck: false,
            authModalShow: false,
            privacyPolicy: this.props.privacyPolicy,
            authorize: this.props.authorize,
            viewMode: this.props.viewMode,
            fullName: this.props.viewMode ? this.props.fullName : '',
            loading: false,
            errorId: null
        }
    }

    componentDidMount() {
        this.handleAgreement();
    }

    handleAgreement = () => {

        if (this.state.privacyPolicy && this.state.authorize && this.state.fullName !== '') {
            this.props.onClick(false, this.state.authorize, this.state.privacyPolicy, this.state.fullName)
        } else {
            this.props.onClick(true, this.state.authorize, this.state.privacyPolicy, this.state.fullName)
        }

    }

    showAuthorizeModalPopup = (event) => {
        this.setState({
            authModalShow: true,
            // authorize: this.props.viewMode ? false : true,
            // authorize: this.props.viewMode ? true : false,
        }, () => this.handleAgreement)


    }
    showPlansModalPrivacy = () => {
        this.setState({
            modalShowPrivacy: true,
            loading: false
        }, () => {
            this.handleAgreement()
        });
    }

    hideAllModal = () => {
        this.setState({
            modalShowPrivacy: false,
            authModalShow: false,
            loading: false

        });
    }

    privacyPolicyClick(result) {

        if (result != null) {
            this.setState({
                modalShowPrivacy: false,
                privacyPolicy: result,
                error: '', loading: false
            }, () => {
                this.handleAgreement()
            });
        } else {
            this.setState({
                modalShowPrivacy: false,
                error: '', loading: false
            }, () => {
            });
        }


    }
    AuthorizationModal(result) {
        if (result != null) {
            this.setState({
                authModalShow: false,
                authorize: result,
                error: '', loading: false
            }, () => {
                this.handleAgreement()
            });
        } else {
            this.setState({
                authModalShow: false,
                error: '', loading: false
            }, () => {
            });
        }
    }

    nameChangeHandler = (val, isValid) => {
        if (isValid) {
            this.setState({
                fullName: val, loading: false, errorId: null

            }, () => {
                this.handleAgreement()
            })
        } else {
            this.setState({
                fullName: '', loading: false, errorId: 0

            }, () => {
                this.handleAgreement()
            })
        }
    };

    render() {
        return (

            <View style={{ backgroundColor: COLORS.white, flex: 1, padding: hp('3%') }}>

                <Text style={{ fontSize: regular, ...testFont, fontWeight: 'bold', marginVertical: hp('2%') }}> Review & accept terms</Text>

                <TouchableOpacity
                    onPress={() => { this.showPlansModalPrivacy() }}
                    style={{ marginBottom: hp("1%"), flexDirection: 'row', alignItems: 'center', marginRight: 10 }}>
                    <CheckBox
                        disabled={true}
                        boxType={'square'}
                        value={this.state.privacyPolicy}
                        tintColors={{ true: COLORS.primaryColor, false: COLORS.graya1 }}
                        onValueChange={
                            result => {
                                !this.state.viewMode ? this.setState({ privacyPolicy: result, }) : null
                                !this.state.viewMode ? this.showPlansModalPrivacy() : null
                            }
                        }
                        style={{ width: 30, height: 30 }}
                    />
                    <Text style={{ marginLeft: hp("1%"), fontSize: regular, ...testFont }}>{`*I agree with the `}
                        <Text style={{
                            marginHorizontal: hp("1%"), fontWeight: 'bold', textDecorationLine: 'underline',
                            fontSize: regular, color: COLORS.primaryColor, ...testFont
                        }}>privacy policy.</Text></Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { this.showAuthorizeModalPopup() }}
                    style={{
                        marginBottom: hp("1%"), flexDirection: 'row',
                        alignItems: 'center', marginRight: 10
                    }}>

                    <CheckBox
                        disabled={true}
                        boxType={'square'}
                        value={this.state.authorize}
                        tintColors={{ true: COLORS.primaryColor, false: COLORS.graya1 }}
                        onValueChange={
                            result => {
                                !this.state.viewMode ? this.setState({ authorize: result, }) : null
                                !this.state.viewMode ? this.showAuthorizeModalPopup() : null
                            }
                        }
                        style={{ width: 30, height: 30 }}
                    />
                    <Text style={{ marginHorizontal: hp("1%"), fontSize: regular, ...testFont }}>{`*I authorize `}
                        <Text style={{
                            marginHorizontal: hp("1%"), fontWeight: 'bold', textDecorationLine: 'underline',
                            fontSize: regular, color: COLORS.primaryColor, ...testFont
                        }}>Universal Health Fellowship to contact my Medical Provider.</Text></Text>
                </TouchableOpacity>
                <Text style={{ fontSize: regular, ...testFont, marginTop: hp('1%'), color: this.state.fullName || this.state.fullName.trim() !== '' ? COLORS.primaryColor : COLORS.subText }}>Approve submission by typing in your full name*</Text>
                <TextInput
                    marginTop={-10}
                    labelActiveTop={-30}
                    editable={!this.state.viewMode}
                    value={this.state.fullName ? this.state.fullName : ''}
                    fontSize={inputTextHeight}
                    fontFamily={FontsApp.fontFamily}
                    error={this.state.errorId == 0 ? "Full name required" : ''}
                    underlineColor={COLORS.underlineColor}
                    underlineActiveColor={COLORS.underlineActiveColor}
                    underlineHeight={hp("0.4%")}
                    onChangeText={result => this.nameChangeHandler(result, result != "")}
                />

                <Text style={{
                    fontSize: regular, color: COLORS.textColor, ...testFont,
                    marginBottom: hp('3%'), marginTop: hp('2%')
                }}>Your medical questionnaire is now ready to be submitted.
                    Review the terms and indicate your acceptance of the same.
                   </Text>
                <Loader loading={this.state.loading} />

                <PrivacyPolicy display={this.state.modalShowPrivacy}
                    viewMode={this.state.viewMode}
                    privacyPolicy={this.privacyPolicyClick.bind(this)} />
                <AuthorizationModal display={this.state.authModalShow}
                    viewMode={this.state.viewMode}
                    AuthorizationModal={(result) => this.AuthorizationModal(result)} />

            </View>

        );
    }
};

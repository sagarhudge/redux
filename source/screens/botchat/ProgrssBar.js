
import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions, View, Text } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
let intervalCount = 1000;
let progressCount = 100;

export default class ProgressBar extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
        }
    }
    componentDidMount() {
        //we can increase `intervalCount` to 10 sec its 1000 ms (1 sec) 
        this.animationListeners();
    }

    animationListeners() {
        this.interval = setInterval(() => {
            this.setState({ progress: this.state.progress + 1 });
            this.state.progress === progressCount && clearInterval(this.interval);
        }, intervalCount)
    }

    render() {
        return (
            <View style={{ width: windowWidth, borderColor: '#FDAD9E',backgroundColor: '#FEE5E0', height: 20, borderWidth: 1 ,marginTop:30}}>
                <View style={{ height: 20, backgroundColor: '#FC4E2C', width: this.state.progress + "%" }} />
                <Text style={{ color: 'black', position: 'absolute', alignSelf: 'center' }}>{this.state.progress}%</Text>
            </View>
        );
    }
}

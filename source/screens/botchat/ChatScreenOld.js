
import React, { Component } from "react";
import { Keyboard, Alert, KeyboardAvoidingView, Text, TouchableOpacity, View, FlatList, Image, StyleSheet, ScrollView } from "react-native";
import { getGateWayToken, getAxiosUser, getAxiosWithToken, postAxiosWithToken, getData, postAxiosGetEmpId, getMyneedsEOS, getFindProvider } from '../../constants/CommonServices'

import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import BaseUrl from '../../constants/BaseUrl'
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from "../../constants/AppStrings";
import moment from "moment";
import HomeFooter from '../../screens/HomeFooter';
import { SafeAreaView } from "react-native";

import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
import { passwordCheck } from "../../constants/utils";
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let headingCard = FontsApp.headingCard
let smallText = FontsApp.smallText
let errorMessage = "ERROR";
import ChatTextInput from '../botchat/component/ChatTextInput'
import { ChatNavigation } from "./ChatNavigation";
import { isAndroid } from "../../constants/PlatformCheck";
import Icon from 'react-native-vector-icons/Entypo';

import { CirclesLoader, PulseLoader, TextLoader, DotsLoader, } from 'react-native-indicator';
import { DeviceEventEmitter } from "react-native";
import { Linking } from "react-native";
import { ChatInput } from "../botchat/ConstantChat";


import { connect } from "react-redux";
import apiCall from "../readux/ActionCreator";

class ChatScreenOld extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;

        this.state = {
            data: ChatInput,
            loading: false,
            error: ' ',
            chats: [],
            username: '',
            isTyping: false,
            loadMore: false,
            listPadding: 0,
            Connection: this.props.route.params.Connection,
        }

    }

    componentDidMount() {
        this.getData();
        // passwordCheck.applyHeaderChat(this.navigation, "YOUR AI-POWERED ASSISTANT");

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));

        console.log('mount called');

        //   this.setupReducer()
    }
    setupReducer() {
        this.props
            .apiCall("https://dev.fabric.carynhealth.com/api/v7/memberportal/getCardDetails/5449")
            .then((response) => {
                const data = this.props.data;
                console.log("resp---------------------\n\n--------", response)
                console.log("resp-------------data-\n\n---------------", data)
                this.setState({
                    data,
                });
            })
            .catch(error => {
                console.log("ERROR----------", error);
            });
    }
    pushInArray() {
        const messages = [{
            "query": "",
            "answers": [{ context: "Hi, I’m Caryn. How can I help you today, " + this.username + '?' }]
        }]

        this.setState({ chats: messages });
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        console.log('unmount called')
    }

    _keyboardDidShow() {
        // Alert.alert('Keyboard Shown');
        this.setState({ listPadding: 200 });

    }

    _keyboardDidHide() {
        //  Alert.alert('Keyboard Hidden');
        this.setState({ listPadding: 0 });

    }
    async getData() {
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.email = await AsyncStorage.getItem('email');
        this.username = await AsyncStorage.getItem('username');
        // this.setState({ username: this.username })
        // console.log(this.username);
        this.pushInArray();
    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
               
                <View style={{ flexDirection: 'row', backgroundColor: COLORS.primaryColor, height: hp('7%') }}>
                    <Image
                        style={{
                            width: wp('20%'),
                            resizeMode: 'stretch',
                            height: hp('5.5%'), alignSelf: 'center',
                            marginHorizontal: wp('3%')
                        }}
                        source={require('../../assets/images/ask_bomba_logo.png')}
                    />
                    <Text style={{
                        width: wp('62%'), alignSelf: 'center',
                        fontSize: wp('4.3%'), ...testFont, fontWeight: 'bold', color: COLORS.white
                    }}>YOUR AI-POWERED ASSISTANT</Text>

                    <TouchableOpacity onPress={() => {
                        this.props.navigation.goBack();
                    }} style={{ alignSelf: 'center', marginLeft: wp('2%') }}>
                        <Icon name={"cross"} size={wp('8%')} color={COLORS.white} />
                    </TouchableOpacity>

                </View>
                <View style={{flex:1,}}>
                    <FlatList
                              style={{ flex: 1,
                                flexGrow: 0 , bottom:0,position:'absolute',
                                 }}  
                        showsVerticalScrollIndicator={true}
                        numColumns={1}
                        
                        data={this.state.chats}
                        ref={ref => this.flatList = ref}
                        onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                        onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                        renderItem={this.renderItem.bind(this)}
                        ListFooterComponent={
                            this.state.isTyping && <View style={{ marginHorizontal: 10, marginVertical: 15 }}>
                                <DotsLoader color={COLORS.grayDark} />
                            </View>
                        }

                        keyExtractor={(item, index) => index.toString()}
                    />
                   
                </View>
                <ChatTextInput
                        onPressSend={(result) => {
                            Keyboard.dismiss();
                            this.perforUpdateChat(result)
                        }} />
                
            </SafeAreaView >

        );
    }

    perforUpdateChat(result) {
        let obj = { "query": result, answers: [] }
        let obj2 = {
            "query": '', "answers": [{
                "context": "No result matches to your query?",
            }]
        }
        let found = false
        let answers;

        ChatInput.map((key, index) => {
            // console.log(key.query, key.query.includes(result), index, result)
            if (key.query.toLocaleLowerCase().includes(result.toLocaleLowerCase())) {
                found = true;
                answers = key.answers;
                return;
            }
        });

        if (found) {
            obj2 = {
                "query": '', "answers": answers
            }
        }

        this.setState({ listPadding: 0, isTyping: true });

        this.setState({
            chats: [...this.state.chats, obj]
        }, () => {
            this.setState({ listPadding: 0, isTyping: false, chats: [...this.state.chats, obj2] });
        });

        this.flatList.scrollToEnd({ animated: true })
    }
    findProvider() {
        getFindProvider(this.accesstoken, this.email).then(resp => {
            console.log(resp)
            if (resp.includes("http://")) {
                Linking.openURL(resp)
            } else {
                let include = passwordCheck.getSessionStatus(resp)
                DeviceEventEmitter.emit('sessionOut', include)
            }
        }).catch(e => {
            console.log(e.message)
        })
    }

    renderItem({ item }) {
        return (
            <View style={{ flex: 1 }}>
                {item.query != '' && <View
                    style={{
                        flexDirection: 'row', flex: 1,
                        alignSelf: 'flex-end'
                    }} >

                    <Text style={{
                        color: COLORS.white, fontSize: hp('2.2%'),
                        flexShrink: 1,
                        borderRadius: 15,
                        backgroundColor: COLORS.blueL,
                        padding: 15,
                        margin: 10
                    }}>{item.query}</Text>

                </View>}
                {item.answers && this.anserList(item.answers)}
                {/* {console.log("item.options",item.answers)} */}
            </View>


        )
    }
    navigationFun(key, message) {
        if (key && key.includes('Find provider')) {
            this.findProvider();
        } else if (key != "")
            ChatNavigation(key.toLowerCase(), this.props, message, this.state.Connection)
    }

    anserList = (answers) => {
        return <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {
                answers &&
                answers.map((key, index) => (
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                this.navigationFun(key.context, key.context);
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row'
                                }} >

                                <Text style={{
                                    color: COLORS.textColor, fontSize: hp('2.2%'),
                                    flexShrink: 1,
                                    borderRadius: 15,
                                    backgroundColor: COLORS.background,
                                    padding: 15,
                                    margin: 10
                                }}>{key.context}</Text>

                            </View>

                        </TouchableOpacity>
                        {key && this.optionList(key)}

                    </View>

                ))

            }
        </View>
    }

    optionList = (answer) => {
        return <ScrollView horizontal showsHorizontalScrollIndicator={false} >

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {
                    answer.options &&
                    answer.options.map((key, index) => (<TouchableOpacity
                        onPress={() => {
                            this.navigationFun(key.intent, key.message);
                        }}>
                        <View
                            style={{ flexDirection: 'row', width: wp('40%'), justifyContent: 'space-around' }}>

                            <Text style={{
                                color: COLORS.white, fontSize: hp('2.2%'),
                                borderRadius: 15,
                                backgroundColor: COLORS.primaryColor,
                                padding: 15,
                            }}>{key.message}</Text>
                        </View>

                    </TouchableOpacity>))

                }
            </View>
        </ScrollView>
    }

};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: regular, color: COLORS.textColor, ...testFont,
    },
    textStyleColor: {
        fontSize: regular, color: 'red', ...testFont,
    },
    textStyleTitle: {
        fontSize: smallText, color: COLORS.subText, marginTop: hp('1%'), ...testFont,
    },
});

const mapDispatchToProps = dispatch => ({
    apiCall: url => dispatch(apiCall(url))
});

const mapStateToProps = state => ({
    data: state.apiReducer.data,
    error: state.apiReducer.error,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatScreenOld);
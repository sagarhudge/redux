import AppStrings from "../../../constants/AppStrings";

export const ChatNavigation = (screen, props, optionSelected, Connection, isHealthyShare) => {
    var path = AppStrings.AppNavigator;
    console.log("ChatNavigation ", screen,);
    let isNotification = true;
    if (screen.includes("my needs") || screen.includes("needs")) {
        path = AppStrings.MyNeedsScreen;
    }
    else if ((screen.includes("my transaction") ||
        screen.includes("transaction") || screen.includes("transactions"))) {
        path = AppStrings.MyTransactionsNav;
    }
    else if (screen.includes("documents")) {
        path = AppStrings.DocumentsNav;
    }
    else if (screen.includes("notifications")) {
        path = AppStrings.NotificationDetails;
    }
    else if (screen.includes("contactcardscreen")) {
        path = AppStrings.ContactCardScreen;
    }
    else if (screen.includes("health questionary")) {
        path = AppStrings.HealthQuestionnaireNav;
    }
    else if (screen.includes("announcements") || screen.includes("announcement")) {
        path = AppStrings.NotificationDetails;
        isNotification = false;
    }
    else if (screen.includes("health questionnaire")) {
        path = AppStrings.HealthQuestionnaireNav;
    }
    else if (screen.includes("program information")) {
        path = AppStrings.ProgramInformation;
    }
    else if (screen.includes("payment wallet") || screen.includes('payment card')) {
        path = AppStrings.paymentNav;
    }
    else if (screen.includes("membership id") && !isHealthyShare) {
        path = AppStrings.DigitalHealthCardNew;
    } else if (screen.includes("membership id") && isHealthyShare) {
        path = AppStrings.HealthyShareCard;
    }
    else if (screen.includes("health tool")) {
        path = AppStrings.HealthToolCard;
    }
    else if (screen.includes("change payment")) {
        path = AppStrings.ChnagePaymentMethod;
    }
    else {
        path = AppStrings.AppNavigator;
    }

    console.log("Navigation to screen AI-------------------------|", path, screen, isNotification);
    props.navigation.navigate(path, { key: optionSelected, Connection: Connection, notification: isNotification });
}
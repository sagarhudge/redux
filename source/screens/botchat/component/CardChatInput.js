import React from "react";
import COLORS from "../../../constants/COLORS";
import { widthToDp as wp, heightToDp as hp } from '../../../assets/StyleSheet/Responsive';
import { FontsApp } from '../../../constants/FontsApp'

let regular = FontsApp.regular
import { View, TextInput, Image, TouchableOpacity } from 'react-native';


let onPressSend = (text, props) => {
    props.onPressSend(text);

};

const CardChatInput = (props) => {
    const [value, onChangeText] = React.useState('');
    const { isDisabled } = props;

    return (
        <View>
            {
                <View style={{
                    // bottom: 0, position: 'absolute',
                    flexDirection: 'row', alignItems: 'center',
                    paddingLeft: wp('2%'),
                    width: wp('80%'),
                }}>
                    <View
                        style={{
                            backgroundColor: COLORS.white,
                            borderRadius: hp('3%'),
                            alignItems: 'center',
                            borderColor: COLORS.primaryColor,
                            height: hp('6%'),
                            flexDirection: 'row',
                            paddingLeft: 4,
                            borderWidth: 2,
                            width: wp('75%'),

                        }}>
                        <TextInput
                            {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                            editable
                            onChangeText={result => onChangeText(result)}
                            value={value}
                            fontSize={hp("2%")}
                            style={{ paddingLeft: 10 }}
                            width={wp('63%')}
                            onSubmitEditing={() => {
                                onChangeText(value)
                            }}
                            placeholder="Search within documents"
                        />
                        <TouchableOpacity style={{
                            backgroundColor: COLORS.primaryColor,
                            height: hp('6%'),
                            width: hp('6%'),
                            padding: 10,
                            alignItems: 'center', justifyContent: 'center',
                            borderRadius: hp('3%')
                        }} onPress={() => {
                            onPressSend(value, props); onChangeText('')
                        }}>
                            <Image
                                style={{
                                    width: wp('8%'),
                                    resizeMode: 'contain',
                                    height: wp('8%'),
                                }}
                                source={require('../../../assets/images/Floating_Button.png')}
                            />
                        </TouchableOpacity>
                    </View>

                </View>

            }
        </View>
    );
}

export default CardChatInput;
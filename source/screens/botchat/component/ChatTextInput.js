import React from "react";
import COLORS from "../../../constants/COLORS";
import { widthToDp as wp, heightToDp as hp } from '../../../assets/StyleSheet/Responsive';
import { FontsApp } from '../../../constants/FontsApp'

let regular = FontsApp.regular
import { View, TextInput } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from "react-native-gesture-handler";

let onPressSend = (text, props) => {
    props.onPressSend(text);

};

const ChatTextInput = (props) => {
    const [value, onChangeText] = React.useState('');
    const { isDisabled } = props;

    return (
        <View style={{
            // bottom: 0, position: 'absolute',
            height: hp('10%'), flexDirection: 'row', alignItems: 'center',
            paddingLeft: wp('2%'), justifyContent: 'center',
            width: wp('100%'), backgroundColor: COLORS.background
        }}>
            <View
                style={{
                    flexDirection: 'row',
                }}>
                <TextInput
                    {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                    editable
                    onChangeText={result => onChangeText(result)}
                    value={value}
                    autoCorrect={false}
                    // keyboardType="visible-password"
                    style={{ backgroundColor: 'white', marginHorizontal: 10, height: hp('7%'), paddingLeft: 15, borderColor: COLORS.grayDark, borderRadius: hp('3.5%'), borderWidth: 2, }}
                    fontSize={hp("2%")}
                    width={wp('80%')}
                    placeholder="Type your request to Caryn..."
                />
                <TouchableOpacity style={{
                    backgroundColor: COLORS.background,
                    height: hp('7%'),
                    width: hp('7%'),
                    padding: 10, marginRight: 10,
                    alignItems: 'center', justifyContent: 'center',
                    borderRadius: hp('3.5%')
                }} onPress={() => {
                    value && onPressSend(value, props);
                    onChangeText('')
                }}>
                    <Icon name="send-sharp" size={wp('8%')} color={value != '' ? COLORS.primaryColor : COLORS.grayDark} />
                </TouchableOpacity>
            </View>

        </View>
    );
}

export default ChatTextInput;
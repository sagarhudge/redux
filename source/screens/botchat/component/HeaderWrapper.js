import React from 'react';
import { Keyboard, View, Text } from 'react-native';
import ChatTextInput from './ChatTextInput';


export default HeaderWrapper = (props) => {

    return (
        <View style={{ flex: 1 }}>

            {props.children}

            <ChatTextInput onPressSend={(result) => {
                Keyboard.dismiss();
                props.callBackWrapper(result)
            }} />

        </View>
    )
}
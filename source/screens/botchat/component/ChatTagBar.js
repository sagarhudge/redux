import React from "react";
import COLORS from "../../../constants/COLORS";
import { widthToDp as wp, heightToDp as hp } from '../../../assets/StyleSheet/Responsive';
import { FontsApp } from '../../../constants/FontsApp'

let regular = FontsApp.regular
import { View, TouchableOpacity, Text } from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';

let onPressSend = (text, props) => {
    props.onPressSend(text);

};

const ChatTagBar = (props) => {
    const [value, onChangeText] = React.useState('');
    const { tag, question } = props;
    console.log(tag, question)

    return (
        <View style={{
            // bottom: 0, position: 'absolute',
            height: hp('10%'), flexDirection: 'row', alignItems: 'center',
            paddingLeft: wp('2%'), justifyContent: 'center',
            width: wp('100%'), backgroundColor: COLORS.background
        }}>
            <View
                style={{
                    backgroundColor: COLORS.white,
                    borderRadius: hp('3.5%'),
                    alignItems: 'center',
                    borderColor: COLORS.primaryColor,
                    height: hp('7%'),
                    flexDirection: 'row',
                    borderWidth: 2,
                    alignSelf: 'center',
                    width: wp('95%'),
                    paddingLeft: 15,
                }}>
                <Text>{question}</Text>
                {

                    tag.map((n, i) => {
                        console.log(n, "-------key---------");

                        return (   <TouchableOpacity style={{
                            height: hp('5%'),
                            paddingHorizontal: 10, minWidth: 20,
                            paddingVertical: 5,marginLeft:5,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: hp('2.5%'),
                            backgroundColor: COLORS.background,
                            borderEndWidth: 1
                        }} onPress={() => {
                            console.log("tagpress")
                        }}>
                            <Text>{n}</Text>
                            <Icon name="close" style={{ marginLeft: wp('1.5%') }} size={wp('4.5%')} color={COLORS.black} />
                        </TouchableOpacity>
                    )})

                }
            </View>

        </View >
    );
}

export default ChatTagBar;
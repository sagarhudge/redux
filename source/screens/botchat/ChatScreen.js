
import React, { createRef, PureComponent } from "react";
import { Keyboard, Alert, KeyboardAvoidingView, Text, TouchableOpacity, View, FlatList, Image, StyleSheet, ScrollView } from "react-native";

import COLORS from "../../constants/COLORS"
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import { SafeAreaView } from "react-native";

import { FontsApp, testFont } from '../../constants/FontsApp'
import { passwordCheck } from "../../constants/utils";

let regular = FontsApp.regular
let smallText = FontsApp.smallText
import { ChatNavigation } from "./component/ChatNavigation";
import Icon from 'react-native-vector-icons/Entypo';

import { DotsLoader, } from 'react-native-indicator';
import { DeviceEventEmitter } from "react-native";
import { Linking } from "react-native";
import { ChatInput } from "../botchat/ConstantChat";

import { connect } from "react-redux";
import { actionCreator } from "../readux/ActionCreator";
import { getAxiosUser, getFindProvider } from "../../constants/CommonServices";
import { isHealthyShare } from "../../constants/DigitalCardType";
import { isAndroid } from "../../constants/PlatformCheck";
import BaseUrl from "../../constants/BaseUrl";
import HeaderWrapper from "./component/HeaderWrapper";
import SendIntentAndroid from "react-native-send-intent";
import { memberApps } from "../../constants/ArrayConst";
// import CryptoJS from "react-native-crypto-js";
var CryptoJS = require('crypto-js');

class ChatScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.resultChat;
        this.state = {
            error: '',
            data: this.props.data,
            username: '',
            Connection: this.props.route.params.Connection,
            isHealthTool: false,
            memberId: '',
            programName: '',
            planInfo: [],
            agenteInformation: {}
        }

    }

    componentDidMount() {
        this.getData();
    }

    setupReducer(message) {
        let method = {
            "text": message,
            "userID": this.state.memberId
        }
        let url = "https://1dmevvy65l.execute-api.us-east-1.amazonaws.com/dev/lexbot";
        this.props.actionCreator(url, method, this.state.data);
    }

    async getData() {
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.email = await AsyncStorage.getItem('email');
        this.username = await AsyncStorage.getItem('username');
        let isHealthTool = await AsyncStorage.getItem('isHealthTool');

        this.programInfoData = JSON.parse(await AsyncStorage.getItem('programInfoData'));

        this.setState({ username: this.username, isHealthTool: isHealthTool && isHealthTool == 'true' ? true : false });

        let idcardData = JSON.parse(await AsyncStorage.getItem("GetIdCardData"));
        this.client_id_locale = await AsyncStorage.getItem('client_id_locale');

        let Data = idcardData[0]
        console.log("planInfoplanInfo--------------------", "", Data.planInfo)

        this.setContactandCardID(Data);

        let AgentInformation = JSON.parse(await AsyncStorage.getItem("AgentInformation"));
        if (AgentInformation) {
            //get agent information from async storage 
            this.setState({
                agenteInformation: AgentInformation.response, memberId: Data.memberId,
            });
        }

        this.getProgramDetails();

    }
    getProgramDetails() {
        var programInfo = BaseUrl.FABRIC_V7 + "memberportal/getProgramInformation/" + this.email;

        getAxiosUser(programInfo)
            .then(response => {
                if (response != null && response.data && response.status == 200) {
                    var resp = response.data;
                    let planInfo = resp.planInfo.map((key, index) => {
                        return key.idcardField
                    });
                    console.log("planInfo--------------------", planInfo)
                    this.setState({ programName: resp.programInfo.programName, planInfo: planInfo })
                }

            }).catch(error => {
                console.log("error", error.message);
            });
    }
    render() {
        const keyboardVerticalOffset = !isAndroid ? 40 : 10

        return (

            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
                {
                    this.headerShow()
                }
                <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    keyboardVerticalOffset={keyboardVerticalOffset}
                    behavior={isAndroid ? null : 'padding'}>

                    {/* HOC-pass component as argument*/}

                    <HeaderWrapper
                        callBackWrapper={(callBack) => { this.perforUpdateChat(callBack) }}>

                        <FlatList
                            showsVerticalScrollIndicator={true}
                            numColumns={1}
                            data={this.state.data}
                            ref={ref => this.flatList = ref}
                            onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                            onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                            renderItem={this.renderItem.bind(this)}
                            ListFooterComponent={
                                this.props.status === "PENDING" &&
                                <View style={{ marginHorizontal: 10, marginVertical: 15 }}>
                                    <DotsLoader color={COLORS.grayDark} />
                                </View>
                            }
                            keyExtractor={(item, index) => index.toString()}
                        />

                    </HeaderWrapper>


                </KeyboardAvoidingView>
            </SafeAreaView >
        );
    }


    findProvider() {
        getFindProvider(this.accesstoken, this.email).then(resp => {
            console.log(resp)
            if (resp.includes("http://")) {
                Linking.openURL(resp)
            } else {
                let include = passwordCheck.getSessionStatus(resp)
                DeviceEventEmitter.emit('sessionOut', include)
            }
        }).catch(e => {
            console.log(e.message)
        })
    }
    checkMedLifeapp = (link, ioslink) => {
        if (isAndroid) {
            SendIntentAndroid.isAppInstalled('com.mdlive.mobile').then((isInstalled) => {
                if (isInstalled) {
                    SendIntentAndroid.openApp('com.mdlive.mobile').then((wasOpened) => {
                    });
                }
                else {
                    Linking.openURL(link).catch(err => {
                    })
                }
            });
        }
        else {
            Linking.openURL(ioslink).catch(err => {
            })
        }
    }
    headerShow() {
        return (<View style={{
            flexDirection: 'row', backgroundColor: COLORS.primaryColor,
            height: hp('7%')
        }}>
            <Image
                style={{
                    width: wp('20%'),
                    resizeMode: 'cover',
                    height: hp('5%'), alignSelf: 'center',
                    marginHorizontal: wp('2%')
                }}
                source={require('../../assets/images/ask_bomba_logo.png')}
            />
            <Text style={{
                ...testFont,
                width: wp('63%'), alignSelf: 'center',
                fontSize: wp('4.3%'), ...testFont, fontWeight: 'bold', color: COLORS.white
            }}>YOUR AI-POWERED ASSISTANT</Text>

            <TouchableOpacity onPress={() => {
                this.props.navigation.goBack();
            }} style={{ alignSelf: 'center', marginLeft: wp('2%') }}>
                <Icon name={"cross"} size={wp('8%')} color={COLORS.white} />
            </TouchableOpacity>

        </View>)
    }

    renderItem({ item, index }) {

        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                {
                    //query asked 
                    item && item.query != "" && <View
                        style={{
                            flex: 1, maxWidth: '90%', backgroundColor: 'red',
                            alignSelf: 'flex-end', borderRadius: 10,
                            backgroundColor: "#4F4F4F",
                            padding: 15, margin: 10,
                        }}>
                        <Text style={{
                            ...testFont,
                            color: COLORS.white,
                            fontSize: hp('2.2%'),
                            flexShrink: 1,
                        }}>{item.query}</Text>

                    </View>
                }

                {
                    //navigational option list
                    item && item.intentRecognized && this.optionList(item.intentRecognized)}

                {
                    //informational or member specific or fallbacks
                    item && item.intentRecognized !== "user_query" && item.AIKBtext && item.AIKBtext.length > 0 && this.anserList(item.AIKBtext, item, item.intentRecognized)}

            </View>)
    }

    optionList = (intent) => {
        // console.log("isHealthTool", this.state.isHealthTool)
        return <ScrollView horizontal showsHorizontalScrollIndicator={false} >

            <View style={{ flexDirection: 'row' }}>
                {
                    ChatInput.map(answer => (
                        (answer.query === intent) && answer.options && answer.options.map((key, index) => (

                            (!this.state.isHealthTool && key.intent === 'Health Tool') ? null :
                                <TouchableOpacity
                                    onPress={() => { this.navigationFun(key.intent, key.message, key) }}
                                    style={styles.optionItem}>
                                    <Text style={{
                                        ...testFont,
                                        color: COLORS.white, fontSize: hp('2.2%'), marginHorizontal: 7, maxWidth: wp('35%'), alignSelf: 'center',
                                    }}>{key.message}</Text>
                                    <Text style={{
                                        ...testFont,
                                        marginHorizontal: 7,
                                        color: COLORS.white, alignSelf: 'center',
                                        fontSize: hp('4%'),
                                    }}><Image
                                    style={styles.arrowImg}
                                    source={require('./arrow.png')} /></Text>
                                </TouchableOpacity>))
                    ))
                }
            </View>
        </ScrollView>
    }

    encrypt(query) {
        //we are passinf program name and plan info in contxt taken from api
        let info = this.state.planInfo.concat(this.state.programName);
        console.log("info-----------------------------------", info);
        const data = {

            contextkeywords: info,
            memberId: this.state.memberId,
            query: query,
            ChannelUId: 'memberportal',
        }
        // let data="1604411970638";
        console.log("stringify-------", JSON.stringify(data))

        let key = CryptoJS.enc.Utf8.parse("000102030405060708090a0b0c0d0e0f");
        let iv = CryptoJS.enc.Utf8.parse("4t7w9z$C&F)J@NcR");

        // let key = "000102030405060708090a0b0c0d0e0f";
        // let iv = "4t7w9z$C&F)J@NcR";

        let input = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
        // console.log("key-------", key, "---iv-----", iv)

        let ciphertext = CryptoJS.AES.encrypt(input, key, { keySize: 256 / 32, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }).toString();

        return ciphertext;
    }

    decrypt(encrypt) {
        // let key = "000102030405060708090a0b0c0d0e0f";
        // let iv = "4t7w9z$C&F)J@NcR";
        let key = CryptoJS.enc.Utf8.parse("000102030405060708090a0b0c0d0e0f");
        let iv = CryptoJS.enc.Utf8.parse("4t7w9z$C&F)J@NcR");

        let bytes = CryptoJS.AES.decrypt(encrypt, key, { iv: iv });
        return bytes.toString(CryptoJS.enc.Utf8);
    }

    anserList = (answers, item, intent) => {
        // let url = `https://inf-labs.com/?isnav=false&ismobile=true&memberId=${this.state.memberId}&query=${item.query}`
        // console.log("Ask Caryn--", url)
        return <View style={{ flex: 1 }}>
            {
                <View
                    style={{
                        borderRadius: 10,
                        backgroundColor: "#eeeeee",
                        padding: 15, alignSelf: 'flex-start',
                        margin: 10, maxWidth: '90%'
                    }}
                >
                    {!answers[0].answer.includes('Hi, I’m Caryn') ? <View>
                        <Text style={{
                            color: COLORS.textColor,
                            fontSize: hp('2.2%')
                        }}>{intent === "fallback"? `I didn't get any specific answers, but found some relevant references in our Knowledge Base. Hope this helps!` :`I also found some answers in our Knowledge Base.`}</Text>

                        {this.componentOption("View Details", item.query)}
                    </View> : <Text style={{
                        color: COLORS.textColor,
                        fontSize: hp('2.2%')
                    }}>{answers[0].answer + this.state.username + " ?"}</Text>
                    }

                    {/* <Text style={{ ...testFont }}>{item.intentRecognized}</Text> */}
                </View>
            }
        </View>
    }

    componentOption(title, query) {
        console.log("Digital Conc. query-----------------", query)
        return <TouchableOpacity
            onPress={() => {
                // Linking.openURL(url)
                let encrypt = this.encrypt(query);
                let data = encodeURIComponent(encrypt);
                console.log("encrypt", data)
                console.log("decrypt", this.decrypt(decodeURIComponent(data)))

                let url = `https://inf-labs.com/?isnav=false&ismobile=true&data=${data}`
                this.props.navigation.navigate("NewEnrollMent", { url: url, isChat: true });
            }}
            style={title == 'View Details' ? styles.readMore : styles.optionItem}>
            <Text style={{
                color: COLORS.white, fontSize: hp('2.2%'), marginHorizontal: 7,
            }}>{title}</Text>

            <Text style={{
                marginHorizontal: 7,
                color: COLORS.white, alignSelf: 'center',
                fontSize: hp('4%'),
            }}><Image
            style={styles.arrowImg}
            source={require('./arrow.png')} /></Text>

        </TouchableOpacity>
    }

    componentDidUpdate(mProps, prevProps) {
        // console.log("componentDidUpdate----------", mProps.error)
        if (mProps.data.length === this.state.data.length) {
            this.setState({ data: mProps.data, error: "" });
            this.flatList.scrollToEnd({ animated: true })
        }
    }

    perforUpdateChat(result) {
        this.resultChat = {
            "query": result,
            "AIKBtext": [],
            "intentRecognized": 'user_query'
        }

        this.setState({ data: [...this.state.data, this.resultChat] }, () => {
            console.log("state updated")
        });
        this.setupReducer(result);//send message
        this.flatList.scrollToEnd({ animated: true })
    }
    //Key=Intent received 
    //message=wat are the benefits in my plan
    WelcomeKit = async () => {
        let welcomeKit = await AsyncStorage.getItem('welcomekit');
        Linking.openURL(welcomeKit)
    }

    navigationFun(key, message, item) {
        console.log("keykey","",key)
        if (key && key.includes('find_provider')) {
            this.findProvider();
        } else if (key && key.includes('UHF')) {
                this.WelcomeKit();
            }
            else if (key === "Call") {
                console.log("this.state.agenteInformation", this.state.agenteInformation)
                passwordCheck.dialNumber(this.state.agenteInformation.phone)
            }
            else if (key === "Call_Customer") {

                passwordCheck.dialNumber(this.state.contactNumber)

            }
            else if (key === "telemed") {
                this.checkMedLifeapp(memberApps[0].androidDeeplink, memberApps[0].iosDeeplink)
            }

            else if (key === "memberPortal") {
                Linking.openURL(BaseUrl.portalUrl)
            } else if (key === "faq") {
                Linking.openURL(item.link)
            }
            else if (key === "healthshareVSinsurance") {
                Linking.openURL(item.link)
            }
            else if (key === "sharingGuideline") {
                Linking.openURL(item.link)
            }
            else if (key === "Email") {
                passwordCheck.openEmail(this.state.agenteInformation.email)
                console.log("this.state.agenteInformation", this.state.agenteInformation)

            } else if (key != "")
                // let isShare = isHealthyShare(this.client_id_locale);
                // console.log(isHealthyShare(this.client_id_locale))
                ChatNavigation(key.toLowerCase(), this.props, message, this.state.Connection, isHealthyShare(this.client_id_locale))
    }
    setContactandCardID(data) {
        data.planInfo.map((data, index) => {
            //   console.log("setContactandCardID", data.idcardField, data.fieldValue);

            if (data.idcardField == "contact number") {
                this.setState({ contactNumber: data.fieldValue });
            }
        })
    }
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: regular, color: COLORS.textColor, ...testFont,
    },
    textStyleColor: {
        fontSize: regular, color: 'red', ...testFont,
    },
    textStyleTitle: {
        fontSize: smallText, color: COLORS.subText, marginTop: hp('1%'), ...testFont,
    },
    readMore: {
        borderRadius: 10,
        flexDirection: 'row',
        alignSelf: 'baseline',
        alignItems: 'center',
        backgroundColor: COLORS.primaryColor,
        // width: wp('40%'),
        height:hp('7%'),
        padding: 5, marginTop: wp('3%')
    },

    optionItem: {
        borderRadius: 10,
        flexDirection: 'row', alignSelf: 'baseline', alignItems: 'center',
        backgroundColor: COLORS.intentColor, maxWidth: wp('50%'),
        padding: wp('3%'), marginVertical: wp('1%'), marginHorizontal: wp('3%')
    },
    arrowImg:{
        flex:1,
        width: wp('3%'),
        height:hp('3%'),
        marginHorizontal: 7,
        alignSelf: 'center',
        
    }

});

const mapDispatchToProps = dispatch => ({
    actionCreator: (url, method, previous) => dispatch(actionCreator(url, method, previous))
});
//data assign to props
const mapStateToProps = state => ({

    data: state.apiReducer.data,
    error: state.apiReducer.error,
    status: state.apiReducer.status,

});
//to content to redux
//curring,.apply ,.call
//HOC
export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);
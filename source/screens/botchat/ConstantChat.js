import AppStrings from "../../constants/AppStrings";

export const ChatInput = [
    {
        "query": "memberPortal",
        "options": [{ message: "Member Portal", intent: 'memberPortal' }],
    },
    {
        "query": "healthtool_card",
        "options": [{ message: "Show me my Health Tool Card", intent: 'Health Tool' }],
    },
    {
        "query": "call_agent",
        "options": [{ message: "Call Agent", intent: 'Call' }],
    },
    {
        "query": "CustomerServiceCall",
        "options": [{ message: "Call Customer Care", intent: 'Call_Customer' }],
    },
    {
        "query": "email_agent",
        "options": [{ message: "Email Agent", intent: 'Email' }],
    },
    {
        "query": "card",
        "options": [{ message: 'Show me my Membership ID', intent: 'Membership ID' },
        { message: "Show me my Health Tool Card", intent: 'Health Tool' },
        { message: "Show me my Payment Card", intent: 'Payment Card' }],
    },
    {
        "query": "IDcard",
        "options": [{ message: 'Show me my Membership ID', intent: 'Membership ID' },
        { message: "Show me my Health Tool Card", intent: 'Health Tool' },
        { message: "Show me my Payment Card", intent: 'Payment Card' }],
    },
    {
        "query": "digitalCard",
        "options": [{ message: 'Show me my Membership ID', intent: 'Membership ID' },
        { message: "Show me my Health Tool Card", intent: 'Health Tool' },
        { message: "Show me my Payment Card", intent: 'Payment Card' }],
    },
    {
        "query": "transactions",
        "options": [{ message: 'Show me my Transactions', intent: 'Transactions' }],
    }
    ,
    {
        "query": "find_provider",
        "options": [{ message: 'Find a provider', intent: 'find_provider' }],
    },
    {
        "query": "faq",
        "options": [{ message: 'Open FAQ', intent: 'faq', link: "https://www.universalhealthfellowship.org/FAQs/" }],
    },
    {
        "query": "guidelines_document",
        "options": [{ message: 'Show me my Documents', intent: 'documents' }],
    },

    {
        "query": "notifications",
        "options": [{ message: 'Show me my Notifications', intent: 'notifications' }],
    },
    {
        "query": "announcements",
        "options": [{ message: 'Show me my announcements', intent: 'announcements' }],
    },
    {
        "query": "telemed",
        "options": [{ message: 'Open Telemed', intent: 'telemed' }],
    },
    {
        "query": "payment_card",
        "options": [{ message: "Show me my Payment Card", intent: 'Payment Card' }],
    },
    {
        "query": "cancellation",
        "options": [{ message: "Email agent to cancel membership", intent: 'Email' }],
    },
    {
        "query": "cancellation",
        "options": [{ message: "Call agent to cancel membership", intent: 'Call' }],
    },
    {
        "query": "UHF",
        "options": [{ message: "Open Welcome Booklet", intent: 'UHF' }],
    },
    {
        "query": "healthshareVSinsurance",
        "options": [{ message: "Open Health Sharing document", intent: 'healthshareVSinsurance', link:"https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Infographics/UHS+-+5+Questions+about+Health+Sharing.pdf" }],
    },
    {
        "query": "refund",
        "options": [{ message: "Email agent to refund membership", intent: 'Email' },{ message: "Call agent to refund membership", intent: 'Call' }],
    },
    {
        "query": "payment_method",
        "options": [{ message: "Change Payment Method", intent: 'change payment' }],
    },
    {
        "query": "debit_card",
        "options": [{ message: "Change Payment Method", intent: 'change payment' }],
    },
    {
        "query": "credit_card",
        "options": [{ message: "Change Payment Method", intent: 'change payment' }],
    },
    {
        "query": "program_information",
        "options": [{ message: "Show me my Program Information", intent: 'program information' }],
    },
    {
        "query": "sharing_plan",
        "options": [{ message: "Show me my Program Information", intent: 'program information' }],
    },
    {
        "query": "NSA",
        "options": [{ message: "Show me my NSA", intent: 'program information' }],
    },
    {
        "query": "change_addons",
        "options": [{ message: AppStrings.chnage_addOns, intent: 'program information' }],
    },
    {
        "query": "health_questionary",
        "options": [{ message: "Show me my Health Questionary", intent: 'health questionary' }],
    },
    {
        "query": "change_dependants",
        "options": [{ message: AppStrings.chnage_dependants, intent: 'program information' }],
    },
    {
        "query": "contact_Information",
        "options": [{ message: AppStrings.ContactInformation, intent: 'ContactCardScreen' }],
    },
    {
        "query": "my_needs",
        "options": [{ message: "Show my needs", intent: 'needs' }],
    },
    {
        "query": "print_IDCard",
        "options": [{ message: 'Print my Membership ID', intent: 'Membership ID' },
        { message: "Print my Health Tool Card", intent: 'Health Tool' },

        { message: "Print my Payment Card", intent: 'Payment Card' }],

    },
    {
        "query": "Xray",
        "options": [{ message: "Open Sharing Guidelines", intent: 'sharingGuideline', link:"https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Important+Documents/UHS-Sharing-Program-Guidelines.pdf" }],
    },
    {
        "query": "surgery",
        "options": [{ message: "Open Sharing Guidelines", intent: 'sharingGuideline', link:"https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Important+Documents/UHS-Sharing-Program-Guidelines.pdf" }],
    },
    // ,
    // {
    //     "query": "print_IDCard",
    //     "options": [{ message: 'Print my Membership ID', intent: 'Membership ID' },
    //     { message: "Print my Health Tool Card", intent: 'Health Tool' },
    //     { message: "Print my Payment Card", intent: 'Payment Card' }],
    // }
]

export const splitMulti = (str, tokens) => {
    var tempChar = tokens[0]; // We can use the first token as a temporary join character
    for (var i = 1; i < tokens.length; i++) {
        str = str.split(tokens[i]).join(tempChar);
    }
    str = str.split(tempChar);
    return str;
}
export const splitMessage = (message) => {
    let list = splitMulti(message, ["AIKB - ", ",url - ", "typeofintent - ", ",query - "]);

    return list;
}
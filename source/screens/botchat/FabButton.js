
import { Image, TouchableOpacity, View } from 'react-native';
import React, { Component } from 'react';
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import COLORS from '../../constants/COLORS';

export const FabButton = props => {
    const { Connection, navigation, canGoBack, style } = props;

    let callbackReturn = () => {
        //props.FabClick();
        console.log("canGoBack----------------------", canGoBack)
        if (canGoBack) {
            navigation.goBack('ChatScreen');
        } else {
            navigation.navigate('ChatScreen', { Connection: Connection });
        }
    };

    return (

        <View style={{
            marginRight: wp('5%'),
            justifyContent: 'center',
            alignItems: 'center', alignSelf: 'flex-end',
            position: 'absolute', //Here is the trick
            marginBottom: style.margin, zIndex: 999, paddingRight: wp('5%'),
            bottom: 0,
        }}>
            <TouchableOpacity
                onPress={() => {
                    callbackReturn()
                }}
            >

                <Image
                    style={{
                        width: wp('13%'),
                        resizeMode: 'contain',
                        height: wp('13%'),
                    }}
                    source={require('../../assets/images/Floating_Button.png')}
                />


            </TouchableOpacity>
        </View>
        // <View></View>

    )

};
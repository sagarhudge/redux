import * as React from 'react';
import { View, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Dashboard from "../screens/Dashboard"
import MemberApps from "../screens/MemberApps"
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import COLORS from '../constants/COLORS';
const initialLayout = { width: Dimensions.get('window').width };

import { FontsApp, testFont } from '../constants/FontsApp'
let headingCardLarge = FontsApp.headingCardLarge;

export default function MainScreen(props) {
  //this is ge t called from Home.js pass navigation props
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'MEMBER SERVICES' },
    { key: 'second', title: 'MEMBER APPS' },
  ]);
  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={({ route }) => {
        switch (route.key) {
          case 'first':
            return <Dashboard navigation={props.navigation} Connection={props.Connection}
            />
          case 'second':
            return <MemberApps navigation={props.navigation} Connection={props.Connection} />
        }
      }}
      onIndexChange={setIndex}
    //  initialLayout={initialLayout}
     // style={styles.container}
      renderTabBar={props =>
        <TabBar
          {...props}
          indicatorStyle={{
            backgroundColor: '#ffc107',
            height: hp('0.5')
          }}
          style={{
            backgroundColor: COLORS.primaryColor,
            minHeight: hp("6%"),
          }}
          labelStyle={{ fontSize: headingCardLarge, ...testFont, color: COLORS.textColor, fontWeight: 'bold' }}
          activeColor={COLORS.white}
          inactiveColor={COLORS.backgroundColor}
        />
      }
    />
  );
}

const styles = StyleSheet.create({
  container: {
    // marginTop: 0
  },
  scene: {
    flex: 1,
  },
});


import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, SafeAreaView } from "react-native";

import styles from "../constants/StyleSheet";
import SendIntentAndroid from "react-native-send-intent";
import { isAndroid } from "../../source/constants/PlatformCheck";
import { Image } from "react-native-elements";
import Loader from "../components/Loader";
import { Card } from "react-native-elements";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLORS from "../constants/COLORS";
import { testFont, FontsApp } from "../constants/FontsApp";
import { memberApps } from "../constants/ArrayConst";
let subHeading = FontsApp.headingCardLarge;

 

export default class MemberApps extends Component {
  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;
    this.state = {
      data: [],
      loading: true,
      error: '',
    }

  }

  componentDidMount() {
    this.getData()
  }

  getData = () => {
    this.setState({
      data: memberApps,
      loading: false,
    });
  };

  renderItem({ item }) {

    return (
      <View style={{
        justifyContent: 'center', height: wp("40%"), width: wp("40%"), alignItems: 'center'
      }} >
        <TouchableOpacity
          onPress={() => {
            switch (item.id) {
              case 1002:
                this.checkMedLifeapp(item.androidDeeplink, item.iosDeeplink);
                break;
            }
          }}>

          <Image
            style={{ height: wp("18%"), width: wp("18%"), resizeMode: 'contain' }}
            source={require('../assets/images/telemed_icon_active_big.png')}
          />
          <Text style={{
            color: COLORS.textColor,
            marginTop: hp("2%"),
            textAlign: 'center',
            fontSize: subHeading, ...testFont,
          }}>{item.title}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  checkMedLifeapp = (link, ioslink) => {
    if (isAndroid) {
      SendIntentAndroid.isAppInstalled('com.mdlive.mobile').then((isInstalled) => {
        if (isInstalled) {
          SendIntentAndroid.openApp('com.mdlive.mobile').then((wasOpened) => {
          });
        }
        else {
          Linking.openURL(link).catch(err => {
          })
        }
      });
    }
    else {
      Linking.openURL(ioslink).catch(err => {
      })
    }
  }

  render() {
    return (

      <View style={{ backgroundColor: COLORS.background, flex: 1 }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          numColumns={2}
          data={this.state.data}
          renderItem={this.renderItem.bind(this)}
          keyExtractor={(item, index) => index.toString()}
        />
        <Loader loading={this.state.loading} />

      </View>

    );
  }
};

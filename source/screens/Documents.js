import React, { Component } from 'react';
import { SafeAreaView, FlatList, TouchableOpacity, Linking, Alert, Text, View, BackHandler, StatusBar, DeviceEventEmitter } from 'react-native';
import Loader from '../components/Loader';
import HomeFooter from '../screens/HomeFooter';
import { DocumentList } from '../constants/ArrayConst';
import COLORS from '../constants/COLORS';
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken } from '../constants/CommonServices'
import BaseUrl from "../constants/BaseUrl"
import AsyncStorage from '@react-native-community/async-storage';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { passwordCheck } from "../constants/utils"
import AppStrings from '../constants/AppStrings';
import { TexhnicalError } from '../components/TexhnicalError';
import { testFont, FontsApp, headerStyle, headerStyleContainer } from '../constants/FontsApp';
import { FabButton } from './botchat/FabButton';

export default class Documents extends Component {

  constructor(props) {
    super(props);

    this.navigation = this.props.navigation;
    this.email;
    this.accessToken;
    this.welcomekit = "";
    this.guidelines = "";
    this.state = {
      data: [],
      loading: false,
      error: '',
      contactNumber: '',
      enable: false,
    }
  }
  async componentDidMount() {
    // if (this.navigation) {
    //   this.navigation.setOptions({
    //     title: AppStrings.documents,
    //     headerStyle: headerStyleContainer,
    //     headerTintColor: COLORS.white,
    //     headerTitleStyle: headerStyle,
    //   });
    // }
    passwordCheck.applyHeader(this.navigation, AppStrings.documents)


    this.email = await AsyncStorage.getItem('email');
    this.accesstoken = await AsyncStorage.getItem('accesstoken');

    //api cal to get open booklet
    this.setState({ loading: true });
    this.getWelcomeBooklet("welcomekit");
  }
  getWelcomeBooklet(type) {

    let verifyUrl = BaseUrl.FABRIC_V1 + "memberportal/planinfo";
    let request = { "email": this.email, "type": type }

    postAxiosWithToken(verifyUrl, request, this.accesstoken).then(response => {

      if (response && response.data && response.data.length != 0) {
        var value = response.data[0].fieldValue;

        if (type === "welcomekit") {
          this.welcomekit = value;
          this.getWelcomeBooklet("guidelines");
        } else {
          this.guidelines = value;
          this.setState({ loading: false });
          this.getData();
        }

      } else {
        this.getData();
        this.setState({ loading: false });
        var message = response.message;
        let include = passwordCheck.getSessionStatus(message)
        DeviceEventEmitter.emit('sessionOut', include)
      }

    });


  }
  getData = () => {
    this.setState({
      data: DocumentList,
      loading: false,
    });
  };

  render() {

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.cardBackground }}>
        <StatusBar barStyle="light-content" backgroundColor='#8461AB' />


        <FlatList
          showsVerticalScrollIndicator={true}
          numColumns={1}
          data={this.state.data}
          renderItem={this.renderItem.bind(this)}
          keyExtractor={(item, index) => index.toString()}
        />
        <FabButton
          style={{ margin: hp('8%') }}

          canGoBack={this.state.canGoBack}
          navigation={this.props.navigation}
        />

        <Loader loading={this.state.loading} />
        <HomeFooter from="doc" isDashboard={false} callbackFooter={(from, isHome) => {
          if (from != "Documents") {
            this.props.navigation.goBack(null);
          }
          if (from === AppStrings.MyNeedsScreen) {
            this.props.navigation.navigate(AppStrings.MyNeedsScreen);
          }
        }} />
      </SafeAreaView>
    );
  }

  renderItem({ item }) {
    return (
      <View  >
        <TouchableOpacity
          onPress={() => {
            this.checkMedLifeapp(item.title, item.url);
          }}>
          <Text style={{ color: COLORS.textColor, padding: wp('2.5%'), fontSize: FontsApp.subHeading, ...testFont }} >{item.title}</Text>
        </TouchableOpacity>

      </View>
    )
  }

  checkMedLifeapp = (title, link) => {
    console.log(title)
    if (title == "Welcome Booklet") {
      link = this.welcomekit;
    }
    if (title == "Sharing Guidelines") {
      link = this.guidelines;
    }
    if (link && link != "") {
      Linking.openURL(link).catch(err => {

      });
    } else {
      //show error here
    }
  };
}
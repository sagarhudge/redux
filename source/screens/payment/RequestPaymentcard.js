import React, { Component } from "react";
import { Keyboard, ScrollView, Alert, Text, StyleSheet, View, FlatList, Image, KeyboardAvoidingView } from "react-native";
import { getGateWayToken, getAxiosUser, getAxiosWithToken, postAxiosWithToken, getData } from '../../constants/CommonServices'

import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import BaseUrl from '../../constants/BaseUrl'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from "../../constants/AppStrings";
import HomeFooter from '../HomeFooter';
import { SafeAreaView } from "react-native";
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
import AwesomeAlertDialog from '../../components/AwesomeAlertDialog';
import TextInput from "react-native-material-textinput";
import { Picker, Icon } from "native-base";
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment";
import stylesCommon from "../../constants/StyleSheet";
import { getAccessToken, getEmail } from "../../constants/GetAsyncValues";

let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let headingCardLarge = FontsApp.headingCardLarge
let inputTextHeight = FontsApp.inputTextHeight;
let lineHeight = hp("0.2%");
import { ButtonBorder } from "../../components/Buttons";
import { passwordCheck } from "../../constants/utils";

export default class RequestPaymentcard extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.accessToken = '';
        this.accessTokenNew = '';
        this.email = '';

        this.state = {
            loading: false,
            purspose: '',
            selectedValue: '',
            provider: '',
            error: '',
            show: false,
            errorId: 0,
            message: '',
            title: "",
            alertCancel: AppStrings.cancel,
            alertConfirm: 'OK',
            alertCLose: AppStrings.cancel,
            canBeClosed: false,
            alertType: 'ok',
            todayDateValid: '',
            name: this.props.route.params.name,
            date: new Date(),
            dateSelected: '',
            showDate: false,

        }

    }

    componentDidMount() {
        // if (this.navigation) {
        //     this.navigation.setOptions({
        //         title: AppStrings.RequestPaymentCard, //Set Header Title

        //         headerStyle: headerStyleContainer,
        //         headerTintColor: COLORS.white,
        //         headerTitleStyle: headerStyle,
        //     });
        // }
        passwordCheck.applyHeader(this.navigation, AppStrings.RequestPaymentCard)

        this.getAsync();

    }

    async getAsync() {
        getEmail().then(mail => {
            this.email = mail;
        });
        getAccessToken().then(token => {
            this.accessToken = token;
        })

    }

    requestCard() {

        if (this.state.selectedValue == "") {
            this.setState({ error: 'Member name required', errorId: 1 })

        } else if (this.state.purspose == "") {
            this.setState({ error: 'Purpose of visit required', errorId: 2 })

        } else if (this.state.dateSelected == "") {
            this.setState({ error: 'Preferred date required', errorId: 3 })

        } else {
            this.setState({ loading: true })

            let object = {
                "Subject": "CaseAPISIT",
                "Origin": "External",
                "External_Application_Name__c": "Member Portal",
                "Status": "New",
                "Type": "Payment Card Request",
                "SuppliedEmail": this.email,
                "Description": `member_name:${this.state.selectedValue},purpose:${this.state.purspose},
                provider_name:${this.state.provider},request_date:${this.state.dateSelected}`,
            }
            let url = BaseUrl.FABRIC_V1 + "memberportal/caseCreation";
            // let url = "https://carynhealth--sit.my.salesforce.com/services/data/v45.0/sobjects/Case";
            console.log("caseCreation url-----", url, "\n", object)
            console.log("this.accessToken-----", this.accessToken)

            postAxiosWithToken(url, object, this.accessToken).then(response => {
                this.setState({ loading: false })
                if (response && response.data) {
                    console.log("card request-----", response.data)
                    if (response.data.success == true) {
                        this.setState({ error: "" })
                        this.props.navigation.goBack();
                    } else {
                        this.setState({ error: "Request card failed" })
                    }

                } else {
                    this.setState({ error: response.message })
                }
            })

        }

    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
                <View style={{ backgroundColor: COLORS.white, flex: 1, }}>
                    <ScrollView  >
                        <View style={{ padding: wp('6%'), flex: 1, }}>
                            <Text style={{ color: COLORS.textColor, ...testFont, fontSize: regular, paddingBottom: wp('3%') }}>
                                We can ease your next care visit by scheduling the appointment and help save money by paying your
                                healthcare provider at the time of your scheduled appointment.
                                Fill in the purpose of your visit and if you have a preferred provider you would like to visit.
                            </Text>

                            <TextInput
                                marginTop={15}
                                labelActiveTop={-30}
                                value={this.state.selectedValue}
                                fontSize={inputTextHeight}
                                label='Member *'
                                paddingLeft={10}
                                error={this.state.errorId == 1 ? this.state.error : ""}
                                labelActiveColor={COLORS.primaryColor}
                                underlineColor={COLORS.primaryColor}
                                underlineActiveColor={COLORS.primaryColor}
                                secureTextEntry={false}
                                underlineHeight={lineHeight}
                                onChangeText={this.onChangeMember}
                            />


                            <TextInput
                                marginTop={15}
                                labelActiveTop={-30}
                                value={this.state.purspose}
                                fontSize={inputTextHeight}
                                label='Purpose of Visit*'
                                paddingLeft={10}
                                error={this.state.errorId == 2 ? this.state.error : ""}
                                labelActiveColor={COLORS.primaryColor}
                                underlineColor={COLORS.primaryColor}
                                underlineActiveColor={COLORS.primaryColor}
                                secureTextEntry={false}
                                underlineHeight={lineHeight}
                                onChangeText={this.onChangePurpose}
                            />
                            <View style={{ justifyContent: 'center' }}>
                                <TextInput
                                    marginTop={15}
                                    labelActiveTop={-30}
                                    value={this.state.dateSelected}
                                    fontSize={inputTextHeight}
                                    label='Preferred Date*'
                                    paddingLeft={10}
                                    error={this.state.errorId == 3 ? this.state.error : ""}
                                    labelActiveColor={COLORS.primaryColor}
                                    underlineColor={COLORS.primaryColor}
                                    underlineActiveColor={COLORS.primaryColor}
                                    secureTextEntry={false}
                                    onFocus={() => {
                                        this.setState({ showDate: true }); Keyboard.dismiss();
                                    }}
                                    underlineHeight={lineHeight}
                                    onChangeText={this.onChangeDate}
                                />
                                <Image
                                    style={{
                                        position: 'absolute', end: 0, marginRight: wp('3%'), bottom: 0, marginBottom: wp('5%'),
                                        height: wp("6%"), width: wp("6%"),
                                    }}
                                    resizeMode='contain'
                                    source={require('../../assets/images/event.png')}
                                />
                            </View>
                            {
                                this.state.showDate ? <DateTimePicker
                                    testID="dateTimePicker"
                                    value={this.state.date}
                                    mode={'datetime'}
                                    is24Hour={false}
                                    display="default"
                                    onChange={this.handleDateChange}
                                /> : null
                            }
                            <TextInput
                                marginTop={15}
                                labelActiveTop={-30}
                                value={this.state.provider}
                                fontSize={inputTextHeight}
                                label='Preferred Healthcare Provider'
                                paddingLeft={10}
                                labelActiveColor={COLORS.primaryColor}
                                underlineColor={COLORS.primaryColor}
                                underlineActiveColor={COLORS.primaryColor}
                                secureTextEntry={false}
                                underlineHeight={lineHeight}
                                onChangeText={this.onChangeProvider}
                            />
                        </View>
                    </ScrollView>
                    <View style={{
                        flexDirection: 'row', backgroundColor: COLORS.blueL, width: wp('100%'),
                        marginBottom: hp('7%'), position: 'absolute', bottom: 0,
                    }}>


                        <ButtonBorder title={"CANCEL"}
                            isDisabled={true} type="blue"
                            callback={() =>
                                this.props.navigation.goBack()
                            } />

                        <ButtonBorder title={"DONE"}
                            isDisabled={true} type="blue"
                            callback={() =>
                                this.requestCard()
                            } />
                    </View>
                    <HomeFooter from="change_payment" isDashboard={false} isOtherScreen={true} callbackFooter={(from, isHome) => {
                        if (from === "Dashboard") {
                            this.props.navigation.goBack(null);
                        } else {
                            this.props.navigation.replace('Documents');
                        }
                    }} />

                    <AwesomeAlertDialog
                        callbackClick={this.callbackClick.bind(this)}
                        show={this.state.show}
                        title={this.state.title}
                        message={this.state.message}
                        alertCancel={this.state.alertCancel}
                        alertConfirm={this.state.alertConfirm}
                        canBeClosed={this.state.canBeClosed}
                        okHide={true}
                        type={this.state.alertType} />

                    <Loader loading={this.state.loading} />
                </View>

            </SafeAreaView >
        )
    }
    onChangePurpose = (purspose) => {
        if (purspose !== "") {
            this.setState({ purspose: purspose, error: '', errorId: 0 }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ purspose: "", error: 'Purpose of visit required', errorId: 1 }, () => {
                this.callBackSaveCard();
            });
        }
    }
    callBackSaveCard(){
        
    }
    onChangeDate = (p_date) => {
        if (p_date !== "") {
            this.setState({ dateSelected: p_date, error: '', errorId: 0 }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ dateSelected: "", error: 'Date required', errorId: 2 }, () => {
                this.callBackSaveCard();
            });
        }
    }
    onChangeMember=(name) =>{
        if (name !== "") {
            this.setState({ selectedValue: name, error: '', errorId: 0 }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ selectedValue: "", error: 'Member name required', errorId: 1 }, () => {
                this.callBackSaveCard();
            });
        }

    }
    callbackClick() {
        this.setState({ show: false })
    }

    handleDateChange = (date) => {
        Keyboard.dismiss();
        console.log("handleDateChange---------------", date, date.nativeEvent.type)
        if (date.type != "dismissed") {
            const valDate = new Date(date.nativeEvent.timestamp);
            // this.state.date = valDate;
            this.setState({
                showDate: false, dateSelected: moment(valDate).format('YYYY-MM-DD'),
                date: valDate, errorId: 0
            }, () => {
                console.log("handleDateChange", valDate)
            })
        } else {
            this.setState({
                showDate: false, errorId: 0
            }, () => {

            })
        }

    }

    onChangeProvider = (val) => {
        if (val != "") {
            this.setState({ provider: val, error: '', errorId: 0 }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ provider: "", error: ' ', errorId: 0 }, () => {
                this.callBackSaveCard();
            });
        }

    }
}
const styles = StyleSheet.create({

    textStylePicker: { fontSize: inputTextHeight, ...testFont, color: COLORS.textColor },
});

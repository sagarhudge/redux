
import React, { Component } from "react";
import { Linking, PermissionsAndroid, Alert, Text, TouchableOpacity, View, SafeAreaView, Image, ImageBackground, StyleSheet } from "react-native";

import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import { captureRef } from "react-native-view-shot";
import CameraRoll from "@react-native-community/cameraroll";
import Share from "react-native-share";
import CardFlip from "react-native-card-flip";

import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { isAndroid } from "../../constants/PlatformCheck";
import AwesomeAlertDialog from "../../components/AwesomeAlertDialog";
import AppStrings from "../../constants/AppStrings";
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
import IconE from 'react-native-vector-icons/Entypo';
let heading = FontsApp.headingCard;
let subHeading = FontsApp.subHeadingCard
let regular = FontsApp.regularCard
let headingCardLarge = FontsApp.headingCardLarge
let imageBackground = hp('32%');
let cardHeight = hp('32%');
let cardWidth = wp('95%');
let imageLogo = wp('40%');
import * as RNFS from 'react-native-fs';
import { getChangePath } from "../../constants/DownloadFile";
import { passwordCheck } from "../../constants/utils";

//HorizontalView
let imageBackgroundH = wp('60%');
let cardHeightH = hp('32%');
let cardWidthH = hp('80%');
export default class PaymentCard extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.ref = React.createRef();
        this.refF = React.createRef();
        this.count = 0;
        this.state = {
            data: this.props.route.params.data,
            loading: false,
            error: '',
            previewSource: null,
            res: null,
            name: '',
            network: '',
            contactNumber: '',
            enrollmentDate: '',
            plainId: null,
            showEmpId: false,
            visibleTap: true,
            groupNo: '',
            cardId: '',
            prefix: '',
            empId: '',
            memberId: '',
            planInfo: [],
            show: false,
            message: '',
            filepath: '',
            flip: 1,
            idTitle: 'Front of ID Card',
            value: {
                format: "jpg",
                quality: 0.9,
            },
            rotate: true,
        }
    }


    componentDidMount() {
        this.getCardData();
        if (this.navigation) {
            this.navigation.setOptions({
                title: AppStrings.PaymentCard,
                headerRight: () => (
                    <View style={{ flexDirection: 'row' }}>
                        {
                            isAndroid ? <TouchableOpacity style={{ marginRight: wp('6%') }}
                                onPress={() => {
                                    this.downloadFilePermission();
                                }}>
                                <Icon name="download" size={wp('6%')} color={COLORS.white} />
                            </TouchableOpacity> : null
                        }
                        <TouchableOpacity style={{ marginRight: wp('6%') }}
                            onPress={() => {
                                // this.setState({ loading: true });
                                this.snapshot('share')
                            }}>
                            <Icon name="share-alt" size={wp('6%')} color={COLORS.white} />
                        </TouchableOpacity>
                    </View>
                ),
                headerStyle: headerStyleContainer,
                headerTintColor: COLORS.white,
                headerTitleStyle: headerStyle
            });
        }

    }
    async getCardData() {
        this.provider_network = await AsyncStorage.getItem("provider_network");
    }
    render() {
        return (

            <SafeAreaView style={{
                backgroundColor: COLORS.graylight, flex: 1, flexDirection: 'column',
                alignItems: 'center', justifyContent: 'center'
            }}>

                <View
                    collapsable={false} style={{
                        transform: [{ rotate: !this.state.rotate ? '90deg' : '0deg' }],
                        marginRight: !this.state.rotate ? wp('-21%') : 0,
                        marginTop: this.state.rotate ? hp('-15%') : 0
                    }}
                    ref={(ref) => this.ref = ref}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ rotate: !this.state.rotate })
                            }}>
                            <IconE name={this.state.rotate ? "circle-with-plus" : "circle-with-cross"} size={wp('8%')} color={COLORS.blueL} />
                        </TouchableOpacity>
                    </View>
                    <CardFlip style={this.state.rotate ? styles.mainCardContainer : styles.mainCardContainerRotate} ref={(card) => (this.card = card)}>

                        <TouchableOpacity
                            activeOpacity={1}
                            ref={(ref) => (this.refF = ref)}
                            style={[styles.touchableCard, styles.touchableCardSecond]}>

                            <View style={{
                                paddingHorizontal: wp('3.5%'),
                                borderRadius: 10,
                                paddingVertical: wp('3%'),
                                backgroundColor: 'white'
                            }}>

                                <ImageBackground
                                    resizeMode={'stretch'}
                                    imageStyle={{ borderTopLeftRadius: 2 }}
                                    style={this.state.rotate ? styles.imageBackground : styles.imageBackgroundRotate}
                                    source={require('../../assets/images/group_51.png')} />

                                <Image
                                    resizeMode={'cover'}
                                    style={styles.imagesLogo}
                                    source={require('../../assets/images/UHS.png')} />

                                <Text style={{
                                    ...testFont, color: '#98335b',
                                    justifyContent: 'center',
                                    fontSize: hp('3.5%'),
                                    fontWeight: 'bold',
                                    marginTop: hp('2.3%')
                                }}>{this.spacify(this.state.data.panNumber, 4, " ")}</Text>

                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start', marginTop: hp('3.5%')
                                }}>
                                    <View style={{ flexDirection: 'row', marginRight: wp('4%') }}>
                                        <Text style={styles.text}>CVV </Text>
                                        <Text style={styles.text_second}>{this.state.data.cvv}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', marginRight: wp('4%') }}>
                                        <Text style={styles.text}>EXPIRY </Text>
                                        <Text style={styles.text_second}>{this.state.data.expirationDate}</Text>

                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.text}>AMOUNT </Text>
                                        <Text style={styles.text_second}>{"$" + this.state.data.amountAuthorized} </Text>
                                    </View>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    marginTop: hp('3.5%')
                                }}>
                                    <Text style={{
                                        ...testFont, color: COLORS.textColor,
                                        justifyContent: 'center',
                                        fontSize: hp('3%'), fontWeight: 'bold',
                                    }}>{this.state.data.memberFirstName + " " + this.state.data.memberLastName}</Text>
                                    <Image
                                        resizeMode={'contain'}
                                        style={styles.imagesLogoMastercard}
                                        source={require('../../assets/images/mc_symbol.png')} />
                                </View>
                            </View>
                            <View style={{
                                backgroundColor: "#333333",
                                position: 'relative',
                                zIndex: -1, marginTop: hp('-2%'), paddingTop: hp('1%')
                            }}>
                                <View style={{ flexDirection: 'row', marginHorizontal: hp('3%'), marginTop: hp('3%'), marginBottom: hp('2%'), justifyContent: 'space-between' }}>
                                    <View style={{ flex: 1, marginRight: wp('1%') }} >
                                        <Text style={styles.headingDetails}>Member</Text>
                                        <Text style={styles.headingText}>{this.state.data.memberFirstName + " " + this.state.data.memberLastName}</Text>
                                    </View>
                                    <View style={{ flex: 1 }} >
                                        <Text style={styles.headingDetails}>Provider</Text>
                                        <Text style={styles.headingText}>{this.state.data.providerName}</Text>
                                    </View>
                                    <View style={{ flex: 1.8, marginLeft: wp('1%') }} >
                                        <Text style={styles.headingDetails}>Procedure Information</Text>
                                        <Text style={styles.headingText}>{this.state.data.procedureInformation}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={[styles.touchableCardBack, styles.touchableCardSecond]}  >

                            <View style={{ backgroundColor: COLORS.white }}>
                            </View>
                        </TouchableOpacity>
                    </CardFlip>
                </View>

                <Loader loading={this.state.loading} />
                {this.showAlert()}
            </SafeAreaView>

        );
    }
    spacify = (str, after, c) => {
        if (!str) {
            return false;
        }
        after = after || 4;
        c = c || " ";
        var v = str.replace(/[^\dA-Z]/g, ''),
            reg = new RegExp(".{" + after + "}", "g");
        return v.replace(reg, function (a) {
            return a + c;
        }).replace(/[^0-9]+$/, "");
    }
    shareSnap(uri, action) {
        if (action === 'share') {
            let f_uri = getChangePath(uri, "Payment_Card");
            console.log("f_uri", f_uri)
            RNFS.copyFile(uri, f_uri ).then(() => {
                const shareOptions = {
                    title: 'Sharevia',
                    message: 'Payment_Card',
                    url: f_uri,
                };
                this.setState({ loading: false, res: null, visibleTap: true });
                Share.open(shareOptions).then((Promise) => { console.log("Promise--------------" + Promise) }).catch(e => {
                    console.log("Exception--------------", e.message)
                }).catch(e => { console.log("---share--", e.message) });
            })


        } else if (action === 'download') {
            let f_uri = getChangePath(uri, "Payment_Card");
            console.log("f_uri", f_uri)
            RNFS.copyFile(uri, f_uri ).then(()=>{
                CameraRoll.save(f_uri)
                .then((savedTo) => {
                    this.setState({ filepath:savedTo,visibleTap: true, show: true, loading: false, message: "Your payment card has been downloaded." })

                }).catch(err => console.log('err:', err))
            })
           
        } else if (action === 'pdf') {
            //  Alert.alert("pdf save successfully");
        }

    }

    snapshot(action) {
        this.setState({ visibleTap: false }, () => {
            captureRef(this.refF, this.state.value)
                .then(res =>
                    this.state.value.result !== "file" ? res
                        : new Promise((success, failure) => Image.getSize(res, (width, height) => (console.log(res, width, height), success(res)), failure)))
                .then(res => {

                    this.shareSnap(res, action);
                }).catch((error) => {
                    (this.setState({ error: '', res: null, previewSource: null }))
                });
        })

    }

    callbackClick(visible, title, button) {
        this.setState({ show: false, message: "" })
        if (this.state.filepath != "") {
            passwordCheck.showLocaleFile(this.state.filepath).then(result => {
                console.log(result)
            })
        }
    }
    showAlert() {
        return <AwesomeAlertDialog
            callbackClick={this.callbackClick.bind(this)}
            show={this.state.show}
            title={""}
            message={this.state.message}
            alertCancel={AppStrings.cancel}
            alertConfirm={AppStrings.ok}
            canBeClosed={false}
            type={""} />
    }

    downloadFilePermission = async () => {

        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        console.log("granted----------------", granted);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            this.setState({ loading: true });

            this.snapshot('download')
        } else {
            Alert.alert('Storage Permission Denied');
        }


    }

};

const styles = StyleSheet.create({

    imageBackground: {
        minHeight: imageBackground,
        aspectRatio: 1, top: 0, bottom: 0, position: 'absolute',
        opacity: 0.5
    },
    imagesLogo: {
        width: imageLogo,
        height: wp('14%')
    },
    imagesLogoMastercard: {
        width: wp('20%'),
        height: wp('10%')
    },
    images: {
        width: wp('34%'),
        height: wp('13%'),

    },
    mainCardContainer: {
        width: cardWidth,
        minHeight: cardHeight

    },
    touchableCard: {
        minHeight: cardHeight
    },
    touchableCardBack: {
        minHeight: cardHeight,
        padding: '4%',
    },
    // landscape
    imageBackgroundRotate: {
        minHeight: imageBackgroundH,
        aspectRatio: 1, top: 0, bottom: 0, position: 'absolute', opacity: 0.5
    },
    imagesLogoRotate: {
        width: imageLogo,
        height: wp('13%'),
    },
    imagesRotate: {
        width: wp('34%'),
        height: wp('13%'),
    },
    mainCardContainerRotate: {
        width: cardWidthH,
        minHeight: cardHeightH,
    },
    touchableCardRotate: {
        minHeight: cardHeightH,
    },
    touchableCardBackRotate: {
        minHeight: cardHeightH,
        padding: '4%',
    },
    //////////---END----//////////
    rowViewMarginTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: hp('0.5%'),
        alignSelf: 'baseline'
    },
    headingDetails: {
        ...testFont, color: '#bdbdbd',
        justifyContent: 'center',
        fontSize: subHeading,
    },
    headingText: {
        ...testFont, color: COLORS.white, marginTop: hp('0.5%'),
        justifyContent: 'center',
        fontSize: subHeading,
    },
    headingDataFirst: {
        flex: 0.4,
        fontSize: subHeading, ...testFont,
        fontWeight: 'bold',
        color: COLORS.headingColor
    },
    headingDataSecond: {
        flex: 0.6,
        fontSize: subHeading, ...testFont,
        fontWeight: 'bold',
        color: COLORS.headingColor
    },
    headingDataLeftMargin: {
        marginLeft: wp('1.2%'),
        flex: 1,
        fontSize: subHeading, ...testFont,
        alignSelf: 'center',
        fontWeight: 'bold',
        color: COLORS.headingColor2
    },
    touchableCardSecond: {
    },
    backImage: {
        height: hp('4%'),
        width: wp('15%'),
    },
    backPurpleBold: {
        fontSize: regular, ...testFont,
        color: COLORS.primaryColor,
        marginTop: hp('1%'),
        fontWeight: 'bold'
    },
    backRegularText: {
        fontSize: regular, ...testFont,
        color: COLORS.textColor,
        marginTop: hp('1%')
    },
    backRegularTextBold: {
        fontSize: regular, ...testFont,
        color: COLORS.textColor,
        marginTop: hp('2%'),
        fontWeight: 'bold'
    },
    backFooter: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center'
    },
    frontFooter: {
        fontSize: subHeading, ...testFont,
        fontWeight: '400',
        color: COLORS.textColor,
        marginBottom: hp('3%'),
        marginHorizontal: wp('3%'),
        textAlign: 'center'
    },
    text: {
        ...testFont, color: COLORS.textColor,
        justifyContent: 'center', fontSize: headingCardLarge, fontWeight: 'bold',
    },
    text_second: {
        ...testFont, color: "#98335b",
        justifyContent: 'center', fontSize: headingCardLarge, fontWeight: 'bold',
    },
    backHeader: { fontSize: regular, ...testFont, color: COLORS.primaryColor, fontWeight: 'bold' }
});
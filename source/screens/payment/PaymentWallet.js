
import React, { Component } from "react";
import { Linking, Image, Alert, Text, TouchableOpacity, View, FlatList, SafeAreaView } from "react-native";

import styles from "../../constants/StyleSheet";
import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import BaseUrl from "../../constants/BaseUrl";
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken, getAxiosUser } from '../../constants/CommonServices'
import moment from "moment";
import { passwordCheck } from "../../constants/utils";
import AppStrings from "../../constants/AppStrings";
import AwesomeAlert from 'react-native-awesome-alerts';
import HomeFooter from '../HomeFooter';
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
import { DeviceEventEmitter } from "react-native";
let heading = FontsApp.heading;
let headingCardStatus = FontsApp.headingCardStatus
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let errorMessage = "There are no payment cards approved for your account. Submitted payment card requests, if any, may take 2-3 days to be processed. To learn more about payment cards and request for one, call Customer Service.";

import { ButtonBorder } from "../../components/Buttons";
import AwesomeAlertDialog from "../../components/AwesomeAlertDialog";
import { FabButton } from "../botchat/FabButton";

export default class PaymentWallet extends Component {
    constructor(props) {
        super(props);

        this.navigation = this.props.navigation;
        // console.log(this.props.route.params)
        this.source;
        this.accesstoken = '';
        this.offset = 0;
        this.previosPage = 0;
        this.nextPage = 1;
        this.email = '';
        this.name = '';
        this.ContactNumber = '';

        this.state = {
            data: [],
            optionSelected: this.props.route.params,
            loading: false,
            error: ' ',
            loadMore: false,
            transactionId: '',
            accountnumber: '',
            show: false,
            confirmShow: true,
            cancelShow: false,
            title: '',
            alertTitle: '',
            alertMessage: '',
            alertFrom: '',

            prefix: '',
            empId: '',
            memberId: '',
        }

    }

    componentDidMount() {
        passwordCheck.applyHeader(this.navigation, AppStrings.paymentWallet)
        this.getAsysnc();
        if (this.state.optionSelected && this.state.optionSelected.optionSelected) {
            // console.log(this.state.optionSelected.optionSelected)
            this.state.optionSelected.optionSelected == "Request Card" && this.requestPaymentCard();
        }
    }

    getTransactionsNEO() {
        this.setState({ loading: true });

        let url = BaseUrl.FABRIC_V1 + "memberportal/paymentCard";

        let obj = {
            "memberNumber": this.state.memberId  //CNEO675423567 //CNEO0001590028
        }
        console.log("url-------------", this.source, url, obj, this.accesstoken);

        postAxiosWithToken(url, obj, this.accesstoken).then(response => {

            if (response && response.data) {
                console.log("response", response.data);

                //  if (response.data.pageList.length > 0) {
                //  this.previosPage = this.offset;
                this.setState({
                    data: [...this.state.data, ...response.data],
                    loading: false, error: ''
                }, () => {
                    // this.offset = this.offset + 1;
                    // let totalCount = response.data.totalrecords
                    // this.setState({ loadMore: totalCount > 10 && totalCount != this.state.data });
                    this.setState({ loadMore: false, loading: false });
                });
                // } else {
                //     this.setState({ loading: false, error: this.state.data.length > 0 ? '' : 'Payment Data Not Available' }, () => {
                //         this.setState({ loadMore: false })
                //     });
                // }

            } else {
                this.nextPage = 0;
                let message = passwordCheck.responseError(response);
                this.setState({ loading: false, error: message == "Error" ? errorMessage : message }, () => { });
            }
        }).catch(e => {
            this.setState({ loading: false, error: e.message }, () => { });
        })
    }
    getTransactions() {
        this.setState({ loading: true });

        let url = BaseUrl.FABRIC_V1 + "memberportal/paymentCard";

        let obj = {
            "memberNumber": this.state.prefix + "" + this.state.empId
        }

        console.log("---url---", this.source, url, obj, this.accesstoken);

        postAxiosWithToken(url, obj, this.accesstoken).then(response => {

            if (response && response.data) {
                console.log("response", response.data);

                // if (response.data.pageList.length > 0) {
                //this.previosPage = this.offset;

                this.setState({ data: [...this.state.data, ...response.data], loading: false, error: '' }, () => {
                    //this.offset = this.offset + 1;
                    // let totalCount = response.data.totalrecords
                    //this.setState({ loadMore: totalCount > 10 && totalCount != this.state.data })
                    this.setState({ loadMore: false, loading: false })

                });

                // } else {
                //     this.setState({ loading: false, error: this.state.data.length > 0 ? '' : 'Payment Data Not Available' }, () => {
                //         this.setState({ loadMore: false })
                //     });
                // }

            } else {
                this.nextPage = 0;
                let message = passwordCheck.responseError(response);
                this.setState({ loading: false, error: message == "Error" ? errorMessage : message }, () => { });

                var mesg = response.message;
                let include = passwordCheck.getSessionStatus(mesg)
                DeviceEventEmitter.emit('sessionOut', include)
            }

        }).catch(e => {
            this.setState({ loading: false, error: e.message }, () => { });
        })
    }

    async getAsysnc() {

        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.source = await AsyncStorage.getItem("source");
        this.email = await AsyncStorage.getItem("email");
        this.name = await AsyncStorage.getItem("username");
        this.ContactNumber = await AsyncStorage.getItem('ContactNumber');

        this.idcardData = JSON.parse(await AsyncStorage.getItem("GetIdCardData"));
        let Data = this.idcardData[0];

        this.setState({
            empId: Data.empId,
            memberId: Data.memberId,
        });

        this.setContactandCardID(Data);

        if (this.source == "NEO") {
            this.getTransactionsNEO();
        } else {
            this.getTransactions();
        }

    }
    setContactandCardID(data) {
        data.planInfo.map((data, index) => {
            if (data.idcardField == "prefix") {
                this.setState({ prefix: data.fieldValue }, () => {
                })
            }
        });

    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>

                <View style={{ backgroundColor: COLORS.white, flex: 1 }}>

                    {
                        this.state.error
                            ?
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: hp("2%") }}>
                                <Text style={{ alignSelf: 'center', fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.error}</Text>
                            </View>
                            :
                            <View >
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    numColumns={1}
                                    data={this.state.data}
                                    renderItem={this.renderItem.bind(this)}
                                    style={{ marginBottom: hp('14%') }}
                                    onEndReachedThreshold={1}
                                    ListFooterComponent={
                                        this.state.loadMore ? <View style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-around'
                                        }}>
                                            {
                                                <Text
                                                    onPress={() =>
                                                        this.goToNext()
                                                    }
                                                    style={styles.footerActive}>
                                                    {`Load More`}</Text>
                                            }
                                        </View> : null}

                                    keyExtractor={(item, index) => index.toString()}
                                />

                            </View>
                    }
                    <View style={{ marginBottom: hp('7%'), position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.blueL, width: '100%', }}>

                        <ButtonBorder
                            title={"REQUEST PAYMENT CARD"}
                            isDisabled={true}
                            type="blue"
                            callback={() => {
                                this.requestPaymentCard();
                                // this.props.navigation.navigate(AppStrings.RequestPaymentCardNav, { name: this.name })
                            }}
                        />


                    </View>

                    <HomeFooter from="transaction" isDashboard={false} isOtherScreen={true} callbackFooter={(from, isHome) => {
                        if (from === AppStrings.dashboard) {
                            this.props.navigation.goBack(null);
                        }
                        if (from === AppStrings.documents) {
                            this.props.navigation.navigate(AppStrings.documents);
                        }

                        if (from === AppStrings.MyNeedsScreen) {
                            this.props.navigation.navigate(AppStrings.MyNeedsScreen);
                        }

                    }} />
                    {this.displayAlert()}
                    <Loader loading={this.state.loading} />
                    <FabButton
                        style={{ margin: hp('16%') }}

                        canGoBack={this.state.canGoBack}
                        navigation={this.props.navigation}
                    />
                </View>
            </SafeAreaView>

        );
    }
    callbackClick(visible, title, button) {
        this.setState({ show: false }, () => { });
    }

    requestPaymentCard() {
        let msg = `We can ease your next visit, by providing you with a payment card that you can use to pay your healthcare provider at the time of your scheduled appointment. To request a payment card call Member Services at ${this.ContactNumber}, Monday to Friday 8am to 8pm CST.`
        this.setState({ show: true, alertMessage: msg, alertTitle: 'Request a New Payment Card', confirmShow: false, cancelShow: true });
    }
    goToPrevios() {
        if (this.previosPage > 0) {
            this.offset = this.previosPage - 1;
            this.getTransactions();
        } else {
            this.offset = 0;
        }

    }
    goToNext() {
        this.getTransactions();
    }
    renderItem({ item }) {

        return (
            <TouchableOpacity
                style={{
                    backgroundColor: item.status === 'sent' ? '#decdf1' : COLORS.white,
                    flexDirection: 'row', flex: 1, alignItems: 'center',
                    borderBottomColor: COLORS.graya1, borderBottomWidth: 1.0
                }}
                onPress={() => { this.setState({ transactionId: (item.transactionId != this.state.transactionId) ? item.transactionId : '' }) }}>
                <View>
                    <View style={{ flexDirection: 'row', paddingVertical: wp('4%') }}>
                        <Image
                            resizeMode='contain'
                            style={{
                                height: wp("4%"), width: wp("4%"), marginTop: hp('8%'), marginLeft: wp('2%')
                            }}
                            source={this.state.transactionId != item.transactionId ? require('../../assets/images/arrow_right.png') : require('../../assets/images/arrow_down.png')}
                        />
                        <View style={{ width: wp('92%'), padding: wp('2%') }}>

                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 0.6 }}>
                                    <Text style={{
                                        color: COLORS.subText,
                                        fontSize: smallText, ...testFont,

                                    }}>Card ID</Text>

                                    <Text style={{
                                        color: COLORS.black,
                                        fontSize: regular, ...testFont,
                                    }}>{item.cardIDNumber ? item.cardIDNumber : "NA"}</Text>

                                </View>

                                {
                                    item.cardStatus ? <View style={{ flex: 0.4, alignItems: 'center' }}>

                                        <View style={{
                                            color: 'white',
                                            paddingVertical: hp('1.3%'),
                                            borderRadius: wp('2%'),
                                            backgroundColor: passwordCheck.statusColourCode(item.cardStatus.toUpperCase()),
                                        }}>
                                            <Text style={{
                                                color: 'white',
                                                paddingHorizontal: wp('3%'),
                                                fontWeight: 'bold', ...testFont,
                                                fontSize: headingCardStatus,
                                            }}>
                                                {item.cardStatus.toUpperCase()}
                                            </Text>
                                        </View>

                                    </View> : null
                                }


                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')
                                    }}>Activation Date</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>
                                        {item.cardActivationDate ? moment(item.cardActivationDate).format('MMMM DD, YYYY') : 'NA'}
                                    </Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')
                                    }}>Expires on</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>{passwordCheck.getExpiryDate(item.expirationDate)}</Text>
                                </View>

                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>

                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')
                                    }}>Member</Text>
                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>{item.memberFirstName || item.memberLastName ? item.memberFirstName + " " + item.memberLastName : 'NA'}</Text>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        marginTop: hp('1%'),
                                        color: COLORS.subText,
                                        fontSize: smallText, ...testFont,

                                    }}>Amount Authorized</Text>

                                    <Text style={{
                                        color: COLORS.black,
                                        fontSize: regular, ...testFont,
                                    }}>
                                        {item.amountAuthorized ? "$" + item.amountAuthorized : 'NA'}
                                    </Text>

                                </View>

                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>

                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={{
                                        color: COLORS.subText,
                                        fontSize: smallText, ...testFont,
                                        marginTop: hp('1%')
                                    }}>Provider</Text>
                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>{item.providerName}</Text>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                </View>
                            </View>

                        </View>

                    </View>
                    {this.state.transactionId === item.transactionId
                        ?
                        <View style={{
                            paddingLeft: wp("7%"),
                            width: wp('100%'), paddingVertical: wp('4%'),
                            backgroundColor: "#f6f6f6",
                            borderTopColor: COLORS.divider,
                            borderTopWidth: 1
                        }}>
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                    }}>Procedure Information</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>{item.procedureInformation ? item.procedureInformation : 'NA'}</Text>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,

                                    }}>Card Request Date</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>
                                        {item.cardRequestDate ? moment(item.cardRequestDate).format('MMMM DD, YYYY') : 'NA'}

                                    </Text>

                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText, marginRight: wp('1.5%'),
                                        marginTop: hp('1%')

                                    }}>Provider Address</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>
                                        {
                                            item.providerAddress1 ? item.providerAddress1 + ", " + item.providerCity + ", " + item.providerState + ", " + item.providerPostalCode : 'NA'
                                        }
                                    </Text>
                                </View>

                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                                    <ButtonBorder title={"VIEW PAYMENT CARD"}
                                        isDisabled={false} type="blue"
                                        callback={() => this.props.navigation.navigate(AppStrings.PaymentCardNav, { data: item })} />
                                </View>
                            </View>
                        </View> : null
                    }

                </View>
            </TouchableOpacity >
        )
    }

    displayAlert() {
        return <AwesomeAlert
            show={this.state.show}
            showProgress={false}
            title={this.state.alertTitle}
            message={this.state.alertMessage}
            closeOnTouchOutside={false}
            closeOnHardwareBackPress={false}
            showCancelButton={this.state.cancelShow}
            titleStyle={{ color: COLORS.black }}
            messageStyle={{ color: COLORS.black }}
            showConfirmButton={this.state.confirmShow}
            cancelText={AppStrings.cancel}
            confirmText={AppStrings.ok}
            confirmButtonColor={COLORS.primaryColor}
            cancelButtonColor={COLORS.cancelButton}

            onCancelPressed={() => {
                this.setState({ show: false });
            }}
            onConfirmPressed={() => {
                this.setState({ show: false });

            }}
        />
    }

    showAlert = (title, message, from) => {
        this.setState({
            show: true, alertTitle: title, alertFrom: from, alertMessage: message
        }, () => { });
    };
};

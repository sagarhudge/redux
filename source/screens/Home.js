import React, { Component } from 'react';
import { SafeAreaView, Text, View, BackHandler, StatusBar, DeviceEventEmitter } from 'react-native';
import Loader from '../components/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../constants/AppStrings';
import HomeFooter from '../screens/HomeFooter';
import { Badge } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import COLORS from "../constants/COLORS"
import MainScreen from "../screens/MainScreen"
import NetInformation from "../components/NetInfo"
import NavigationDrawerHeader from "../routing/NavigationDrawerHeader"
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AwesomeAlertDialog from '../components/AwesomeAlertDialog';
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../constants/FontsApp'
import NotificationPopup from '../components/NotificationPopup';
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
import RNExitApp from 'react-native-exit-app';
// import { getAccessToken, getEmail } from '../constants/getAsyncValues';

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;
    this.showStatusDialog
    this.state = {
      loading: false,
      showNotification: false,

      title: AppStrings.exitApplication,
      message: '',
      alertCancel: AppStrings.cancel,
      alertConfirm: 'Exit Application',
      alertCLose: AppStrings.cancel,
      show: false,
      canBeClosed: true,
      Connection: null,
      alertType: 'Exit',

      notificationCount: 0,
      updateNotification: false
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this.DeviceEventEmitter = DeviceEventEmitter.addListener('notifyCount', this.deepLink.bind(this));
    this.DeviceEventEmitter = DeviceEventEmitter.addListener('sessionOut', this.sessionOut.bind(this));

    this.NavigationDrawerHeader();
    AsyncStorage.setItem("isLogin", "true");
    this.getAsync();
    // console.log("home-----------------------",getEmail())

  }

  async getAsync() {
    this.completionStatus = JSON.parse(await AsyncStorage.getItem('completionStatus'));
    this.showStatusDialog = true;

    if (this.completionStatus) {
      for (let i = 0; i < this.completionStatus.length; i++) {
        if (this.completionStatus[i] != 0) {
          this.showStatusDialog = false;
          break;
        }
      }
    }

    if (this.showStatusDialog || this.showStatusDialog == null || this.showStatusDialog == undefined) {
      this.setState({
        alertType: 'HQ_Notice', show: true, title: '',
        canBeClosed: true, message: AppStrings.hq_notice,
        alertConfirm: 'SKIP', alertCancel: 'YES'
      });
    }

  }

  NavigationDrawerHeader() {

    if (this.navigation) {
      this.navigation.setOptions({
        title: AppStrings.dashboard,
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={this.navigation} />
        ),
        headerRight: () => (
          <TouchableOpacity style={{ marginRight: hp('2%')}}
            onPress={() => {
              this.state.notificationCount != 0 ?
                this.setState({ showNotification: !this.state.showNotification }) : null
            }}>
            {
              this.state.notificationCount > 0 ? <Badge
                value={this.state.notificationCount}
                status="error" containerStyle={{ position: 'relative' }}>
              </Badge> : null
            }
            <Icon name="bell" size={wp('4%')} color={COLORS.white} />
          </TouchableOpacity>
        ),

        headerStyle: headerStyleContainer,
        headerTintColor: COLORS.white,
        headerTitleStyle: headerStyle,
      });
    }

  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>

        <View style={{ flex: 1 }}>
        <MainScreen
          navigation={this.navigation}
          name={"prop test"}
          Connection={this.state.Connection} />

        <StatusBar barStyle="light-content" backgroundColor='#8461AB' />

        <Loader loading={this.state.loading} />
        {
          this.state.showNotification &&
            <NotificationPopup
              notificationDismiss={
                this.notificationDismiss.bind(this)}
              showNotification={this.state.showNotification} navigation={this.props.navigation} />
            
        }
        <HomeFooter from="dash" isDashboard={true} callbackFooter={(from, isHome) => {

          if (from === AppStrings.documents) {
            this.props.navigation.navigate(AppStrings.documents);
          }

          if (from === AppStrings.MyNeedsScreen) {
            this.props.navigation.navigate(AppStrings.MyNeedsScreen);
          }

        }} />


        <NetInformation abc={this.verifyConnection.bind(this)} />

        <AwesomeAlertDialog
          callbackClick={this.callbackClick.bind(this)}
          show={this.state.show}
          title={this.state.title}
          message={this.state.message}
          alertCancel={this.state.alertCancel}
          alertConfirm={this.state.alertConfirm}
          canBeClosed={this.state.canBeClosed}
          okHide={true}
          type={this.state.alertType} />

  </View>
      </SafeAreaView>
    );
  }

  notificationDismiss(load) {
   //DeviceEventEmitter.emit('customName', { data: load, type: 'Load' })
    this.setState({ showNotification: false });

  }

  deepLink(notifyCount) {
    console.log('notifyCount.data', notifyCount.data);
    this.setState({ notificationCount: notifyCount.data }, () => {
      this.NavigationDrawerHeader();
    });
  }

  sessionOut(sessionOut) {
    // console.log("Session_Expired---- home", sessionOut, " HQ Status ", this.showStatusDialog);
    if (sessionOut) {
      this.setState({ alertType: 'session', show: sessionOut, title: AppStrings.sessionExpired, message: AppStrings.sessionMessage, alertConfirm: AppStrings.ok, canBeClosed: false, alertCancel: AppStrings.cancel });
    }
  }

  handleBackPress = async () => {
    let index = this.props.navigation.canGoBack();
    // console.log(index, "back press----home-------------")
    // handle the index we get
    if (index) {
      this.props.navigation.goBack();
    } else {
      this.handleBackButton();
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    this.DeviceEventEmitter.remove();
  }

  handleBackButton = () => {
    this.setState({
      alertType: 'Exit', show: true,
      alertCancel: AppStrings.cancel,
      canBeClosed: true, message: "",
      title: AppStrings.exitApplication,
      alertConfirm: 'Exit Application'
    });
    return true;
  }

  verifyConnection(isConnected) {
    if (this.state.Connection != isConnected)
      this.setState({ Connection: isConnected }, () => {
        DeviceEventEmitter.emit('connectionCheck', isConnected);
      });

  }

  hideAlert() {
    this.setState({ show: false }, () => { });
  }

   
  callbackClick(visible, title, button) {
    this.hideAlert();

    if (button === "ok") {
      if (title == "session") {
        AsyncStorage.setItem("isLogin", "false")
        this.props.navigation.replace('Auth');
      } else if (title == "HQ_Notice") {

      } else {
        AsyncStorage.setItem('login', 'false');
        RNExitApp.exitApp();
        // BackHandler.exitApp();
      }
    } else {

      if (title == "HQ_Notice") {
        this.props.navigation.navigate(AppStrings.HealthQuestionnaireNav)
      }
    }
  }

}
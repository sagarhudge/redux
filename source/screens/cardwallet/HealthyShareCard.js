
import React, { Component } from "react";
import { Linking, PermissionsAndroid, Alert, Text, TouchableOpacity, View, SafeAreaView, Image, ImageBackground, StyleSheet } from "react-native";

import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import { captureRef } from "react-native-view-shot";
import CameraRoll from "@react-native-community/cameraroll";
import Share from "react-native-share";
import CardFlip from "react-native-card-flip";
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconE from 'react-native-vector-icons/Entypo';
import { isAndroid } from "../../constants/PlatformCheck";
import moment from "moment";
import AwesomeAlertDialog from "../../components/AwesomeAlertDialog";
import AppStrings from "../../constants/AppStrings";
import { FontsApp, headerStyle, headerStyleContainer, testFont, } from '../../constants/FontsApp'
import * as RNFS from 'react-native-fs';
import { renameAndDownload, renameAndDownloadNew } from "../../constants/DownloadFile";
import { passwordCheck } from "../../constants/utils";
import { FabButton } from "../botchat/FabButton";

let heading = FontsApp.headingCard;
let subHeading = FontsApp.subHeadingCard
let regular = FontsApp.regularCard
let smallTextCard = FontsApp.smallTextCard
let headingCardLarge = FontsApp.headingCardLarge
const regex = /(<([^>]+)>)/ig;
let imageBackground = wp('65%');
let cardHeight = hp('35%');
let cardWidth = wp('95%');

//HorizontalView
let imageBackgroundH = wp('65%');
let cardHeightH = hp('35%');
let cardWidthH = hp('80%');



export default class HealthyShareCard extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;

        this.ref = React.createRef();
        this.refF = React.createRef();
        this.refB = React.createRef();
        this.count = 0;
        this.params = this.props.route.params;

        this.state = {
            data: [],
            loading: false,
            error: '',
            previewSource: null,
            res: null,
            dependantsMemberid: [],
            dependants: [],
            name: '',
            network: '',
            contactNumber: '',
            enrollmentDate: '',
            plainId: null,
            showEmpId: false,
            visibleTap: true,
            groupNo: '',
            cardId: '',
            prefix: '',
            empId: '',
            memberId: '',
            filepath: '',
            idTitle: 'Front of ID Card',
            planInfo: [],
            show: false,
            message: '',
            value: {
                format: "jpg",
                quality: 0.9,
            },
            rotate: true,
            source: '',
            optionSelected: this.params && this.params.key ? this.params.key : "",
            canGoBack: this.params && this.params.key ? this.props.route.params.key : false,
        }
    }

    componentWillUnmount() {
        // Orientation.lockToPortrait();
    }
    getChangePath = (uri, f_name) => {
        let uriArray = uri.split("/");
        let nameToChange = uriArray[uriArray.length - 1];
        let renamedURI = uri.replace(
            nameToChange, f_name + ".png"
        );
        return renamedURI;
    }
    componentDidMount() {
        // Orientation.lockToPortrait();
        this.getCardData();

        if (this.navigation) {
            this.navigation.setOptions({
                title: 'Healthy Share',
                headerRight: () => (
                    <View style={{ flexDirection: 'row' }}>
                        {
                            isAndroid ? <TouchableOpacity style={{ marginRight: wp('6%') }}
                                onPress={() => {
                                    this.downloadFilePermission();
                                }}>
                                <Icon name="download"
                                    size={wp('6%')} color={COLORS.white} />
                            </TouchableOpacity> : null

                        }

                        <TouchableOpacity style={{ marginRight: wp('6%') }}
                            onPress={() => {
                                // this.setState({ loading: true });

                                this.snapshot('share')
                            }}>
                            <Icon name="share-alt" size={wp('6%')} color={COLORS.white} />
                        </TouchableOpacity>

                    </View>
                ),
                headerStyle: headerStyleContainer,
                headerTintColor: COLORS.white,
                headerTitleStyle: headerStyle
            });
        }
    }
    async getCardData() {
        this.idcardData = JSON.parse(await AsyncStorage.getItem("GetIdCardData"));
        this.provider_network = await AsyncStorage.getItem("provider_network");
        // this.AgentInformation = JSON.parse(await AsyncStorage.getItem("AgentInformation"));
        this.idcardDataAll = this.idcardData;
        this.idcardData = this.idcardData[0];
        // console.log("idcardDataAll", this.idcardDataAll[1].empId);
        let source = await AsyncStorage.getItem("source");

        this.setState({
            planInfo: this.idcardData.planInfo,
            plainId: this.idcardData.planId,
            empId: this.idcardData.empId,
            memberId: this.idcardData.memberId,
            groupNo: this.idcardData.groupNo,
            network: this.provider_network,
            source: source,
            enrollmentDate: moment(this.idcardData.enrollmentDate).format('MM/DD/YYYY'),
            name: this.idcardData.firstName + " " + this.idcardData.lastName
        });

        this.setContactandCardID(this.idcardData);
        this.getDependant(this.idcardDataAll);
        this.sethealthcarddata();


    }
    render() {
        return (
            <SafeAreaView style={{
                backgroundColor: COLORS.graylight, flex: 1,
                alignItems: 'center', justifyContent: 'center'
            }}>
                <View
                    collapsable={false} style={{ transform: [{ rotate: !this.state.rotate ? '90deg' : '0deg' }], marginRight: !this.state.rotate ? wp('-14%') : 0, marginTop: this.state.rotate ? hp('-10%') : 0 }}
                    ref={(ref) => this.ref = ref}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontSize: headingCardLarge, ...testFont, marginVertical: hp('1%') }}>{this.state.idTitle}</Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ rotate: !this.state.rotate })
                            }}
                        >
                            <IconE name={this.state.rotate ? "circle-with-plus" : "circle-with-cross"} size={wp('8%')} color={COLORS.blueL} />
                        </TouchableOpacity>
                    </View>
                    <CardFlip style={

                        this.state.rotate ? styles.mainCardContainer : styles.mainCardContainerRotate} ref={(card) => (this.card = card)}>

                        {/*first visible view*/}
                        <TouchableOpacity
                            activeOpacity={1}
                            ref={(ref) => this.refF = ref}
                            style={[this.state.rotate ? styles.touchableCard : styles.touchableCardRotate, styles.touchableCardSecond]}
                            onPress={() => {
                                this.setState({ idTitle: 'Back of ID Card' })
                                this.card.flip()
                            }}>
                            {/* Header Images */}
                            <View >
                                <ImageBackground
                                    resizeMode={'stretch'}
                                    imageStyle={{ borderTopLeftRadius: 2.5 }}
                                    style={this.state.rotate ? styles.imageBackground : styles.imageBackgroundRotate}
                                    source={require('../../assets/images/group_53.png')} />

                                <View style={{ padding: hp('2%') }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Image
                                            resizeMode={'contain'}
                                            style={{ width: wp('30%'), height: hp('6%') }}
                                            source={require('../../assets/images/UHS.png')}
                                        />

                                        {this.state.network == 'AFMC' ?
                                            <Image
                                                resizeMode={'contain'}
                                                style={{ width: wp('30%'), height: hp('6%') }}
                                                source={require('../../assets/images/AZFMC_logo.png')}
                                            />
                                            : this.state.network == 'PHCS' || this.state.network == 'smartshare25' ||
                                                this.state.network == 'smartshare50' || this.state.network == 'healthyLife'
                                                ? (
                                                    <Image
                                                        resizeMode={'contain'}
                                                        style={{ width: wp('30%'), height: hp('6%') }}
                                                        source={require('../../assets/images/phcs_only.png')}
                                                    />
                                                ) : null}


                                    </View>
                                    {
                                        this.state.visibleTap ? <View style={{
                                            minHeight: this.state.rotate ? hp('40%') : wp('80%'),
                                            position: 'absolute', width: this.state.rotate ? wp('100%') : hp('80%'),
                                            alignItems: 'center', justifyContent: 'center'
                                        }}>
                                            <Image
                                                resizeMode={'contain'}
                                                style={{
                                                    opacity: 0.5,
                                                    alignSelf: 'center',
                                                    width: wp('20%%'),
                                                    height: wp('20%')
                                                }}
                                                source={require('../../assets/images/tap_icon.png')}
                                            />
                                        </View> : null
                                    }

                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('2%'), }}>

                                        <View style={{ flex: 1 }}>

                                            <Text style={{ fontSize: heading, ...testFont, fontWeight: 'bold', marginBottom: wp('1%') }}>
                                                {this.state.name}
                                            </Text>

                                            <Text style={styles.headingData}>
                                                {this.state.source != "NEO" ? this.state.prefix + "" + this.state.empId : this.state.source == "NEO" ? this.state.memberId : ''}
                                            </Text>

                                            {
                                                this.state.dependants.length != 0 ?
                                                    <Text style={styles.backRegularTextLarge}>
                                                        healthylife dependent
                                                    </Text> : null
                                            }

                                            {
                                                this.state.dependants.length != 0 ? this.state.dependants.map((data, i) =>
                                                    <View>
                                                        <Text style={styles.headingData}>{this.state.showEmpId && this.state.source != "NEO" ? this.state.prefix + "" + data.empId : this.state.source == "NEO" ? data.memberId : ''}
                                                        </Text>
                                                    </View>
                                                ) : null
                                            }
                                        </View>


                                        <View style={{ flex: 1, alignItems: 'flex-start' }}>
                                            <Text style={{ fontSize: subHeading, ...testFont, fontWeight: 'bold' }}>Program Details</Text>

                                            <View style={styles.rowViewMarginTop}>
                                                <Text style={styles.headingDetails}>Member since</Text>
                                                <Text style={styles.headingDataList}>{this.state.enrollmentDate}</Text>

                                            </View>
                                            <View style={styles.rowViewMarginTop}>
                                                <Text style={styles.headingDetails}>Group No.</Text>
                                                <Text style={styles.headingDataList}>{this.state.groupNo}</Text>

                                            </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('0.5%'), marginBottom: hp('1.5%') }}>
                                                <Text style={styles.headingDetails}>Program ID</Text>
                                                <Text style={styles.headingDataList}>{this.state.plainId}</Text>

                                            </View>

                                            {

                                                this.state.planInfo.length != 0 ? this.state.planInfo.map((data) =>
                                                    data.idcardField != "group" && data.idcardField != "prefix" && data.idcardField != 'Surgery EasyShare Limit per surgery' && data.idcardField != 'Chiropractic' && data.idcardField != "card id" && data.idcardField != "contact number" && data.idcardField != "Program Details" ?
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('0.5%') }}>
                                                            <Text style={styles.headingDetails}>{data.idcardField.replace(regex, '')}</Text>
                                                            <Text style={styles.headingDataList}>{data.fieldValue.replace(regex, '')}</Text>

                                                        </View> : null
                                                ) : null
                                            }
                                        </View>
                                    </View>

                                    {this.getFooter()}
                                </View>
                            </View>
                        </TouchableOpacity>
                        {/*Back view*/}
                        <TouchableOpacity
                            activeOpacity={1}
                            ref={(ref) => this.refB = ref}

                            style={[this.state.rotate ? styles.touchableCardBack : styles.touchableCardBackRotate, styles.touchableCardSecond]}
                            onPress={() => {
                                this.setState({ idTitle: 'Front of ID Card' })
                                this.card.flip()
                            }
                            }>
                            {
                                this.state.visibleTap ? <View style={{
                                    minHeight: this.state.rotate ? hp('40%') : wp('80%'),
                                    position: 'absolute',
                                    width: this.state.rotate ? wp('100%') : hp('80%'),
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <Image
                                        resizeMode={'contain'}
                                        style={{
                                            opacity: 0.5,
                                            alignSelf: 'center',
                                            width: wp('20%%'),
                                            height: wp('20%')
                                        }}
                                        source={require('../../assets/images/tap_icon.png')}
                                    />
                                </View> : null
                            }

                            {this.getHeader()}

                            {/* PARAGRAPH View */

                                this.getBackData()
                            }

                            {
                                this.getFooterSmall()
                            }
                        </TouchableOpacity>
                    </CardFlip>
                </View>

                <Loader loading={this.state.loading} />
                {this.showAlert()}
                <FabButton
                    style={{ margin: hp('3%') }}
                    canGoBack={this.state.canGoBack}
                    navigation={this.props.navigation}
                />
            </SafeAreaView>

        );
    }
    downloadFilePermission = async () => {

        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        // const granted2 = await PermissionsAndroid.request(
        //     PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        // );
        console.log("granted----------------", granted);
        //&& granted2 === PermissionsAndroid.RESULTS.GRANTED
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            this.setState({ loading: true });

            this.snapshot('download')
        } else {
            Alert.alert('Storage Permission Denied');
        }


    }

    getFooterSmall() {
        return (<View style={styles.backFooterSmall}>
            <Text style={{ fontSize: smallTextCard, ...testFont, color: COLORS.textColor, flex: 1.1, }} >
                www.UniversalHealthFellowship.org
            </Text>

            <View style={{
                flexDirection: 'row', flex: 1.1,
                alignItems: 'center', justifyContent: 'center'
            }}>
                <Image
                    resizeMode={'contain'}
                    style={styles.backImage}
                    source={require('../../assets/images/thank-you-for.png')} >
                </Image>
                <Image
                    resizeMode={'contain'}
                    style={styles.backImage}
                    source={require('../../assets/images/sharing.png')} >
                </Image>
            </View>

            <Text style={{
                fontSize: smallTextCard, ...testFont,
                color: COLORS.textColor, flex: 0.8, textAlign: 'right'
            }} >
                {this.state.cardId}</Text>
        </View>)
    }
    getFooterBig() {
        return (<View>
            <View style={{
                flexDirection: 'row',
                marginTop: hp('1%'),
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center'
            }}>
                <Image
                    resizeMode={'contain'}
                    style={styles.backImage}
                    source={require('../../assets/images/thank-you-for.png')} >
                </Image>

                <Image
                    resizeMode={'contain'}
                    style={styles.backImage}
                    source={require('../../assets/images/sharing.png')} >
                </Image>
            </View>
            <View style={styles.backFooter}>

                <Text style={{ fontSize: regular, ...testFont, color: COLORS.textColor, flex: 1 }} >
                    www.UniversalHealthFellowship.org
                </Text>

                {
                    !(this.state.network == 'PHCS' ||
                        this.state.network == 'healthyLife') ?
                        <Text style={{ fontSize: regular, color: COLORS.textColor, flex: 1, textAlign: 'right' }} >
                            {this.state.cardId}</Text> : null
                }
            </View>
        </View>)
    }
    getFooter() {
        switch (this.state.network) {
            //one line
            case 'healthyLife':
                return (
                    <Text style={styles.footerText}>
                        Universal HealthShare plans from Universal Health Fellowship are health care cost sharing ministry programs,
                        not insurance. We are IN NETWORK for MultiPlan’s PHCS Network for practitioner & ancillary providers.
                        {"\n"} Providers & Members confirmations and questions {this.state.contactNumber}
                    </Text>
                )
            //big one
            case 'smartshare25':
                return (
                    <Text style={styles.footerText}>
                        Universal HealthShare plans from Universal Health Fellowship are health care cost sharing ministry programs, not insurance.
                        We are IN NETWORK for MultiPlan’s PHCS Network for practitioner & ancillary providers.
                        {"\n"} Providers & Members confirmations and questions {this.state.contactNumber}

                    </Text>
                )
            //big one
            case 'smartshare50':
                return (
                    <Text style={styles.footerText}>
                        Universal HealthShare plans from Universal Health Fellowship are health care cost sharing ministry programs, not insurance.
                        We are IN NETWORK for MultiPlan’s PHCS Network for practitioner & ancillary providers.
                        {"\n"}Providers & Members confirmations and questions {this.state.contactNumber}

                    </Text>
                )
        }
    }

    getDependant(data) {
        data.map((data, index) => {
            if (index != 0) {
                this.state.dependants.push(data.empId)
                this.state.dependantsMemberid.push(data.memberId)
            }
        })
    }

    setContactandCardID(data) {
        data.planInfo.map((data, index) => {
            if (data.idcardField == "contact number") {
                this.setState({ contactNumber: data.fieldValue })
            }
            if (data.idcardField == "card id") {

                this.setState({ cardId: data.fieldValue }, () => {
                })
            }
            if (data.idcardField == "prefix") {
                this.setState({ prefix: data.fieldValue }, () => {

                })
            }
            if (data.idcardField == "group") {
                this.setState({ groupNo: data.fieldValue }, () => {

                })
            }
        })

    }
    getHeader() {
        switch (this.state.network) {
            case 'healthyLife':
            case 'smartshare25':
            case 'smartshare50':
                return (
                    <View>
                        <Text style={styles.backHeader} >
                            For Preventive Services Appointments visit: PreventiveServices.UniversalHealthFellowship.org
                        </Text>

                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 0.7 }}>
                                <Text style={styles.backHeader} >
                                    For Customer Service call: {this.state.contactNumber}
                                </Text>
                                <Text style={styles.backHeader} >
                                    To find a provider visit FindProvider.UniversalHealthFellowship.org
                                </Text>
                            </View>
                            <View style={{ flex: 0.3 }}>
                                <Text style={styles.backId}>EDI #40585</Text>
                            </View>
                        </View>
                    </View>
                )


        }
    }
    getBackData() {

        switch (this.state.network) {

            case 'healthyLife':
                return (
                    <View style={{ paddingBottom: wp('8%') }} >

                        <Text style={styles.backPurpleBold} >
                            Providers, send needs requests to: PO Box 106 Rutherford, NJ 07070-0106
                        </Text>

                        <Text style={styles.backRegularText} >
                            Pre-notification is required before these procedures will be eligible for sharing: All Inpatient Hospital
                            Confinements, All Surgical Procedures (Inpatient, Outpatient and Ambulatory, Organ and Tissue Transplant Services)
                            Cancer Treatment and Oncology Services, Independent Lab Tests and Imaging, Home Health Care Services, Carpal Tunnel
                            Treatments. In addition, Pre-certification to confirm medical necessity is required before these procedures may be
                            eligible for sharing: Transplant of any organ or tissue, a coronary bypass or graft of any kind, or a knee or hip
                            replacement. For Medical Emergencies Seek Immediate Medical Help.
                        </Text>

                        <Text style={styles.backRegularText} >
                            Participating member assignment of eligible medical expense sharing payment is permitted as consideration in full for
                            services rendered. Reimbursement for hospital facility services is determined at a percentage of the facility’s
                            Medicare allowable amounts (140% for Inpatient and 155% for Outpatient Services), or, in the absence of an applicable
                            CMS fee schedule, in accordance with published UHF Sharing Guidelines. Acceptance of sharing payment for Eligible
                            Expenses constitutes waiver of facility/provider right to balance bill patient.
                        </Text>

                        <Text style={styles.backRegularText} >
                            See applicable Sharing Guidelines for more details.
                            <Text style={styles.backRegularTextBold} >Universal Health Fellowship</Text> is a
                            not-for-profit ministry that facilitates medical expense sharing through Universal HealthShare Programs. Universal
                            HealthShare is not insurance and does not guarantee that eligible medical bills will be shared or otherwise paid.
                            Universal HealthShare is not a discount card or program.
                        </Text>
                    </View>

                )
            case 'smartshare25':

                return (
                    <View style={{ paddingBottom: wp('4%') }} >
                        <Text style={styles.backPurpleBold} >
                            Providers, send needs requests to: PO Box 106 Rutherford, NJ 07070-0106
                        </Text>
                        <Text style={{
                            fontSize: regular, ...testFont,
                            color: COLORS.textColor,
                            marginTop: hp('1%'), textAlign: 'center', marginLeft: hp('8%'), marginRight: hp('8%'),
                            fontWeight: 'bold', fontStyle: 'italic'
                        }} >
                            All medical services for the Healthy Life EasyShare Program 25 are limited
                            to an annual maximum of $25,000 per member.
                        </Text>

                        <Text style={styles.backRegularText} >
                            Member assignment to Providers of eligible medical expense sharing reimbursement as consideration for services
                            rendered is permitted by Universal Health Fellowship (UHF). Sharing reimbursement for eligible hospital or
                            ambulatory surgical center services/expenses is determined at a percentage of the facility’s Medicare allowable
                            amounts (140% for Inpatient and 155% for Outpatient Services), or, absent an applicable CMS fee schedule, in
                            accordance with current published UHF Sharing Guidelines. See applicable Sharing Guidelines for details.
                        </Text>

                        <Text style={styles.backRegularText} >
                            See applicable Sharing Guidelines for details.
                            <Text style={styles.backRegularTextBold}>
                                Universal Health Fellowship (UHF). </Text>  is a not-for-profit ministry that facilitates medical expense sharing through Universal HealthShare Programs.
                            Universal HealthShare is not insurance and does not guarantee that eligible medical bills will be shared or
                            otherwise paid. Universal HealthShare is not a discount card or program.
                        </Text>
                    </View>

                )
            case 'smartshare50':

                return (
                    <View style={{ paddingBottom: wp('4%') }} >
                        <Text style={styles.backPurpleBold} >
                            Providers, send needs requests to:  PO Box 106 Rutherford, NJ 07070-0106
                        </Text>
                        <Text style={{
                            fontSize: regular, ...testFont,
                            color: COLORS.textColor,
                            marginTop: hp('1%'), textAlign: 'center', marginLeft: hp('8%'), marginRight: hp('8%'),
                            fontWeight: 'bold', fontStyle: 'italic'
                        }} >
                            All medical services for the Healthy Life EasyShare Program 50 are limited
                            to an annual maximum of $50,000 per member.
                        </Text>

                        <Text style={styles.backRegularText} >
                            Member assignment to Providers of eligible medical expense sharing reimbursement as consideration for services
                            rendered is permitted by Universal Health Fellowship (UHF). Sharing reimbursement for eligible hospital or
                            ambulatory surgical center services/expenses is determined at a percentage of the facility’s Medicare allowable
                            amounts (140% for Inpatient and 155% for Outpatient Services), or, absent an applicable CMS fee schedule, in
                            accordance with current published UHF Sharing Guidelines. See applicable Sharing Guidelines for details.
                        </Text>

                        <Text style={styles.backRegularText} >
                            See applicable Sharing Guidelines for details.
                            <Text style={styles.backRegularTextBold}>
                                Universal Health Fellowship (UHF).
                            </Text>  is a not-for-profit ministry that facilitates medical expense sharing through Universal HealthShare Programs.
                            Universal HealthShare is not insurance and does not guarantee that eligible medical bills will be shared or
                            otherwise paid. Universal HealthShare is not a discount card or program.
                        </Text>
                    </View>

                )

        }
    }

    shareSnap(uri, previewSource, action) {
        if (action === 'share') {
            shareFile(uri, previewSource, "UHS_Membership_ID_HL")

            this.setState({ loading: false, previewSource: null, res: null, visibleTap: true });

        } else if (action === 'download') {

            renameAndDownloadNew(uri, previewSource, "UHS_Membership_ID_HL").then(result => {
                if (result.includes("ERROR") || result.includes("error") || result.includes("Error")
                    || result.includes("failed") || result.includes("No such file or directory")) {
                    this.setState({ show: true, message: "Your membership ID card download failed, Please try again.", filepath: '', loading: false, visibleTap: true })

                } else {
                    this.setState({ show: true, message: "Your membership ID card has been downloaded.", filepath: result, loading: false, visibleTap: true, previewSource: null, res: null, })

                }
            })

        } else if (action === 'pdf') {
            //Alert.alert("pdf save successfully");
        }

    }
    callbackClick(visible, title, button) {
        this.setState({ show: false, message: "" });
        if (this.state.filepath != "") {
            passwordCheck.showLocaleFile(this.state.filepath).then(result => {
                console.log(result)
            })
        }
    }
    showAlert() {
        return <AwesomeAlertDialog
            callbackClick={this.callbackClick.bind(this)}
            show={this.state.show}
            title={""}
            message={this.state.message}
            alertCancel={AppStrings.cancel}
            alertConfirm={AppStrings.ok}
            canBeClosed={false}
            type={""} />

    }
    snapshot(action) {
        this.setState({ visibleTap: false }, () => {
            captureRef(this.count == 0 ? this.refF : this.refB, this.state.value)
                .then(res =>
                    this.state.value.result !== "file" ? res
                        : new Promise((success, failure) => Image.getSize(res, (width, height) => (console.log(res, width, height), success(res)), failure)))
                .then(res => {

                    if (this.state.previewSource) {
                        this.count = 0
                        this.shareSnap(res, this.state.previewSource, action);
                    } else {
                        this.setState({
                            error: '',
                            res: null,
                            previewSource: res
                        }, () => {
                            this.count = 1
                            setTimeout(() => this.snapshot(action), 500);

                        });
                    }

                }).catch((error) => {
                    (this.setState({ error: '', res: null, previewSource: null }))
                });
        })

    }


    sethealthcarddata() {
        if (
            this.state.plainId == '1001' ||
            this.state.plainId == '1002' ||
            this.state.plainId == '1003' ||
            this.state.plainId == '1004' ||
            this.state.plainId == '1005' ||
            this.state.plainId == '1006'
            || this.state.plainId == '1017'
            || this.state.plainId == '1018' || this.state.plainId == '1019' || this.state.plainId == '1020' || this.state.plainId == '1021' || this.state.plainId == '1022'
        ) {
            this.setState({
                // network: 'PHCS',
                // channel: 'NEO',
                // contactNumber: '(888) 366-6243',
                // // cardId: '1kNEOUHSR071820E072020',
                // cardId: 'UHSR101920E101220',
                showEmpId: false
            })
        }

        if (
            this.state.plainId == '7001' ||
            this.state.plainId == '7002' ||
            this.state.plainId == '7003' ||
            this.state.plainId == '7004' ||
            this.state.plainId == '7005' ||
            this.state.plainId == '7006'
            || this.state.plainId == '7017'
            || this.state.plainId == '7018' || this.state.plainId == '7019' || this.state.plainId == '7020' || this.state.plainId == '7021' || this.state.plainId == '7022'
        ) {
            this.setState({
                // network: 'PHCS',
                // channel: 'Tutela',
                // contactNumber: '(800) 987-1990',
                // // cardId: '7kTTUHSR071720E072020',
                // cardId: 'UHSR101920E101220',
                showEmpId: true
            })
        }

        if (
            this.state.plainId == '8001' ||
            this.state.plainId == '8002' ||
            this.state.plainId == '8003' ||
            this.state.plainId == '8004' ||
            this.state.plainId == '8005' ||
            this.state.plainId == '8006'
            || this.state.plainId == '8017'
            || this.state.plainId == '8018' || this.state.plainId == '8019' || this.state.plainId == '8020' || this.state.plainId == '8021' || this.state.plainId == '8022'
        ) {
            this.setState({
                // network: 'PHCS',
                // channel: 'HST',
                // contactNumber: '(888) 942-4725',
                // // cardId: '8kHSTUHSR071720E072020',
                // cardId: 'UHSR101920E101220',
                showEmpId: true
            })
        }

        if (
            this.state.plainId == '9001' ||
            this.state.plainId == '9002' ||
            this.state.plainId == '9003' ||
            this.state.plainId == '9004' ||
            this.state.plainId == '9005' ||
            this.state.plainId == '9006'
            || this.state.plainId == '9017'
            || this.state.plainId == '9018' || this.state.plainId == '9019' || this.state.plainId == '9020' || this.state.plainId == '9021' || this.state.plainId == '9022'
        ) {
            this.setState({
                // network: 'PHCS',
                // channel: 'Parish',
                // contactNumber: '(855) 030-4941',
                // // cardId: '9kPBUHSR071720E072020',
                // cardId: 'UHSR101920E101220',
                showEmpId: true
            })
        }

        if (
            this.state.plainId == '10001' ||
            this.state.plainId == '10002' ||
            this.state.plainId == '10003' ||
            this.state.plainId == '10004' ||
            this.state.plainId == '10005' ||
            this.state.plainId == '10006'

            || this.state.plainId == '10017'
            || this.state.plainId == '10018' || this.state.plainId == '10019' || this.state.plainId == '10020' || this.state.plainId == '10021' || this.state.plainId == '10022'
        ) {
            this.setState({
                // network: 'PHCS',
                // channel: 'CHS',
                // planIds: [10001, 10002, 10003, 10004, 10005, 10006],
                // contactNumber: '(888) 792-4722',
                // // cardId: '9kPBUHSR071720E072020',
                // cardId: 'UHSR101920E101220',
                showEmpId: true
            })
        }

        if (
            this.state.plainId == '11001' ||
            this.state.plainId == '11002' ||
            this.state.plainId == '11003' ||
            this.state.plainId == '11004' ||
            this.state.plainId == '11005' ||
            this.state.plainId == '11006'

            || this.state.plainId == '11017'
            || this.state.plainId == '11018' || this.state.plainId == '11019' || this.state.plainId == '11020' || this.state.plainId == '11021' || this.state.plainId == '11022'
        ) {
            this.setState({
                // network: 'PHCS',
                // channel: 'CHS-Plus',
                // contactNumber: '(888) 792-4722',
                // // cardId: '9kPBUHSR071720E072020',
                // cardId: 'UHSR101920E101220',
                showEmpId: true
            })
        }

        if (
            this.state.plainId == '12001' ||
            this.state.plainId == '12002' ||
            this.state.plainId == '12003' ||
            this.state.plainId == '12004' ||
            this.state.plainId == '12005' ||
            this.state.plainId == '12006'

            || this.state.plainId == '12017'
            || this.state.plainId == '12018' || this.state.plainId == '12019' || this.state.plainId == '12020' || this.state.plainId == '12021' || this.state.plainId == '12022'
        ) {
            this.setState({
                // network: 'PHCS',
                // channel: 'BIG',
                // contactNumber: '(855) 809-0110',
                // // cardId: '12kBGUHS071720E072020',
                // cardId: 'UHSR101920E101220',
                showEmpId: true
            })
        }

        if (
            this.state.plainId == '6001' ||
            this.state.plainId == '6002' ||
            this.state.plainId == '6003' ||
            this.state.plainId == '6004' ||
            this.state.plainId == '6005' ||
            this.state.plainId == '6006'

            || this.state.plainId == '6017'
            || this.state.plainId == '6018' || this.state.plainId == '6019' || this.state.plainId == '6020' || this.state.plainId == '6021' || this.state.plainId == '6022'
        ) {
            this.setState({
                // network: 'AFMC',
                // channel: 'AFA',
                // contactNumber: '(855) 229-0257',
                // // cardId: '6kAFAUHSR071820E072020',
                // cardId: '6kAFAUHSR101920E101220',
                showEmpId: true
            })
        }

        if (this.state.plainId == '1011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'NEO',
                // contactNumber: '(888) 366-6243',
                // cardId: 'SSR101920E101220',
                showEmpId: false //not sure for other than 1011 plans
            })
        }





        if (this.state.plainId == '6011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'UHF',
                // contactNumber: '(888) 791-4722',
                // cardId: 'SSR101920E101220',
                showEmpId: true
            })
        }

        if (this.state.plainId == '13001' || this.state.plainId == '13002' || this.state.plainId == '13003' || this.state.plainId == '13004'
            || this.state.plainId == '13005' || this.state.plainId == '13006' || this.state.plainId == '13017'
            || this.state.plainId == '13018' || this.state.plainId == '13019' || this.state.plainId == '13020' || this.state.plainId == '13021' || this.state.plainId == '13022') {
            this.setState({
                // network: 'PHCS',
                // channel: 'Aspire',
                // contactNumber: '(888) 992-4789',
                // cardId: 'UHSR101920E101220',
                showEmpId: true
            })
        }

        // if (this.state.plainId == '7011' || this.state.plainId == '8011' || this.state.plainId == '12011' || this.state.plainId == '9011' || this.state.plainId == '10011' || this.state.plainId == '11011' || this.state.plainId == '13011' ) {
        // this.setState({
        // network: 'Smartshare',
        // channel: 'PHCS',
        // contactNumber: '(855) 809-0110',
        // cardId: 'UHSR101920E101220',
        // showEmpId: true
        // })
        // }

        if (this.state.plainId == '7011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'PHCS',
                // contactNumber: '(800) 987-1990',
                // cardId: 'UHSR101920E101220',
                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '8011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'PHCS',
                // contactNumber: '(888) 942-4725',
                // cardId: 'SSR101920E101220',
                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '9011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'PHCS',
                // contactNumber: '(855) 030-4941',
                // cardId: 'SSR101920E101220',
                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '10011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'PHCS',
                // contactNumber: '(888) 792-4722',
                // cardId: 'SSR101920E101220',
                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '11011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'PHCS',
                // contactNumber: '(888) 792-4722',
                // cardId: 'SSR101920E101220',
                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '12011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'PHCS',
                // contactNumber: '(855) 809-0110',
                // cardId: 'SSR101920E101220',
                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '13011') {
            this.setState({
                // network: 'Smartshare',
                // channel: 'PHCS',
                // contactNumber: '(888) 992-4789',
                // cardId: 'SSR101920E101220',
                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '14001' || this.state.plainId == '14002' || this.state.plainId == '14003' || this.state.plainId == '14004'
            || this.state.plainId == '14005' || this.state.plainId == '14006' || this.state.plainId == '14017'
            || this.state.plainId == '14018' || this.state.plainId == '14019' || this.state.plainId == '14020' || this.state.plainId == '14021' || this.state.plainId == '14022' || this.state.plainId == '14011') {
            this.setState({
                showEmpId: true
            })
        }

        if (this.state.plainId == '15001' || this.state.plainId == '15002' || this.state.plainId == '15003' || this.state.plainId == '15004'
            || this.state.plainId == '15005' || this.state.plainId == '15006' || this.state.plainId == '15017'
            || this.state.plainId == '15018' || this.state.plainId == '15019' || this.state.plainId == '15020' || this.state.plainId == '15021' || this.state.plainId == '15022' || this.state.plainId == '15011') {
            this.setState({
                showEmpId: true
            })
        }
        if (this.state.plainId == '20120' || this.state.plainId == '20140' || this.state.plainId == '20160' || this.state.plainId == '20151'
            || this.state.plainId == '20152' || this.state.plainId == '20220' || this.state.plainId == '20240' || this.state.plainId == '20260' || this.state.plainId == '20251'
            || this.state.plainId == '20252' || this.state.plainId == '20320' || this.state.plainId == '20340' || this.state.plainId == '20360' || this.state.plainId == '20351'
            || this.state.plainId == '20352' || this.state.plainId == '20420' || this.state.plainId == '20440' || this.state.plainId == '20460' || this.state.plainId == '20451'
            || this.state.plainId == '20452' || this.state.plainId == '20520' || this.state.plainId == '20540' || this.state.plainId == '20560' || this.state.plainId == '20551'
            || this.state.plainId == '20552') {
            this.setState({
                showEmpId: true
            })
        }


    };
}


const styles = StyleSheet.create({

    imageBackground: {
        width: imageBackground,
        aspectRatio: 1, top: 0, bottom: 0, opacity: 0.5,
        position: 'absolute',
    },

    mainCardContainer: {
        width: cardWidth,
        minHeight: cardHeight,
    },
    touchableCard: {
        minHeight: cardHeight, borderRadius: 10,
    },
    touchableCardBack: {
        minHeight: cardHeight, borderRadius: 10,
        padding: '2%',
    },

    //asaad
    imageBackgroundRotate: {
        width: imageBackgroundH,
        aspectRatio: 1, top: 0, bottom: 0, opacity: 0.5,
        position: 'absolute',
    },
    mainCardContainerRotate: {
        width: cardWidthH,
        minHeight: cardHeightH,
    },
    touchableCardRotate: {
        minHeight: cardHeightH, borderRadius: 10,
    },
    touchableCardBackRotate: {
        minHeight: cardHeightH, borderRadius: 10,
        padding: '2%',
    },
    ///

    rowViewMarginTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: hp('0.5%'), alignSelf: 'baseline'
    },

    headingDetails: {
        flex: 1,
        fontSize: subHeading, ...testFont,
        color: COLORS.headingColor2
    },

    headingData: {
        fontSize: heading, ...testFont,
        fontWeight: 'bold',
        color: COLORS.headingColor2
    },
    headingDataList: {
        fontSize: heading, ...testFont,
        fontWeight: 'bold', flex: 1,
        color: COLORS.headingColor2
    },

    touchableCardSecond: {
        backgroundColor: COLORS.white,

    },

    backImage: {
        aspectRatio: 1,
        width: wp('20%'),
    },

    backPurpleBold: {
        fontSize: regular, ...testFont,
        color: COLORS.primaryColor,
        marginTop: hp('1%'),
        fontWeight: 'bold'

    },

    backRegularText: {
        fontSize: regular, ...testFont,
        color: COLORS.textColor,
        marginTop: hp('1%')
    },
    backRegularTextLarge: {
        fontSize: heading, ...testFont,
        color: COLORS.textColor,
        marginTop: hp('1%')
    },
    backRegularTextBold: {
        fontSize: regular, ...testFont,
        color: COLORS.textColor,
        marginTop: hp('2%'),
        fontWeight: 'bold'
    },
    backFooter: {
        flexDirection: 'row', margin: hp('2%'), position: 'absolute',
        alignItems: 'center', bottom: 0, justifyContent: 'center'
    },
    backFooterSmall: {
        flexDirection: 'row', margin: hp('1%'),
        position: 'absolute', alignItems: 'center', bottom: 0
    }, backImage: {
        height: hp('4%'),
        width: wp('15%'),
    },
    footerText: {
        fontSize: subHeading, ...testFont, fontWeight: '400', marginHorizontal: wp('3%'),

        color: COLORS.textColor, marginTop: hp('3%'), textAlign: 'center'
    }
    , backHeader: {
        fontSize: regular, ...testFont,
        color: COLORS.primaryColor, fontWeight: 'bold'
    },
    backId: {
        fontSize: regular, ...testFont, marginTop: 5, textAlign: 'center',
        color: COLORS.white, backgroundColor: COLORS.headingColor2,
        padding: wp('1.5%'), fontWeight: 'bold',
    }
});



import React, { Component } from "react";
import { Linking, PermissionsAndroid, Alert, Text, TouchableOpacity, View, SafeAreaView, Image, ImageBackground, StyleSheet } from "react-native";

import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import { captureRef } from "react-native-view-shot";
import CameraRoll from "@react-native-community/cameraroll";
import Share from "react-native-share";
import CardFlip from "react-native-card-flip";

import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconE from 'react-native-vector-icons/Entypo';
import { isAndroid } from "../../constants/PlatformCheck";
import moment from "moment";
import AwesomeAlertDialog from "../../components/AwesomeAlertDialog";
import AppStrings from "../../constants/AppStrings";
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
import { passwordCheck } from "../../constants/utils";
import * as RNFS from 'react-native-fs';
import { renameAndDownload, renameAndDownloadNew, shareFile } from "../../constants/DownloadFile";
import { FabButton } from "../botchat/FabButton";


let heading = FontsApp.headingCard;
let subHeading = FontsApp.subHeadingCard
let regular = FontsApp.regularCard
let smallTextCard = FontsApp.smallTextCard
let headingCardLarge = FontsApp.headingCardLarge
let imageBackground = hp('32%');
let cardHeight = hp('32%');
let cardWidth = wp('95%');
let imageLogo = wp('40%');

//HorizontalView
let imageBackgroundH = wp('60%');
let cardHeightH = hp('32%');
let cardWidthH = hp('80%');
export default class HealthToolCard extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.ref = React.createRef();
        this.refF = React.createRef();
        this.refB = React.createRef();
        this.count = 0;
        this.params = this.props.route.params;

        this.state = {
            data: [],
            loading: false,
            error: '',
            previewSource: null,
            res: null,
            name: '',
            network: '',
            contactNumber: '',
            enrollmentDate: '',
            plainId: null,
            showEmpId: false,
            visibleTap: true,
            groupNo: '',
            cardId: '',
            prefix: '',
            empId: '',
            memberId: '',
            planInfo: [],
            show: false,
            message: '',
            filepath: '',
            flip: 1,
            idTitle: 'Front of ID Card',
            value: {
                format: "jpg",
                quality: 0.9,
            },
            rotate: true,
            source: '',
            optionSelected: this.params && this.params.key ? this.params.key : "",
            canGoBack: this.params && this.params.key ? this.props.route.params.key : false,

        }
    }

    componentWillUnmount() {
        // Orientation.lockToPortrait();

    }
    componentDidMount() {
        // Orientation.lockToPortrait();
        this.getCardData();
        if (this.navigation) {
            this.navigation.setOptions({
                title: 'Health Tool',
                headerRight: () => (
                    <View style={{ flexDirection: 'row' }}>
                        {
                            isAndroid ? <TouchableOpacity style={{ marginRight: wp('6%') }}
                                onPress={() => {
                                    this.downloadFilePermission();
                                }}>
                                <Icon name="download" size={wp('6%')} color={COLORS.white} />
                            </TouchableOpacity> : null

                        }

                        <TouchableOpacity style={{ marginRight: wp('6%') }}
                            onPress={() => {
                                // this.setState({ loading: true });

                                this.snapshot('share')
                            }}>
                            <Icon name="share-alt" size={wp('6%')} color={COLORS.white} />
                        </TouchableOpacity>

                    </View>
                ),
                headerStyle: headerStyleContainer,
                headerTintColor: COLORS.white,
                headerTitleStyle: headerStyle
            });
        }

    }
    async getCardData() {
        this.idcardData = JSON.parse(await AsyncStorage.getItem("GetIdCardData"));
        this.provider_network = await AsyncStorage.getItem("provider_network");
        // this.AgentInformation = JSON.parse(await AsyncStorage.getItem("AgentInformation"));
        this.idcardData = this.idcardData[0];
        let source = await AsyncStorage.getItem("source");

        this.setState({
            planInfo: this.idcardData.planInfo,
            plainId: this.idcardData.planId,
            empId: this.idcardData.empId,
            memberId: this.idcardData.memberId,
            groupNo: this.idcardData.groupNo,
            network: this.provider_network,
            source: source,
            enrollmentDate: moment(this.idcardData.enrollmentDate).format('MM/DD/YYYY'),
            name: this.idcardData.firstName + " " + this.idcardData.lastName
        });

        this.setContactandCardID(this.idcardData);
        // this.sethealthcarddata();


    }
    render() {
        return (

            <SafeAreaView style={{
                backgroundColor: COLORS.graylight, flex: 1, flexDirection: 'column',
                alignItems: 'center', justifyContent: 'center'
            }}>

                <View
                    collapsable={false} style={{
                        transform: [{ rotate: !this.state.rotate ? '90deg' : '0deg' }],
                        marginRight: !this.state.rotate ? wp('-13%') : 0, marginTop: this.state.rotate ? hp('-10%') : 0
                    }}
                    // onLayout={(event) => { this.find_dimesions(event.nativeEvent.layout) }}
                    ref={(ref) => this.ref = ref}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ fontSize: headingCardLarge, ...testFont, marginVertical: hp('1%') }}>{this.state.idTitle}</Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ rotate: !this.state.rotate })
                            }}>
                            <IconE name={this.state.rotate ? "circle-with-plus" : "circle-with-cross"} size={wp('8%')} color={COLORS.blueL} />
                        </TouchableOpacity>
                    </View>

                    <CardFlip style={this.state.rotate ? styles.mainCardContainer :
                        styles.mainCardContainerRotate} ref={(card) => (this.card = card)}>


                        {/*first visible view*/}
                        <TouchableOpacity
                            activeOpacity={1}
                            ref={(ref) => (this.refF = ref)}
                            style={[styles.touchableCard, styles.touchableCardSecond]}
                            onPress={() => {
                                this.setState({ idTitle: 'Back of ID Card' })
                                this.card.flip()
                            }}>
                            {/* Header Images */}

                            <View >
                                <ImageBackground
                                    resizeMode={'stretch'}
                                    imageStyle={{ borderTopLeftRadius: 2.5 }}
                                    style={this.state.rotate ? styles.imageBackground : styles.imageBackgroundRotate}
                                    source={require('../../assets/images/group_51.png')} />

                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}>
                                    <Image
                                        resizeMode={'stretch'}
                                        style={styles.imagesLogo}
                                        source={require('../../assets/images/UHS.png')}
                                    />
                                    <Image
                                        resizeMode={'stretch'}
                                        style={styles.images}
                                        source={require('../../assets/images/Health_Tools_logo.png')}
                                    />
                                </View>
                                {
                                    this.state.visibleTap ? <View style={{
                                        minHeight: this.state.rotate ? hp('40%') : wp('50%'),
                                        position: 'absolute', width: this.state.rotate ? wp('100%') : hp('80%'),
                                        alignItems: 'center', justifyContent: 'center'
                                    }}>
                                        <Image
                                            resizeMode={'contain'}
                                            style={{
                                                opacity: 0.5,
                                                alignSelf: 'center',
                                                width: wp('20%%'),
                                                height: wp('20%')
                                            }}
                                            source={require('../../assets/images/tap_icon.png')}
                                        />
                                    </View> : null
                                }
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between', padding: hp('3%')

                                }}>

                                    <View style={{ flex: 1 }}>

                                        <Text style={{ fontSize: heading, ...testFont, fontWeight: 'bold', marginBottom: wp('1%') }}>{this.state.name}</Text>

                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.headingDataFirst}>Member ID</Text>
                                            <Text style={styles.headingDataSecond}>{
                                                this.state.source != "NEO"
                                                    ?
                                                    this.state.prefix + "" + this.state.empId
                                                    :
                                                    this.state.source == "NEO" ? this.state.memberId : ''}
                                            </Text>
                                        </View>

                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.headingDataFirst}>Group ID</Text>
                                            <Text style={styles.headingDataSecond}>{this.state.groupNo}</Text>
                                        </View>

                                    </View>


                                    <View style={{ flex: 1, alignItems: 'flex-start' }}>
                                        <Text style={{ fontSize: subHeading, ...testFont, marginBottom: wp('2%') }}>
                                            To Access Your Health Tools,Use These Contact Numbers:
                                        </Text>

                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('0.5%') }}>
                                            <Text style={styles.headingData}>Aetna Dental</Text>
                                            <Text style={styles.headingDataLeftMargin}>855-647-6764</Text>

                                        </View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('0.5%') }}>
                                            <Text style={styles.headingData}>Coast to Coast Vision</Text>
                                            <Text style={styles.headingDataLeftMargin}>855-647-6764</Text>

                                        </View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('0.5%') }}>
                                            <Text style={styles.headingData}>Telephonic EAP</Text>
                                            <Text style={styles.headingDataLeftMargin}>866-799-2998</Text>

                                        </View>

                                    </View>
                                </View>
                                <Text style={styles.frontFooter}>
                                    This program is <Text style={styles.backRegularTextBold}>NOT INSURANCE COVERAGE</Text>  and does not meet the minimum creditable coverage requirements under the Affordable Care Act or Massachusetts M.G.L. c. 111M and 956 CMR 5.00.

                                </Text>
                            </View>
                        </TouchableOpacity>
                        {/*second flip view*/}
                        <TouchableOpacity
                            activeOpacity={1}
                            ref={(ref) => this.refB = ref}
                            style={[styles.touchableCardBack, styles.touchableCardSecond]}
                            onPress={() => {
                                this.setState({ idTitle: 'Front of ID Card' })

                                this.card.flip()
                            }
                            }>

                            {/* PARAGRAPH View */

                                this.getBackData()
                            }

                            {
                                this.state.visibleTap ? <View style={{
                                    minHeight: this.state.rotate ? hp('40%') : wp('50%'),
                                    position: 'absolute', width: this.state.rotate ? wp('100%') : hp('80%'),
                                    alignItems: 'center', justifyContent: 'center'
                                }}>
                                    <Image
                                        resizeMode={'contain'}
                                        style={{
                                            opacity: 0.5,
                                            alignSelf: 'center',
                                            width: wp('20%%'),
                                            height: wp('20%')
                                        }}
                                        source={require('../../assets/images/tap_icon.png')}
                                    />
                                </View> : null
                            }


                            <View style={styles.backFooter}>
                                <Text style={{
                                    fontSize: smallTextCard, ...testFont,
                                    color: COLORS.textColor, flex: 1.1,
                                }} >www.UniversalHealthFellowship.org</Text>

                                <View style={{
                                    flexDirection: 'row', flex: 1.1,
                                }}>
                                    <Image
                                        resizeMode={'contain'}
                                        style={styles.backImage}
                                        source={require('../../assets/images/thank-you-for.png')} >
                                    </Image>
                                    <Image
                                        resizeMode={'contain'}
                                        style={styles.backImage}
                                        source={require('../../assets/images/sharing.png')} >
                                    </Image>
                                </View>

                                <Text style={{
                                    fontSize: smallTextCard, ...testFont,
                                    color: COLORS.textColor, flex: 0.8, textAlign: 'right'
                                }} >UHFHTR102820E110120</Text>
                            </View>
                        </TouchableOpacity>
                    </CardFlip>

                </View>

                <Loader loading={this.state.loading} />
                {this.showAlert()}
                <FabButton
                    style={{ margin: hp('3%') }}
                    canGoBack={this.state.canGoBack}
                    navigation={this.props.navigation}
                />
            </SafeAreaView>

        );
    }

    find_dimesions(layout) {
        const { x, y, width, height } = layout;
        console.warn(x);
        console.warn(y);
        console.warn(width);
        console.warn(height);

    }
    setContactandCardID(data) {
        data.planInfo.map((data, index) => {
            if (data.idcardField == "contact number") {
                this.setState({ contactNumber: data.fieldValue })
            }
            if (data.idcardField == "card id") {
                this.setState({ cardId: data.fieldValue }, () => {

                })
            }
            if (data.idcardField == "prefix") {
                this.setState({ prefix: data.fieldValue }, () => {

                })
            }
            if (data.idcardField == "group") {
                this.setState({ groupNo: data.fieldValue }, () => {

                })
            }
        })

    }

    getBackData() {

        return (
            <View style={{ paddingBottom: hp('3%') }}>

                <Text style={styles.backHeader} >
                    Discount Plan Organization: New Benefits, PO Box 803475, Dallas, TX 75380-3475, 800-800-7616
                </Text>

                <Text style={styles.backHeader} >
                    For other questions, call Universal HealthShare Customer Service: {this.state.contactNumber}
                </Text>

                <Text style={styles.backRegularText} >
                    <Text style={styles.backRegularTextBold} >Aetna Dental </Text>
                    provides discounts on dental procedures through a network of dental practice locations
                    nationwide. Members present this card with payment in full of the discounted fee, for immediate savings
                    when services are rendered. Actual costs and savings vary by provider, service and geographical area.
                    <Text style={styles.backRegularTextBold} >Call 855-647-6764 </Text>
                    for more information or to locate dental network providers. This dental discount program
                    is not available to VT residents.

                </Text>

                <Text style={styles.backRegularText} >
                    <Text style={styles.backRegularTextBold} > Coast to Coast VisionTM</Text> has contracted with 20,000+ locations nationwide. Prescription glasses and contact lenses are discounted 20% to 60% in most cases. Eye exams and surgery are discounted 10% to 30% where available. LASIK discounts are available. This network includes ophthalmologists, optometrists, and national chain locations such as Pearle Vision, JCPenney Optical, Target Optical, LensCrafters, Visionworks and QualSight LASIK.
                    <Text style={styles.backRegularTextBold} >Call 855-647-6764 </Text>
                    for more information or to locate vision network providers.
                </Text>

                <Text style={styles.backRegularText} >
                    <Text style={styles.backRegularTextBold} >
                        Telephonic EAP   </Text> provides free unlimited, confidential telephone counseling services 24/7. <Text style={styles.backRegularTextBold} >Call 866-799-2998</Text> for more information or to speak with an EAP counselor.
                    is a not-for-profit ministry that facilitates medical expense sharing through
                    Universal HealthShare Programs. Universal HealthShare is not insurance and does not guarantee that eligible medical
                    bills will be shared or otherwise paid. Universal HealthShare is not a discount card or program.
                </Text>
            </View>
        )
    }

    shareSnap(uri, previewSource, action) {
        if (action === 'share') {
            // const shareOptions = {
            //     title: 'Share via',
            //     message: 'Health Card',
            //     urls: [uri, previewSource],
            // };
            shareFile(uri, previewSource, "UHS_HealthTool_Card")

            this.setState({ loading: false, previewSource: null, res: null, visibleTap: true });
            // Share.open(shareOptions).then((Promise) => { console.log("Promise--------------" + Promise) }).catch(e => {
            //     console.log("Exception--------------", e.message)
            // });

        } else if (action === 'download') {

            renameAndDownloadNew(uri, previewSource, "UHS_HealthTool_Card").then(result => {
                if (result.includes("ERROR") || result.includes("error") || result.includes("Error")
                    || result.includes("failed") || result.includes("No such file or directory")) {
                    this.setState({ show: true, message: "Your HealthTools ID card download failed, Please try again.", filepath: '', loading: false, visibleTap: true })

                } else {
                    this.setState({ show: true, message: "Your HealthTools ID card has been downloaded.", filepath: result, previewSource: null, res: null, loading: false, visibleTap: true })

                }
            })
        } else if (action === 'pdf') {
            //  Alert.alert("pdf save successfully");
        }

    }

    snapshot(action) {
        this.setState({ visibleTap: false }, () => {
            captureRef(this.count == 0 ? this.refF : this.refB, this.state.value)
                .then(res =>

                    this.state.value.result !== "file" ? res
                        : new Promise((success, failure) => Image.getSize(res, (width, height) => (console.log(res, width, height), success(res)), failure)))
                .then(res => {

                    if (this.state.previewSource) {

                        this.count = 0;
                        this.shareSnap(res, this.state.previewSource, action);

                    } else {
                        this.setState({
                            error: '',
                            res: null,
                            previewSource: res
                        }, () => {
                            this.count = 1;
                            setTimeout(() => this.snapshot(action), 500);
                        });
                    }

                }).catch((error) => {
                    (this.setState({ error: '', res: null, previewSource: null }))
                });
        })

    }

    callbackClick(visible, title, button) {
        this.setState({ show: false, message: "" });
        if (this.state.filepath != "") {
            passwordCheck.showLocaleFile(this.state.filepath).then(result => {
                console.log(result)
            })
        }
    }
    showAlert() {
        return <AwesomeAlertDialog
            callbackClick={this.callbackClick.bind(this)}
            show={this.state.show}
            title={""}
            message={this.state.message}
            alertCancel={AppStrings.cancel}
            alertConfirm={AppStrings.ok}
            canBeClosed={false}
            type={""} />

    }
    downloadFilePermission = async () => {

        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );

        console.log("granted----------------", granted);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            this.setState({ loading: true });

            this.snapshot('download')
        } else {
            Alert.alert('Storage Permission Denied');
        }


    }
    sethealthcarddata() {
        if (
            this.state.plainId == '1001' ||
            this.state.plainId == '1002' ||
            this.state.plainId == '1003' ||
            this.state.plainId == '1004' ||
            this.state.plainId == '1005' ||
            this.state.plainId == '1006'
            || this.state.plainId == '1017'
            || this.state.plainId == '1018' || this.state.plainId == '1019' || this.state.plainId == '1020' || this.state.plainId == '1021' || this.state.plainId == '1022'
        ) {
            this.setState({

                showEmpId: false
            })
        }

        if (
            this.state.plainId == '7001' ||
            this.state.plainId == '7002' ||
            this.state.plainId == '7003' ||
            this.state.plainId == '7004' ||
            this.state.plainId == '7005' ||
            this.state.plainId == '7006'
            || this.state.plainId == '7017'
            || this.state.plainId == '7018' || this.state.plainId == '7019' || this.state.plainId == '7020' || this.state.plainId == '7021' || this.state.plainId == '7022'
        ) {
            this.setState({

                showEmpId: true
            })
        }

        if (
            this.state.plainId == '8001' ||
            this.state.plainId == '8002' ||
            this.state.plainId == '8003' ||
            this.state.plainId == '8004' ||
            this.state.plainId == '8005' ||
            this.state.plainId == '8006'
            || this.state.plainId == '8017'
            || this.state.plainId == '8018' || this.state.plainId == '8019' || this.state.plainId == '8020' || this.state.plainId == '8021' || this.state.plainId == '8022'
        ) {
            this.setState({

                showEmpId: true
            })
        }

        if (
            this.state.plainId == '9001' ||
            this.state.plainId == '9002' ||
            this.state.plainId == '9003' ||
            this.state.plainId == '9004' ||
            this.state.plainId == '9005' ||
            this.state.plainId == '9006'
            || this.state.plainId == '9017'
            || this.state.plainId == '9018' || this.state.plainId == '9019' || this.state.plainId == '9020' || this.state.plainId == '9021' || this.state.plainId == '9022'
        ) {
            this.setState({

                showEmpId: true
            })
        }

        if (
            this.state.plainId == '10001' ||
            this.state.plainId == '10002' ||
            this.state.plainId == '10003' ||
            this.state.plainId == '10004' ||
            this.state.plainId == '10005' ||
            this.state.plainId == '10006'

            || this.state.plainId == '10017'
            || this.state.plainId == '10018' || this.state.plainId == '10019' || this.state.plainId == '10020' || this.state.plainId == '10021' || this.state.plainId == '10022'
        ) {
            this.setState({

                showEmpId: true
            })
        }

        if (
            this.state.plainId == '11001' ||
            this.state.plainId == '11002' ||
            this.state.plainId == '11003' ||
            this.state.plainId == '11004' ||
            this.state.plainId == '11005' ||
            this.state.plainId == '11006'

            || this.state.plainId == '11017'
            || this.state.plainId == '11018' || this.state.plainId == '11019' || this.state.plainId == '11020' || this.state.plainId == '11021' || this.state.plainId == '11022'
        ) {
            this.setState({

                showEmpId: true
            })
        }

        if (
            this.state.plainId == '12001' ||
            this.state.plainId == '12002' ||
            this.state.plainId == '12003' ||
            this.state.plainId == '12004' ||
            this.state.plainId == '12005' ||
            this.state.plainId == '12006'

            || this.state.plainId == '12017'
            || this.state.plainId == '12018' || this.state.plainId == '12019' || this.state.plainId == '12020' || this.state.plainId == '12021' || this.state.plainId == '12022'
        ) {
            this.setState({

                showEmpId: true
            })
        }

        if (
            this.state.plainId == '6001' ||
            this.state.plainId == '6002' ||
            this.state.plainId == '6003' ||
            this.state.plainId == '6004' ||
            this.state.plainId == '6005' ||
            this.state.plainId == '6006'

            || this.state.plainId == '6017'
            || this.state.plainId == '6018' || this.state.plainId == '6019' || this.state.plainId == '6020' || this.state.plainId == '6021' || this.state.plainId == '6022'
        ) {
            this.setState({

                showEmpId: true
            })
        }

        if (this.state.plainId == '1011') {
            this.setState({

                showEmpId: false //not sure for other than 1011 plans
            })
        }


        if (this.state.plainId == '6011') {
            this.setState({

                showEmpId: true
            })
        }

        if (this.state.plainId == '13001' || this.state.plainId == '13002' || this.state.plainId == '13003' || this.state.plainId == '13004'
            || this.state.plainId == '13005' || this.state.plainId == '13006' || this.state.plainId == '13017'
            || this.state.plainId == '13018' || this.state.plainId == '13019' || this.state.plainId == '13020' || this.state.plainId == '13021' || this.state.plainId == '13022') {
            this.setState({

                showEmpId: true
            })
        }

        if (this.state.plainId == '7011') {
            this.setState({

                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '8011') {
            this.setState({

                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '9011') {
            this.setState({

                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '10011') {
            this.setState({

                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '11011') {
            this.setState({

                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '12011') {
            this.setState({

                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '13011') {
            this.setState({

                showEmpId: true //not sure for other than 1011 plans
            })
        }

        if (this.state.plainId == '14001' || this.state.plainId == '14002' || this.state.plainId == '14003' || this.state.plainId == '14004'
            || this.state.plainId == '14005' || this.state.plainId == '14006' || this.state.plainId == '14017'
            || this.state.plainId == '14018' || this.state.plainId == '14019' || this.state.plainId == '14020' || this.state.plainId == '14021' || this.state.plainId == '14022' || this.state.plainId == '14011') {
            this.setState({
                showEmpId: true
            })
        }

        if (this.state.plainId == '15001' || this.state.plainId == '15002' || this.state.plainId == '15003' || this.state.plainId == '15004'
            || this.state.plainId == '15005' || this.state.plainId == '15006' || this.state.plainId == '15017'
            || this.state.plainId == '15018' || this.state.plainId == '15019' || this.state.plainId == '15020' || this.state.plainId == '15021' || this.state.plainId == '15022' || this.state.plainId == '15011') {
            this.setState({
                showEmpId: true
            })
        }
    }
};

const styles = StyleSheet.create({

    imageBackground: {
        width: imageBackground,
        aspectRatio: 1, top: 0, bottom: 0, position: 'absolute', opacity: 0.5
    },
    imagesLogo: {
        width: imageLogo,
        height: wp('13%'),
    },
    images: {
        width: wp('34%'),
        height: wp('13%'),
    },
    mainCardContainer: {
        width: cardWidth,
        minHeight: cardHeight,
        borderRadius: 5,
    },
    touchableCard: {
        minHeight: cardHeight, borderRadius: 10,

    },
    touchableCardBack: {
        minHeight: cardHeight, borderRadius: 10,
        padding: '4%',
    },
    // landscape
    imageBackgroundRotate: {
        width: imageBackgroundH,
        aspectRatio: 1, top: 0, bottom: 0, opacity: 0.5,
        position: 'absolute',
    },
    imagesLogoRotate: {
        width: imageLogo,
        height: wp('13%'),
    },
    imagesRotate: {
        width: wp('34%'),
        height: wp('13%'),
    },
    mainCardContainerRotate: {
        width: cardWidthH,
        minHeight: cardHeightH,
        borderRadius: 5,
    },
    touchableCardRotate: {
        minHeight: cardHeightH,
    },
    touchableCardBackRotate: {
        minHeight: cardHeightH,
        padding: '4%',
    },
    //////////---END----//////////
    rowViewMarginTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: hp('0.5%'),
        alignSelf: 'baseline'
    },
    headingDetails: {
        flex: 1,
        fontSize: subHeading, ...testFont,
        color: COLORS.headingColor2
    },
    headingData: {
        flex: 1,
        fontSize: subHeading, ...testFont,
        fontWeight: 'bold',
        color: COLORS.headingColor
    },
    headingDataFirst: {
        flex: 0.4,
        fontSize: subHeading, ...testFont,
        fontWeight: 'bold',
        color: COLORS.headingColor
    },
    headingDataSecond: {
        flex: 0.6,
        fontSize: subHeading, ...testFont,
        fontWeight: 'bold',
        color: COLORS.headingColor
    },
    headingDataLeftMargin: {
        marginLeft: wp('1.2%'),
        flex: 1,
        fontSize: subHeading, ...testFont,
        alignSelf: 'center',
        fontWeight: 'bold',
        color: COLORS.headingColor2
    },
    touchableCardSecond: {
        backgroundColor: COLORS.white,
    },
    backImage: {
        height: hp('4%'),
        width: wp('15%'),
    },
    backPurpleBold: {
        fontSize: regular, ...testFont,
        color: COLORS.primaryColor,
        marginTop: hp('1%'),
        fontWeight: 'bold'
    },
    backRegularText: {
        fontSize: regular, ...testFont,
        color: COLORS.textColor,
        marginTop: hp('1%')
    },
    backRegularTextBold: {
        fontSize: regular, ...testFont,
        color: COLORS.textColor,
        marginTop: hp('2%'),
        fontWeight: 'bold'
    },
    backFooter: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center'
    },
    frontFooter: {
        fontSize: subHeading, ...testFont,
        fontWeight: '400',
        color: COLORS.textColor,
        marginBottom: hp('3%'),
        marginHorizontal: wp('3%'),
        textAlign: 'center'
    },
    backHeader: { fontSize: regular, ...testFont, color: COLORS.primaryColor, fontWeight: 'bold' }
});
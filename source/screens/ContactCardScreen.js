
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken, getCardEnable } from '../constants/CommonServices'
import BaseUrl from '../constants/BaseUrl';
import { Alert } from 'react-native';
import AppStrings from '../constants/AppStrings';
import { passwordCheck } from '../constants/utils';
import COLORS from '../constants/COLORS';
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import HomeFooter from './HomeFooter';
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../constants/FontsApp'
import ReleaseManagement from '../constants/Dev_Production';
import { getSendNeedsToContact } from '../constants/GetCardData';
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let errorMessage = AppStrings.tryAgain;
export default class ContactCardScreen extends Component {
    constructor(props) {
        super(props)
        this.navigation = this.props.navigation;
        this.state = {
            contactNumber: '',
            cardId: '',
            network: '',
            error: '',
            agentemail: '',
            agentname: '',
            agentno: '',
            enableCard: false,
        }
    }
    async componentDidMount() {
        //api call and 

        // if (this.navigation) {
        //     this.navigation.setOptions({
        //         title: AppStrings.ContactInformation,
        //         headerStyle: headerStyleContainer,
        //         headerTintColor: COLORS.white,
        //         headerTitleStyle: headerStyle,
        //     });
        // }
        console.log("ContactCardScreen----","ContactCardScreen")
        passwordCheck.applyHeader(this.navigation, AppStrings.ContactInformation)

        this.idcardData = JSON.parse(await AsyncStorage.getItem("GetIdCardData"));
        let provider_network = await AsyncStorage.getItem("provider_network");
        let AgentInformation = JSON.parse(await AsyncStorage.getItem("AgentInformation"));
        let cardEnable = false;
        getCardEnable(this.props.route.params.SpecialistArray, "ContactInformation").then(
            result => {
                cardEnable = result;
            }
        )

        this.setState({ network: provider_network ? provider_network : '', enableCard: cardEnable && cardEnable.enable ? cardEnable.enable : false });

        if (AgentInformation) {
            //get agent information from async storage 
            this.setState({
                agentemail: AgentInformation.response.email,
                agentname: AgentInformation.response.name,
                agentno: AgentInformation.response.phone,
            });
        }
        this.sourceID = await AsyncStorage.getItem("sourceID");

        if (this.idcardData) {

            //Contact number and card Id get
            this.setContactandCardID(this.idcardData[0]);

            //networl provider agentInfo get api calls
            this.getNetworkProviderData(this.idcardData[0].planId);
            this.agentInfoget(this.sourceID)
        }

    }
    agentInfoget(sourceid) {
        if (sourceid) {
            let url = BaseUrl.FABRIC_V4 + "enrollment/getAgentDetails/" + sourceid
            getAxiosWithToken(url, "").then(res => {

                if (res.data && res.data.code == 200 && res.data.response) {

                    this.setState({
                        agentemail: res.data.response.email,
                        agentname: res.data.response.name,
                        agentno: res.data.response.phone,
                    }, () => {
                        AsyncStorage.setItem("AgentInformation", JSON.stringify(res.data))
                    });
                } else {
                    console.log("Contact card get agent details", res.message);
                    this.setState({
                        agentemail: '',
                        agentname: '',
                        agentno: '',
                    });
                }

            })
        } else {
            this.setState({
                agentemail: '',
                agentname: '',
                agentno: '',
            });
        }
    }

    getNetworkProviderData(plainid) {
        console.log("this.plainid ", plainid, " this.sourceID " + this.sourceID);
        let url = BaseUrl.FABRIC_V7 + "/memberportal/getProviderNetwork/" + plainid;
        getAxiosWithToken(url, "").then(success => {

            if (success && success.data) {
                console.log("provider_network-Contact", success.data.provider_network);
                this.setState({ network: success.data.provider_network, error: '' }, () => {
                    AsyncStorage.setItem("provider_network", success.data.provider_network)
                })
            } else {
                let message = passwordCheck.responseError(success);
                this.setState({ error: message == "Error" ? errorMessage : message }, () => { });
            }

        }).catch(e => {
            this.setState({ error: e.message });
        });

    }
    setContactandCardID(data) {
        data.planInfo.map((data, index) => {
            if (data.idcardField == "contact number") {
                this.setState({ contactNumber: data.fieldValue })
            }
            if (data.idcardField == "card id") {
                this.setState({ cardId: data.fieldValue }, () => {

                })
            }
        })

    }
    render() {
        return (
            <View style={{ flex: 1 }} >
                {
                    this.state.error == ''
                        ?
                        <View style={{ flex: 1 }}>
                            {
                                this.getPhcsView()

                            }
                        </View>
                        :
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: hp("2%") }}>
                            <Text style={{ alignSelf: 'center', fontSize: regular, ...testFont,fontWeight:'bold' }}>{this.state.error}</Text>
                        </View>
                }

                <HomeFooter from="contact" isDashboard={false} isOtherScreen={true} callbackFooter={(from, isHome) => {
                    if (from === "Dashboard") {
                        this.props.navigation.goBack(null);
                    } else {
                        this.props.navigation.replace('Documents');
                    }

                }} />
            </View >
        )
    }
    getPhcsView() {
        return (<View style={{ padding: hp('3%') }}>
            <Text style={{ fontSize: regular, color: '#4f4f4f', ...testFont }}>For Pre-notification or Customer Service call:
            </Text>

            <Text
                onPress={() => {
                    passwordCheck.dialNumber(this.state.contactNumber)
                }}
                style={{ fontSize: regular, color: '#33ab8f', fontWeight: 'bold', ...testFont }}>{this.state.contactNumber}
            </Text>

            <Text style={{ fontSize: regular, color: '#4f4f4f', marginTop: hp("1%"), ...testFont }}>For Telemedicine call:
           </Text>

            <Text
                onPress={() => {
                    passwordCheck.dialNumber('1 (888) 501-2405')
                }}
                style={{ fontSize: regular, color: '#33ab8f', fontWeight: 'bold', ...testFont }}>1 (888) 501-2405
            </Text>
            <Text style={{ fontSize: regular, color: '#4f4f4f', marginTop: hp("1%"), ...testFont }}>Send needs to:
           </Text>

            <Text style={{ fontSize: regular, color: '#33ab8f', fontWeight: 'bold', ...testFont }}>{
                getSendNeedsToContact(this.state.network)
            }
            </Text>

            {
                this.state.agentname || this.state.agentno || this.state.agentemail
                    ?
                    <View>
                        <Text style={{
                            fontSize: regular, color: '#4f4f4f',
                            marginTop: hp("1%"), ...testFont
                        }}>Your Agent Details:</Text>
                        <Text style={{
                            fontSize: regular,
                            color: '#33ab8f', fontWeight: 'bold', ...testFont
                        }}>{this.state.agentname}</Text>

                        <Text
                            onPress={() => {
                                passwordCheck.dialNumber(this.state.agentno)
                            }}
                            style={{
                                fontSize: regular, color: '#33ab8f',
                                fontWeight: 'bold', ...testFont
                            }}>{this.state.agentno}</Text>

                        <Text
                            onPress={() => {
                                passwordCheck.openEmail(this.state.agentemail)
                            }}
                            style={{
                                fontSize: regular, color: '#33ab8f',
                                fontWeight: 'bold', ...testFont
                            }}>{this.state.agentemail}</Text>
                    </View>
                    :
                    null
            }



        </View>);
    }
    getSendNeedsTo() {
        switch (this.state.network) {
            case "PHCS":
                return 'P.O. Box 211223, Eagan, MN 55121';
            case "Smartshare":
                return 'P.O. Box 211223, Eagan, MN 55121';
            case "smartshare25":
                return 'PO Box 106 Rutherford, NJ 07070-0106';
            case "smartshare50":
                return 'PO Box 106 Rutherford, NJ 07070-0106';
            case "healthyLife":
                return 'PO Box 106 Rutherford, NJ 07070-0106';
            case "AFMC":
                return 'Arizona Foundation, PO Box 2909, Phoenix, AZ 85062-2909';
            default:
                break;
        }
    }
}







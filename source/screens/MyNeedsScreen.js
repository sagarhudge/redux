
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, Image, StyleSheet } from "react-native";
import { getGateWayToken, getAxiosUser, getAxiosWithToken, postAxiosWithToken, getData, postAxiosGetEmpId, getMyneedsEOS } from '../constants/CommonServices'

import Loader from "../components/Loader";
import COLORS from "../constants/COLORS"
import BaseUrl from '../constants/BaseUrl'
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from "../constants/AppStrings";
import moment from "moment";
import HomeFooter from '../screens/HomeFooter';
import { SafeAreaView } from "react-native";

import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../constants/FontsApp'
import { passwordCheck } from "../constants/utils";
import { FabButton } from "./botchat/FabButton";
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let headingCard = FontsApp.headingCard
let smallText = FontsApp.smallText
let errorMessage = "No needs have been entered into the system. Submitted needs may take 2-3 days to be displayed here. For any clarifications call Customer Service.";

export default class MyNeedsScreen extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.accessToken = '';
        this.offset = 0;
        this.previosPage = 0;
        this.email = '';
        this.state = {
            data: [],
            loading: false,
            error: ' ',
            loadMore: false,
            transactionId: '',
            canGoBack: this.props.route.params && this.props.route.params.key ? this.props.route.params.key : false,

        }

    }

    componentDidMount() {
        this.getData();
        passwordCheck.applyHeader(this.navigation, AppStrings.myNeeds);

    }

    async getData() {
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.email = await AsyncStorage.getItem('email');
        // this.getMyNeedsDetails();
        this.setState({ loading: true, error: '' }, () => { });
        postAxiosGetEmpId(this.email).then(response => {

            if (response && response.data && response.data.length > 0) {
                let id = response.data[0].empi;
                this.getNeeds(id);
            } else {
                this.setState({ loading: false, error: errorMessage }, () => { })

            }

        }).catch(e => {
            console.log("response------------------", response);

            this.setState({ loading: false, error: e.message }, () => { })
        });
    }

    getNeeds(id) {
        getMyneedsEOS(id).then(response => {
            if (response && response.data) {
                if (response.data.length > 0) {
                    console.log("response------------------", response.data);

                    this.setState({ loading: false, data: response.data }, () => { });
                } else {
                    this.setState({ loading: false, error: errorMessage }, () => { });
                }

            } else {
                let message = passwordCheck.responseError(success);
                this.setState({ loading: false, error: message == "Error" ? errorMessage : message }, () => { });
            }
        }).catch(e => {

            this.setState({ loading: false, error: e.message }, () => { });

        });
    }

    render() {
        return (

            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
                {this.state.error || this.state.data.length == 0 ?
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: hp("2%") }}>
                        <Text style={{ alignSelf: 'center', fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.error}</Text>
                    </View>
                    :
                    <FlatList
                        style={{ marginBottom: hp('7%') }}
                        showsVerticalScrollIndicator={true}
                        numColumns={1}
                        data={this.state.data}
                        renderItem={this.renderItem.bind(this)}
                        ListFooterComponent={
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Text
                                    style={{
                                        color: COLORS.textColor,
                                        padding: wp('1%'),
                                        fontSize: subHeading, ...testFont,
                                    }}
                                >{AppStrings.needsFooter}
                                </Text>
                            </View>}
                        keyExtractor={(item, index) => index.toString()}
                    />

                }

                <HomeFooter from="needs" isDashboard={false}
                    isOtherScreen={true}
                    callbackFooter={(from, isHome) => {

                        if (from === AppStrings.dashboard) {
                            this.props.navigation.goBack(null);
                        }

                        if (from === AppStrings.documents) {
                            this.props.navigation.replace(AppStrings.documents);
                        }

                    }} />
                <FabButton
                    style={{ margin: hp('8%') }}

                    canGoBack={this.state.canGoBack}
                    navigation={this.props.navigation}
                />
                <Loader loading={this.state.loading} />

            </SafeAreaView >

        );
    }

    renderItem({ item }) {
        var status = item.status.toUpperCase();
        if (status == "PAID" || status == "DENIED") {
            status = "COMPLETE"
        }

        return (
            <View
                style={{
                    borderBottomColor: COLORS.graya1, borderBottomWidth: 1.0
                }}>
                <TouchableOpacity onPress={() => {
                    this.setState({
                        transactionId:
                            (item.bill_key != this.state.transactionId) ?
                                item.bill_key : ''
                    })
                }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            resizeMode='contain'
                            style={{
                                height: wp("4%"), width: wp("4%"), marginTop: hp('12%'), marginLeft: wp('2%')
                            }}
                            source={this.state.transactionId != item.transactionId ? require('../assets/images/arrow_right.png') : require('../assets/images/arrow_down.png')}
                        />
                        <View style={{
                            padding: hp('2%'), width: wp('100%')
                        }}>

                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 0.6 }}>
                                    <Text style={styles.textStyleTitle}>Expense Number</Text>
                                    <Text style={styles.textStyle}>{item.bill_key}</Text>
                                </View>
                                <View style={{ flex: 0.4 }}>
                                    <View style={{
                                        alignSelf: 'center',
                                        paddingVertical: hp('1.3%'),
                                        paddingHorizontal: hp('3%'),
                                        borderRadius: 5,
                                        backgroundColor: status == 'COMPLETE' ? COLORS.green : COLORS.red

                                    }}>
                                        <Text
                                            style={{
                                                alignSelf: 'center',
                                                fontWeight: '700',
                                                fontSize: headingCard, ...testFont,
                                                color: COLORS.white,
                                            }}>{status}</Text>
                                    </View>
                                </View>
                            </View>

                            <Text style={styles.textStyleTitle}>Provider</Text>
                            <Text style={styles.textStyle}>{item.paid_provider_name}</Text>
                            <Text style={styles.textStyleTitle}>Member</Text>
                            <Text style={styles.textStyle}>{item.first_name + " "} {item.last_name}</Text>

                            <View style={{ flexDirection: 'row', flex: 1 }}>

                                <View style={{ flex: 1 }}>
                                    <Text style={styles.textStyleTitle}>Date of Service</Text>
                                    <Text style={styles.textStyle}>{moment(item.start_of_service_date, 'YYYY-MM-DDTHH: mm: ss').format('MMMM DD, YYYY')}</Text>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <Text style={styles.textStyleTitle}>Charged</Text>
                                    <Text style={styles.textStyle}>{"$" + (item.charged_amount != 0 ? item.charged_amount.toFixed(2) : 0)}</Text>
                                </View>

                            </View>
                        </View>
                    </View>
                    {
                        this.state.transactionId === item.bill_key
                            ?
                            <View style={{
                                paddingLeft: wp("10%"),
                                width: wp('100%'), paddingVertical: wp('2%'),
                                backgroundColor: "#f6f6f6",
                                borderTopColor: COLORS.divider,
                                borderTopWidth: 1
                            }}>

                                <View style={{ flex: 1, flexDirection: 'row', }}>

                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.textStyleTitle}>Ineligible for Sharing</Text>
                                        <Text style={styles.textStyle}>{"$" + (item.ineligible_amount != 0 ? item.ineligible_amount.toFixed(2) : 0)}</Text>
                                    </View>

                                    <View style={{ flex: 1, paddingRight: wp('3%') }}>
                                        <Text style={styles.textStyleTitle}>Discount to Billed Charges</Text>
                                        <Text style={styles.textStyle}>{"$" + (item.repricing_amount != 0 ? item.repricing_amount.toFixed(2) : 0)}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1 }}>



                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.textStyleTitle}>Eligible for Sharing</Text>
                                        <Text style={styles.textStyle}>{"$" + (item.eligible_for_sharing != 0 ? item.eligible_for_sharing.toFixed(2) : 0)}</Text>
                                    </View>

                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.textStyleTitle}>Non-Shareable Amount</Text>
                                        <Text style={styles.textStyle}>{"$" + (item.nsa != 0 ? item.nsa.toFixed(2) : 0)}</Text>
                                    </View>

                                </View>

                                <View style={{ flexDirection: 'row', flex: 1 }}>

                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.textStyleTitle}>Consultation Fee</Text>
                                        <Text style={styles.textStyle}>{"$" + (item.consultation_fee != 0 ? item.consultation_fee.toFixed(2) : 0)}</Text>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.textStyleTitle}>Member Responsibility</Text>
                                        <Text style={styles.textStyle}>{"$" + (item.member_responsibility != 0 ? item.member_responsibility : 0)}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1 }}>

                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.textStyleTitle}>Shared By UHF</Text>
                                        <Text style={styles.textStyle}>{"$" + (item.paid_amount != 0 ? item.paid_amount.toFixed(2) : 0)}</Text>
                                    </View>

                                </View>

                            </View>
                            : null
                    }
                </TouchableOpacity>
            </View>

        )
    }
    combineDate = (x, y) => {
        if (x == y) { return x }
        const z = (x + '-' + y)
        return z
    }

    getMyNeedsDetails() {
        var getMyneedsReport = BaseUrl.FABRIC_V7 + "memberportal/getMyneedsReport/" + this.email;
        this.setState({ loading: true, loadMore: false }, () => { });
        console.log("getMyNeedsDetails", getMyneedsReport);

        getAxiosUser(getMyneedsReport)
            .then(success => {

                if (success != null && success.data && success.status === 200) {
                    var response = success.data.pageList;
                    console.log("getMyNeedsDetails", response);
                    if (response.length > 0) {
                        this.setState({ data: response, loading: false, error: '' }, () => { });
                    } else {
                        this.setState({ loading: false, error: errorMessage }, () => { });
                    }
                } else {
                    let message = passwordCheck.responseError(success);
                    this.setState({ loading: false, error: message == "Error" ? errorMessage : message }, () => { });
                }

            }).catch(error => {
                console.log("error", error);
                this.setState({ loading: false, error: error.message }, () => { });

            });
    }

};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: regular, color: COLORS.textColor, ...testFont,
    },
    textStyleColor: {
        fontSize: regular, color: 'red', ...testFont,
    },
    textStyleTitle: {
        fontSize: smallText, color: COLORS.subText, marginTop: hp('1%'), ...testFont,
    },
});

import React, { Component } from 'react';
import { SafeAreaView, Text, View, Image, TouchableOpacity, DeviceEventEmitter } from 'react-native';
import Loader from '../components/Loader';
import AppStrings from '../constants/AppStrings';
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import { WebView } from 'react-native-webview';
import COLORS from '../constants/COLORS';
import { isAndroid } from '../constants/PlatformCheck';
import { testFont } from '../constants/FontsApp';
import Icon from 'react-native-vector-icons/Entypo';
import NativeToWeb from './botchat/NativeToWeb';

export default class NewEnrollMent extends Component {

    constructor(props) {
        super(props);

        this.WebViewRef;
        this.logout = false;

        this.state = {
            loading: true,
            url: this.props.route.params.url,
            isChat: this.props.route.params.isChat,

        }
        console.log("url---navigated-----", this.props.route.params.url)
        console.log(this.state.isChat)
    }
    componentDidMount() {
        if (this.state.isChat) {

        }
    }
    hideSpinner() {
        this.setState({ loading: false })
    }
    onMessage(message) {
        console.log("onMessage", message)
    }
    render() {
        const injectedJavascript = `(function() {
            const meta = document.createElement('meta'); 
            meta.setAttribute('content', 'width=device-width, 
            initial-scale=1, maximum-scale=1, user-scalable=no'); 
            meta.setAttribute('name', 'viewport'); 
            document.getElementsByTagName('head')[0].appendChild(meta);
          })();`;

        // const webViewScript = `
        //   setTimeout(function() { 
        //     window.ReactNativeWebView.postMessage("What is MRI?"); 
        //   }, 1000);
        //   true; // note: this is required, or you'll sometimes get silent failures
        // `;
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    {
                        !isAndroid && !this.state.isChat ? <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.goBack();
                            }}
                            style={{ backgroundColor: COLORS.background, flexDirection: 'row', alignItems: 'center' }}>
                            <Image
                                tintColor='black'
                                resizeMode='contain'
                                style={{
                                    transform: [{ rotate: '180deg' }], height: wp("4%"), width: wp("4%"), marginLeft: wp('2%'),
                                }}
                                source={require('../assets/images/arrow_right.png')}
                            />
                            <Text style={{ padding: wp('3%'), ...testFont, fontSize: wp('5%'), fontWeight: 'bold' }}>BACK</Text>

                        </TouchableOpacity> : !isAndroid && this.headerShow()

                    }
                    <WebView
                        sharedCookiesEnabled={true}
                        source={{ uri: this.state.url }}
                        onLoadEnd={() => this.hideSpinner()}
                        onMessage={this.onMessage.bind(this)}
                        ref={WEBVIEW_REF => (this.WebViewRef = WEBVIEW_REF)}
                        injectedJavaScript={injectedJavascript}
                        startInLoadingState={true}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        onLoadProgress={({ nativeEvent }) => {
                            console.log("NewEnrollMent", nativeEvent)
                            if (nativeEvent.url.endsWith("/login")) {
                                if (!this.logout) {
                                    this.logout = true;
                                    this.props.navigation.goBack();
                                }
                            }
                        }}
                    />
                    {/* <NativeToWeb/> */}
                    <Loader loading={this.state.loading} />
                </View>
            </SafeAreaView>
        )
    }

    headerShow() {
        return (<View style={{ flexDirection: 'row', backgroundColor: COLORS.primaryColor, height: hp('7%') }}>
            <Image
                style={{
                    width: wp('20%'),
                    resizeMode: 'cover',
                    height: hp('5%'), alignSelf: 'center',
                    marginHorizontal: wp('2%')
                }}
                source={require('../assets/images/ask_bomba_logo.png')}
            />
            <Text style={{
                ...testFont,
                width: wp('62%'),
                alignSelf: 'center',
                fontSize: wp('4.3%'), ...testFont, fontWeight: 'bold', color: COLORS.white
            }}>YOUR AI-POWERED ASSISTANT</Text>

            <TouchableOpacity onPress={() => {
                this.props.navigation.goBack();
            }} style={{ alignSelf: 'center', marginLeft: wp('2%') }}>
                <Icon name={"cross"} size={wp('8%')} color={COLORS.white} />
            </TouchableOpacity>

        </View>)
    }
}
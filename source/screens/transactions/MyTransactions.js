
import React, { Component } from "react";
import { Linking, Image, Alert, Text, TouchableOpacity, View, FlatList, SafeAreaView } from "react-native";

import styles from "../../constants/StyleSheet";
import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import BaseUrl from "../../constants/BaseUrl";
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken, getAxiosUser } from '../../constants/CommonServices'
import moment from "moment";
import { passwordCheck } from "../../constants/utils";
import AppStrings from "../../constants/AppStrings";
import AwesomeAlert from 'react-native-awesome-alerts';
import HomeFooter from '../HomeFooter';
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
let heading = FontsApp.heading;
let headingCardStatus = FontsApp.headingCardStatus
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let errorMessage = "My Transactions Data Not Available";

import { ButtonBorder } from "../../components/Buttons";
import { FabButton } from "../botchat/FabButton";


export default class MyTransactions extends Component {
    constructor(props) {
        super(props);

        this.navigation = this.props.navigation;
        this.source;
        this.sourceID = '';
        this.accessToken = '';
        this.offset = 0;
        this.previosPage = 0;
        this.nextPage = 1;
        this.email = '';
        this.name = '';

        this.state = {
            data: [],
            loading: false,
            error: ' ',
            loadMore: false,
            transactionId: '',
            accountnumber: '',
            show: false,
            confirmShow: false,
            title: '',
            alertTitle: '',
            alertMessage: '',
            alertFrom: '',
            errorMessage: '',
        }

    }

    async componentDidMount() {
        // this.getAsysnc();
        // if (this.navigation) {
        //     this.navigation.setOptions({
        //         title: AppStrings.myTransactions, //Set Header Title

        //         headerStyle: headerStyleContainer,
        //         headerTintColor: COLORS.white,
        //         headerTitleStyle: headerStyle,
        //     });
        // }
        passwordCheck.applyHeader(this.navigation, AppStrings.myTransactions)

        this.source = await AsyncStorage.getItem("source");
        console.log(" this.source ", this.source);
        this.email = await AsyncStorage.getItem("email");
        this.name = await AsyncStorage.getItem("username");
        console.log("this.name ", this.name);
        // this.getAccountNumber();


        if (this.source == "NEO") {
            this.getTransactionsNEO();
        } else {
            this.getTransactions();
        }


        // this.props.navigation.addListener('focus', () => {
        //     console.log("", "");

        //     this.getAccountNumber();
        // });

    }
    getTransactionsNEO() {
        this.setState({ loading: true });

        let url = BaseUrl.FABRIC_V7 + "memberportal/getMytransactionReport/" + this.email + '&page=' + this.offset + '&size=10'

        console.log("url", this.source, url);
        getAxiosUser(url).then(response => {

            if (response && response.data && response.data.pageList) {
                console.log("response", response.data.pageList);

                if (response.data.pageList.length > 0) {
                    this.previosPage = this.offset;
                    this.setState({ data: [...this.state.data, ...response.data.response], loading: false, error: '' }, () => {
                        this.offset = this.offset + 1;
                        let totalCount = response.data.totalrecords
                        this.setState({ loadMore: totalCount > 10 && totalCount != this.state.data.length })

                    });
                } else {
                    this.setState({ loading: false, error: this.state.data.length > 0 ? '' : 'My Transactions Data Not Available' }, () => {
                        this.setState({ loadMore: false })
                    });
                }

            } else {
                this.nextPage = 0;
                let message = passwordCheck.responseError(response);
                this.setState({ error: message == "Error" ? errorMessage : message ,loading: false,}, () => { });
            }

        }).catch(e => {
            this.setState({ loading: false, error: e.message }, () => { });
        })
    }
    async getTransactions() {


        this.setState({ loading: true });

        this.sourceID = await AsyncStorage.getItem("sourceID");

        let url = BaseUrl.FABRIC_V6 + "transaction/transactionHistory?searchKey=source&searchValue=" + this.sourceID + "&orderKey=createdDate&order=desc" + '&page=' + this.offset + '&size=10'

        console.log("url", this.source, url);
        getAxiosUser(url).then(response => {
            if (response && response.data && response.data.response) {
                if (response.data.response.length != 0) {
                    this.previosPage = this.offset;
                    this.setState({ data: [...this.state.data, ...response.data.response], loading: false, error: '' }, () => {
                        this.offset = this.offset + 1;
                        let totalCount = response.data.response[0].totalRecord
                        this.setState({ loadMore: totalCount > 10 && totalCount != this.state.data.length })
                    });
                } else {
                    this.setState({ loading: false, error: this.state.data.length != 0 ? '' : 'My Transactions Data Not Available' }, () => {
                        this.setState({ loadMore: false })
                    });
                }

            } else {
                this.nextPage = 0;
                let message = passwordCheck.responseError(response);
                this.setState({ error: message == "Error" ? errorMessage : message }, () => { });
            }

        }).catch(e => {
            this.setState({ loading: false, error: e.message }, () => { });
        })
    }
    async getAccountNumber() {

        this.sourceID = await AsyncStorage.getItem("sourceID");

        this.setState({ loading: true }, () => {

        });
        let url = BaseUrl.FABRIC_V6 + "transaction/getLast4AccountNumber/" + this.sourceID;
        console.log("getAccountNumber--------------------", url);
        getAxiosUser(url).then(response => {

            if (response && response.data && response.data.response) {
                let number = response.data.response;
                console.log("number----------------", number);

                this.setState({ accountnumber: number, loading: false, }, () => {
                });

            } else {
                let message = passwordCheck.responseError(response);
                console.log("message", message);
                this.setState({ error: '', loading: false }, () => { });
            }

        }).catch(e => {
            this.setState({ loading: false, error: e.message }, () => { });
        })
    }

    async getAsysnc() {
        this.sourceID = await AsyncStorage.getItem("sourceID");
    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>

                <View style={{ backgroundColor: COLORS.white, flex: 1 }}>

                    {
                        this.state.error
                            ?
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: hp("2%") }}>
                                <Text style={{ alignSelf: 'center', fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.error}</Text>
                            </View>
                            :
                            <View >
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    numColumns={1}
                                    data={this.state.data}
                                    renderItem={this.renderItem.bind(this)}
                                    style={{ marginBottom: hp('14%') }}
                                    onEndReachedThreshold={1}
                                    ListFooterComponent={
                                        this.state.loadMore ? <View style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-around'
                                        }}>
                                            {
                                                <Text
                                                    onPress={() =>
                                                        this.goToNext()
                                                    }
                                                    style={styles.footerActive}>
                                                    {`Load More`}</Text>
                                            }
                                        </View> : null}

                                    keyExtractor={(item, index) => index.toString()}
                                />

                            </View>
                    }
                    <View style={{ marginBottom: hp('7%'), position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: COLORS.blueL, width: '100%' }}>
                        {/* <Text
                            onPress={() => {
                                if (this.source == 'NEO')
                                    this.showAlert("", "To change your payment method, call Member Services team at :\n(888) 366-6243, Monday through Friday 8:00 am to 8:00 pm CST.", "OK");
                                else
                                    this.props.navigation.navigate(AppStrings.ChnagePaymentMethod)
                            }}
                            style={{
                                color: 'white', borderRadius: FontsApp.borderRadiusRound, ...testFont, fontSize: wp('4%'), fontWeight: 'bold', alignSelf: 'center',
                                paddingHorizontal: wp('8%'), paddingVertical: wp('2%'), borderWidth: 2, borderColor: 'white', margin: wp('1.5%')
                            }}>CHANGE PAYMENT METHOD</Text> */}

                        <ButtonBorder title={"CHANGE PAYMENT METHOD"}
                            isDisabled={true} type="blue"
                            callback={() => {
                                if (this.source == 'NEO')
                                    this.showAlert("", "To change your payment method, call Member Services team at :\n(888) 366-6243, Monday through Friday 8:00 am to 8:00 pm CST.", "OK");
                                else
                                    this.props.navigation.navigate(AppStrings.ChnagePaymentMethod)

                            }}
                        />
                    </View>

                    <HomeFooter from="transaction" isDashboard={false} isOtherScreen={true} callbackFooter={(from, isHome) => {
                        if (from === AppStrings.dashboard) {
                            this.props.navigation.goBack(null);
                        }
                        if (from === AppStrings.documents) {
                            this.props.navigation.navigate(AppStrings.documents);
                        }

                        if (from === AppStrings.MyNeedsScreen) {
                            this.props.navigation.navigate(AppStrings.MyNeedsScreen);
                        }

                    }} />
                    {this.displayAlert()}
                    <Loader loading={this.state.loading} />
                    <FabButton
                        style={{ margin: hp('16%') }}
                        canGoBack={this.state.canGoBack}
                        navigation={this.props.navigation}
                    />

                </View>
            </SafeAreaView>

        );
    }
    goToPrevios() {
        if (this.previosPage > 0) {
            this.offset = this.previosPage - 1;
            this.getTransactions();
        } else {
            this.offset = 0;
        }

    }
    goToNext() {
        this.getTransactions();
    }
    renderItem({ item }) {

        return (
            <TouchableOpacity
                style={{
                    backgroundColor: item.status === 'sent' ? '#decdf1' : COLORS.white,
                    flexDirection: 'row', flex: 1, alignItems: 'center',
                    borderBottomColor: COLORS.graya1, borderBottomWidth: 1.0
                }}
                onPress={() => { this.setState({ transactionId: (item.transactionId != this.state.transactionId) ? item.transactionId : '' }) }}>
                <View>
                    <View style={{ flexDirection: 'row', paddingVertical: wp('4%') }}>
                        <Image
                            resizeMode='contain'
                            style={{
                                height: wp("4%"), width: wp("4%"), marginTop: hp('8%'), marginLeft: wp('2%')
                            }}
                            source={this.state.transactionId != item.transactionId ? require('../../assets/images/arrow_right.png') : require('../../assets/images/arrow_down.png')}
                        />
                        <View style={{ width: wp('92%'), padding: wp('2%') }}>

                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 0.6 }}>
                                    <Text style={{
                                        color: COLORS.subText,
                                        fontSize: smallText, ...testFont,

                                    }}>Transaction ID</Text>

                                    <Text style={{
                                        color: COLORS.black,
                                        fontSize: regular, ...testFont,
                                    }}>{item.transactionId ? item.transactionId : "NA"}</Text>

                                </View>
                                {
                                    item.authorizationMessage ? <View style={{ flex: 0.4, alignItems: 'center' }}>

                                        <View style={{
                                            color: 'white',
                                            paddingVertical: hp('1.3%'),
                                            borderRadius: wp('2%'),
                                            backgroundColor: passwordCheck.statusColourCode(item.authorizationMessage),
                                        }}>
                                            <Text style={{
                                                color: 'white',
                                                paddingHorizontal: wp('3%'),
                                                fontWeight: 'bold', ...testFont,
                                                fontSize: headingCardStatus,
                                            }}>
                                                {item.authorizationMessage}
                                            </Text>
                                        </View>

                                    </View> : null
                                }
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')
                                    }}>Transaction Date</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>{moment(item.createdDate).format('MMMM DD, YYYY')}</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')

                                    }}>Total Amount</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>{item.transactionAmount ? "$" + item.transactionAmount.toFixed(2) : 'NA'}</Text>

                                </View>
                            </View>
                            <Text style={{
                                color: COLORS.subText,
                                fontSize: smallText, ...testFont,
                                marginTop: hp('1%')
                            }}>Payment Method</Text>
                            <Text style={{
                                color: COLORS.black, ...testFont,
                                fontSize: regular,
                            }}>{item.type}</Text>
                        </View>

                    </View>
                    {this.state.transactionId === item.transactionId
                        ?
                        <View style={{
                            paddingLeft: wp("7%"),
                            width: wp('100%'), paddingVertical: wp('4%'),
                            backgroundColor: "#f6f6f6",
                            borderTopColor: COLORS.divider,
                            borderTopWidth: 1
                        }}>

                            <Text style={{
                                color: COLORS.subText, ...testFont,
                                fontSize: smallText,

                            }}>Monthly Share Contribution</Text>

                            <Text style={{
                                color: COLORS.black, ...testFont,
                                fontSize: regular,

                            }}>{item.monthlyShare ? "$" + item.monthlyShare.toFixed(2) : 'NA'}</Text>

                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText, marginRight: wp('1.5%'),
                                        marginTop: hp('1%')

                                    }}>UHF Monthly Membership Fee</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>

                                        {
                                            item.uhfMonthlyFee ? "$" + item.uhfMonthlyFee.toFixed(2) : 'NA'
                                        }

                                    </Text>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')

                                    }}>Application Fee</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>

                                        {item.applicationFee ? "$" + item.applicationFee.toFixed(2) : 'NA'}

                                    </Text>

                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')

                                    }}>Refund Amount</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>
                                        {
                                            item.refundAmount ? "$" + item.refundAmount.toFixed(2) : 'NA'
                                        }
                                    </Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text style={{
                                        color: COLORS.subText, ...testFont,
                                        fontSize: smallText,
                                        marginTop: hp('1%')

                                    }}>Refund Description</Text>

                                    <Text style={{
                                        color: COLORS.black, ...testFont,
                                        fontSize: regular,
                                    }}>

                                        {item.refundDescription != null ? item.refundDescription : 'NA'}

                                    </Text>

                                </View>
                            </View>

                        </View> : null
                    }

                </View>
            </TouchableOpacity>
        )
    }

    displayAlert() {
        return <AwesomeAlert
            show={this.state.show}
            showProgress={false}
            title={this.state.alertTitle}
            message={this.state.alertMessage}
            closeOnTouchOutside={false}
            closeOnHardwareBackPress={false}
            showCancelButton={this.state.confirmShow}
            titleStyle={{ color: COLORS.black }}
            messageStyle={{ color: COLORS.black }}
            showConfirmButton={true}
            cancelText={AppStrings.cancel}
            confirmText={AppStrings.ok}
            confirmButtonColor={COLORS.primaryColor}
            cancelButtonColor={COLORS.cancelButton}

            onCancelPressed={() => {
                this.setState({ show: false });
            }}
            onConfirmPressed={() => {
                this.setState({ show: false });

            }}
        />
    }

    showAlert = (title, message, from) => {
        this.setState({
            show: true, alertTitle: title, alertFrom: from, alertMessage: message
        }, () => { });
    };
};

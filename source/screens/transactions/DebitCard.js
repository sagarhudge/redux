
import React, { Component } from 'react';
import { View, Text, StyleSheet, KeyboardAvoidingView } from 'react-native';

import stylesCommon from "../../constants/StyleSheet";

import COLORS from '../../constants/COLORS';
import TextInput from "react-native-material-textinput";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Picker, Icon } from "native-base";
import { isAndroid } from '../../constants/PlatformCheck';

import { FontsApp, testFont } from '../../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular

let inputTextHeight = FontsApp.inputTextHeight;
let lineHeight = hp("0.2%");
import AsyncStorage from '@react-native-community/async-storage';

export default class DebitCard extends Component {
    constructor(props) {
        super(props)
        console.log("DebitCard ", this.props.reset, this.props.source, this.props.name);
        // console.log("DebitCard ", this.props);
        this.state = {
            error: "",
            bankName: "",
            nameOnAccount: this.props.name,
            source: this.props.source,
            routungNumber: "",
            accountNumber: "",
            selectedValue: "first",
            errorId: null,

        }
    }
    componentDidMount() {
        
        this.getAsync();
    }
    async getAsync() {
        let sourceID = await AsyncStorage.getItem("sourceID");
        let name = await AsyncStorage.getItem("username");
        this.setState({ source: sourceID, nameOnAccount: name })
    }
    callBackSaveCard = () => {
        console.log("callBackSaveCard " + this.state.selectedValue, this.state.source)
        let object = {
            'accountName': this.state.nameOnAccount,
            'accountNumber': this.state.accountNumber,
            'accountType': this.state.selectedValue,
            'bankName': this.state.bankName,
            // 'cardNumber': "",
            //  'cvv': "",
            // 'expiryMonth': "",
            //  'expiryYear': "",
            'holderName': this.state.nameOnAccount,
            'paymentType': "ACH",
            'routingNo': this.state.routungNumber,
            'source': this.state.source,
        }
        console.log("----------------------", this.state.source)
        if (!this.state.bankName.trim()) {
            // this.setState({ error: 'Bank name required', errorId: 0 })
            this.props.saveCard(null, "debit")
        }
        else if (!this.state.nameOnAccount.trim()) {
            // this.setState({ error: 'Name on account required', errorId: 1 })
            this.props.saveCard(null, "debit")
        }
        else if (!this.state.selectedValue.trim() || this.state.selectedValue === 'first') {
            // this.setState({ error: 'Select account type', errorId: 4 })
            this.props.saveCard(null, "debit")
        }
        else if (this.state.routungNumber.length != 9) {
            // this.setState({
            //     error: this.state.routungNumber != ""
            //         && this.state.routungNumber.length != 9 ? 'Routing number up to 9 digits' : 'Routing number required'
            //     , errorId: 2
            // }, () => { });
            this.props.saveCard(null, "debit")
        }
        else if (this.state.accountNumber.length != 17) {
            this.setState({ error: 'Account number required', errorId: 3 })
            this.props.saveCard(null, "debit")
        }
        else {
            this.props.saveCard(object, "debit")
        }
    }

    onChangeCode = (name) => {

        if (name !== "") {
            this.setState({ bankName: name, error: '', errorId: null }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ bankName: "", error: 'Bank name required', errorId: 0 }, () => {
                this.callBackSaveCard();
            });
        }

    }
    onChangeName = (nameOnAccount) => {

        if (nameOnAccount !== "") {
            this.setState({ nameOnAccount: nameOnAccount, error: '', errorId: null }, () => { this.callBackSaveCard(); });
        } else {
            this.setState({ nameOnAccount: "", error: 'Name on account required', errorId: 1 }, () => { this.callBackSaveCard(); });
        }

    }
    onRoutingNumberChange = (routungNumber) => {

        if (routungNumber !== "") {
            this.setState({ routungNumber: routungNumber, error: routungNumber.length < 9 ? 'Routing number up to 9 digits' : '', errorId: routungNumber.length < 9 ? 2 : null }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ routungNumber: '', error: 'Routing number required', errorId: 2 }, () => { this.callBackSaveCard(); });

        }


    }
    onAccountNumberChange = (accountNumber) => {

        if (accountNumber !== "") {
            this.setState({ accountNumber: accountNumber, error: accountNumber.length < 17 ? 'Account number up to 17 digits' : '', errorId: accountNumber.length < 17 ? 3 : null }, () => { this.callBackSaveCard(); });
        } else {
            this.setState({ accountNumber: '', error: 'Account number required', errorId: 3 }, () => { this.callBackSaveCard(); });
        }

    }
    render() {
        return (

            <KeyboardAvoidingView
                style={{ flex: 1 }}
                behavior={isAndroid ? null : 'padding'}   >
                <View style={{
                    alignSelf: 'center', width: wp('100%'), paddingLeft: wp('5%'),
                    paddingRight: wp('5%'), flex: 1, marginBottom: hp('20%')
                }} >
                    <TextInput
                        marginTop={15}
                        labelActiveTop={-30}
                        value={this.state.bankName}
                        fontSize={inputTextHeight}
                        label='Bank Name*'
                        paddingLeft={10}
                        error={this.state.errorId == 0 ? this.state.error : ""}
                        labelActiveColor={COLORS.primaryColor}
                        underlineColor={COLORS.primaryColor}
                        underlineActiveColor={COLORS.primaryColor}
                        secureTextEntry={false}
                        underlineHeight={lineHeight}
                        onChangeText={this.onChangeCode}
                    />
                    <TextInput
                        marginTop={15}
                        labelActiveTop={-30}
                        value={this.state.nameOnAccount}
                        fontSize={inputTextHeight}
                        label='Name On Account*'
                        error={this.state.errorId == 1 ? this.state.error : ""}
                        paddingLeft={10}
                        labelActiveColor={COLORS.labelActiveColor}
                        underlineColor={COLORS.underlineColor}
                        underlineActiveColor={COLORS.underlineActiveColor}
                        secureTextEntry={false}
                        underlineHeight={lineHeight}
                        onChangeText={this.onChangeName}
                    />

                    <View style={{
                        borderColor: COLORS.divider, borderBottomWidth: 1.5,
                        marginBottom: hp('1.5%')
                    }}>
                        <Picker
                            selectedValue={this.state.selectedValue}
                            mode="dropdown"
                            iosHeader="Select account Type"
                            iosIcon={<Icon name="ios-arrow-down" />}
                            itemStyle={{ color: COLORS.textColor }}
                            style={{ width: wp('90%'), marginHorizontal: -9 }}
                            onValueChange={(itemValue, itemIndex) => {
                                this.setState({ error: '', selectedValue: itemValue, errorId: null })
                            }}  >
                            <Picker.Item style={styles.textStylePicker} label={'Select account Type'} value="first" />

                            <Picker.Item style={styles.textStylePicker} label="CHECKING" value="CHECKING" />
                            <Picker.Item style={styles.textStylePicker} label="SAVINGS" value="SAVINGS" />
                        </Picker>
                        {
                            this.state.errorId == 4 ? <Text style={stylesCommon.errorRegular}>{this.state.error}</Text> : null
                        }

                    </View>

                    <TextInput
                        keyboardType="numeric"
                        labelActiveTop={-30}
                        value={this.state.routungNumber}
                        fontSize={inputTextHeight}
                        label='Routing Number*'
                        error={this.state.errorId == 2 ? this.state.error : ""}
                        paddingLeft={10}
                        labelActiveColor={COLORS.primaryColor}
                        underlineColor={COLORS.primaryColor}
                        underlineActiveColor={COLORS.primaryColor}
                        secureTextEntry={false}
                        maxLength={9}
                        underlineHeight={lineHeight}
                        onChangeText={this.onRoutingNumberChange}
                    />

                    <TextInput
                        marginTop={15}
                        keyboardType="numeric"
                        labelActiveTop={-30}
                        value={this.state.accountNumber}
                        fontSize={inputTextHeight}
                        label='Account Number*'
                        error={this.state.errorId == 3 ? this.state.error : ""}
                        paddingLeft={10}

                        labelActiveColor={COLORS.primaryColor}
                        underlineColor={COLORS.primaryColor}
                        underlineActiveColor={COLORS.primaryColor}
                        secureTextEntry={false}
                        maxLength={17}
                        underlineHeight={lineHeight}
                        onChangeText={this.onAccountNumberChange}
                    />


                </View >


            </KeyboardAvoidingView>
        )
    }


}

const styles = StyleSheet.create({

    textStylePicker: { fontSize: inputTextHeight, ...testFont },
});






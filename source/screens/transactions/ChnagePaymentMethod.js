import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, Image, KeyboardAvoidingView } from "react-native";
import { getGateWayToken, getAxiosUser, getAxiosWithToken, postAxiosWithToken, getData } from '../../constants/CommonServices'

import Loader from "../../components/Loader";
import COLORS from "../../constants/COLORS"
import BaseUrl from '../../constants/BaseUrl'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from "../../constants/AppStrings";
import moment from "moment";
import HomeFooter from '../HomeFooter';
import DividerH from '../../components/DividerH';
import DebitCard from '../../screens/transactions/DebitCard';
import CreditCard from '../../screens/transactions/CreditCard';
import { isAndroid } from "../../constants/PlatformCheck";
import styles from "../../constants/StyleSheet";
import { SafeAreaView } from "react-native";
import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../../constants/FontsApp'
import AwesomeAlertDialog from '../../components/AwesomeAlertDialog';
import { passwordCheck } from "../../constants/utils";

let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let headingCardLarge = FontsApp.headingCardLarge
import { ButtonBorderEnable } from "../../components/Buttons";
import { FabButton } from "../botchat/FabButton";

export default class ChnagePaymentMethod extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.accessToken = '';
        this.email = '';
        this.source = '';
        this.sourceID = '';

        this.state = {
            data: [],
            loading: false,
            error: '',
            loadMore: false,
            accountNumber: '',
            name: '',
            type: true,
            object: null,
            typeName: '',
            reset: false,
            show: false,
            message: '',
            title: "",
            alertCancel: AppStrings.cancel,
            alertConfirm: 'OK',
            alertCLose: AppStrings.cancel,
            canBeClosed: false,
            alertType: 'ok',
            doneDisable: true,
        }

    }

    componentDidMount() {

        passwordCheck.applyHeader(this.navigation, AppStrings.ChnagePayment)
        console.log("Start of the function")
        this.getAsync();
        for(let i = 0; i < 20; i++){
            console.log("Start of the function",i)
        }

    }
    async getAsync() {
        this.source = await AsyncStorage.getItem("source");
        this.email = await AsyncStorage.getItem("email");
        this.sourceID = await AsyncStorage.getItem("sourceID");
        this.name = await AsyncStorage.getItem("username");
        this.accessToken = await AsyncStorage.getItem("accessToken");

        this.getAccountNumber();
        this.setState({ error: this.sourceID ? '' : 'Source is not registered', name: this.name })

    }

    getAccountNumber() {
        this.setState({ loading: true }, () => {
        });
        let url = BaseUrl.FABRIC_V6 + "transaction/getLast4AccountNumber/" + this.sourceID;
        console.log("getAccountNumber--------------------", url);
        getAxiosUser(url).then(response => {

            if (response && response.data && response.data.response) {
                let accNumber = response.data.response;

                let number = "";
                let message = "";
                let show = accNumber.includes('error_message');
                if (show) {
                    number = "";
                    message = JSON.parse(accNumber).error_message;
                }
                else {
                    number = accNumber;
                }
                console.log("getAccountNumber----------------", number, " message ", message, " dialog show ", show);

                this.setState({ accountNumber: number, loading: false, show: show, message: message }, () => {
                });

            } else {
                let message = passwordCheck.responseError(response);
                console.log("message", message);
                this.setState({ error: '', loading: false }, () => { });
            }

        }).catch(e => {
            this.setState({ loading: false, error: e.message }, () => { });
        })
    }
    render() {
        return (
            <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
                <View>

                    <ScrollView  >
                        <View>

                            <View style={{ backgroundColor: COLORS.white, flex: 1, }}>
                                <Text style={{ color: COLORS.textColor, ...testFont, fontSize: regular, padding: wp('3%') }}>
                                    The fellowship incurs additional fees related to processing credit cards that we do not incur when
                                    processing ACH payments. We therefore encourage our members to pay by ACH, yet offer credit card payments
                                    as an option as well. If you choose to pay by credit card, you will incur an additional 3.5% fee to cover
                                    the fellowship’s cost.
                                </Text>


                                <View style={{
                                    flexDirection: 'row', padding: wp('3%'),
                                    alignItems: 'center',
                                    backgroundColor: COLORS.background,
                                    borderWidth: 1,
                                    borderColor: COLORS.black,
                                    borderRadius: wp('1.5%'),
                                    marginHorizontal: wp('4%'),
                                    marginTop: wp('2%')
                                }}>
                                    <Text style={{ color: COLORS.grayDark, ...testFont, fontSize: regular, flex: 1 }}>{`Account Number\ncurrently on file`}</Text>
                                    <Text style={{ color: COLORS.grayDark, flex: 1, ...testFont, fontSize: regular, color: COLORS.black }}>{this.state.accountNumber ? this.state.accountNumber : 'NA'}</Text>
                                </View>
                                {
                                    this.state.error != ""
                                        ?
                                        <View style={{ marginLeft: wp('4%') }}>
                                            <Text style={styles.errorContainer}>{this.state.error}</Text>
                                        </View>
                                        : null
                                }

                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp('2%'), }}>
                                    <TouchableOpacity
                                        onPress={() => { this.setState({ type: true, error: '', doneDisable: true }) }}
                                        style={{
                                            flexDirection: 'row', padding: wp('3%'),
                                            flex: 1, justifyContent: 'center'
                                        }}>
                                        <Image
                                            resizeMode='contain'
                                            style={{ height: wp('5%'), width: wp('5%') }}
                                            source={this.state.type ? require('../../assets/images/radioButtonChecked.png') : require('../../assets/images/radioButtonUnchecked.png')}
                                        />

                                        <Text style={{
                                            color: this.state.type ? COLORS.primaryColor : COLORS.textColor,
                                            marginLeft: wp('3%'), ...testFont, fontSize: regular, fontWeight: 'bold',
                                        }}>ACH DEBIT</Text>

                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => { this.setState({ type: false, error: '', doneDisable: true }) }}
                                        style={{ flexDirection: 'row', padding: wp('3%'), flex: 1, justifyContent: 'center', }}>
                                        <Image
                                            resizeMode='contain'
                                            style={{ height: wp('5%'), width: wp('5%') }}
                                            source={!this.state.type ? require('../../assets/images/radioButtonChecked.png') : require('../../assets/images/radioButtonUnchecked.png')}
                                        />

                                        <Text style={{
                                            color: !this.state.type ? COLORS.primaryColor : COLORS.textColor,
                                            marginLeft: wp('3%'), fontWeight: 'bold',
                                            ...testFont, fontSize: regular
                                        }}>CREDIT CARD</Text>

                                    </TouchableOpacity>

                                </View>
                                {
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ borderBottomWidth: 2, flexDirection: 'row', borderBottomColor: this.state.type ? COLORS.primaryColor : COLORS.divider, flex: 1 }} />
                                        <View style={{ borderBottomWidth: 2, flexDirection: 'row', borderBottomColor: !this.state.type ? COLORS.primaryColor : COLORS.divider, flex: 1 }} />

                                    </View>
                                }
                                {/* View Pager Credit and Debit */}
                                {
                                    this.state.type
                                        ?
                                        <DebitCard name={this.state.name}
                                            source={this.sourceID}
                                            reset={this.state.reset}

                                            navigation={this.props.navigation}
                                            saveCard={(data, type) => this.saveCard(data, type)} />
                                        :
                                        <CreditCard name={this.state.name}
                                            source={this.sourceID}
                                            reset={this.state.reset}
                                            navigation={this.props.navigation}
                                            saveCard={(data, type) => this.saveCard(data, type)} />
                                }

                            </View>

                        </View>
                    </ScrollView>

                    <View style={{
                        flexDirection: 'row', backgroundColor: COLORS.blueL,
                        marginBottom: hp('7%'), position: 'absolute', bottom: 0
                    }}>

                        <ButtonBorderEnable title={"CANCEL"}
                            isDisabled={false} type="blue"
                            callback={() =>
                                this.props.navigation.navigate(AppStrings.MyTransactionsNav)
                            } />

                        <ButtonBorderEnable
                            title={"DONE"}
                            isDisabled={this.state.doneDisable}
                            type="blue"
                            callback={() => { this.state.doneDisable ? null : this.callBackSaveCard() }
                            } />
                    </View>

                    <HomeFooter from="change_payment" isDashboard={false} isOtherScreen={true} callbackFooter={(from, isHome) => {
                        if (from === "Dashboard") {
                            this.props.navigation.goBack(null);
                        } else {
                            this.props.navigation.replace('Documents');
                        }

                    }} />

                    <AwesomeAlertDialog
                        callbackClick={this.callbackClick.bind(this)}
                        show={this.state.show}
                        title={this.state.title}
                        message={this.state.message}
                        alertCancel={this.state.alertCancel}
                        alertConfirm={this.state.alertConfirm}
                        canBeClosed={this.state.canBeClosed}
                        okHide={true}
                        type={this.state.alertType} />

                    <Loader loading={this.state.loading} />

                    <FabButton
                        style={{ margin: hp('16%') }}
                        canGoBack={this.state.canGoBack}
                        navigation={this.props.navigation}
                    />
                </View>

            </SafeAreaView>
        )
    }
    callbackClick(visible, title, button) {
        this.setState({ show: false })
        if (this.state.message.includes('Updated payment')) {
            this.props.navigation.goBack();
        }
    }
    callBackSaveCard() {
        console.log("saveCard", this.state.object, this.state.typeName);
        let url = BaseUrl.FABRIC_V6 + "transaction/storeTransaction"
        console.log("url", url);
        this.setState({ error: '', loading: true });

        postAxiosWithToken(url, this.state.object, this.accessToken).then(response => {
            console.log("response", response);

            if (response.data && response.data.code == 200) {
                this.setState({ error: '', loading: false, show: true, message: 'Updated payment details successfully!', canBeClosed: false, title: '' });

            } else {
                let errorMessage = "";
                if (response.data && response.data.response) {
                    let array;
                    array = JSON.parse(response.data.response).error_message.split('-')
                    if (array.length > 1) {
                        errorMessage = array[1]
                    }
                } else if (response.data.code == 204) {
                    let msg = response.data.message;
                    this.setState({ error: '', loading: false, show: true, message: msg, canBeClosed: false, title: '' });

                } else {
                    errorMessage = response.message ? response.message : "Failed to change payment method";
                }
                this.setState({ loading: false, error: errorMessage });

            }
        })
    }

    saveCard(obj, type) {
        if (obj) {
            this.setState({ object: obj, typeName: type, doneDisable: false }, () => {
                // console.log("obj-1", this.state.doneDisable)

            })
        } else {
            if (this.state.object != obj) {
                this.setState({ object: obj, typeName: type, doneDisable: true }, () => {
                    //    console.log("obj-2", this.state.doneDisable)

                })
            }
        }
        //&& Object.keys(this.state.object).length !== 0
        // let disable = true;
        // if (this.state.object != null ) {
        //     disable = false;
        // }
        // console.log("obj", this.state.object != null)


        // this.setState({ object: obj, typeName: type, doneDisable: disable }, () => {
        //     console.log("obj", this.state.doneDisable)

        // })

    }
}

import React, { Component } from 'react';
import { View, Text, Image, KeyboardAvoidingView, StyleSheet } from 'react-native';

import COLORS from '../../constants/COLORS';
import TextInput from "react-native-material-textinput";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Picker, Icon } from "native-base";
import { isAndroid } from '../../constants/PlatformCheck';
import stylesCommon from "../../constants/StyleSheet";

import { FontsApp, testFont } from '../../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular

let inputTextHeight = FontsApp.inputTextHeight;
let lineHeight = hp("0.2%");

export default class CreditCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            error: '',
            cardNumber: '',
            source: this.props.source,
            cardHolderName: this.props.name,
            routungNumber: '',
            selectedValue: "first",
            selectedYear: 0,
            errorId: null,
            cvv: '',
        }
        console.log("DebitCard ", this.props.reset);

        this.baseState = this.state

    }

    callBackSaveCard() {
        let object = {
            'accountName': this.state.cardHolderName,
            //   'accountNumber': "",
            //  'accountType': "",
            //  'bankName': "",
            'cardNumber': this.state.cardNumber,
            'cvv': this.state.cvv,
            'expiryMonth': this.state.selectedValue,
            'expiryYear': this.state.selectedYear,
            'holderName': this.state.cardHolderName,
            'paymentType': "CC",
            //   'routingNo': "",
            'source': this.state.source,
        }

        if (this.state.cardNumber.length != 16) {
            // this.setState({
            //     error: !this.state.cardNumber.trim() &&
            //         this.state.cardNumber.length != 16 ? 'Card number up to 16 digits' : 'Card number required',
            //     errorId: 0
            // }, () => { });
            this.props.saveCard(null, "Credit");
        } else if (!this.state.cardHolderName.trim()) {
            //    this.setState({ error: 'Card holder name name required', errorId: 1 })
            this.props.saveCard(null, "Credit");
        }
        else if (!this.state.selectedValue.trim() || this.state.selectedValue === 'first') {
            //   this.setState({ error: 'Select Expiration Month', errorId: 3 })
            this.props.saveCard(null, "Credit");
        }
        else if (this.state.selectedYear == 0) {
            // this.setState({ error: 'Select Expiration year', errorId: 4 })
            this.props.saveCard(null, "Credit");

        }
        else if (this.state.cvv.length < 3) {
            // this.setState({
            //     error: cvv != "" && cvv.length < 3 ? 'Enter valid CVV' : 'CVV required',
            //     errorId: 2
            // }, () => { });
            this.props.saveCard(null, "Credit");

        }
        else {
            this.props.saveCard(object, "Credit");
        }
    }

    render() {
        return (
            <View
                style={{ flex: 1 }}
                behavior={isAndroid ? null : 'padding'}   >
                <View style={{
                    alignSelf: 'center', width: wp('100%'), paddingLeft: wp('5%'),
                    paddingRight: wp('5%'), flex: 1, marginBottom: hp('20%')
                }} >
                    <TextInput
                        marginTop={15}
                        keyboardType="numeric"
                        labelActiveTop={-30}
                        value={this.state.cardNumber}
                        fontSize={inputTextHeight}
                        label='Card Number*'
                        labelActiveColor={COLORS.primaryColor}
                        underlineColor={COLORS.primaryColor}
                        paddingLeft={10}
                        underlineActiveColor={COLORS.primaryColor}
                        secureTextEntry={false}
                        maxLength={16}
                        error={this.state.errorId == 0 ? this.state.error : ""}
                        underlineHeight={lineHeight}
                        onChangeText={this.oncardNumber}
                    />
                    <TextInput
                        marginTop={15}
                        labelActiveTop={-30}
                        value={this.state.cardHolderName}
                        fontSize={inputTextHeight}
                        label='Card Holder Name*'
                        error={this.state.errorId == 1 ? this.state.error : ""}
                        paddingLeft={10}
                        labelActiveColor={COLORS.labelActiveColor}
                        underlineColor={COLORS.underlineColor}
                        underlineActiveColor={COLORS.underlineActiveColor}
                        secureTextEntry={false}
                        underlineHeight={lineHeight}
                        onChangeText={this.oncardHolderName}
                    />
                    <View style={{ borderColor: COLORS.divider, borderBottomWidth: 1.5, marginBottom: hp('1%') }}>
                        <Picker
                            mode="dropdown"
                            iosHeader="Expiration Month"
                            iosIcon={<Icon name="ios-arrow-down" />}
                            style={{ width: wp('90%'), marginHorizontal: -9 }}
                            selectedValue={this.state.selectedValue}
                            itemStyle={{ color: COLORS.textColor }}
                            onValueChange={(itemValue, itemIndex) => this.setState({ error: '', selectedValue: itemValue, errorId: null }, () => {
                                this.callBackSaveCard();

                            })}>

                            <Picker.Item style={styles.textStylePicker} label={'Expiration Month'} value="first" />

                            <Picker.Item style={styles.textStylePicker} label="Jan" value="1" />
                            <Picker.Item style={styles.textStylePicker} label="Feb" value="2" />
                            <Picker.Item style={styles.textStylePicker} label="Mar" value="3" />
                            <Picker.Item style={styles.textStylePicker} label="Apr" value="4" />
                            <Picker.Item style={styles.textStylePicker} label="May" value="5" />
                            <Picker.Item style={styles.textStylePicker} label="Jun" value="6" />
                            <Picker.Item style={styles.textStylePicker} label="July" value="7" />
                            <Picker.Item style={styles.textStylePicker} label="Aug" value="8" />
                            <Picker.Item style={styles.textStylePicker} label="Sep" value="9" />
                            <Picker.Item style={styles.textStylePicker} label="Oct" value="10" />
                            <Picker.Item style={styles.textStylePicker} label="Nov" value="11" />
                            <Picker.Item style={styles.textStylePicker} label="Dec" value="12" />
                        </Picker>
                        {
                            this.state.errorId == 3
                                ?
                                <Text style={stylesCommon.errorRegular}>{this.state.error}</Text>
                                : null
                        }
                    </View>
                    <View style={{ borderColor: COLORS.divider, borderBottomWidth: 1.5, marginBottom: hp('1%') }}>
                        <Picker
                            mode="dropdown"
                            iosHeader="Expiration Year"
                            iosIcon={<Icon name="ios-arrow-down" />}
                            style={{ width: wp('90%'), marginHorizontal: -9 }}
                            selectedValue={this.state.selectedYear}
                            itemStyle={{ color: COLORS.textColor }}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({ error: '', selectedYear: itemValue, errorId: null }, () => {
                                    this.callBackSaveCard();

                                })}    >
                            <Picker.Item style={styles.textStylePicker} label={'Expiration Year'} value={0} />

                            <Picker.Item style={styles.textStylePicker} label="2021" value={2021} />
                            <Picker.Item style={styles.textStylePicker} label="2022" value={2022} />
                            <Picker.Item style={styles.textStylePicker} label="2023" value={2023} />
                            <Picker.Item style={styles.textStylePicker} label="2024" value={2024} />
                            <Picker.Item style={styles.textStylePicker} label="2025" value={2025} />
                            <Picker.Item style={styles.textStylePicker} label="2026" value={2026} />
                            <Picker.Item style={styles.textStylePicker} label="2027" value={2027} />
                            <Picker.Item style={styles.textStylePicker} label="2028" value={2028} />
                            <Picker.Item style={styles.textStylePicker} label="2029" value={2029} />
                            <Picker.Item style={styles.textStylePicker} label="2030" value={2030} />
                            <Picker.Item style={styles.textStylePicker} label="2031" value={2031} />
                        </Picker>
                        {
                            this.state.errorId == 4
                                ?
                                <Text style={stylesCommon.errorRegular}>{this.state.error}</Text>
                                : null
                        }
                    </View>
                    <TextInput
                        keyboardType="numeric"
                        labelActiveTop={-30}
                        value={this.state.cvv}
                        paddingLeft={10}
                        fontSize={inputTextHeight}
                        label='CVV*'
                        labelActiveColor={COLORS.primaryColor}
                        underlineColor={COLORS.primaryColor}
                        underlineActiveColor={COLORS.primaryColor}
                        secureTextEntry={false}
                        maxLength={4}
                        error={this.state.errorId == 2 ? this.state.error : ""}
                        underlineHeight={lineHeight}
                        onChangeText={this.onCVVNumber}
                    />

                </View >

            </View>
        )
    }

    oncardNumber = (cardNumber) => {
        if (cardNumber !== "") {
            this.setState({
                cardNumber: cardNumber, error: cardNumber.length < 16 ? 'Card number up to 16 digits' : '',
                errorId: cardNumber.length < 16 ? 0 : null
            }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ cardNumber: '', error: 'Card number required', errorId: 0 }, () => {
                this.callBackSaveCard();
            });
        }


    }

    oncardHolderName = (cardHolderName) => {

        if (cardHolderName !== "") {
            this.setState({ cardHolderName: cardHolderName, error: '', errorId: null }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ cardHolderName: "", error: 'Card holder name required', errorId: 1 }, () => {
                this.callBackSaveCard();
            });
        }
    }

    onCVVNumber = (cvv) => {

        if (cvv !== "") {
            this.setState({ cvv: cvv, error: cvv.length < 3 ? 'Enter valid CVV' : '', errorId: cvv.length < 3 ? 2 : null }, () => {
                this.callBackSaveCard();
            });
        } else {
            this.setState({ cvv: '', error: 'CVV required', errorId: 2 }, () => {
                this.callBackSaveCard();
            });
        }

    }



}


const styles = StyleSheet.create({

    heading: { fontSize: inputTextHeight, color: '#33ab8f', fontWeight: 'bold' },
    textStylePicker: { fontSize: inputTextHeight, ...testFont, color: COLORS.textColor },
    button: {
        textAlign: 'center', margin: '2%', fontSize: wp('4%'), flex: 1,
        borderRadius: 20, fontWeight: 'bold',
        backgroundColor: COLORS.blueL, padding: wp('2%'), color: 'white'
    }
});





import { postAxiosWithToken } from '../../constants/CommonServices';
import { fetchData, fetchSuccess, fetchError } from "./Action";

export const actionCreator = (url, method, previous) => dispatch => {
  dispatch(fetchData());
  return new Promise(() => {
    postAxiosWithToken(url, method, '').then(response => {
      console.log("-------response----------", response)

      if (previous && response.data) {
        console.log("-------response-----1-----")

        previous.push(response.data);//add data to existing response 
        dispatch(fetchSuccess(previous));

      } else if (response.data) {
        console.log("-------response-----2-----")//initial response

        let previous = [];
        previous.push(response.data);
        dispatch(fetchSuccess(previous));

      }else{
        console.log("-------response-----3-----")

        dispatch(fetchError("ERROR"));

      }

    })
      .catch(error => {
        console.log("-----------------", error.message)

        dispatch(fetchError(error));

      });

  });
};



import { Left } from 'native-base';
import { getUsername } from '../../constants/GetAsyncValues';
import ACTION_TYPES from './ActionTypes';
const initialState = {
  data: [{
    "query": "",
    "AIKBtext": [{ answer: "Hi, I’m Caryn. How can I help you today, " }],
    "intentRecognized": 'default'
  }],
  error: '',
  status: 'SUCCESS'
};
//Pure component ,pure function
//never write logic 
//just to update and renturn new state 
//never update existing state =copy =>new state return
const apiReducer = (state = initialState, action) => {
  console.log("...state",state)
  switch (action.type) {
    case ACTION_TYPES.API_PENDING:
      return {
        ...state,
        status: 'PENDING'
      };
    case ACTION_TYPES.API_SUCCESS:
      return {
        ...state,
        data: action.payload,
        status: 'SUCCESS'
      };
    case ACTION_TYPES.API_ERROR:
      return {
        ...state,
        error: action.payload,
        status: 'SUCCESS'
      };

    default:
      return state;
  }
};

export default apiReducer;

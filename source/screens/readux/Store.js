import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';//middle ware
import { createLogger } from 'redux-logger';
import apiReducer from './Reducer';
//store never be duplicate 
//can't have multiple store but can have multiple reducer
const appReducers = combineReducers({
  apiReducer
});

const rootReducer = (state, action) => appReducers(state, action);

const logger = createLogger();

let middleware = [];
middleware = [...middleware, thunk, logger];

export default createStore(
  rootReducer,
  compose(applyMiddleware(...middleware))
);

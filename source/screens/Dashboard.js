import React, { Component } from 'react';
import { SafeAreaView, FlatList, TouchableOpacity, Image, Alert, Text, View, BackHandler, StatusBar, DeviceEventEmitter } from 'react-native';
import Loader from '../components/Loader';
import MemberAppPopup from '../components/MemberAppPopup';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../constants/AppStrings';
import { Card } from "react-native-elements";

import NotificationCard from '../components/dashboard/NotificationCard';
import DocumentsCard from '../components/dashboard/DocumentsCard';
import MemberSmallCard from '../components/dashboard/MemberSmallCard';
import ContactCard from '../components/dashboard/ContactCard';
import { DocumentList, DashboarsCard } from "../constants/ArrayConst"
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import { ScrollView } from 'react-native-gesture-handler';
import { Linking } from 'react-native';
import { getAxiosUser, getAxiosWithToken, getFindProvider, postAxiosWithToken } from '../constants/CommonServices'
import BaseUrl from '../constants/BaseUrl'
import { passwordCheck } from '../constants/utils';
import styles from "../constants/StyleSheet";
import COLORS from '../constants/COLORS';
import AwesomeAlertDialog from '../components/AwesomeAlertDialog';
import AnnouncementDialog from '../components/AnnouncementDialog';

import { FontsApp, testFont } from '../constants/FontsApp'
import { FabButton } from './botchat/FabButton';
import { isHealthyShare } from '../constants/DigitalCardType';

let headingCardLarge = FontsApp.headingCardLarge;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallTextCard = FontsApp.smallTextCard

let imageNames = "";
let errorMessage = AppStrings.tryAgain;

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.offset = 0;
        this.previosPage = 0;
        this.email = '';
        this.accesstoken = '';
        this.client_id_locale;
        this.state = {
            loading: false,
            data: [],
            announcement: [],
            AllCards: [],
            cardLoadComplete: false,
            Connection: this.props.Connection,
            titleNotification: "notification",
            titleNotices: "Notices",

            title: "",
            message: '',
            alertCancel: AppStrings.cancel,
            alertConfirm: '',
            alertCLose: AppStrings.cancel,
            canBeClosed: false,
            okHide: false,
            alertType: '',
            show: false,

            updateNotification: false,
            noticeData: null,
            showAlert: false,
        }
    }
    componentWillUnmount() {
        //this.onFocusSubscribe();
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        //to receive notification click data here
        this.DeviceEventEmitter = DeviceEventEmitter.addListener('customName', this.deepLink.bind(this));
        //connectionCheck to check internet connection
        this.DeviceEventEmitter = DeviceEventEmitter.addListener('connectionCheck', this.connectionCheck.bind(this));
        this.props.navigation.addListener('focus', () => {
            //note notification flow condition
            if (this.state.updateNotification == true) {
                this.setState({ updateNotification: false }, () => {
                    this.getNotificationDetails(true);
                })
            }

        });

    }

    async getOffline() {
        let AllCards = JSON.parse(await AsyncStorage.getItem('AllCards'));
        this.setState({ AllCards: AllCards, loading: false, cardLoadComplete: true });
    }

    async dbInite() {

        this.client_id_locale = await AsyncStorage.getItem('client_id_locale');

        var urlPath = await AsyncStorage.getItem("notifyUrl");
        var cardTitle = await AsyncStorage.getItem("cardTitle");
        var notificationId = await AsyncStorage.getItem("notification_id");

        if (urlPath && cardTitle && notificationId) {
            var object = {
                "urlPath": urlPath,
                "notificationId": notificationId
            }
            this.updateNotificationStatus(object);
            this.removeNotification();
        }

    }
    getAllCards = (client_id_locale) => {
        var notificatioGet = BaseUrl.FABRIC_V7 + "memberportal/getCardDetails/" + client_id_locale;
        this.setState({ loading: true }, () => { });
        console.log("getAllCards--------url", notificatioGet);

        getAxiosUser(notificatioGet, '')
            .then(success => {

                if (success != null && success.data && success.data.code == 200) {

                    AsyncStorage.setItem("AllCards", JSON.stringify(success.data.response)).then(result => {
                    });

                    this.setState({ loading: false, AllCards: success.data.response, cardLoadComplete: true }, () => {
                    });
                } else {

                    this.showAlert("", success.message, "")

                }

                this.getNotificationDetails(true);

            }).catch(error => {
                console.log("error", error.message);
                this.setState({ loading: false, error: error.message }, () => { });
            });
    }

    removeNotification() {
        this.removeItemValue("notifyUrl");
        this.removeItemValue("notify");
        this.removeItemValue("notification_id");
    }

    deepLink(result) {
        console.log("Depp================", result)
        //receive notification data
        if (result.type == 'View') {
            this.updateNotificationStatus(result.data);
        } else {
            this.getNotificationDetails(false);
        }
        if (result.type == "Load") {
            console.log("sadas", result.type)
            this.setState({ updateNotification: true })
        }

    }
    connectionCheck = (connectionCheck) => {
        this.setState({ Connection: connectionCheck });
        this.getStorageData();
        if (connectionCheck)
            this.dbInite().then(done => {
                this.getAllCards(this.client_id_locale);
            });
        else {
            //get cards from storage or async
            this.getOffline();
        }
    }
    async getStorageData() {
        this.email = await AsyncStorage.getItem('email');
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.ContactNumber = await AsyncStorage.getItem('ContactNumber');

    }
    render() {

        return (
            <View style={{
                backgroundColor: COLORS.background, flex: 1,
            }}>

                <ScrollView
                    showsVerticalScrollIndicator={false} >
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>

                        <NotificationCard
                            notificationClick={(result) => {
                                if (result != null) {
                                    this.updateNotificationStatus(result);
                                } else {
                                    //load notifications on detail screen

                                    this.navigation.navigate(AppStrings.NotificationDetails, { notification: true });
                                }
                            }}
                            SpecialistArray={this.state.data}
                            loading={true}
                        />
                        <NotificationCard
                            notificationClick={(result) => {
                                if (result != null) {
                                    this.showAlertNotice(result)
                                } else {
                                    //load announcement on detail screen
                                    this.navigation.navigate(AppStrings.NotificationDetails, { notification: false });
                                }

                            }}
                            SpecialistArray={this.state.announcement}
                            loading={false}
                        />

                        <FlatList
                            showsVerticalScrollIndicator={false}
                            scrollEnabled={false}
                            numColumns={2}
                            data={DashboarsCard}
                            renderItem={this.renderItem.bind(this)}
                            keyExtractor={(item, index) => index.toString()} />

                        {
                            this.state.cardLoadComplete &&
                            <DocumentsCard
                                DocumentList={DocumentList}
                                navigation={this.props.navigation}
                                Connection={this.state.Connection}
                                AllCards={this.state.AllCards} />}
                        {
                            this.state.cardLoadComplete && <ContactCard SpecialistArray={this.state.AllCards} />
                        }

                    </View>
                    {/* @copyright */}
                    <Text style={{
                        color: '#6c757d',
                        fontSize: smallTextCard,
                        ...testFont,
                        marginBottom: hp('8%'),
                        textAlign: 'center',
                        marginTop: hp('2%')
                    }}>{AppStrings.copyright}</Text>

                </ScrollView>

                <Loader loading={this.state.loading} />

                <MemberAppPopup />


                <FabButton
                    style={{ margin: hp('8%') }}

                    canGoBack={false}
                    navigation={this.props.navigation}
                />

                <AwesomeAlertDialog
                    callbackClick={this.callbackClick.bind(this)}
                    show={this.state.show}
                    title={this.state.title}
                    message={this.state.message}
                    alertCancel={this.state.alertCancel}
                    alertConfirm={this.state.alertConfirm}
                    canBeClosed={this.state.canBeClosed}
                    okHide={this.state.okHide}
                    type={this.state.alertType} />

                <AnnouncementDialog
                    display={this.state.showAlert}
                    announcementResult={this.announcementResult.bind(this)}

                    noticeData={this.state.noticeData}
                />
            </View>
        );
    }

    renderItem({ item }) {
        return (
            <Card containerStyle={styles.cardStyle} >
                {this.maincardRender(item)}
            </Card>
        )
    }

    callbackClick(visible, title, button) {
        this.hideAlert();

        if (button === "ok") {

        } else {

        }
    }
    announcementResult(result, data) {
        this.hideAlert();
        if (result) {

            let cardTitle;
            if (data.cardTitle) {
                cardTitle = data.cardTitle;
            } else {
                cardTitle = passwordCheck.getCardTypeByPath(data.urlPath)
            }
            // console.log("cardTitle--------------------------", cardTitle, data.urlPath)
            if (cardTitle == AppStrings.notices) {
                Linking.openURL(AppStrings.noticeUrl)
            } else {
                this.checkDisableAndNavigate(cardTitle, data.urlPath)
            }
        }

    }

    hideAlert() {
        this.setState({ show: false, showAlert: false }, () => { });
    }

    maincardRender(item) {
        imageNames = passwordCheck.imageofCardByName(item.title);
        return (
            item.title === AppStrings.cardWallet ?
                <MemberSmallCard
                    callbackMembercard={this.memberCardClick.bind(this)}
                    AllCards={this.state.AllCards} />
                : <TouchableOpacity
                    onPress={() => {
                        switch (item.title) {
                            case AppStrings.notices:
                                Linking.openURL(item.link);
                                break;
                            case AppStrings.FAQs:
                                Linking.openURL(item.link);
                                break;
                            case AppStrings.findProvider:
                                //load api 
                                this.findProvider();
                                break;
                            // case AppStrings.paymentWallet:
                            //     //load api 
                            //     this.props.navigation.navigate(AppStrings.paymentNav)
                            //     break;
                            default: {
                                this.checkDisableAndNavigate(item.cardType, '')

                            }
                        }
                    }}
                    style={{ alignItems: 'center', justifyContent: 'center' }}>

                    <Image
                        style={{ width: wp('16%'), resizeMode: 'contain', height: wp('16%') }}
                        source={imageNames}
                    />

                    <Text style={{
                        color: '#5f2161',
                        marginTop: hp("1.5%"),
                        textAlign: 'center',
                        fontSize: headingCardLarge, ...testFont,
                        fontWeight: 'bold'
                    }}>{item.title}</Text>

                </TouchableOpacity>

        );
    }

    memberCardClick(clickType) {
        let newArray = passwordCheck.ArrayFilterFunction(this.state.AllCards, clickType);

        if (newArray && newArray.enable && clickType == 'MembershipId') {
            //check health card or member id card

            if (isHealthyShare(this.client_id_locale)) {
                this.props.navigation.navigate(AppStrings.HealthyShareCard);
            }
            else
                this.props.navigation.navigate(AppStrings.DigitalHealthCardNew);

        } else {
            if (newArray && newArray.enable) {
                this.props.navigation.navigate(AppStrings.HealthToolCard);

            }

        }

    }

    findProvider() {
        if (this.state.Connection) {
            getFindProvider(this.accesstoken, this.email).then(resp => {
                console.log(resp)
                if (resp.includes("http://")) {
                    Linking.openURL(resp)
                } else {
                    let include = passwordCheck.getSessionStatus(resp)
                    DeviceEventEmitter.emit('sessionOut', include)
                }
            }).catch(e => {
                this.showAlert("", e.message, "")
            })
        } else {
            this.showAlert("", AppStrings.noInternet, "")
        }
    }

    showAlert(title, message, buttonType) {
        this.setState({
            alertType: '', show: true,
            title: title, message: message == "Network Error" ? AppStrings.noInternet : message, alertConfirm: AppStrings.ok,
            canBeClosed: true, alertCancel: AppStrings.cancel, okHide: false
        });
    }
    showAlertNotice(result) {
        this.setState({
            showAlert: true, noticeData: result
        });
    }
    handleBackPress = async () => {
        this.props.navigation.goBack();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.DeviceEventEmitter.remove();
    }

    async removeItemValue(key) {
        try {
            await AsyncStorage.removeItem(key);
            return true;
        }
        catch (exception) {
            return false;
        }
    }
    getAnnouncements(value) {
        var notificatioGet = BaseUrl.V2_API_PATH + "Twillio/getAnnouncemetDetails?page=0&size=5"
        this.setState({ loading: value, loadMore: false }, () => { });
        console.log("getAnnouncements-----------url-------", notificatioGet);
        getAxiosUser(notificatioGet)
            .then(success => {

                if (success != null && success.data && success.data.code === 200) {
                    this.previosPage = this.offset;
                    let response = success.data.response.notificationDetails;
                    // console.log("getAnnouncements-----------success.data-------", response);
                    //  response = [...response, ...response];

                    response.sort((a, b) => -(a.type.localeCompare(b.type)))

                    this.setState({
                        announcement: response,
                        loading: false, error: ''
                    }, () => {
                        this.offset = this.offset + 1;
                    });

                } else {

                    this.setState({ loading: false, error: success && success.data ? success.data.message : 'Server Error' }, () => { });
                }

            }).catch(error => {
                console.log("error", error);
                this.setState({ loading: false, error: error.message }, () => { });
            });
    }

    getNotificationDetails(value) {
        var notificatioGet = BaseUrl.V2_API_PATH + "Twillio/getNotificationDetails/" + this.email
        this.setState({ loading: value, loadMore: false }, () => { });
        console.log("getNotificationDetails-----------url-------", notificatioGet);
        getAxiosUser(notificatioGet)
            .then(success => {
                if (success != null && success.data && success.data.code === 200) {
                    this.previosPage = this.offset;
                    let response = success.data.response.notificationDetails;
                    var filterArray = response.filter(
                        data => data.status == 'sent'
                    );

                    // console.log("getNotificationDetails------filterArray------", filterArray);

                    DeviceEventEmitter.emit('notifyCount', { data: filterArray.length })
                    this.setState({
                        data: filterArray.slice(0, 2),
                        loading: false, error: ''
                    }, () => {
                        this.offset = this.offset + 1;
                    });

                } else {

                    this.setState({ loading: false, error: success && success.data ? success.data.message : 'Server Error' }, () => { });
                }
                this.getAnnouncements(true);

            }).catch(error => {
                console.log("error", error);
                this.setState({ loading: false, error: error.message }, () => { });
            });
    }

    updateNotificationStatus(data) {

        let notId = data.notificationID ? data.notificationID : data.notificationId;
        this.setState({ loading: true });

        let cardTitle;
        if (data.cardTitle) {
            cardTitle = data.cardTitle;
        } else {
            cardTitle = passwordCheck.getCardTypeByPath(data.urlPath)
        }

        console.log("cardTitle-1-", data.cardTitle, " urlPath ", data.urlPath, " cardTitle ", cardTitle);

        if (notId && !(data.type == "ANNOUNCEMENT" || data.type == "announcement" || data.type == "NOTICES" || data.type == "notices")) {
            var statusUrl = BaseUrl.V2_API_PATH + "Twillio/updateNotificationStatus";
            var postObject = {
                "notificationId": notId
            }
            postAxiosWithToken(statusUrl, postObject, "").then(resp => {
                this.setState({ loading: false });

                if (resp && resp.data && resp.data.code === 200) {
                    this.checkDisableAndNavigate(cardTitle, data.urlPath);
                    this.getNotificationDetails(false);
                } else {

                    let message = passwordCheck.responseError(resp);
                    this.setState({ error: message == "Error" ? errorMessage : message }, () => { });
                    this.showAlert("", message, "")
                }

            }).catch(error => {
                // console.log("error", error);
                this.setState({ loading: false });
            });

        } else {
            console.log("notificationId-is- ", notId);
            this.setState({ loading: false });
            if ((data.type == "ANNOUNCEMENT" || data.type == "announcement"
                || data.type == "NOTICES" || data.type == "notices")) {
                this.checkDisableAndNavigate(cardTitle, data.urlPath)
            }
        }

    }

    checkDisableAndNavigate(cardTitle, urlPath) {
        console.log("checkDisableAndNavigate---------------", cardTitle, urlPath,);

        //TO check card is enable or disable
        //returns last entry (match)
        let newArray = passwordCheck.ArrayFilterFunction(this.state.AllCards, cardTitle);

        if (newArray && newArray.enable)
            passwordCheck.navigationUtil(newArray.cardtitle, this.props, this.state.Connection, urlPath);
        else {
            let message = `We’re facing some technical difficulties, due to which this feature is currently unavailable. For support, call Member Services at ${this.ContactNumber}, Monday through Friday, 8.00am to 8.00pm CST.`
            this.showAlert("", message, "");
        }

    }

}
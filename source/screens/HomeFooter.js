/**
 * Created by PERSCITUS-42 on 09-Dec-19.
 */
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AppStrings from '../constants/AppStrings';
import COLORS from "../constants/COLORS";
import { getCardEnable } from "../constants/CommonServices";
import { FontsApp, testFont } from "../constants/FontsApp";
let heading = FontsApp.headingCard
export default HomeFooter = props => {
    const {
        isDashboard,
        isOtherScreen, from,
        ...attributes
    } = props;

    let callbackReturn = (from, isHome, cardType) => {
        getCardEnable(null, cardType).then(
            resp => {
                console.log("HomeFooter---------------------", from, cardType, resp)
                props.callbackFooter(resp ? from : "disable", isHome);
            }); 
    };

    return (

        <View style={styles.bottomView}>

            <TouchableOpacity style={styles.bottomViewIn} onPress={() => {
                callbackReturn(AppStrings.dashboard, isDashboard, AppStrings.dashboard);
            }}>
                <Image
                    source={isDashboard && !isOtherScreen ? require('../assets/images/dashboard_icon_wh_active.png') : require('../assets/images/dashboard_icon_wh.png')}
                    style={{ width: wp('4%'), height: hp('4%'), resizeMode: 'contain' }}
                />
                <Text style={isDashboard && !isOtherScreen ? styles.textStyle :
                    styles.textStyleDisable}>{AppStrings.dashboard}</Text>

            </TouchableOpacity>

            <TouchableOpacity style={styles.bottomViewIn} onPress={() => {
                callbackReturn(AppStrings.documents, isDashboard, AppStrings.Doc_Type);
            }}>
                <Image
                    source={!isDashboard && from == "doc" ? require('../assets/images/documents_icon_active.png') : require('../assets/images/documents_icon.png')}
                    style={{ width: wp('4%'), height: hp('4%'), resizeMode: 'contain' }}
                />
                <Text style={!isDashboard && from == "doc" ?
                    styles.textStyle : styles.textStyleDisable}>{AppStrings.documents}</Text>

            </TouchableOpacity>
            <TouchableOpacity style={styles.bottomViewIn} onPress={() => {
                callbackReturn(AppStrings.MyNeedsScreen, isDashboard, AppStrings.MyNeeds_Type);
            }}>

                <Image
                    source={!isDashboard && from == "needs" ? require('../assets/images/needs_icon_wh_active.png') : require('../assets/images/needs_icon_wh.png')}
                    style={{ width: wp('4%'), height: hp('4%'), resizeMode: 'contain' }}
                />

                <Text style={!isDashboard && from == "needs" ? styles.textStyle : styles.textStyleDisable}>{AppStrings.myNeeds}</Text>

            </TouchableOpacity>
        </View>
    )

}

const styles = StyleSheet.create({

    bottomView: {
        width: '100%',
        height: hp("7%"),
        flexDirection: 'row',
        backgroundColor: COLORS.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute', //Here is the trick
        zIndex: 999,
        bottom: 0, //Here is the trick

    },
    bottomViewIn: {
        flex: 1,
        height: hp("7%"),
        alignItems: 'center',
        justifyContent: 'center'
    },
    textStyle: {
        color: COLORS.white,
        fontSize: heading, ...testFont,
        fontWeight: 'bold'
    },
    textStyleDisable: {
        color: COLORS.graylight,
        fontSize: heading, ...testFont,
        fontWeight: 'bold'
    },
});
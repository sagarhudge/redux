
import React, { Component } from "react";
import { Linking, ScrollView, Alert, Text, TouchableOpacity, View, FlatList, Image, StyleSheet, DeviceEventEmitter } from "react-native";
import { getGateWayToken, getAxiosUser, getAxiosWithToken, postAxiosWithToken, getData } from '../constants/CommonServices'

import Loader from "../components/Loader";
import COLORS from "../constants/COLORS"
import BaseUrl from '../constants/BaseUrl'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from "../constants/AppStrings";
import moment from "moment";
import HomeFooter from '../screens/HomeFooter';
import styles from "../constants/StyleSheet";
import * as Progress from 'react-native-progress';
import { SafeAreaView } from "react-native";

import { FontsApp, headerStyle, headerStyleContainer, testFont } from '../constants/FontsApp'
import { passwordCheck } from "../constants/utils";
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading;
let regular = FontsApp.regular;
let smallText = FontsApp.smallText;
let errorMessage = "Program Information is not available";
const regex = /(<([^>]+)>)/ig;
import { ButtonSmall } from "../components/Buttons";
import AwesomeAlertDialog from '../components/AwesomeAlertDialog';
import { FabButton } from "./botchat/FabButton";

export default class ProgramInformation extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.accesstoken = '';
        this.offset = 0;
        this.previosPage = 0;
        this.email = '';
        this.sourceID = '';
        this.programInfoData = {};
        this.client_id_locale = '';
        this.chatKey = this.props.route.params && this.props.route.params.key ? this.props.route.params.key : '';
        // console.log("--------------------------------------------",this.props.route.params.key)
        this.state = {
            data: {},
            loading: false,
            programname: '',
            Connection: this.props.route.params.Connection,
            error: ' ',
            loadMore: false,
            subID: '',
            clientName: '',
            associationId: '',
            brokerId: '',
            clientID: '',
            isNeo: true,
            isHL: true,
            canGoBack: this.props.route.params.key ? this.props.route.params.key : false,
            title: "",
            message: '',
            alertCancel: AppStrings.cancel,
            alertConfirm: '',
            alertCLose: AppStrings.cancel,
            canBeClosed: false,
            okHide: false,
            alertType: '',
            show: false,

        }
        console.log("PI--------------", this.props.route.params)

    }

    componentWillUnmount() {
        this.DeviceEventEmitter.remove();
    }

    showAlert(title, message, buttonType) {
        this.setState({
            alertType: '', show: true,
            title: title, message: message, alertConfirm: AppStrings.ok,
            canBeClosed: false, alertCancel: AppStrings.cancel, okHide: true
        });
    }

    componentDidMount() {
        this.DeviceEventEmitter = DeviceEventEmitter.addListener('connectionCheck', this.connectionCheck.bind(this));

        passwordCheck.applyHeader(this.navigation, AppStrings.programInformation)

        this.getData();

    }

    connectionCheck = (connectionCheck) => {
        console.log("connectionCheck_PInfo", connectionCheck);
        this.setState({ Connection: connectionCheck });
    }

    async getData() {
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.email = await AsyncStorage.getItem('email');
        this.client_id_locale = await AsyncStorage.getItem("client_id_locale");
        this.source = await AsyncStorage.getItem("source");

        let AgentInformation = JSON.parse(await AsyncStorage.getItem("AgentInformation"))

        if (AgentInformation) {
            //get agent information from async storage 
            this.setState({
                brokerId: AgentInformation.response.brokerId, isNeo: this.getNeoEnable(), isHL: this.disableAddOnHLEnable()
            });
        }

        await AsyncStorage.getItem("sourceID").then(data => {
            this.sourceID = data;
            console.log("---------sourceID------------", this.state.brokerId, data)
            this.getMyNeedsDetails();
        });



    }
    async getOfflineData() {
        this.programInfoData = JSON.parse(await AsyncStorage.getItem('programInfoData'));
        //  console.log(" this.programInfoData", this.programInfoData);
        this.setState({ data: this.programInfoData, error: '' });

        if (this.programInfoData) {
            this.programInfo(this.programInfoData.programInfo.programName);
            this.callChange();

        }
        else
            this.setState({ data: this.programInfoData, error: "Program Information is not availabel" });
    }
    render() {
        return (

            <SafeAreaView style={{ backgroundColor: COLORS.background, flex: 1 }}>
                <View style={{ flex: 1 }}>
                    {this.state.error ?
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: hp("2%") }}>
                            <Text style={{ alignSelf: 'center', fontSize: regular, ...testFont, fontWeight: 'bold' }}>{this.state.error}</Text>
                        </View>
                        : <ScrollView>
                            <View style={{ flex: 1, marginTop: hp('2%'), marginBottom: hp('7%') }}>
                                <View style={{ flex: 1, padding: hp("3%"), backgroundColor: COLORS.white }} >

                                    <Text style={{
                                        fontFamily: 'Anton-Regular', fontSize: hp('8%'), color: COLORS.textColor,
                                        fontWeight: 'bold', marginBottom: hp('3%'),
                                    }}>
                                        {this.state.programname}
                                    </Text>

                                    <Text style={{
                                        ...testFont,
                                        fontSize: regular,
                                        color: COLORS.textColor,
                                        fontWeight: 'bold'
                                    }}>
                                        Summary
                                    </Text>

                                    <View>
                                        <Text style={{
                                            ...testFont,
                                            fontSize: regular,
                                            marginTop: hp('3%'),
                                            color: COLORS.subText, marginBottom: hp('2%')
                                        }}>
                                            Non-Sharable amount
                                        </Text>
                                        <Progress.Bar
                                            color={COLORS.red}
                                            unfilledColor={COLORS.redLight}
                                            borderWidth={0}
                                            height={hp('1.5%')}
                                            progress={((this.state.data.nsa.met * 1.0) / this.state.data.nsa.total) ? (this.state.data.nsa.met * 1.0) / this.state.data.nsa.total : 0}
                                            width={wp('90%')} />


                                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                            <Text style={{
                                                ...testFont, flex: 1, fontSize: regular, color: COLORS.red,
                                                marginBottom: hp('1%'), fontWeight: 'bold'
                                            }}>
                                                {" $" + this.state.data.nsa.met + " met"}

                                            </Text>
                                            <Text style={{
                                                ...testFont, flex: 1, fontSize: regular, color: COLORS.textColor,
                                                marginBottom: hp('1%'), textAlign: 'right'
                                            }}>
                                                {"$" + this.numberWithCommas(this.state.data.nsa.remaining) + " remaining"}
                                            </Text>
                                        </View>
                                    </View>


                                    {
                                        this.state.data.acsm && this.state.data.acsm.total != 0 ?
                                            <View>
                                                <Text style={{
                                                    ...testFont, fontSize: regular,
                                                    marginTop: hp('2%'), color: COLORS.subText, marginBottom: hp('1%')
                                                }}>Annual Co-Share Maximum Amount</Text>

                                                <Progress.Bar
                                                    color={COLORS.yellow}
                                                    unfilledColor={COLORS.yellowLight}
                                                    height={hp('1.5%')}
                                                    borderWidth={0}
                                                    progress={(this.state.data.acsm.met * 1.0) / this.state.data.acsm.total}
                                                    width={wp('90%')} />

                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={{
                                                        ...testFont, flex: 1, fontSize: regular, color: COLORS.yellow,
                                                        marginBottom: hp('1%'), fontWeight: 'bold'
                                                    }}>
                                                        {" $" + this.state.data.acsm.met + " met"}
                                                    </Text>
                                                    <Text style={{
                                                        ...testFont, flex: 1, fontSize: regular, color: COLORS.textColor,
                                                        marginBottom: hp('1%'), textAlign: 'right'
                                                    }}>
                                                        {"$" + this.numberWithCommas(this.state.data.acsm.remaining) + " remaining"}

                                                    </Text>
                                                </View>
                                            </View>
                                            : null
                                    }
                                    <Text style={{ ...testFont, fontSize: regular, color: COLORS.textColor, fontWeight: 'bold', marginTop: hp('5%') }}>
                                        Eligible Services
                                    </Text>
                                    {/* FlatList no scroll planInfo */}

                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        numColumns={2}
                                        scrollEnabled={false}
                                        data={this.state.data.planInfo}
                                        renderItem={this.renderItem.bind(this)}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                    <Text style={{
                                        ...testFont, fontSize: regular, color: COLORS.textColor,
                                        fontWeight: 'bold', marginTop: hp('5%')
                                    }}>
                                        Sharing Limits
                                    </Text>

                                    {/* FlatList no scroll expenseLimits */}
                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        numColumns={2}
                                        scrollEnabled={false}
                                        data={this.state.data.expenseLimits}
                                        renderItem={this.renderItem.bind(this)}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>
                                {this.AddOnFooter()}
                            </View>
                        </ScrollView>

                    }
                    <HomeFooter
                        from="programInfo"
                        isDashboard={false}
                        isOtherScreen={true}
                        callbackFooter={(from, isHome) => {
                            if (from === AppStrings.dashboard) {
                                // this.props.navigation.goBack(null);
                                this.props.navigation.navigate("Home");
                            }
                            if (from === AppStrings.documents) {
                                this.props.navigation.navigate(AppStrings.documents);
                            }

                            if (from === AppStrings.MyNeedsScreen) {
                                this.props.navigation.navigate(AppStrings.MyNeedsScreen);
                            }

                        }} />

                    <Loader loading={this.state.loading} />
                    <FabButton
                        style={{ margin: hp('8%') }}

                        canGoBack={this.state.canGoBack}
                        navigation={this.props.navigation}
                    />
                    <AwesomeAlertDialog
                        callbackClick={this.hideAlert.bind(this)}
                        show={this.state.show}
                        title={this.state.title}
                        message={this.state.message}
                        alertCancel={this.state.alertCancel}
                        alertConfirm={this.state.alertConfirm}
                        canBeClosed={this.state.canBeClosed}
                        okHide={this.state.okHide}
                        type={this.state.alertType} />
                </View>
            </SafeAreaView >

        );
    }
    AddOnFooter = () => {
        return <View style={{
            backgroundColor: COLORS.background,
            padding: wp('3%'), alignItems: 'center',
        }}>
            <ButtonSmall title={"CHANGE ADD-ONS"}
                isDisabled={this.state.isNeo ? this.state.isHL ? false : true : true}
                type="blue"
                callback={() => this.state.isNeo ? this.state.isHL ? this.changeProgram() : null : null} />

            <View style={{ marginVertical: wp('2%') }}>
                <ButtonSmall
                    title={"CHANGE DEPENDENTS"}
                    isDisabled={!this.state.isNeo}
                    type="blue"
                    callback={() => { this.state.isNeo ? this.updateHouseHold() : null }} />
            </View>
            <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', width: wp('100%'), paddingRight: wp('4%') }}>
                <Text style={{ ...testFont, fontSize: regular, color: COLORS.textColor, fontWeight: '700' }}>Need help?</Text>
                <Text style={{ ...testFont, fontSize: regular, color: COLORS.textColor }}>Chat with our Health Share Representative</Text>
                <Text
                    onPress={() => { passwordCheck.dialNumber('1 (888) 366 6243') }}
                    style={{ ...testFont, fontSize: regular, color: COLORS.textColor }}>or call 1 (888) 366 6243.</Text>

            </View>
        </View>

    }
    hideAlert() {
        this.setState({ show: false }, () => { });
    }

    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    disableAddOnHLEnable() {
        console.log("--------------------------------------------------", this.client_id_locale)

        if (this.client_id_locale == "6548" || this.client_id_locale == 6548
            || this.client_id_locale == "4367" || this.client_id_locale == 4367
            || this.client_id_locale == "4376" || this.client_id_locale == 4376
            || this.client_id_locale == "5540" || this.client_id_locale == 5540
        ) {

            return false;
        } else {
            return true;
        }
    }
    getNeoEnable() {
        if (this.source == "NEO") {
            return false;
        } else {
            return true;
        }
    }


    changeProgram() {
        let obj = {
            "clientId": this.state.clientName,
            "associationId": this.state.associationId,
            "brokerId": this.state.brokerId ? this.state.brokerId : "qaaspire",
            "fromMember": true,
            "isSelectProgram": true,
            "user_subId": this.email,
            "memberId": this.sourceID,
            "subID": this.state.subID,
            "fromNative": true,
        }

        if (this.state.clientName && this.state.associationId && this.state.brokerId && this.state.subID && this.email && this.sourceID) {
            this.getEncryptData(obj);

        } else {
            this.showAlert("", "Data is not available for this user!", "")

        }

    }
    updateHouseHold() {
        let obj = {
            "clientId": this.state.clientName,
            "associationId": this.state.associationId,
            "brokerId": this.state.brokerId ? this.state.brokerId : "qaaspire",
            "fromMember": true,
            "user_subId": this.email,
            "memberId": this.sourceID,
            "isHouseholdUpdate": true,
            "subID": this.state.subID,
            "isEditCensus": false,
            "fromNative": true,

        }
        if (this.state.clientName && this.state.associationId && this.state.brokerId && this.state.subID && this.email && this.sourceID) {
            this.getEncryptData(obj);
        } else {
            this.showAlert("", "Data is not available for this user!", "")
        }

    }
    renderItem({ item }) {
        return (

            <View style={{ flex: 1 }}>
                <Text style={{
                    ...testFont, fontSize: regular, ...testFont,
                    marginTop: hp('1%'),
                    color: COLORS.subText
                }}>
                    {item.idcardField.replace(regex, '')}
                </Text>
                <Text style={{
                    ...testFont, fontSize: regular, ...testFont,
                    color: COLORS.textColor, fontWeight: 'bold'
                }}>
                    {item.fieldValue ? item.fieldValue.replace(regex, '') : 'NA'}
                </Text>
            </View>

        )
    }

    programInfo(str1) {
        if (str1.includes("+")) {
            var res1 = str1.split(" ");
            this.setState({ programname: res1[0] + "\nwith " + res1[2] })
        } else {
            // alert("+ not fond");
            this.setState({ programname: str1, showwithacsm: false })
        }
    }

    callChange() {
        if (this.chatKey == AppStrings.chnage_addOns) {
            this.state.isNeo ? this.state.isHL ? this.changeProgram() : null : null
        } else if (this.chatKey == AppStrings.chnage_dependants) {
            this.state.isNeo ? this.updateHouseHold() : null
        }
        console.log("---------chatKey------------", this.chatKey)
    }

    getMyNeedsDetails() {
        var programInfo = BaseUrl.FABRIC_V7 + "memberportal/getProgramInformation/" + this.email;
        this.setState({ loading: true, loadMore: false }, () => { });
        console.log("programInfo----------------------------", programInfo);

        getAxiosUser(programInfo)
            .then(response => {
                if (response != null && response.data && response.status == 200) {
                    var resp = response.data;
                    this.setState({ data: resp, loading: false, error: '' }, () => {
                        this.programInfo(resp.programInfo.programName);
                        AsyncStorage.setItem('programInfoData', JSON.stringify(resp));
                    });

                    this.getClientDetails(this.client_id_locale);

                } else {
                    let message = passwordCheck.responseError(response);
                    this.setState({ loading: false, error: message == "Error" ? errorMessage : message }, () => { });
                    console.log("----------" + message);

                    if (AppStrings.noInternet == message) {
                        this.getOfflineData();
                    }

                }

            }).catch(error => {
                console.log("error", error.message);
                this.setState({ loading: false, error: error.message }, () => { });

            });
    }
    getClientDetails(cId) {
        let ClientDetails = BaseUrl.FABRIC_V3 + 'enrollment/getClient';
        this.setState({ loading: true, error: '' }, () => { });
        let clientId = {
            "clientId": cId,
        }

        console.log("getClientDetails----------------------------", ClientDetails, cId, this.accesstoken);

        postAxiosWithToken(ClientDetails, clientId, this.accesstoken).then(response => {

            if (response.data.response) {
                this.setState({
                    clientID: response.data.response.clientId,
                    associationId: response.data.response.association,
                    clientName: response.data.response.clientName,
                });
                this.getEnrollMember(this.sourceID);
            } else {
                let message = passwordCheck.responseError(response);
                this.setState({ loading: false, }, () => { });
                console.log("postAxiosWithToken ", message)
            }
        }).catch(error => {
            this.setState({ loading: false, }, () => { });
            console.log("catch1", error.message)

        })
    }

    getEnrollMember = (memberId) => {
        let URL = BaseUrl.FABRIC_V3 + 'enrollment/getEnrollMemberInfoById/' + memberId;
        console.log("getEnrollMember-------------------------------------", URL)
        getAxiosUser(URL)
            .then(response => {

                if (response.data && response.data.response) {
                    this.setState({ subID: response.data.response.subId, loading: false, }, () => this.callChange()
                    )
                    console.log("getEnrollMember subId----------------------------------x`", response.data.response.subId, " memberId ", this.sourceID)

                } else {
                    let message = passwordCheck.responseError(response);
                    console.log("response error ", message)
                    this.setState({ loading: false }, () => { });
                }

            }).catch(error => {
                this.setState({ loading: false }, () => { });
                console.log("catch error ", error.message)

            })
    }
    getEncryptData(obj) {
        let url = BaseUrl.FABRIC_V4 + "encrypt/encryptData"
        this.setState({ loading: true }, () => { });
        console.log("getEncryptData------------------------- ", url, obj)

        postAxiosWithToken(url, obj, this.accesstoken).then(response => {
            if (response.data) {
                let URL = BaseUrl.REACT_APP_ENROLLMENT + 'login#state=' + response.data.response
                // let URL = 'http://localhost:3001/login#state=' + response.data.response
                console.log("Linking ", URL)
                this.props.navigation.navigate("NewEnrollMent", { url: URL });
                // Linking.openURL(URL)
            } else {
                let message = passwordCheck.responseError(response);
                console.log("error ", message)
                this.setState({ loading: false }, () => { });
            }
            this.setState({ loading: false }, () => { });

        }).catch(error => {
            this.setState({ loading: false }, () => { });
            console.log("catch error ", error.message)
        })
    }

};
import * as RNFS from 'react-native-fs';
import CameraRoll from "@react-native-community/cameraroll";
import { exp } from 'react-native-reanimated';
import Share from "react-native-share";

const ExternalDirectoryPath = RNFS.ExternalDirectoryPath;

export const shareFile = async (uri, previewSource, name) => {
    let f_uri = getChangePath(uri, name + "_Back");

    let S_uri = getChangePath(previewSource, name + "_Front");

    RNFS.copyFile(uri, f_uri).then(() => {
        RNFS.copyFile(previewSource, S_uri).then(() => {
            const shareOptions = {
                title: 'Sharevia',
                message: 'HealthCard',
                urls: [f_uri, S_uri],
            };
            // this.setState({ loading: false, previewSource: null, res: null, visibleTap: true });

            Share.open(shareOptions).then((Promise) => { console.log("Promise--------------" + Promise) }).catch(e => {
                console.log("Exception--------------", e.message)
                //this.card.flip();
            });
        })
    })
}

export const renameAndDownload = async (uri, previewSource, name) => {

    let f_uri = getChangePath(uri, name + "_Back");

    let S_uri = getChangePath(previewSource, name + "_Front");

    return RNFS.copyFile(uri, f_uri)
        .then(() => {
            return CameraRoll.save(f_uri).then(savedTo => {
                return RNFS.copyFile(previewSource, S_uri)
                    .then((saved) => {
                        return CameraRoll.save(S_uri)
                    })
                    .catch(err => {
                        console.log('err2-----------:*', err.message)
                        return err.message;
                    });
            }).catch(err => {
                console.log('err-----------:', err.message)
                return err.message;
            });

        })
        .catch(err => {
            return err.message;
        });

}


export const renameAndDownloadNew = async (uri, previewSource, name) => {

    let f_uri = getChangePath(uri, name + "_Back");

    let S_uri = getChangePath(previewSource, name + "_Front");

    return RNFS.copyFile(uri, f_uri)
        .then(() => {
            return RNFS.copyFile(previewSource, S_uri)
                .then(() => {
                    return CameraRoll.save(f_uri).then(() => {
                        return CameraRoll.save(S_uri)
                    })
                })
                .catch(err => {
                    console.log('err2-----------:*', err.message)
                    return err.message;
                });
        })
        .catch(err => {
            return err.message;
        });

}
export const deleteFile = targetName => {
    // let path="content://media/external_primary/images/media/"+"UHS_Membership_ID_Back.png"
    let path = `${ExternalDirectoryPath}/` + "UHS_Membership_ID_Back.png";
    RNFS.unlink(path)
        .then(result => {
            console.log(result, '--------------successfully deleted!!');
        }).catch(e => {
            console.log(e.message, '--------------not deleted!!', path);

        });
    // RNFS.unlink(`${ExternalDirectoryPath}/${targetName}`)
    //    .then(result => {
    //        console.log(result, 'successfully deleted!!');
    //    });
};
export const getChangePath = (uri, f_name) => {
    let uriArray = uri.split("/");
    let nameToChange = uriArray[uriArray.length - 1];
    let renamedURI = uri.replace(
        nameToChange, f_name + ".png"
    );
    return renamedURI;
}
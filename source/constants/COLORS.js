const COLORS = {
   
    primaryColor: '#543379',
    primarylight: '#9c48d4',
    intentColor: '#2DA5C8',
    white: '#ffffff',
    black: '#000000',
    green: '#28a745',
    yellow: '#fec465',
    red: '#eb5757',
    blueL: '#17a2b8',

    yellowLight: '#ffe3b5',
    redLight: '#f08080',

    textColor: '#000000',
    subText: '#454d58',
    lableColor: '#2A3E61',
    headingColor: '#8850ab',
    headingColor2: '#420045',
    labelActiveColor: '#533278',
    cancelButton: '#e9716f',

    grayDark: '#808080',
    graylight: '#d3d3d3',
    grayBackground: '#E6E5E4',
    borderColor: '#e8e8e8',
    divider: '#d4d4d4',
    background: '#f2f2f0',
    cardBackground: '#f8f9fa',
    cardaText: '#808080',
    graya1: '#a1a1a1',
    underlineColor: '#b9b9b9',
    underlineActiveColor: '#773ba5'
};

export default COLORS;
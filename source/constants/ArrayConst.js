/**
 * Created by PERSCITUS-42 on 05-Dec-19.
 */

import React from "react";
import AppStrings from "../constants/AppStrings"

export const DocumentList = [

    {
        "id": 3,
        "title": "Welcome Booklet",
        "url": ""
    },
    {
        "id": 4,
        "title": "Sharing Guidelines",
        "url": ""
    },
    {
        "id": 5,
        "title": "Member Responsibilities",
        "url": "https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Important+Documents/UHS+Member+Responsibilities.pdf"
    },
    {
        "id": 6,
        "title": "Statement of Shared Faith and Beliefs",
        "url": "https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Important+Documents/UHF-Statement-of-Shared-Faith-%26-Beliefs.pdf"
    },
    {
        "id": 7,
        "title": "Get Started with Health Sharing",
        "url": "https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Infographics/UHS+-+How+do+I+Get+Started+with+Health+Sharing.pdf"
    },
    {
        "id": 8,
        "title": "5 Questions about Health Sharing",
        "url": "https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Infographics/UHS+-+5+Questions+about+Health+Sharing.pdf"
    },
    {
        "id": 9,
        "title": "Tips on Telemedicine",
        "url": "https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Infographics/UHS+-+Tips+on+Telemedicine.pdf"
    },
    {
        "id": 10,
        "title": "4 Great Reasons to share UHS",
        "url": "https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Infographics/UHS+-+4+Great+Reasons+to+share+Universal+HealthShare+with+Your+Friends.pdf"
    },
    {
        "id": 11,
        "title": "3 Quick Facts for Doctors",
        "url": "https://carynhealth-memberportal-prod-documents.s3.us-east-2.amazonaws.com/Infographics/UHS+-+Doctors!+3+Quick+Facts+about+Health+Sharing.pdf"
    }

];

export const DashboarsCard = [
    {
        id: 1,
        title: AppStrings.cardWallet,
        image: "membership_card_icon_big.png",
        cardType: AppStrings.MemberID_Type,
        cardType2: AppStrings.HealthTool_Type,
        link: ''
    },
    {
        id: 4,
        cardType: AppStrings.PI_Type,
        title: AppStrings.programInformation,
        image: "program_info_icon_big.png",
        link: ''
    },
    {
        id: 7,
        title: AppStrings.findProvider,
        cardType: AppStrings.FindProvider_Type,
        image: "find_provider_icon_active_big.png",
        link: ''
    },

    {
        id: 5,
        cardType: AppStrings.MyNeeds_Type,
        title: AppStrings.myNeeds,
        image: "my_needs_big.png",
        link: ''
    },

    // {
    //     id: 3,
    //     title: AppStrings.notices,
    //     cardType: AppStrings.Notices_Type,
    //     image: "notices_icon_big.png",
    //     link: 'https://www.universalhealthfellowship.org/notices/'
    // },

    {
        id: 6,
        title: AppStrings.myTransactions,
        cardType: AppStrings.Transaction_Type,
        image: "my_transactions_icon_payment.png",
        link: ''
    },
    {
        id: 2,
        title: AppStrings.paymentWallet,
        cardType: AppStrings.Payment_Type,
        image: "my_transactions_icon_active.png",
        link: ''
    },
    {
        id: 8,
        title: AppStrings.healthQuestionnaire,
        cardType: AppStrings.HQ_Type,
        image: "medical_icon_active_big.png",
        link: ''
    },
    {
        id: 9,
        cardType: AppStrings.FAQ_Type,
        title: AppStrings.FAQs,
        image: "FAq_active_big.png",
        link: 'https://www.universalhealthfellowship.org/FAQs/'
    },
];
export const draweMenuList = [
    {
        id: 1,
        title: AppStrings.dashboard,
        image: "dashboard_icon_active_big.png",
        link: '',
        navigate: 'AppNavigator'

    },
    {
        id: 2,
        title: AppStrings.myNotifications,
        image: "my_notifications_icon_active.png",
        link: '',
        navigate: AppStrings.NotificationDetails,
        cardType: AppStrings.NotificationDetails
    },
    {
        id: 7,
        title: AppStrings.announcementsNotices,
        image: "notices_icon_big.png",
        link: 'https://www.universalhealthfellowship.org/notices/',
        // cardType: AppStrings.Notices_Type,
        navigate: AppStrings.NotificationDetails,
        cardType: AppStrings.announcementsNotices_type
    },
    {
        id: 3,
        title: AppStrings.documents,
        image: "documents_icon_active_big.png",
        link: '',
        cardType: AppStrings.Doc_Type,
        navigate: AppStrings.documents


    },
    {
        id: 4,
        title: AppStrings.telemed,
        image: "telemed_icon_active_big.png",
        link: '',
        iosDeeplink: "https://itunes.apple.com/us/app/mdlive/id839671393",
        androidDeeplink: "https://play.google.com/store/apps/details?id=com.mdlive.mobile"

    },
    {
        id: 5,
        title: AppStrings.uhsCard,
        image: "membership_card_icon_big.png",
        link: '',
        cardType: AppStrings.MemberID_Type,
        cardType2: AppStrings.HealthTool_Type,
        navigate: 'MembershipId'

    },
    {
        id: 6,
        title: AppStrings.paymentWallet,
        image: "my_transactions_icon_active.png",
        link: '',
        cardType: AppStrings.Payment_Type,
        navigate: AppStrings.paymentNav

    },

    {
        id: 8,
        title: AppStrings.programInformation,
        image: "program_info_icon_big.png",
        link: '',
        cardType: AppStrings.PI_Type,
        navigate: AppStrings.ProgramInformation

    },
    {
        id: 9,
        title: AppStrings.myNeeds,
        image: "my_needs_big.png",
        link: '',
        cardType: AppStrings.MyNeeds_Type,
        navigate: AppStrings.MyNeedsScreen

    },
    {
        id: 10,
        title: AppStrings.myTransactions,
        image: "my_transactions_icon_active.png",
        link: '',
        navigate: 'MyTransactions',
        cardType: AppStrings.Transaction_Type,
    },
    {
        id: 11,
        title: AppStrings.findProvider,
        image: "find_provider_icon_active_big.png",
        cardType: AppStrings.FindProvider_Type,
        link: ''

    },
    {
        id: 12,
        title: AppStrings.healthQuestionnaire,
        image: "medical_icon_active_big.png",
        cardType: AppStrings.HealthQuestionnaireNav,
        navigate: AppStrings.HealthQuestionnaireNav,
        link: ''
    },
    {
        id: 13,
        title: AppStrings.FAQs,
        image: "FAq_active_big.png",
        link: 'https://www.universalhealthfellowship.org/FAQs/',
        cardType: AppStrings.FAQ_Type,
    },
    {
        id: 14,
        title: AppStrings.ContactInformation,
        image: "contact_info_icon_active_big.png",
        link: '',
        cardType: AppStrings.contact_type,
        navigate: AppStrings.ContactCardScreen,
    },
    {
        id: 15,
        title: AppStrings.signOut,
        image: "logout_icon_big.png",
        link: ''

    },
];

export const memberApps = [

    {
        "id": 1002,
        "title": "Telemed",
        "image": "https://img.icons8.com/color/2x/therapy.png",
        "createdBy": null,
        "createdAt": "2019-01-02T18:30:00.000+0000",
        "iosDeeplink": "https://itunes.apple.com/us/app/mdlive/id839671393",
        "androidDeeplink": "https://play.google.com/store/apps/details?id=com.mdlive.mobile",
        "displayIndex": 1000
    }

];
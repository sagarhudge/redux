import React from "react";
import AppStrings from "../constants/AppStrings"
import COLORS from "../constants/COLORS"
import AsyncStorage from '@react-native-community/async-storage';
import { isAndroid } from "./PlatformCheck";
import { Linking } from "react-native";
import * as RNFS from 'react-native-fs';
import moment from "moment";
import { FontsApp, headerChat, headerStyle, headerStyleContainer, testFont } from '../constants/FontsApp'
import FileViewer from 'react-native-file-viewer';

export const passwordCheck = {

  chechForNumbers: (string) => {
    var matches = string.match(/\d+/g);
    let value = matches != null ? true : false;
    return value;

  },

  checkForUpperCase: (string) => {
    var matches = string.match(/[A-Z]/);
    let value = matches != null ? true : false;
    return value;

  },

  checkForLowerCase: (string) => {
    var matches = string.match(/[a-z]/);
    let value = matches != null ? true : false;
    return value;
  },

  checkForSymbols: (string) => {
    var symbols = new RegExp(/[^A-Za-z0-9]/);
    let value = symbols.test(string) ? true : false
    return value;
  },

  checkPwdLength: (string) => {
    let value = string.length > 7 ? true : false;
    return value;
  },
  validateEmail: (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    }
    else {
      return true;
    }
  },

  isValidJsonString: (jsonString) => {

    if (!(jsonString && typeof jsonString === "string")) {
      return false;
    }

    try {
      JSON.parse(jsonString);
      return true;
    } catch (error) {
      return false;
    }

  },
  isJson: (str) => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  },
  navigationHeader: (navigationHeader, title) => {
    if (navigationHeader) {
      return navigationHeader.setOptions({
        title: title, //Set Header Title

        headerStyle: {
          backgroundColor: COLORS.primaryColor, //Set Header color
          height: hp("6%")
        },
        headerTintColor: COLORS.white, //Set Header text color
        headerTitleStyle: {
          fontSize: hp("2.5%")
        },
      });
    }
  }
  ,
  navigationUtil: (screen, props, Connection, urlPath) => {
    var path = AppStrings.AppNavigator;
    //direct naviugation through card title 
    console.log("Notification", " ", screen, " urlPath ", urlPath, " Connection ", Connection);

    if (screen == "MyNeedsMobile" || screen == "MyNeeds" || screen == "Needs") {
      path = AppStrings.MyNeedsScreen;
    }
    else if ((screen == "MyTransactionMobile" ||
      screen == "MyTransaction" || screen == "Transaction") && urlPath != "MobileTransaction") {
      path = AppStrings.MyTransactionsNav;
    }
    else if (screen == "DocumentsScreen" || screen == "Documents" || screen == "Document") {
      path = AppStrings.DocumentsNav;
    }
    else if (screen == "MobileMedical" || screen == "HealthQuestionnaire" || screen == "Medical") {
      path = AppStrings.HealthQuestionnaireNav;
    }
    else if (screen == "ProgramInformation") {
      path = AppStrings.ProgramInformation;
    }
    else if (screen == "Paymentwallet") {
      path = AppStrings.paymentNav;
    }
    else if (screen == "DigitalHealthCardNew" || screen == "MembershipId") {
      path = AppStrings.DigitalHealthCardNew;
    }
    else if (screen == "HealthToolsCard" || screen == "HealthTool") {
      path = AppStrings.HealthToolCard;
    } else if ((screen == "MyTransactionMobile" ||
      screen == "MyTransaction" || screen == "ChangePayment") && urlPath == "MobileTransaction") {
      path = AppStrings.ChnagePaymentMethod;
    }
    else {
      path = AppStrings.AppNavigator;
    }
    console.log("Navigation to screen-------------------------", path, screen, urlPath)
    props.navigation.navigate(path, Connection = { Connection });
  },

  getCardTypeByPath: (screen) => {
    //get card type by url path
    let path = screen;

    if (screen == "MyNeedsMobile" || screen == "MyNeeds" || screen == "Needs") {
      path = AppStrings.MyNeeds_Type;
    }
    else if (screen == "MyTransactionMobile" ||
      screen == "MyTransaction" || screen == "Transaction") {
      path = AppStrings.Transaction_Type;
    }
    else if (screen == "DocumentsScreen" || screen == "Documents" || screen == "Document") {
      path = AppStrings.Doc_Type;
    }
    else if (screen == "MobileMedical" || screen == "HealthQuestionnaire" || screen == "Medical") {
      path = AppStrings.HQ_Type;
    }
    else if (screen == "ProgramInformation") {
      path = AppStrings.PI_Type;
    } else if (screen == "Paymentwallet") {
      path = AppStrings.Payment_Type;
    }
    else if (screen == "DigitalHealthCardNew" || screen == "MembershipId") {
      path = AppStrings.MemberID_Type;
    }
    else if (screen == "HealthToolsCard" || screen == "HealthTool") {
      path = AppStrings.HealthTool_Type;
    }
    else if (screen == "notices") {
      path = AppStrings.notices;
    }
    else if ((screen == "MobileTransaction" ||
      screen == "ChangePayment")) {
      path = AppStrings.Transaction_Type;

    }
    else {
      path = "";
    }
    return path;

  },
  imageofCardByName: (title) => {
    if (title === AppStrings.cardWallet)
      return require("../assets/images/membership_card_icon_big.png");
    else if (title === AppStrings.notices || title === AppStrings.announcementsNotices)
      return require("../assets/images/notices_icon_big.png");
    else if (title === AppStrings.paymentWallet)
      return require("../assets/images/my_transactions_icon_active.png");
    else if (title === AppStrings.programInformation)
      return require("../assets/images/program_info_icon_big.png");
    else if (title === AppStrings.myNeeds)
      return require("../assets/images/my_needs_big.png");
    else if (title === AppStrings.myTransactions)
      return require("../assets/images/my_transactions_icon_payment.png");
    else if (title === AppStrings.findProvider)
      return require("../assets/images/find_provider_icon_active_big.png");
    else if (title === AppStrings.healthQuestionnaire)
      return require("../assets/images/medical_icon_active_big.png");
    else if (title === AppStrings.FAQs)
      return require("../assets/images/FAq_active_big.png");
    else if (title === AppStrings.signOut)
      return require("../assets/images/logout_icon_big.png");
    else if (title === AppStrings.dashboard)
      return require("../assets/images/dashboard_icon_active_big.png");
    else if (title === AppStrings.documents)
      return require("../assets/images/documents_icon_active_big.png");
    else if (title === AppStrings.telemed)
      return require("../assets/images/telemed_icon_active_big.png");
    else if (title === AppStrings.uhsCard)
      return require("../assets/images/membership_card_icon_big.png");
    else if (title === AppStrings.myNotifications)
      return require("../assets/images/my_notifications_icon_active.png");
    else if (title === AppStrings.ContactInformation)
      return require("../assets/images/contact_info_icon_active_big.png");

  },
  notificationIcon: (title) => {
    // var title=title.toLowerCase();
    if (title == 'ALERT')
      return require("../assets/images/notification_alert_icon.png");
    else if (title == 'REMINDER')
      return require("../assets/images/notification_reminder_icon.png");
    else if (title == 'Notice' || title == 'NOTICE')
      return require("../assets/images/notification_notice_icon.png");
    else if (title == 'ANNOUNCEMENT')
      return require("../assets/images/notification_announcement_icon.png");
    else if (title == 'UPDATE')
      return require("../assets/images/notification_update_icon.png");
    else if (title == 'SURVEY')
      return require("../assets/images/notification_survey_icon.png");
    else
      return require("../assets/images/notification_promo_icon.png");

  },
  ArrayFilterFunction: (ArrayFilter, type) => {

    let array = ArrayFilter.filter(function (data) {
      return data.cardtitle == type;
    });

    return array[0];
  },
  
  responseError: (reponse) => {

    return reponse &&
      reponse.data ?
      reponse.data.message :
      reponse.message == "Network Error" ?
        AppStrings.noInternet :
        'Error'

  },
  showLocaleFile: (path) => {

    return FileViewer.open(path)
      .then((result) => {
        // success
        // console.log("---------------open--------------", result,path)
        return result;

      })
      .catch(error => {
        // error
        // console.log("------------error-----------------", error.message)
        return error
      });
  },
  getSessionStatus: (response) => {

    if (response) {
      let include = response.includes('401');
      return include;
    }

    return false;
  },

  statusColourCode: (response) => {
    switch (response) {

      case "ACTIVE":
      case "APPROVED":
        return "#27ae60";
      case "EXPIRED":
        return "#999999";
      case "VOIDED":
        return "#a9a9a9";
      case "DECLINED":
        return "#eb5757";
      case "BATCHED":
        return "#ffa500";
      case "PENDING":
        return "#ffa500";
      case "ISSUE":
        return "#ffa500";
      case "Audit":
        return "#27ae60";
      case "Authorized":
        return "#a9a9a9";
      case "Denied":
        return "#eb5757";
      case "Reversed":
        return "#ffa500";
      case "Pending":
        return "#ffa500";
      case "InReview ":
        return "#eb5757";
      case "Final ":
        return "#27ae60";
      default:
        return "#eb5757";
    }

  },
  dialNumber: (number) => {

    let phoneNumber = '';

    if (isAndroid) {
      phoneNumber = `tel:${number}`;
    }
    else {
      phoneNumber = `telprompt:${number}`;
    }

    Linking.openURL(phoneNumber);
  },
  openEmail: (email) => {
    const subject = " "
    const message = " "
    Linking.openURL(`mailto:${email}?subject=${subject}&body=${message}`)
  },

  getFilePathChange: () => {
    RNFS.readDir(RNFS.DownloadDirectoryPath) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
      .then((result) => {
        console.log('GOT RESULT***********************************************', result);

      })

  },

  getDateInUTC: (date, getInMillisecs) => {

    if (date) {
      let newDateTime = new Date(date)
      //  console.log(" newDateTime ",newDateTime)
      return new Date(newDateTime)

    }
    return date

  },

  dateformat: (dateEn) => {
    const enrollmentDate = passwordCheck.getDateInUTC(dateEn, true)
    let nowdate = passwordCheck.getDateInUTC(new Date(), true)

    var day = moment(enrollmentDate).format('DD')

    var mon = moment(enrollmentDate).format('MM')

    var year = moment(enrollmentDate).format('YYYY')

    var date = mon + "/" + day + "/" + year
    var todayDate = moment(nowdate).format('MM/DD/YYYY')

    // console.log(" todayDate ",todayDate," enrollmentDate ",enrollmentDate," dateEn ",dateEn," date",date)


    let hr = moment(enrollmentDate).format('hh')

    if (date !== todayDate) {

      return moment(date).format('MMM DD')

    } else {


      if (hr == '00') {
        return moment(enrollmentDate).format('m') + 'mins'
      }
      if (hr > 12) {
        return moment(enrollmentDate).format('hh:mm A')
      } else {
        return moment(enrollmentDate).format('hh:mm A')
      }

    }

  },

  getExpiryDate: (date) => {
    console.log("date", date)
    const isDateValid = moment(date, "MM/YY").isValid();
    console.log("isDateValid", isDateValid)

    if (isDateValid) {
      return moment(date, "MM/YY").format('MMMM YY');
    } else {
      return "NA"
    }
    return date;
  },

  applyHeader: (navigation, title) => {
    if (navigation) {
      navigation.setOptions({
        title: title, //Set Header Title

        headerStyle: headerStyleContainer,
        headerTintColor: COLORS.white,
        headerTitleStyle: headerStyle,
      });
    }
  },
  applyHeaderChat: (navigation, title) => {
    if (navigation) {
      navigation.setOptions({
        title: title, //Set Header Title

        headerStyle: headerChat,
        headerTintColor: COLORS.white,
        headerTitleStyle: headerStyle,
      });
    }
  }

}


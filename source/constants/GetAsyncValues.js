import AsyncStorage from '@react-native-community/async-storage';

export const getEmail = () => {
    return getAsync("email");
}
export const getClientID_4 = () => {
    return getAsync("client_id_locale");
}
export const getUsername = () => {
    return getAsync("username");
}
export const getAccessToken = () => {
 
    return getAsync("accesstoken");
}
export const getAllCards = () => {
    return getAsync("AllCards");
}
export const getSource = () => {
    return getAsync("source");
}
export const getMemberIdSource = () => {
    return getAsync("sourceID");
}
export const getGetIdCardData = () => {
    return getAsync("GetIdCardData");
}
export const getProvider_network = () => {
    return getAsync("provider_network");
}
export const getAgentInformation = () => {
    return getAsync("AgentInformation");
}
export const getAsync = async (key) => {
    return await AsyncStorage.getItem(key)
}
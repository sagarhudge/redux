import AsyncStorage from '@react-native-community/async-storage';
import BaseUrl from "../constants/BaseUrl";
import axios from 'axios';
import ReleaseManagement from '../constants/Dev_Production'
import { passwordCheck } from './utils';
import { exp } from 'react-native-reanimated';
import { Linking } from 'react-native';


export const getCardEnable = async (CardsList, cardType) => {
    //enable disable
    return getEnableCardList(CardsList).then(AllCards => {

        let newArray = passwordCheck.ArrayFilterFunction(AllCards, cardType);
        console.log("getCardEnable", cardType, newArray)

        return newArray && newArray.enable ? true : newArray && !newArray.enable ? false : true;
    });


}

export const getEnableCardList = async (CardsList) => {
    let AllCards = null;
    if (CardsList != null && CardsList != undefined) {
        AllCards = CardsList;
    } else {
        let data = await AsyncStorage.getItem('AllCards');
        AllCards = JSON.parse(data);
    }
    return AllCards;
}
export const getDataToken = async (suburl, token) => {
    let data = {
        method: 'GET',
        headers: {

            "Authorization": token
        }
    };
    return await fetch(suburl, data)
        .then((response) => {

            // try{
            //     if(response!=null&&response.headers!=null&&response.headers.get('content-type')!=null)
            //     {
            //         console.log("content-type "+response.headers.get('content-type'));
            //      if (response.headers.get('content-type').match(/application\/json/)) {
            //          return response.json();
            //      }

            //     }
            //    }catch(e){
            //     return response;

            //    }

            return response.text();
            // You can also try "return response.text();"

        })
};


export const getData = async (suburl) => {
    let data = {
        method: 'GET',
    };
    return await fetch(suburl, data)
        .then((response) => {
            // In this case, we check the content-type of the response
            try {
                if (response != null && response.headers != null && response.headers.get('content-type') != null) {
                    console.log("content-type " + response.headers.get('content-type'));
                    if (response.headers.get('content-type').match(/application\/json/)) {
                        return response.json();
                    }

                }
            } catch (e) {
                return response;

            }

            return response;
            // You can also try "return response.text();"
        });
};

export const postData = async (url, object, token) => {
    console.log("in_call" + token)
    return await fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
            , Authorization: token
        },
        body: JSON.stringify(object)
        // body:object
    });
}

export const postDataNoToken = async (url, object) => {
    return await fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },

        body: JSON.stringify(object)
    });
}

export const getGateWayToken = async () => {
    let request;

    if (ReleaseManagement.releaseType == 'PROD') {
        request = {
            "username": "admin",
            "password": "x1TXVUtXL6PaBWam"
        }
    } else {
        request = {
            "username": "admin",
            "password": "testpass"
        }
    }

    let url = BaseUrl.FABRIC_V1 + "login";
    console.log("getGateWayToken " + ReleaseManagement.releaseType + " " + url)
    return postAxiosWithToken(url, request, "");
}
export const getAxiosUser = async (url) => {
    return axios.get(url).then(response => {
        return response;
    }).catch(error => {
        if (error.response) {
            return error.response.data;
        } else {
            return error;
        }
    })
}
export const getAxiosWithToken = async (suburl, token) => {
    // https://stackoverflow.com/questions/41519092/using-axios-get-with-authorization-header-in-react-native-app

    var headers = token != "" ? {
        Authorization: token
    } : null
    return axios.get(suburl, { headers: headers }).then(response => {
        return response;
    }).catch(error => {
        if (error.response) {
            console.log(error.response.data)
            console.log(error.response.status);
            return error.response.data;
        } else {
            return error;
        }
    });
}

export const postAxiosWithToken = async (url, dataObject, token) => {

    var headers = token != "" ?
        {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        } : {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }

    return axios.post(url,
        dataObject,
        { headers: headers })
        .then((response) => {
            // console.log("response get details:" + response.data);
            return response;
        })
        .catch((error) => {
            // console.log(error.response.data);
            console.log(JSON.stringify(error));
            return error;
        });

}
export const postAxiosGetEmpId = async (email) => {
    let url = BaseUrl.REACT_APP_NEW_BASE_URL_SHAREPLUS + '/login';
    console.log("url------postAxiosGetEmpId----------------------*********************-", url, "\n", email)
    let request = {
        "username": "regulator",
        "password": "##Infyadmin1"
    }
    let headers = {
        'Content-type': 'application/json',
        'x-api-key': 'lIIwjqkmZF3V5T2Mk8qYG3aUStltuWQAaPkpq9JL'
    }

    return axios.post(url,
        request,
        { headers: headers })
        .then((response) => {

            let token = response.data.data.id_token
            // let email = email;
            // console.log("token----------------------*********************-", token)
            let url2 = BaseUrl.REACT_APP_NEW_BASE_URL_SHAREPLUS + '/member-report?report-type=getEmpi&email=' + email
            console.log("url2----------------------*********************-", url2)

            return axios.get(url2, {

                headers: {
                    'x-api-key': 'lIIwjqkmZF3V5T2Mk8qYG3aUStltuWQAaPkpq9JL',
                    'token': token
                }

            }).then(response => {

                    return response

                }).catch((error) => {
                    console.log(JSON.stringify(error));
                    return error;
                });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
            return error;
        });

}

export const getMyneedsEOS = (empId) => {
    //uses username and password to log into the API
    let request = {
        "username": "regulator",
        "password": "##Infyadmin1"
    }
    let url = BaseUrl.REACT_APP_NEW_BASE_URL_SHAREPLUS + '/login';
    console.log("My needsEOS", url)
    return axios.post(url, request, {
        headers: {
            'Content-type': 'application/json',
            'x-api-key': 'lIIwjqkmZF3V5T2Mk8qYG3aUStltuWQAaPkpq9JL'
        }
    }).then(response => {

        console.log("#######-EOS-######", empId)


        let token = response.data.data.id_token

        //returns api response from the needs API
        return axios.get(BaseUrl.REACT_APP_NEW_BASE_URL_NEEDS + '/test/subcriber/needs?empi=' + empId, {
            headers: {
                'x-api-key': 'lIIwjqkmZF3V5T2Mk8qYG3aUStltuWQAaPkpq9JL',
                'token': token
            }
        }).then(response => {
            return response
        }).catch(err => {

            return error;
        })
    })



}

export const getFindProvider = (accesstoken,email) => {
    let url = BaseUrl.FABRIC_V1 + 'memberportal/planinfo';
    let request = {
        'email': email,
        'type': "findaprovider"
    }
    console.log("findProvider", url, accesstoken, request);

    //this.setState({ loading: true });

    return postAxiosWithToken(url, request, accesstoken).then(success => {

        // this.setState({ loading: false });

        if (success && success.data && success.data.length != 0) {
            // Linking.openURL(success.data[0].fieldValue);
            return success.data[0].fieldValue;
        } else {
            var message = success.message;
            //let include = passwordCheck.getSessionStatus(message)
            //DeviceEventEmitter.emit('sessionOut', include)
            return message;
        }

    }).catch(e => {
        //this.setState({ loading: false });
        return e.message;
    });
}


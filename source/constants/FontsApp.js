// import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import COLORS from './COLORS';
import { isAndroid } from './PlatformCheck';
//const perc3="3%"
//const perc2.5="2.5%"

//use in both cases hp and wp
export const FontsApp = {

   heading:hp('3%'),//20px
   subHeading: hp('2.5%'),//18px
   regular: hp('2%'),  //14px
   medium: hp('2.3%'),//16px
   smallText: hp('1.7%'),//12px

   headingCardLarge: wp('4%'),//22px
   headingCard: wp('3%'),//20px
   subHeadingCard: wp('2.5%'),//18px
   regularCard: wp('2%'),//14p
   smallTextCard: wp('1.7%'),//12px
   headingCardStatus: wp('3.3%'),//20px

   inputTextHeight: hp('2.5%'),//18px
   labelActiveTop: -25,
   borderRadiusRound: wp('5%'),

   fontFamily: 'Roboto'

}

export const testFont = Platform.select({
   ios: { fontFamily: 'Roboto' },
   android: { fontFamily: 'Roboto' },
})

export const headerStyle = {
   fontSize: wp('4.5%'), ...testFont, fontWeight: 'bold'
}

export const headerStyleContainer = {
   backgroundColor: COLORS.primaryColor,
   height: hp("7%")
}
export const headerChat = {
   backgroundColor: COLORS.yellow,
   height: hp("7%")
}
export const cardTitle = {
   color: '#5f2161',
   textAlign: 'center',
   fontSize: wp('5%'), ...testFont,
   marginLeft: wp('2%')

}

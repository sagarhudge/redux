/**
 * Created by PERSCITUS-42 on 21-Nov-19.
 */

import React from "react";
import {Dimensions, Platform} from "react-native";

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export const isIphoneX =
    Platform.OS === 'ios' &&
    (deviceHeight === 812 ||
        deviceWidth === 812 ||
        deviceHeight === 896 ||
        deviceWidth === 896);

export const isAndroid =
    Platform.OS === 'android';
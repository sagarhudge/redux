export const getSendNeedsToContact = (network) => {
    switch (network) {
        case "PHCS":
            return 'P.O. Box 211223, Eagan, MN 55121';
        case "Smartshare":
            return 'P.O. Box 211223, Eagan, MN 55121';
        case "smartshare25":
            return 'PO Box 106 Rutherford, NJ 07070-0106';
        case "smartshare50":
            return 'PO Box 106 Rutherford, NJ 07070-0106';
        case "healthyLife":
            return 'PO Box 106 Rutherford, NJ 07070-0106';
        case "AFMC":
            return 'Arizona Foundation, PO Box 2909, Phoenix, AZ 85062-2909';
        default:
            break;
    }
}

 
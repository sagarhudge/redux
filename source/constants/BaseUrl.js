import ReleaseManagement from '../constants/Dev_Production'

const BaseUrl = {
   FABRIC_V1: ReleaseManagement.BaseApi + 'v1/',
   FABRIC_V2: ReleaseManagement.BaseApi + 'v2/',
   FABRIC_V4: ReleaseManagement.isTest ? ReleaseManagement.BaseApiTest + 'v4/' : ReleaseManagement.BaseApi + 'v4/',//can point to test
   FABRIC_V7: ReleaseManagement.isTest ? ReleaseManagement.BaseApiTest + 'v7/' : ReleaseManagement.BaseApi + 'v7/',//can point to test
   FABRIC_V6: ReleaseManagement.isTest ? ReleaseManagement.BaseApiTest + 'v6/' : ReleaseManagement.BaseApi + 'v6/',//can point test
   FABRIC_V_02: ReleaseManagement.isTest ? ReleaseManagement.BaseApiTest + 'v2.0/' : ReleaseManagement.BaseApi + 'v2.0/',//can point test
   FABRIC_V3: ReleaseManagement.isTest ? ReleaseManagement.BaseApiTest + 'v3.0/' : ReleaseManagement.BaseApi + 'v3.0/',//can point test
   V2_API_PATH: ReleaseManagement.isTest ? ReleaseManagement.BaseApiTest + 'v2/' : ReleaseManagement.BaseApi + 'v8/',  //can point test
   REACT_APP_NEW_BASE_URL_SHAREPLUS: ReleaseManagement.releaseType == "PROD" ?
      "https://jc08o9dt14.execute-api.us-east-2.amazonaws.com/v0"
      : "https://jc08o9dt14.execute-api.us-east-2.amazonaws.com/v0",

   REACT_APP_NEW_BASE_URL_NEEDS: ReleaseManagement.releaseType == "PROD"
      ? "https://jc08o9dt14.execute-api.us-east-2.amazonaws.com"
      : "https://jc08o9dt14.execute-api.us-east-2.amazonaws.com",

   REACT_APP_ENROLLMENT: ReleaseManagement.releaseType == "PROD" ?
      "https://enroll.universalhealthfellowship.org/" :
      "https://uat.enroll.universalhealthfellowship.org/",
   portalUrl: ReleaseManagement.portalUrl,

}

export default BaseUrl;
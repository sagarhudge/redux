import {Alert} from 'react-native';
import React from 'react';
import AppStrings from '../constants/AppStrings'

export const showAlert1 = (header, title) => {
    Alert.alert(header, title,
        [
            {text: AppStrings.cancel, onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
            {text: AppStrings.ok, onPress: () => console.log('OK Pressed')},
        ], {cancelable: true})
};

export const showAlert2 = () => {
    Alert.alert(
        'Alert Title',
        'My Alert Msg',
        [
            {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
            {
                text: AppStrings.cancel,
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            {text: AppStrings.ok, onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: true}
    )
};
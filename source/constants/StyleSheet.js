import React from "react-native";
import { Dimensions } from "react-native";
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import EStyleSheet from "react-native-extended-stylesheet";
import COLORS from "./COLORS";
import { isIphoneX } from "../constants/PlatformCheck";
import { testFont, FontsApp } from "./FontsApp";
let regular = FontsApp.regular;
let subHeadingCard = FontsApp.subHeadingCard;
let smallText = FontsApp.smallText;
let heading = FontsApp.heading;
const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({
    $rem: entireScreenWidth / 380,
    $appColorOrange: COLORS.primaryColor
});

export default styles = EStyleSheet.create({
    //error 
    errorContainer: {
        borderWidth: 1,
        borderColor: '#FF5722',
        color: '#FF5722',
        fontSize: smallText, ...testFont,
        padding: wp('2%'),
        marginTop: hp('2%'),
        fontWeight: 'bold', width: wp('80%')

    },
    error: {
        borderWidth: 1,
        borderColor: '#FF5722',
        color: '#FF5722',
        fontSize: smallText, ...testFont,
        padding: wp('3%'),
        fontWeight: 'bold', width: wp('80%')

    },

    errorRegular: {
        color: '#FF5722',
        fontSize: smallText, ...testFont,
        padding: wp('3%'),
        fontWeight: 'bold',

    },
    cardStyle: {
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        height: wp("41%"),
        width: wp("41%")
    },
    memberCardStyle: {
        //container member card
        borderRadius: 15,
        height: hp("20%"), width: wp("40%"), backgroundColor: COLORS.cardBackground
    },

    memberCard: {
        //header member card
        flexDirection: 'row',
        borderRadius: 10,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        width: wp("40%"),
        paddingStart: wp('3%')

    }, memberCardStyleWhite: {
        //container member card
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: wp("40%"),
        width: wp("40%"),
        backgroundColor: COLORS.white
    },
    //footer
    footerActive: {
        alignSelf: 'center',
        fontSize: regular, ...testFont,
        margin: hp('2%'),
        borderRadius: 5,
        paddingLeft: wp('4%'),
        paddingRight: wp('4%'),
        paddingBottom: hp('1%'),
        fontWeight: '800',
        paddingTop: hp('1%'),
        color: COLORS.primaryColor,
        borderColor: COLORS.primaryColor, borderWidth: 1,
    },
    footerInActive: {
        alignSelf: 'center',
        fontSize: subHeadingCard, ...testFont,
        margin: hp('2%'),
        borderRadius: 5,
        paddingLeft: wp('4%'),
        paddingRight: wp('4%'),
        paddingBottom: hp('1%'),
        fontWeight: '800',
        paddingTop: hp('1%'),
        color: COLORS.graya1,
        borderColor: COLORS.graya1, borderWidth: 1,
    },

    //Signin and Forgot Screen StyleSheet
    Img: {
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: 'contain',
    },
    universalImg: {
        height: hp('6%'),
        width: wp('35%'),
        resizeMode: 'contain',
    },
    welcomeImg: {
        height: hp('25%'),
        width: wp('50%'),
        top: 10,
        resizeMode: 'contain',
    },
    gotoSigin: {
        top: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    sendEmail: {
        marginTop: hp("1.5%"),
        marginBottom: hp("1.5%"),
        color: COLORS.black,
        fontSize: smallText, ...testFont,
        textAlign: 'center'
    },
    copyright: {
        fontSize: 8,
        textAlign: 'center',
        padding: 10,
        color: '#A9A4A3'
    },

    launchapplogo: {
        width: wp('5'),
        height: hp('5'),
        resizeMode: 'contain',
        alignItems: 'center'

    },
    launchappTitle: {
        color: 'black',
        marginTop: hp("1%"),
        textAlign: 'center',
        fontSize: regular, ...testFont,
    },
    launchicontouch: {
        alignItems: 'center',
        width: '80rem',
        height: '80rem',

    },


});

import React, { useEffect } from "react";
import { Button } from "react-native-elements";
import COLORS from "../constants/COLORS";
import { View, Text } from 'react-native';

import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';

import { FontsApp, testFont } from "../constants/FontsApp";
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText

export const TexhnicalError = props => {

    const { title, isDisabled } = props;
    const [check, setCheck] = React.useState(null);
    useEffect(() => { getToken(); }, []);

    const getToken = async () => {
        let data = await AsyncStorage.getItem('ContactNumber');
        setCheck(data);
    }

    return (
        <View style={{ paddingHorizontal: wp('5%'), minHeight: hp('30%'), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: regular, ...testFont }}>We’re facing some technical difficulties, due to
            which this feature is currently unavailable.
            For support, call Member Services at {check}, Monday through Friday, 8.00am to 8.00pm CST.
            </Text>
        </View>
    )

};

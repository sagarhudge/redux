
import React from 'react';
import { Text, TouchableOpacity, View, FlatList, Image } from "react-native";
import COLORS from "../constants/COLORS"
import { Card, CardItem, Body } from "native-base";

import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import moment from "moment";
import { passwordCheck } from '../constants/utils';
import AppStrings from '../constants/AppStrings';
import { FontsApp, testFont } from '../constants/FontsApp'
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
const RadioButtons = (props) => {
    const {
        type,
        options, viewMode,
        ...attributes
    } = props;

    let onPressButton = (data) => {
        props.onPressButton(data);
    }
    return (

        <View pointerEvents={viewMode ? "none" : null} style={{ flexDirection: 'row', marginTop: hp('1%') }}>

            {
                options.map((key, index) => (
                    <TouchableOpacity
                        key={key.id}
                        onPress={() => { onPressButton(key.id) }}

                        style={{ flexDirection: 'row', padding: hp('1%') }}>

                        <Image
                            resizeMode='contain'
                            style={{ height: hp('3%'), width: hp('3%') }}
                            source={type == key.id ? require('../assets/images/radioButtonChecked.png') : require('../assets/images/radioButtonUnchecked.png')}
                        />

                        <Text style={{ color: COLORS.textColor, marginLeft: wp('2%'), fontSize: regular, ...testFont }}>{key.option}</Text>

                    </TouchableOpacity>
                ))
            }



        </View>

    )

}
export default RadioButtons;

import React from 'react';
import { SafeAreaView } from 'react-native';
import { Text, Image, Modal, AppState, StyleSheet, View, TouchableOpacity } from 'react-native';
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import { isAndroid } from '../constants/PlatformCheck';

import AppStrings from '../constants/AppStrings'
import COLORS from '../constants/COLORS';
import { FontsApp, testFont, } from '../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
const AlertForeground = props => {
    const {
        loading, foregroundData,
        ...attributes
    } = props;
    const name = foregroundData != null ? foregroundData.title : "";
    const value = foregroundData != null ? foregroundData.body : "";
    let callbackReturn = (type) => {
        props.callbackClick(type);
    };
    return (
        <SafeAreaView>
            <Modal
                transparent={true}
                animationType={'fade'}
                visible={loading}
                onRequestClose={() => null}>

                <View style={styles.modalBackground}>
                    <TouchableOpacity onPress={() => {
                        callbackReturn("Cancel");

                    }}>
                        <View style={styles.activityIndicatorWrapper}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Image
                                style={{ height: hp('2.5%'), width: wp('2.5%'), resizeMode: 'contain', }}
                                source={require('../assets/images/ic_launcher.png')} />
                                <Text style={{ marginLeft: wp('1.5%'), fontSize: smallText, ...testFont }}>
                                    {AppStrings.appName}
                                </Text>
                            </View>

                            <Text style={{ fontWeight: 'bold', ...testFont, fontSize: regular, }}>
                                {name}
                            </Text>

                            <Text style={{ fontSize: regular, ...testFont }}>
                                {value.trim()}
                            </Text>

                            <View
                                onPress={() => { }}
                                style={{
                                    flexDirection: 'row', justifyContent: 'flex-end'
                                }}>
                                <TouchableOpacity onPress={() => { callbackReturn("View"); }}>
                                    <Text style={{
                                        textAlign: 'center', padding: wp('3%'),
                                        color: 'green', fontWeight: 'bold', fontSize: regular, ...testFont
                                    }}> View </Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { callbackReturn("Cancel"); }}>
                                    <Text style={{
                                        textAlign: 'center', color: 'red', padding: wp('3%'),
                                        fontWeight: 'bold', fontSize: regular, ...testFont
                                    }}> Cancel </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableOpacity>

                </View>

            </Modal>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({

    modalBackground: {
        flex: 1,
        top: isAndroid ? 0 : 25,
        width: wp("100%"),
        paddingLeft: wp('2%'),
        paddingEnd: wp('2%'),
        paddingTop: '1%',
        flexDirection: 'column',
        backgroundColor: '#00000040'
    },

    activityIndicatorWrapper: {
        backgroundColor: COLORS.white,
        minHeight: hp("12%"),
        paddingLeft: '3%',
        paddingRight: '3%',
        paddingTop: '3%',
        borderRadius: 10,
        justifyContent: 'center',
        textAlign: 'left'
    }

});

export default AlertForeground;
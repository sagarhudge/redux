import React from 'react';
import COLORS from '../constants/COLORS';
import AwesomeAlert from 'react-native-awesome-alerts';

const AwesomeAlertDialog = props => {
    const {
        show, title, message, alertCancel, alertConfirm, canBeClosed, type, okHide,
        ...attributes
    } = props;

    let callbackReturn = (visible, title, button) => {
        props.callbackClick(visible, title, button);
    };
    return (
        <AwesomeAlert
            show={show}
            showProgress={false}
            title={title}
            message={message}
            closeOnTouchOutside={false}
            closeOnHardwareBackPress={false}
            titleStyle={{ color: COLORS.textColor }}
            messageStyle={{ color: COLORS.textColor }}
            showConfirmButton={okHide != null || okHide != undefined ? okHide : true}
            showCancelButton={
                canBeClosed
            }
            cancelText={alertCancel}
            confirmText={alertConfirm}
            confirmButtonColor={COLORS.primaryColor}
            cancelButtonColor={alertCancel == "YES" ? COLORS.primaryColor : COLORS.cancelButton}

            onCancelPressed={() => {
                callbackReturn(false, type, "cancel");
            }}

            onConfirmPressed={() => {

                callbackReturn(false, type, "ok");

            }}
        />
    )

}

export default AwesomeAlertDialog;
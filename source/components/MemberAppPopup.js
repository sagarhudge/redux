
import { TouchableOpacity, ImageBackground, Text, StyleSheet, View } from "react-native";
import AppStrings from '../constants/AppStrings'
import React from "react";
import AsyncStorage from '@react-native-community/async-storage';

import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import { FontsApp, testFont, } from '../constants/FontsApp'
let heading = FontsApp.heading
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
export default class MemberAppPopup extends React.Component {

    constructor(props) {
        super(props);
        //   this.unsubscribe;
        this.props = props;
        this.state = { visible: false }
    }

    async componentDidMount() {
        var alert = await AsyncStorage.getItem('memberAlert');
        console.log('alert', alert);
        this.setState({ visible: alert === null ? true : alert === 'false' ? false : true });
    }

    render() {

        return (
            <View style={this.state.visible?styles.bottomView:null}>
                {
                    this.state.visible ?
                        <ImageBackground
                            style={styles.bottomView}
                            resizeMode='contain'
                            source={require('../assets/images/callout_01.png')}  >
                            <View style={styles.viewstyle}>
                                <Text style={styles.textStyle}>Check out Member Apps</Text>
                                <Text style={styles.textStyleNormal}> {`Talk to a doctor 24/7 for $0\nusing our Telemedicine App`}</Text>
                                <TouchableOpacity onPress={() => {
                                    AsyncStorage.setItem("memberAlert", "false");

                                    this.setState({ visible: false });

                                }
                                } style={{ marginTop: hp('1.5%'), backgroundColor: '#e9716f', borderRadius: hp('6%'), width: wp('20%'), height: hp('5.5%'), justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ textAlign: "center", ...testFont, fontSize: regular, color: "white", }}>Close</Text>
                                </TouchableOpacity>
                            </View>

                        </ImageBackground>
                        :
                        null
                }

            </View>

        );
    }
}

const styles = StyleSheet.create({

    bottomView: {
        width: '100%',
        height: hp("20%"),
        position: 'absolute',
        top: 0, 
    },

    viewstyle: {
        marginTop: hp('1%'),
        padding: hp('4%'),
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        color: '#000000',
        fontSize: regular, ...testFont,
        textAlign: 'center',
        fontWeight: 'bold'

    },
    textStyleNormal: {
        color: '#000000',
        fontSize: regular, ...testFont,
        textAlign: 'center',

    },
});
/**
 * Created by PERSCITUS-42 on 09-Dec-19.
 */
import { Image, Text, Modal, StyleSheet, View, SafeAreaView } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import AppStrings from '../constants/AppStrings'
import React from "react";


import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

export default class NoInternetScreen extends React.Component {

    constructor(props) {
        super(props);
     //   this.unsubscribe;
        this.props = props;
        this.state = { connectionInfo: '', connected: false }
    }

    callbackreturn(isConnected, type) {
        this.props.abc(isConnected);
    }

    componentDidMount() {
        this.unsubscribe = NetInfo.addEventListener(state => {
            if (state.isConnected !== this.state.connected) {
                //if its already connected or disconnected to call function again and again
                this.setState({ connected: state.isConnected });
                this.callbackreturn(state.isConnected, state.type)
            }
        });

        NetInfo.fetch().then(state => {
          
            this.setState({ connected: state.isConnected });
            this.callbackreturn(state.isConnected, state.type)

        });
        

    }

    componentWillUnmount() {
        this.unsubscribe && this.unsubscribe();
    }

    render() {
        const { connectionInfo } = this.state;

        return (

            !this.state.connected ?
                <SafeAreaView  style={{ flex: 1, alignItems:'center', justifyContent: 'center', backgroundColor: '#d4d4d4', width:wp("100%"), height:hp("100%"), position:'absolute' }}>
                       <Image
                                style={{ height: hp('10%'), width: wp('40%'), resizeMode: 'contain', margin:hp('5%') }}
                                source={require('../assets/images/noInternet.png')} />
                   <Text style={{fontSize:hp('2.5%'), textAlign:'center', fontWeight:'bold'}}>{AppStrings.noInternet}</Text>
                </SafeAreaView> : null

        );
    }
}


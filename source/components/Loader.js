import React from 'react';
import {ActivityIndicator, Modal, StyleSheet, View} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLORS from '../constants/COLORS';
const Loader = props => {
    const {
        loading,
        ...attributes
    } = props;

    return (
        <Modal

            transparent={true}
            animationType={'none'}
            visible={loading}
            onRequestClose={() =>null}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator
                    color="#0000ff"
                    size="small"
                    animating={loading}/>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor:COLORS.white,
        height: hp("15%"),
        width: hp("15%"),
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});

export default Loader;
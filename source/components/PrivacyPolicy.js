import React from 'react';
import { Text, Modal, StyleSheet, View, TouchableOpacity } from 'react-native';
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import { WebView } from 'react-native-webview';
import Icon from 'react-native-vector-icons/FontAwesome';

import COLORS from '../constants/COLORS';
import { FontsApp, testFont } from '../constants/FontsApp'
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText

const PolicyHTML = '<div class="modal-body" style=" text-align: justify; word-spacing: -2px;font-size: 35px;padding: 15px"><div class="modal-body" style=" text-align: justify; word-spacing: -2px;"><div style="padding-top: 8px; font-weight: bold;">Privacy Statement</div><div style="padding-top: 8px;">This Privacy Statement is effective May 1, 2019.</div><div style="padding-top: 8px;">Universal Health Fellowship understands that when our clients, partners and other individuals provide personal data to us, they place their trust in us. We take this trust seriously and are committed to respecting each individual’s privacy and protecting the personal data we handle.</div><div style="padding-top: 8px;">This Privacy Statement describes our privacy practices to help you understand what personal data we collect, use, share and transfer and to inform you about the choices you can make regarding your personal data.</div><div style="padding-top: 8px;">This Privacy Statement applies to the Universal Health Fellowship websites, domains, or applications that link to this Statement. We may supplement this Privacy Statement with a privacy statement for a specific product, service or website. When we do, the more specific statement is applicable.</div><div style="padding-top: 8px; font-weight: bold;">How We Collect and Use Personal Data</div><div style="padding-top: 8px;">We collect and use personal data that you provide in order to operate our business, provide our products and services, send marketing and other communications, and comply with applicable laws and regulations. In addition to the data you provide to us directly, we may also process personal data about you that we receive from our clients or third parties.</div><div style="padding-top: 8px;">The types of personal data we process will depend on the purpose, including:</div><div style="padding-top: 8px;">Provide products and services – contact details (such as name, email, address, company name, phone number and other information necessary) to provide services to our clients and our client’s customers, including providing product support updates;</div><div style="padding-top: 8px;">Improve our products and services – contact details to conduct quality controls and evaluate the performance of our products and services, including conducting customer satisfaction surveys;</div><div style="padding-top: 8px;">Conduct due diligence – contact details and publicly available information about financial or reputational status of a client or third party supplier/partner;</div><div style="padding-top: 8px;">Generate sales and marketing leads – contact details, marketing preferences, publicly available social media information to maintain a client relationship management database and send relevant newsletters, solution updates, event notifications and other marketing communications;</div><div style="padding-top: 8px;">Manage relationships with our clients, suppliers and partners – contact details and payment information in order to execute contracts, generate invoices and make payments; and</div><div style="padding-top: 8px;">Respond to inquiries or requests for information – contact details for electronic communication.</div><div style="padding-top: 8px;">When we receive personal data about you from our clients in order to provide our services, Universal Health Fellowship processes the personal data as instructed by our clients and in accordance with our contractual obligations. Our clients are responsible for complying with regulations or laws regarding notice, disclosure, and/or obtaining consent prior to transferring the personal data to us for processing.</div><div style="padding-top: 8px; font-weight: bold;">How We Protect Your Personal Data</div><div style="padding-top: 8px;">We use reasonable security procedures and technical and organizational measures to protect against accidental or unlawful destruction, loss, disclosure or use of personal data we handle. We limit access to and use of your personal data to authorized persons and trusted third parties who have a reasonable need to know the information in order to perform our services and business operations and who are bound by confidentiality obligations.</div><div style="padding-top: 8px; font-weight: bold;">How Long We Retain Personal Data</div><div style="padding-top: 8px;">We retain your personal data only for as long as is necessary to fulfill the purpose for which the data was collected from you and in consideration of and compliance with applicable legal or regulatory requirements to maintain the data for legitimate purposes. For example, where required by law for audits or accounting requirements, to enforce our agreements or handle disputes. When personal data is no longer needed for the purpose it was collected or processed or to comply with a legal obligation, we securely destroy it.</div><div style="padding-top: 8px; font-weight: bold;">How We Share Your Personal Data</div><div style="padding-top: 8px;">We do not sell or otherwise disclose personal data about our website visitors or others that interact with Universal Health Fellowship or our products or services, except as described herein.</div><div style="padding-top: 8px;">We may share your personal data with authorized Universal Health Fellowship personnel with a need to know the information in order to process the personal data for the purpose we collected it. We also share personal data with third parties who are acting on our behalf in order to provide the products or services you request or to support our relationship with you. These third parties are not authorized by us to use or disclose the information except as necessary to perform services on our behalf pursuant to a contractual obligation or to comply with legal requirements. Universal Health Fellowship requires such third parties to comply with applicable data protection and privacy laws and agree to implement and maintain appropriate technical and organizational security measures to safeguard the personal data.</div><div style="padding-top: 8px;">Our sharing may include:</div><div style="padding-top: 8px;">With any of our Universal Health Fellowship subsidiaries and trusted third party suppliers/partners in order to perform our services or business operations;</div><div style="padding-top: 8px;">With our professional advisors and insurers to run our business;</div><div style="padding-top: 8px;">With legal authorities, government agencies, law enforcement authorities or other government officials when required by applicable laws or regulations or or pursuant to legal process (including for national security purposes);when we believe disclosure is necessary or appropriate to prevent physical harm or financial loss or in connection with an investigation of suspected or actual fraud or illegal activity; or when we believe that disclosure is necessary to protect our rights, protect your safety or the safety of others;</div><div style="padding-top: 8px;">With appropriate third parties in connection with the sale, transfer or financing of all or part of a Universal Health Fellowship business or its assets, including any such activities associated with a bankruptcy proceeding.</div><div style="padding-top: 8px; font-weight: bold;">How We Treat Personal Data from Individuals Outside the U.S.</div><div style="padding-top: 8px;">Universal Health Fellowship is a U.S. enterprise based in the U.S. Our products, services, and offerings are only available to parties within the U.S., and may be subject to further geographic or other limitations within the U.S. While we cannot prevent individuals from outside the U.S. from providing contact information to Universal Health Fellowshipvia our contact forms, we will not knowingly use information from parties outside the U.S., and will periodically purge any individual contact data known to be from individuals outside the U.S. from our databases.</div><div style="padding-top: 8px; font-weight: bold;">How We Respect Your Privacy in Marketing</div><div style="padding-top: 8px;">Universal Health Fellowship uses various means of communication and tools to market its products and services. We provide commercial marketing email to persons who consent or where we have a legitimate interest and it is otherwise permissible under applicable law. You may opt-out of our commercial marketing emails by using the “Unsubscribe” or “Opt-out” link in the commercial email or by contacting us at <a href="mailto:info@UniversalHealthFellowship.org." style="color: rgb(83, 50, 120); font-weight: bold;">info@universalhealthfellowship.org</a>.</div><div style="padding-top: 8px;">We may strive to make your Universal Health Fellowship experience the best it can be by using various tools that provide us with information about how you and other visitors interact with our website so that we can tailor your Universal Health Fellowship experience and improve our web properties. These tools may include Cookies and other automatic data collection tools.</div><div style="padding-top: 8px; font-weight: bold;">How To Request Access To Your Personal Data</div><div style="padding-top: 8px;">We rely on you to provide accurate, complete and current personal data to us.</div><div style="padding-top: 8px;">If you need to correct or update the personal data you provided to us, we will respond in a timely manner to all reasonable requests to access, correct or delete your personal data. Requests and questions can be submitted to <a href="mailto:info@UniversalHealthFellowship.org." style="color: rgb(83, 50, 120); font-weight: bold;">info@universalhealthfellowship.org</a>.</div><div style="padding-top: 8px; font-weight: bold;">How to Contact Us</div><div style="padding-top: 8px;">If you have any questions or comments regarding this statement or our privacy practices, or any concerns regarding a possible violation of this statement, our practices or any applicable privacy law please contact Universal Health Fellowship at <span><a href="mailto:info@UniversalHealthFellowship.org." style="color: rgb(83, 50, 120); font-weight: bold;">info@universalhealthfellowship.org </a></span><span> and we will promptly respond.</span></div><div style="padding-top: 8px; font-weight: bold;">Our mailing address is:</div><div style="padding-top: 8px;">Universal Health Fellowship, Inc.</div><div style="padding-top: 8px;">Attn: Privacy Administrator</div><div style="padding-top: 8px;">4555 Mansell Road, Suite 300,</div><div style="padding-top: 8px;">Alpharetta, GA 30022</div><div style="padding-top: 8px;">United States</div><div style="padding-top: 8px;"><a href="http://www.universalhealthfellowship.org" style="color: rgb(83, 50, 120); font-weight: bold;" target="_blank">www.UniversalHealthFellowship.org</a></div><div style="padding-top: 8px; font-weight: bold;">Links to Other Websites</div><div style="padding-top: 8px;">As a convenience to our website visitors, we sometimes provide links to other websites. These linked sites are not under the control of Universal Health Fellowship and we are not responsible for their content. You should review the privacy statements of any linked website to understand their privacy practices before using the site.</div><div style="padding-top: 8px; font-weight: bold;">Children</div><div style="padding-top: 8px;">It is not our intent to collect personal data from children under the age of consent. Our websites are not designed to attract children and we request that children under the age of consent not submit personal data to us through our websites.</div><div style="padding-top: 8px; font-weight: bold;">Updates to Our Privacy Statement</div><div style="padding-top: 8px;">We may update this Privacy Statement from time to time. When we update this statement, we will also update the date at the top. Only the current statement is effective, so please review it periodically.</div></div></div>';


const PrivacyPolicy = props => {

    const {
        display, viewMode,
        ...attributes
    } = props;

    let buttonPress = (result) => {
        props.privacyPolicy(result);
    }

    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={display}
            onRequestClose={() => null}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <View style={{ height: hp('7%'), backgroundColor: '#34AB8F', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingHorizontal: wp('3%') }}>
                        <Text style={{ fontSize: subHeading, ...testFont, color: COLORS.white, marginLeft: wp('2%'), fontWeight: 'bold' }}>Privacy Policy</Text>
                        <TouchableOpacity onPress={() => buttonPress(null)}>
                            <Icon name="close" size={wp('4%')} color={COLORS.white} />
                        </TouchableOpacity>
                    </View>
                    <WebView
                        cacheEnabled
                        sharedCookiesEnabled={true}
                        source={{ html: PolicyHTML }} />

                    <View
                        pointerEvents={viewMode ? "none" : null}
                        style={{
                            flexDirection: 'row', alignItems: 'center',
                            justifyContent: 'flex-end', margin: hp('2%')
                        }}>
                        <Text
                            onPress={() => { buttonPress(false) }}
                            style={{ padding: hp('2%'), fontSize: smallText, ...testFont, borderRadius: 12, backgroundColor: !viewMode ? '#FEC043' : COLORS.graylight, fontWeight: 'bold' }}>CANCEL</Text>
                        <Text
                            onPress={() => { buttonPress(true) }}
                            style={{ padding: hp('2%'), borderRadius: 12, fontSize: smallText, ...testFont, backgroundColor: !viewMode ? '#FEC043' : COLORS.graylight, marginLeft: wp('2%'), fontWeight: 'bold' }}>ACCEPT</Text>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },

    activityIndicatorWrapper: {
        backgroundColor: COLORS.white,
        height: hp("90%"),
        width: wp("90%"),
    }
});

export default PrivacyPolicy;
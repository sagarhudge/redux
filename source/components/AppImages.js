import React from "react";
import styles from "../constants/StyleSheet";
 
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Image, View } from 'react-native';

export const AppImages = props => {
    return (
        <View style={styles.Img}>

            <Image
                style={styles.universalImg}
                source={require('../assets/images/UHS.png')} />
            <Image
                style={styles.welcomeImg}
                source={require('../assets/images/welcome_image_mobile.png')}
            />
        </View>
    )
};
export const AppImagesSplash = props => {
    return (
        <View style={{ alignContent: 'center', justifyContent: 'center', alignItems: 'center' }}>
            <Image
                style={{ height: hp("9"), width: "50%", resizeMode: 'contain', }}
                source={require('../assets/images/UHS.png')}
            />
            <Image
                style={{ width: wp('78%'), height: hp('40%'), resizeMode: 'contain', marginTop: hp("3%") }}
                source={require('../assets/images/welcome_image_mobile.png')}
            />

        </View>
    )
};
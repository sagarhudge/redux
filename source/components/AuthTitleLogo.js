
import React, { Component } from 'react';
import { View, Text, Image, } from 'react-native';
import styles from '../assets/StyleSheet/Styles';
 export default class AuthTitleLogo extends Component {
  render() {
    return (
      <View style={styles.authtitle_container}>
        <Text style={styles.authtitle_login_text}>Log in</Text>

        <Image source={require('../assets/images/logo.png')} style={styles.authtitle_logo} />
      </View>
    )
  }
}








import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, Linking, Keyboard, DeviceEventEmitter, StyleSheet } from 'react-native';
import COLORS from "../../constants/COLORS"
import { Card, CardItem, Body } from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
import BaseUrl from "../../constants/BaseUrl"
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken, getCardEnable } from '../../constants/CommonServices'
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import { DocumentList } from '../../constants/ArrayConst';
import AppStrings from '../../constants/AppStrings';
import { Alert } from 'react-native';
import { passwordCheck } from '../../constants/utils';
import { testFont, FontsApp, cardTitle } from '../../constants/FontsApp'
import { TexhnicalError } from '../TexhnicalError';
import CardChatInput from '../../screens/botchat/component/CardChatInput';
let heading = FontsApp.heading;
let subHeading = FontsApp.headingCardLarge
let regular = FontsApp.regular
let smallText = FontsApp.smallText

export default class DocumentsCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            error: "",
            enable: false,
            contactNumber: '',
            Connection: this.props.Connection,
            query: '',
            memberId: '',
        }
        console.log("--------------------------------", this.props.Connection)
    }
    async componentDidMount() {
        this.email = await AsyncStorage.getItem('email');
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        let idcardData = JSON.parse(await AsyncStorage.getItem("GetIdCardData"));
        let Data = idcardData[0]
        this.setState({ memberId: Data.memberId, })
        getCardEnable(this.AllCards, AppStrings.Doc_Type).then(
            resp => {
                this.setState({ loading: true, enable: resp });
            }
        );

        this.getWelcomeBooklet("welcomekit");

    }
    getWelcomeBooklet(type) {
        let getWelcomeBooklet = BaseUrl.FABRIC_V1 + "memberportal/planinfo";
        let request = { "email": this.email, "type": type }
        // console.log("getWelcomeBooklet", getWelcomeBooklet, this.accesstoken);
        postAxiosWithToken(getWelcomeBooklet, request, this.accesstoken).then(response => {
            this.setState({ loading: false });
            if (response && response.data && response.data.length != 0) {
                var value = response.data[0].fieldValue;
                if (type === "welcomekit") {
                    this.welcomekit = value;
                    AsyncStorage.setItem('welcomekit',value)
                    this.getWelcomeBooklet("guidelines");
                } else {
                    this.guidelines = value;
                    AsyncStorage.setItem('guidelines',value)

                    this.getData();
                }
            } else {
                this.getData();
                var message = response.message;
                let include = passwordCheck.getSessionStatus(message)
                DeviceEventEmitter.emit('sessionOut', include)
            }
        });
    }
    getData = () => {
        this.setState({
            data: DocumentList,
            loading: false,
        });
    };

    render() {

        return (
            <View  >
                <Card style={{ width: wp('90%'), marginTop: hp('2.5%') }}>
                    <CardItem button bordered >

                        <TouchableOpacity
                            style={{ flexDirection: 'row', alignItems: 'center' }} >
                            <Image
                                style={{ height: wp("6%"), width: wp("6%"), resizeMode: 'contain' }}
                                source={require('../../assets/images/documents_icon_active_big.png')}
                            />
                            <Text style={cardTitle}>{AppStrings.documents}</Text>
                        </TouchableOpacity>
                    </CardItem>

                    {/* {this.state.data && this.state.data.length > 0 && <CardItem style={{ backgroundColor: '#cdaccf', }}>
                        <CardChatInput
                            onPressSend={(result) => {
                                Keyboard.dismiss();
                                let url = `https://inf-labs.com/?isnav=false&ismobile=true&memberId=${this.state.memberId}&query=${result}`

                                this.props.navigation.navigate("NewEnrollMent", { url: url, isChat: true });

                                // this.props.navigation.navigate("ChatScreen", { isSearch: true, result: result, Connection: this.state.Connection })
                            }} />
                    </CardItem>
                    } */}
                    {
                        this.state.enable ?
                            <CardItem style={{
                                backgroundColor: COLORS.cardBackground,
                                height: hp('30%')
                            }}>

                                <FlatList
                                    showsVerticalScrollIndicator={true}
                                    persistentScrollbar
                                    nestedScrollEnabled
                                    numColumns={1}

                                    scrollEnabled={true}
                                    data={this.state.data}
                                    renderItem={this.renderItem.bind(this)}
                                    keyExtractor={(item, index) => index.toString()}
                                />

                            </CardItem>
                            :
                            <TexhnicalError title={this.state.contactNumber} />
                    }


                </Card>
            </View>
        )
    }
    renderItem({ item }) {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.checkMedLifeapp(item.title, item.url);
                }}>
                <Text style={styles.textStyle} >{item.title}</Text>
            </TouchableOpacity>
        )
    }
    checkMedLifeapp = (title, link) => {
        console.log(title, link)
        if (title == "Welcome Booklet") {
            link = this.welcomekit;
        }
        if (title == "Sharing Guidelines") {
            link = this.guidelines;
        }
        if (link && link != "") {
            Linking.openURL(link).catch(err => { });
        } else {
            //show error here
        }
    };

}

const styles = StyleSheet.create({

    heading: { fontSize: regular, ...testFont, color: '#33ab8f', fontWeight: 'bold' },
    textStyle: {
        color: '#000000',
        padding: wp('2.5%'),
        fontSize: regular, ...testFont,

    },

});







import React from 'react';
import { Text, TouchableOpacity, View, FlatList, Image, StyleSheet } from "react-native";
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import COLORS from "../../constants/COLORS"
import { Card, CardItem, Body } from "native-base";

import moment from "moment";
import { passwordCheck } from '../../constants/utils';
import AppStrings from '../../constants/AppStrings';

import { cardTitle, FontsApp, testFont, } from '../../constants/FontsApp'
let heading = FontsApp.heading
let subHeading = FontsApp.headingCardLarge
let regular = FontsApp.regular
let smallText = FontsApp.smallText

const NotificationCard = (props) => {

    const {
        SpecialistArray, loading,
        ...attributes
    } = props;
    // console.log("nameTitle", loading)
    let onPressNotification = (data) => {
        props.notificationClick(data);
    }

    let renderItem = ({ item }) => {

        return (
            <TouchableOpacity
                style={{
                    backgroundColor: COLORS.white,
                    flexDirection: 'row',
                    flex: 1,
                    alignItems: 'center',
                    borderBottomColor: COLORS.graylight,
                    borderBottomWidth: 1.0
                }}
                onPress={(result) => { onPressNotification(item); }}>
                <Image
                    style={{ height: hp("10%"), width: wp("11%"), resizeMode: 'contain' }}
                    source={passwordCheck.notificationIcon(loading ? item.title : item.type)} />
                <View>
                    <View style={{ flexDirection: 'row', width: wp('67%'), justifyContent: 'space-between' }}>

                        <Text numberOfLines={1} ellipsizeMode='tail'
                            style={{
                                color: COLORS.subText, flex: 1,
                                fontSize: regular, ...testFont,
                                marginLeft: wp('2%'),
                            }}>{loading ? item.title : item.type}</Text>

                        <Text style={{
                            color: COLORS.subText,
                            fontSize: regular, ...testFont,
                            textAlign: 'auto', marginLeft: 3
                        }}>{passwordCheck.dateformat(item.createdDate ? item.createdDate : item.created_date)}</Text>
                    </View>
                    <Text
                        numberOfLines={2} ellipsizeMode='tail'
                        style={{
                            color: 'black',
                            fontSize: regular, ...testFont,
                            marginLeft: wp('2%'), width: wp('67%')
                        }}>{loading ? item.message.trim() : item.title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    // marginTop: loading ? hp('3%') : hp('2%')
    return (
        <View style={{ alignItems: 'center' }} >
            <Card style={{ width: wp('90%'),  }}>
                <CardItem button bordered >
                    <TouchableOpacity
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => {
                            props.notificationClick(null);
                        }}>
                        <Image
                            style={{ height: wp("8%"), width: wp("7%"), resizeMode: 'contain' }}
                            source={loading ? require('../../assets/images/my_notifications_icon_active.png') : require('../../assets/images/notices_icon_big.png')}
                        />
                        <Text style={{
                            color: '#5f2161', flex: 2,
                            textAlign: 'left',
                            fontSize: wp('5%'), ...testFont,
                            marginLeft: wp('2%')
                        }}>{loading ? AppStrings.myNotifications : AppStrings.announcementsNotices}</Text>

                        <Text style={{
                            color: '#808080',
                            textAlign: 'right', fontWeight: 'bold',
                            fontSize: wp('3.5%'), ...testFont,
                            flex: 1,
                        }}>View All</Text>
                    </TouchableOpacity>

                </CardItem>
                <CardItem >
                    {SpecialistArray != 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            numColumns={1}
                            data={SpecialistArray}
                            renderItem={renderItem.bind(this)}
                            keyExtractor={(item, index) => index.toString()}
                        /> :
                        <View style={{
                            flex: 1, minHeight: hp('10%'),
                            alignItems: 'center', justifyContent: 'center'
                        }}>
                            <Text style={{ fontSize: regular, ...testFont, }}>
                                {loading ? "You currently have no new notifications." : "You currently have no new announcements & notices."}
                            </Text>
                        </View>
                    }

                </CardItem>

            </Card>
        </View>
    )


}

const styles = StyleSheet.create({

    heading: { fontSize: regular, ...testFont, color: '#33ab8f', fontWeight: 'bold' },
    textStyle: {
        fontSize: regular, ...testFont, color: '#4f4f4f', marginTop: hp("1%")
    },

});


export default NotificationCard;





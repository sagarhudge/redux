
import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, DeviceEventEmitter, StyleSheet } from 'react-native';
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import AppStrings from '../../constants/AppStrings';
import COLORS from "../../constants/COLORS"
import { passwordCheck } from '../../constants/utils';
import { postAxiosWithToken } from '../../constants/CommonServices';
import BaseUrl from '../../constants/BaseUrl';
import AsyncStorage from '@react-native-community/async-storage';
import {FontsApp,testFont,} from '../../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
let headingCardLarge = FontsApp.headingCardLarge;

export default class MemberSmallCard extends Component {
    constructor(props) {
        super(props)
        this.accesstoken;
        this.imageNames = passwordCheck.imageofCardByName(AppStrings.cardWallet);
        this.state = {
            cardNameVisible: true,
            AllCards: this.props.AllCards,
            showhealthtools: false,

        }
    }

    callbackReturn = (type) => {
        this.props.callbackMembercard(type);
        this.setState({ cardNameVisible: true });
    };

    componentDidMount() {

        this.getCards();

    }

    async getCards() {
        let url = BaseUrl.FABRIC_V1 + "memberportal/getGroupNumber"
        this.email = await AsyncStorage.getItem('email');
        let isHealthTool = await AsyncStorage.getItem('isHealthTool');
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        
        let object = {
            "email": this.email
        }

         this.setState({ showhealthtools: isHealthTool && isHealthTool == 'true' ? true : false });
         console.log('GET CARD WALLET CARDS-', url, object, this.accesstoken)

         postAxiosWithToken(url, object, this.accesstoken).then(res => {

            if (res && res.data && res.status == 200) {

                let isHealthTool=false;
                if (typeof res.data === 'string') {
                    isHealthTool = true;
                } else {
                    isHealthTool = false;
                }
                this.setState({ showhealthtools: isHealthTool })

                console.log('GET CARD WALLET CARDS-----', isHealthTool);
                AsyncStorage.setItem("isHealthTool", "" + isHealthTool);

            } else {
                this.setState({ showhealthtools: false })

                var message = res.message;
                console.log("isHealthTool",'ERROR-----------------', message);
                AsyncStorage.setItem("isHealthTool", "false" );

                let include = passwordCheck.getSessionStatus(message)
                DeviceEventEmitter.emit('sessionOut', include)

            }

        }).catch(e => {
            console.log("Error-CardWalletSmall", e.message)
        })

    }

    render() {
        return (
            <View>
                {
                    this.state.cardNameVisible
                        ?
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ cardNameVisible: false });
                            }}
                            style={styles.memberCardStyleWhite}>

                            <Image
                                style={{ width: wp('16%'), resizeMode: 'contain', height: wp('16%') }}
                                source={this.imageNames}
                            />

                            <Text style={{
                                color: '#5f2161',
                                marginTop: hp("3%"),
                                textAlign: 'center',
                                fontSize: headingCardLarge,...testFont,
                                fontWeight: 'bold'
                            }}>{AppStrings.cardWallet}</Text>

                        </TouchableOpacity>
                        :
                        <View style={styles.memberCardStyle} >
                            <View style={styles.memberCard}>
                                <Image
                                    style={{
                                        height: wp("6%"),
                                        width: wp("6%"),
                                        resizeMode: 'contain'
                                    }}
                                    source={this.imageNames}
                                />
                                <Text style={styles.heading}>{AppStrings.cardWallet}</Text>
                            </View>
                            <Text onPress={() => {
                                this.callbackReturn("MembershipId");
                            }}
                                style={styles.cardTitleSub}>Membership Card</Text>
                            {
                                this.state.showhealthtools ?
                                    <Text onPress={() => {
                                        this.callbackReturn("HealthTool");
                                    }}
                                        style={styles.cardTitleSub}>Health Tools</Text>
                                    : null
                            }

                        </View>
                }

            </View>
        )
    }
}


const styles = StyleSheet.create({

    heading: {
        color: '#5f2161',
        textAlign: 'center',
        fontSize: regular,...testFont,
        marginLeft: wp('3%'),
        fontWeight: 'bold',
    },
    textStyle: {
        color: '#000000',
        padding: hp('1%'),
        fontSize: regular,...testFont,
        marginBottom: hp('1%')
    },
    cardTitle:
    {
        color: '#5f2161',
        textAlign: 'center',
        fontSize: heading,...testFont,
        marginLeft: wp('2%')

    },
    cardTitleSub:
    {
        color: 'black',
        textAlign: 'center',
        fontSize: regular,...testFont,
        paddingVertical: wp('3%'),
        paddingHorizontal: wp('1%'),

    },
    memberCardStyle: {
        //container member card
        borderRadius: 5,
        height: wp("40%"),
        width: wp("40%"),
        backgroundColor: COLORS.cardBackground
    },
    memberCardStyleWhite: {
        //container member card
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        height: wp("40%"),
        width: wp("40%"),
        backgroundColor: COLORS.white
    },
    memberCard: {
        //header member card
        flexDirection: 'row',
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: COLORS.white,
        width: wp("40%"),
        height: wp('10%'),
        paddingStart: wp('3%')

    }
});





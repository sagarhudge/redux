

import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { widthToDp as wp, heightToDp as hp } from '../../assets/StyleSheet/Responsive';
import { Card, CardItem, Body } from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
import { getGateWayToken, getAxiosWithToken, postAxiosWithToken } from '../../constants/CommonServices'
import BaseUrl from '../../constants/BaseUrl';
import { Alert } from 'react-native';
import AppStrings from '../../constants/AppStrings';
import { passwordCheck } from '../../constants/utils';
import COLORS from '../../constants/COLORS';
import { TexhnicalError } from '../../components/TexhnicalError';
import { testFont, FontsApp, cardTitle } from '../../constants/FontsApp'
import ReleaseManagement from "../../constants/Dev_Production";
import { getSendNeedsToContact } from '../../constants/GetCardData';

let heading = FontsApp.heading;
let subHeading = FontsApp.headingCardLarge
let regular = FontsApp.regular
let smallText = FontsApp.smallText
export default class ContactCard extends Component {
    constructor(props) {
        super(props)
        this.sourceID = ''
        this.state = {
            contactNumber: '',
            cardId: '',
            network: '',
            error: '',
            agentemail: '',
            agentname: '',
            agentno: '',
            enableCard: false
        }
    }
    async componentDidMount() {

        this.idcardData = JSON.parse(await AsyncStorage.getItem("GetIdCardData"))
        let provider_network = await AsyncStorage.getItem("provider_network");
        let AgentInformation = JSON.parse(await AsyncStorage.getItem("AgentInformation"))
        let cardEnable = passwordCheck.ArrayFilterFunction(this.props.SpecialistArray, AppStrings.contact_type);

        this.setState({
            network: provider_network ? provider_network : '',
            enableCard: cardEnable && cardEnable.enable ? cardEnable.enable : false
        });

        if (AgentInformation) {
            //get agent information from async storage 
            this.setState({
                agentemail: AgentInformation.response.email,
                agentname: AgentInformation.response.name,
                agentno: AgentInformation.response.phone,
            });
        }

        this.sourceID = await AsyncStorage.getItem("sourceID");

        if (this.idcardData) {

            //Contact number and card Id get
            this.setContactandCardID(this.idcardData[0]);

            //networl provider agentInfo get api calls
            this.getNetworkProviderData(this.idcardData[0].planId);
            this.agentInfoget(this.sourceID)
        }

    }
    agentInfoget(sourceid) {
        if (sourceid) {
            let url = BaseUrl.FABRIC_V4 + "enrollment/getAgentDetails/" + sourceid
            console.log("agent Information -------------", url)
            getAxiosWithToken(url, "").then(res => {

                if (res.data && res.data.code == 200 && res.data.response) {

                    this.setState({
                        agentemail: res.data.response.email,
                        agentname: res.data.response.name,
                        agentno: res.data.response.phone,
                    }, () => {
                        AsyncStorage.setItem("AgentInformation", JSON.stringify(res.data))
                    });
                } else {
                     this.resetInfo();

                }

            })
        } else {
            this.resetInfo();
        }
    }

    resetInfo() {
        this.setState({
            agentemail: '',
            agentname: '',
            agentno: '',
        });
    }

    getNetworkProviderData(plainid) {
        console.log("this.plainid ", plainid, " this.sourceID " + this.sourceID);
        let url = BaseUrl.FABRIC_V7 + "/memberportal/getProviderNetwork/" + plainid;
        getAxiosWithToken(url, "").then(res => {

            if (res && res.data) {
                console.log("provider_network-Contact", res.data.provider_network);
                this.setState({ network: res.data.provider_network }, () => {
                    AsyncStorage.setItem("provider_network", res.data.provider_network)
                })
            } else {
                console.log("contactcard", res.message);

            }

        }).catch(e => {
            this.setState({ error: e.message });
        });

    }
    setContactandCardID(data) {

        data.planInfo.map((data, index) => {
            //   console.log("setContactandCardID", data.idcardField, data.fieldValue);

            if (data.idcardField == "contact number") {
                this.setState({ contactNumber: data.fieldValue })
            }
            if (data.idcardField == "card id") {
                this.setState({ cardId: data.fieldValue }, () => {

                })
            }
        })

    }
    render() {
        return (
            <View style={{ alignItems: 'center' }} >
                <Card style={{ width: wp('90%'), marginTop: hp('2%') }}>
                    <CardItem button bordered >
                        <TouchableOpacity
                            style={{
                                flexDirection: 'row', justifyContent: 'space-around',
                                alignItems: 'center'
                            }}>
                            <Image
                                style={{
                                    height: wp("6%"), width: wp("6%"), resizeMode: 'contain'
                                }}
                                source={require('../../assets/images/contact_info_icon_active_big.png')}
                            />
                            <Text style={cardTitle}>{AppStrings.ContactInformation}</Text>

                        </TouchableOpacity>
                    </CardItem >
                    {


                        < CardItem style={{ backgroundColor: COLORS.background }} >
                            {
                                this.state.enableCard ?
                                    this.getPhcsView() :
                                    <TexhnicalError title={this.state.contactNumber} />

                            }


                        </CardItem>

                    }


                </Card>
            </View >
        )
    }
    getPhcsView() {
        return (

            <View style={{ paddingHorizontal: hp('1%') }}>
                <Text style={{
                    fontSize: regular,
                    color: '#4f4f4f', ...testFont
                }}>For Pre-notification or Customer Service call:</Text>

                <Text
                    onPress={() => {
                        passwordCheck.dialNumber(this.state.contactNumber)
                    }} style={styles.heading}>{this.state.contactNumber}</Text>

                <Text style={styles.textStyle}>For Telemedicine call:</Text>

                <Text
                    onPress={() => {
                        passwordCheck.dialNumber('1 (888) 501-2405')
                    }}
                    style={styles.heading}>1 (888) 501-2405</Text>
                <Text style={styles.textStyle}>Send needs to:</Text>

                <Text style={styles.heading}>{getSendNeedsToContact(this.state.network)}</Text>

                {
                    this.state.agentname || this.state.agentno || this.state.agentemail
                        ?
                        <View>
                            <Text style={styles.textStyle}>Your Agent Details:
                        </Text>
                            <Text style={styles.heading}>{this.state.agentname}</Text>

                            <Text
                                onPress={() => {
                                    passwordCheck.dialNumber(this.state.agentno)
                                }}
                                style={styles.heading}>{this.state.agentno}</Text>

                            <Text
                                onPress={() => {
                                    passwordCheck.openEmail(this.state.agentemail)
                                }}
                                style={styles.heading}>{this.state.agentemail}</Text>
                        </View>
                        :
                        null
                }



            </View>);
    }

}

const styles = StyleSheet.create({

    heading: { fontSize: regular, ...testFont, color: '#33ab8f', fontWeight: 'bold' },
    textStyle: {
        fontSize: regular, ...testFont, color: '#4f4f4f', marginTop: hp("1%")
    },

});






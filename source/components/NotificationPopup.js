
import React, { Component } from "react";
import { DeviceEventEmitter, Modal, StyleSheet, Text, TouchableOpacity, View, FlatList, Image } from "react-native";
import { getGateWayToken, getAxiosUser, getAxiosWithToken, postAxiosWithToken, getData } from '../constants/CommonServices'

import COLORS from "../constants/COLORS"
import BaseUrl from '../constants/BaseUrl'
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import { passwordCheck } from '../constants/utils';
import AppStrings from "../constants/AppStrings";
import moment from "moment";
import { FontsApp ,testFont} from '../constants/FontsApp'
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let errorMessage = "You currently have no new notifications.";

export default class NotificationPopup extends Component {
    constructor(props) {
        super(props);
        this.navigation = this.props.navigation;
        this.accessToken = '';
        this.email = '';
        this.state = {
            data: [],
            filterArray: [],
            loading: false,
            error: ' ',
            loadMore: false,
            Connection: true,
            showNotification: this.props.showNotification
        }

    }

    componentDidMount() {

        this.getData();

    }
    async getData() {
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
        this.email = await AsyncStorage.getItem('email');
        this.getNotificationDetails();
    }

    render() {
        return (
            <Modal
                transparent={true}
                animationType={'none'}
                visible={true}
                onRequestClose={() => null}>
                <TouchableOpacity
                    onPress={() => {
                        this.callBackDismiss(false);
                    }}
                    style={stylesSheet.container}>

                    <View style={stylesSheet.bottomView}>
                        {
                            this.state.showNotification && this.state.data.length > 0 ?
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    numColumns={1}
                                    data={this.state.data}
                                    renderItem={this.renderItem.bind(this)}
                                    onEndReachedThreshold={1}
                                    ListFooterComponent={
                                        this.state.filterArray.length > 5 ? <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                            <Text onPress={() => this.goToNext()}
                                                style={{ padding: wp('5%'), }}>
                                                {`See all`}</Text>
                                        </View> : null
                                    }

                                    keyExtractor={(item, index) => index.toString()}
                                /> : null
                        }
                    </View >

                </TouchableOpacity>
            </Modal>
        );
    }
    callBackDismiss() {
        this.props.notificationDismiss();
    }

    goToNext() {
        this.setState({ showNotification: false, data: [] }, () => {
            this.props.navigation.navigate(AppStrings.NotificationDetails, { notification: true });
        })
    }

    renderItem({ item }) {
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: item.status === 'sent' ? '#ffffff' :
                        COLORS.white, flexDirection: 'row',
                    alignItems: 'center', paddingVertical: wp('3%'),
                    borderBottomColor: COLORS.graya1, borderBottomWidth: 1.0
                }}
                onPress={() => {
                    this.callBackDismiss(true);
                   // this.updateNotificationStatus(item.notificationID, item.urlPath);
                   DeviceEventEmitter.emit('customName', { data: item, type: 'View' })

                }}>

                <Image
                    style={{ marginRight: wp('2%'), height: wp("10%"), width: wp("10%"), resizeMode: 'contain' }}
                    source={passwordCheck.notificationIcon(item.title)} />

                <View style={{ width: wp('60%') }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                        <Text style={{
                            color: 'black',
                            fontSize: regular,...testFont,

                        }}>{item.title}</Text>

                        <Text style={{
                            color: 'black',
                            fontSize: regular,...testFont,
                        }}>
                            {passwordCheck.dateformat(item.created_date)}
                        </Text>
                    </View>
                    <Text style={{
                        color: 'black', flex: 1,
                        fontSize: regular,...testFont,
                    }}>{item.message.trim()}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    getNotificationDetails() {
        var notificatioGet = BaseUrl.V2_API_PATH + "Twillio/getNotificationDetails/" + this.email
        this.setState({ loading: true, loadMore: false }, () => { });
         getAxiosUser(notificatioGet)
            .then(success => {
                if (success != null && success.data && success.data.code === 200) {
                    var response = success.data.response.notificationDetails;
                    var filterArray = response.filter(
                        data => data.status == 'sent'
                    );
                    this.setState({
                        data: filterArray.slice(0, 5),
                        filterArray: filterArray,
                        loading: false, error: ''
                    }, () => {
                    });

                } else {
                    let message = passwordCheck.responseError(success);
                    console.log(message);
                    this.setState({ loading: false, error: message == "Error" || message == "Notification details not available" ? errorMessage : message }, () => { });
                }

            }).catch(error => {
                this.setState({ loading: false, error: error.message }, () => { });
            });
    }
    updateNotificationStatus(notification_id, path) {
        if (notification_id) {
            var statusUrl = BaseUrl.V2_API_PATH + "Twillio/updateNotificationStatus";
            var postObject = {
                "notificationId": notification_id
            }
            postAxiosWithToken(statusUrl, postObject, "").then(resp => {
                // this.getNotificationDetails();
                passwordCheck.navigationUtil(path, this.props, this.state.Connection);

            });
        } else {
            console.log("notificationId- null");
        }

    }
};
const stylesSheet = StyleSheet.create({

    bottomView: {
        width: wp('80%'),
        maxHeight: hp("60%"),
        backgroundColor: '#ffffff',
        borderRadius: 5,
        marginRight: wp('4%'),
        top: 0, end: 0,
        paddingHorizontal: wp('2%')
    },

    container: {
        top: 0, paddingTop: hp('7%'), height: '100%',
        alignItems: 'flex-end',
        flexDirection: 'column',
        backgroundColor: '#00000040'
    },

});
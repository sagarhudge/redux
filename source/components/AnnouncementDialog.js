import React from 'react';
import { Text, Modal, StyleSheet, View, TouchableOpacity } from 'react-native';
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import Icon from 'react-native-vector-icons/FontAwesome';
import COLORS from '../constants/COLORS';
import { FontsApp, testFont } from '../constants/FontsApp';

let subHeading = FontsApp.subHeading;
let smallText = FontsApp.smallText;
let regular = FontsApp.regular;
import moment from "moment";
import { passwordCheck } from '../constants/utils';
import { ScrollView } from 'react-native';
import { ButtonSmall } from "../components/Buttons";

const AnnouncementDialog = props => {

    const { display, noticeData, ...attributes } = props;
    const regex = /(<([^>]+)>)/ig;
    let buttonPress = (result) => { props.announcementResult(result, noticeData); }
    // console.log("noticeData---------------------", noticeData)
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={display}
            onRequestClose={() => null}>

            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <ScrollView persistentScrollbar={true} contentContainerStyle={{ marginRight: 5 }}>
                        <View style={{ marginBottom: wp('20%'), }}>
                            <View
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    flexDirection: 'row',

                                }}>

                                <Text style={{
                                    fontSize: subHeading,
                                    ...testFont,
                                    color: COLORS.primaryColor, flex: 1,

                                    fontWeight: 'bold'
                                }}>{noticeData ? noticeData.title : ''}</Text>

                                <TouchableOpacity style={{ margin: wp('3%') }} onPress={() => buttonPress(false, noticeData)}>
                                    <Icon
                                        name="close"
                                        size={wp('6%')}
                                        color={COLORS.black} />
                                </TouchableOpacity>

                            </View>

                            <Text style={{
                                fontSize: subHeading,
                                ...testFont,
                                color: COLORS.textColor, marginVertical: wp('3%'),
                                fontWeight: 'bold'
                            }}>
                                {noticeData ? moment(noticeData.createdDate).format('MMMM DD,YYYY') : ''}
                            </Text>

                            <Text style={{
                                fontSize: regular,
                                ...testFont,
                                color: COLORS.textColor,
                            }}>{noticeData ? replaceHtml(noticeData.message) : ''}</Text>

                        </View>
                    </ScrollView>
                    <View
                        style={{
                            position: 'absolute', bottom: 0, padding: wp('5%'), backgroundColor: COLORS.white,
                            alignItems: 'center', justifyContent: 'center', width: wp('90%'), flexDirection: 'row', justifyContent: 'space-around'
                        }}>

                        <ButtonSmall title={"OK"} isDisabled={false} type="blue"
                            callback={() => buttonPress(false, noticeData)} />

                        <ButtonSmall title={"VIEW DETAILS"} isDisabled={false} type="blue"
                            callback={() => buttonPress(true, noticeData)} />

                    </View>
                </View>
            </View>
        </Modal>
    )
    function replaceHtml(txt){
        return txt.replace(/(<html[^>]+>|<html>|<\/html>)/g, "").replace(/(<body[^>]+>|<body>|<\/body>)/g, "").replace(/(<b[^>]+>|<b>|<\/b>)/g, "").replace(regex, `\n`)
    }
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },

    activityIndicatorWrapper: {
        backgroundColor: COLORS.white,
        maxHeight: hp("90%"),
        width: wp("90%"), padding: wp('4%')
    },
    button: {
        color: 'white', borderRadius: FontsApp.borderRadiusRound,
        ...testFont,
        fontSize: wp('4%'), fontWeight: 'bold', alignSelf: 'center',
        paddingHorizontal: wp('8%'), paddingVertical: wp('2%'),
        borderWidth: 2, borderColor: COLORS.blueL,
        backgroundColor: COLORS.blueL, height: wp("10%")

    }
});

export default AnnouncementDialog;
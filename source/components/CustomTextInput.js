import React from "react";
import { Button } from "react-native-elements";
import COLORS from "../constants/COLORS";
import { TouchableOpacity, Text } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
// import TextField from 'react-native-text-field';
import { FontsApp } from '../constants/FontsApp'
let heading = FontsApp.heading;
let subHeading = FontsApp.subHeading
let subHeadingCard = FontsApp.subHeadingCard
let regular = FontsApp.regular
import TextInput from "react-native-material-textinput";

export const CustomTextInput = props => {
    const { title, isDisabled, errorMessage, maxLength, placeHolder, multiline, errorId } = props;

    let onInputChange = (text, isEmpty) => {
        props.callbackInput(text, isEmpty);
    };

    let validate = (text) => {

        if (text == "") {
            // Return error message as string if the text is not valid.
            return 'Password need to be at least 4 digits.'
        }


        return true


    };

    return (
        <TextInput
            editable={!isDisabled}
            marginTop={hp('2%')}
            labelActiveTop={-25}
            value={this.state.fullName}
            fontSize={regular}
            label={title}
            labelActiveColor={COLORS.labelActiveColor}
            error={errorId == 0 ? errorMessage : ''}

            underlineColor={COLORS.underlineColor}
            underlineActiveColor={COLORS.underlineActiveColor}
            underlineHeight={hp("0.4%")}

            onChangeText={result => this.onInputChange(result, result != "")}
        />
        // <TextField
        //     title={title}
        //     placeholder={''}
        //     onInputChange={(text) => onInputChange(text, text != "")}
        //     onValidate={(text) => validate(text)}
        //     isSecured={false}
        //     isMultiline={multiline}
        //     width={300}

        //     selectionColor={COLORS.primaryColor}
        //     autoCapitalize={'none'}
        //     autoCorrect={false}
        //     style={{ marginTop: 5, marginBottom: 5 }}

        //     titleStyle={{ color: COLORS.primaryColor, fontSize: regular }}
        //     textFieldStyle={{ borderColor: COLORS.primaryColor, borderBottomWidth: 1 }}
        //     textInputStyle={{ color: COLORS.primaryColor }}
        //     placeholderStyle={{ color: COLORS.graya1 }}

        //     invalidTextFieldStyle={{ borderColor: COLORS.red }}
        //     invalidTextInputStyle={{ color: COLORS.red }}
        //     invalidHintStyle={{ fontSize: regular }}

        // />
    )


};

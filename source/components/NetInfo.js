/**
 * Created by PERSCITUS-42 on 09-Dec-19.
 */
import { Image, Text, Modal, StyleSheet, View } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import AppStrings from '../constants/AppStrings'
import React from "react";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

export default class NetInformation extends React.Component {

    constructor(props) {
        super(props);
     //   this.unsubscribe;
        this.props = props;
        this.state = { connectionInfo: '', connected: false }
    }

    callbackreturn(isConnected, type) {
        this.props.abc(isConnected);
    }

    componentDidMount() {
        this.unsubscribe = NetInfo.addEventListener(state => {
            if (state.isConnected !== this.state.connected) {
                //if its already connected or disconnected to call function again and again
                this.setState({ connected: state.isConnected });
                this.callbackreturn(state.isConnected, state.type)
            }
        });

        NetInfo.fetch().then(state => {
            this.setState({ connected: state.isConnected });
            this.callbackreturn(state.isConnected, state.type)

        });
  
    }

    componentWillUnmount() {
        this.unsubscribe && this.unsubscribe();
 
    }

    render() {
        const { connectionInfo } = this.state;

        return (

            !this.state.connected ?
                <View style={styles.bottomView}>
                    <Text style={styles.textStyle}>{AppStrings.noInternet}</Text>
                </View> : <View></View>

        );
    }
}

const styles = StyleSheet.create({

    bottomView: {
        width: '100%',
        height: hp("7%"),
        backgroundColor: '#000000',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute', //Here is the trick
        zIndex: 999,
        bottom: 0, //Here is the trick

    },
    textStyle: {
        color: '#fff',
        fontSize: 14,
    },
});
import React from 'react';
import { Text, Modal, StyleSheet, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import COLORS from '../constants/COLORS';
import Icon from 'react-native-vector-icons/FontAwesome';
import { FontsApp, testFont, } from '../constants/FontsApp'
let heading = FontsApp.heading
let subHeading = FontsApp.subHeading
let regular = FontsApp.regular
let smallText = FontsApp.smallText
const AuthorizationModal = props => {
    const {
        display, viewMode,
        ...attributes
    } = props;

    let buttonPress = (result) => {
        props.AuthorizationModal(result);
    }


    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={display}
            onRequestClose={() => null}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <View style={{ height: hp('7%'), backgroundColor: '#34AB8F', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingHorizontal: wp('3%') }}>
                        <Text style={{ fontSize: subHeading, ...testFont, color: COLORS.white, marginLeft: wp('2%'), fontWeight: 'bold' }}> Importan Privacy Notice</Text>
                        <TouchableOpacity onPress={() => buttonPress(null)}>
                            <Icon name="close" size={wp('4%')} color={COLORS.white} />
                        </TouchableOpacity>
                    </View>

                    <ScrollView>
                        <View style={{ padding: wp('3%') }}>

                            <Text style={{ fontSize: regular, ...testFont, color: COLORS.textColor, fontWeight: 'bold' }}>
                                Authorization for Universal Health Fellowship and CarynHealth to Contact Medical Provider
                            </Text>

                            <Text style={{ fontSize: regular, ...testFont, color: COLORS.textColor, marginTop: wp('2%') }}>
                                To Whom It May Concern:
                            </Text>

                            <Text style={{ fontSize: regular, ...testFont, color: COLORS.textColor, marginBottom: hp('1%'), marginTop: hp('2%') }}>
                                <Text style={{ fontSize: regular, ...testFont, color: COLORS.textColor, marginBottom: hp('3%'), marginTop: hp('2%'), fontWeight: 'bold' }}>
                                    Universal HealthShare
                                </Text>
                                (the “Program”), hereby request, authorize and grant authority to Universal Health Fellowship (“UHF”),
                            or its appointed health plan manager CarynHealth Solutions, LLC (“CarynHealth”), to contact and act on my behalf
                            in dealing with my physician and medical providers (the “Facility”) and any other interested party in reference to
                            charges for treatment, care and services provided in connection with the medical need and medical billing
                           (the “Medical Bill”), including without limitation charges that the Program has determined exceed the Allowable Claim Limits under the terms of the Program Document.
                           </Text>

                            <Text style={{ fontSize: regular, ...testFont, color: COLORS.textColor, marginBottom: hp('3%') }}>
                                Additionally, by my acceptance, I acknowledge that I have authorized the Facility to release any records and
                                information related to the Medical Bill, including Protected Health Information (PHI), to UHF and CarynHealth.
                                I am requesting that such Protected Health Information be disclosed under this authorization, as permitted by 164.508(1)(iv)
                                of the privacy regulations issued pursuant to the Health Insurance Portability and Accountability Act (“HIPAA Privacy Rule”).
                                I have retained a copy of this authorization for my records. I understand that I have the right to revoke this authorization
                                in writing, at any time, by sending a written statement of the revocation to UHF and CarynHealth.
                                Unless revoked, this release will remain in effect and valid for one year from the date of this authorization.
                        </Text>
                        </View>
                    </ScrollView>
                    <View
                        pointerEvents={viewMode ? "none" : null}
                        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', padding: wp('3%') }}>
                        <Text
                            onPress={() => { buttonPress(false) }}
                            style={{ padding: hp('2%'), borderRadius: 12, ...testFont, fontSize: smallText, backgroundColor: !viewMode ? '#FEC043' : COLORS.graylight, fontWeight: 'bold' }}>CANCEL</Text>
                        <Text
                            onPress={() => { buttonPress(true) }}
                            style={{ padding: hp('2%'), borderRadius: 12, fontSize: smallText, backgroundColor: !viewMode ? '#FEC043' : COLORS.graylight, marginLeft: wp('2%'), fontWeight: 'bold' }}>ACCEPT</Text>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: COLORS.white,
        height: hp('90%'),
        width: wp("90%"),
    }
});

export default AuthorizationModal;
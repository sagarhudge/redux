import { View, Text, StyleSheet } from "react-native";
import React, { Component } from "react";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AppStrings from "../constants/AppStrings"

const  Snakebar= props => {
  const {
    isConnected,
      ...attributes
  } = props;


    return (

      !isConnected ?
        <View style={styles.bottomView}>
          <Text style={styles.textStyle}>{AppStrings.noInternet}</Text>
        </View> : null

    );
  }

const styles = StyleSheet.create({

  bottomView: {
    width: '100%',
    height: hp("7%"),
    backgroundColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', //Here is the trick
    zIndex:999,
    top: 0, //Here is the trick

  },
  textStyle: {
    color: '#fff',
    fontSize: 14,
  },
});
export default Snakebar;
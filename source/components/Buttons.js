import React from "react";
import COLORS from "../constants/COLORS";
import { TouchableOpacity, Text, View } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { testFont } from "../constants/FontsApp";

export const ButtonSmall = props => {
    const { title, isDisabled, type } = props;

    let callbackReturn = () => {
        props.callback();
    };
    // console.log(title, isDisabled, type)

    return (

        <View>
            {/* if you are passing type then show the blue color button else red button  consider disable enable also*/}
            {type == "blue" ?
                <TouchableOpacity onPress={() => callbackReturn()} style={{
                    backgroundColor: isDisabled ? COLORS.graylight : COLORS.blueL, borderRadius: hp('3%'), borderColor: isDisabled ? COLORS.white : COLORS.blueL,
                    paddingHorizontal: wp('8%'), height: hp('6%'), justifyContent: 'center', alignItems: 'center'
                }}>
                    <Text style={{ ...testFont, textAlign: "center", fontSize: wp("4%"), color: "white", fontWeight: 'bold' }}>{title}</Text>

                </TouchableOpacity>
                :
                <TouchableOpacity onPress={() => callbackReturn()} style={{
                    backgroundColor: COLORS.cancelButton, borderRadius: hp('3%'),
                    width: wp('80%'), height: hp('6%'), justifyContent: 'center', alignItems: 'center'
                }}>
                    <Text style={{ ...testFont, textAlign: "center", fontSize: hp("2.4%"), color: "white", }}>{title}</Text>

                </TouchableOpacity>}
        </View>

    )

};

export const ButtonBorder = props => {
    const { title, isDisabled, type } = props;

    let callbackReturn = () => {
        props.callback();
    };

    return (
        <View style={{ flex: 1, margin: wp('1.5%') }}>
            <TouchableOpacity onPress={() => callbackReturn()} style={{
                backgroundColor: isDisabled ? COLORS.blueL : COLORS.white,
                borderColor: !isDisabled ? COLORS.blueL : COLORS.white, borderRadius: hp('3%'), borderWidth: 2,
                paddingHorizontal: isDisabled ? wp('8%') : wp('3%'), height: hp('6%'), justifyContent: 'center', alignItems: 'center'
            }}>
                <Text style={{ ...testFont, textAlign: "center", fontSize: isDisabled ? wp('4%') : wp('3.5%'), color: !isDisabled ? COLORS.blueL : COLORS.white, fontWeight: 'bold' }}>{title}</Text>

            </TouchableOpacity>
        </View>)

}

export const ButtonBorderEnable = props => {
    const { title, isDisabled, type } = props;

    let callbackReturn = () => {
        props.callback();
    };

    return (
        <View style={{ flex: 1, margin: wp('1.5%') }}>
            <TouchableOpacity onPress={() => isDisabled ? null : callbackReturn()} style={{
                backgroundColor: COLORS.blueL,
                borderColor: isDisabled?COLORS.graylight:COLORS.white, borderRadius: hp('3%'), borderWidth: 2,
                paddingHorizontal: wp('3%'), height: hp('6%'), justifyContent: 'center', alignItems: 'center',
            }}>
                <Text style={{ ...testFont, textAlign: "center", fontSize: wp('3.5%'), color: isDisabled?COLORS.graylight:COLORS.white, fontWeight: 'bold' }}>{title}</Text>

            </TouchableOpacity>
        </View>)

}

import { StyleSheet } from "react-native";
import COLORS from "../../constants/COLORS";

import { widthToDp, heightToDp, getDynamicStyles } from './Responsive';

export default StyleSheet.create({
    authhome_container: {
        flex: 1,
    },

    authhome_textview: {
        backgroundColor: 'yellow',

    },
    authhome_text: {
        fontSize: widthToDp("5%"),
    },
    authhome_image: {
        width: widthToDp("100%"),
        height: heightToDp("60%"),
    },
    authhome_bottomview: {
        height: heightToDp("40%"),
        // backgroundColor:'yellow',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    authhome_loginbtn: {
        width: widthToDp("80%"),
        height: heightToDp(6),
        // backgroundColor:"white",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:  FontsApp.borderRadiusRound,
        marginBottom: heightToDp("4%"),
    },
    authhome_logintext: {
        color: COLORS.white,
        fontSize: widthToDp('5%'),

    },
    authhome_createbtn: {
        width: widthToDp("80%"),
        height: heightToDp(6),
        backgroundColor: "#dcdcdc",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:  FontsApp.borderRadiusRound,
        marginBottom: heightToDp("4%"),

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,

        elevation: 6,
    },
    authhome_createtext: {
        color: COLORS.lableColor,
        fontSize: widthToDp('5%'),
    },

    // ================AuthHeader.js============

    authheader_container: {
        width: '100%',
        backgroundColor: COLORS.primaryColor,
        height: heightToDp("7%"),
        padding: widthToDp("4%"),

        // borderColor:'black',
        // borderWidth:2,
    },

    // ================AuthTitleLogo.js============
    authtitle_container: {
        width: '100%',
        // backgroundColor:'yellow',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: heightToDp("2%"),

    },
    authtitle_login_text: {
        color: COLORS.lableColor,
        fontSize: widthToDp('8%'),
        fontWeight: '700',
    },

    authtitle_logo: {
        marginTop: heightToDp('4%'),
        // width:widthToDp('25%'),
        // height:heightToDp("25%"),
        height: 100,
        width: 100,
        resizeMode: "contain",
    },

    // ======================LoginScreen.js===============



    login_container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: COLORS.white,

    },

    login_input_container: {
        marginTop: heightToDp('10%'),
        borderColor: 'green',
        borderWidth: 2,
        width: widthToDp('85%'),
        alignSelf: 'center',
        padding: widthToDp('2%'),


    },

    login_middel_container: {
        flex: 1,
        backgroundColor: COLORS.white,

        alignItems: 'center',
    },
    login_parent_view: {
        marginTop: heightToDp("15%"),
        width: widthToDp('85%'),
        height: 150,
        backgroundColor: COLORS.white,
        borderRadius: 5,
        padding: widthToDp('2%'),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,

        elevation: 9,
    },
    login_username_view: {

        flex: 1,
        flexDirection: 'row'
    },
    login_user_icon_view: {
        width: widthToDp("15%"),
        // borderColor:'black',
        // borderWidth:2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    login_input_view: {
        // width:widthToDp("15%"),
        marginLeft: 10,
        flex: 1,
        // borderColor:'green',
        // borderWidth:2,
        // alignItems:'center',
        justifyContent: 'center',
    },
    login_profile_logo: {
        width: 30,
        height: 30,
    },
    login_name_label: {
        fontSize: widthToDp(2.5),
        color: COLORS.lableColor,
    },
    login_name_input: {
        marginTop: heightToDp('-1%'),
        marginLeft: -4,
        fontSize: widthToDp(4.5),
        color: COLORS.lableColor,
    },
    login_bottom_view: {
        backgroundColor: COLORS.white,
        marginTop: heightToDp('12%'),
        // paddingBottom:50,
        // justifyContent:'flex-end',
        alignItems: 'center',


        //   borderColor:'green',
        // borderWidth:2,
    },
    login_otp_btn: {
        backgroundColor: '#F35959',
        width: widthToDp('85%'),
        // flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: widthToDp('4%'),
        borderRadius: heightToDp('4%'),
        height: heightToDp('7%'),
    },
    login_sendotp_text: {
        fontSize: widthToDp(4.5),
        color: COLORS.white,
        fontWeight: '700',

    },
    login_subtext_container: {
        marginTop: heightToDp('3%'),
        flexDirection: 'row',
    },
    login_bottom_subtext1: {
        color:COLORS.lableColor,
        fontSize: widthToDp('3%'),
    },
    login_bottom_subtext_red: {
        color: '#F35959',
        fontSize: widthToDp('3%'),
        fontWeight: '700',
    }




})
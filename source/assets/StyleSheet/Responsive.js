import { Dimensions, PixelRatio } from 'react-native'
import { getPixelSizeForLayoutSize } from 'react-native/Libraries/Utilities/PixelRatio';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const { width, height } = Dimensions.get('window');

const widthToDp = number => {
    let givenWidth =
        typeof number === 'number' ? number : parseFloat(number);

    return PixelRatio.roundToNearestPixel((width * givenWidth) / 100);
};

const heightToDp = number => {
    let givenHeight =
        typeof number === 'number' ? number : parseFloat(number);

    return PixelRatio.roundToNearestPixel((height * givenHeight) / 100);
};

const listenToOrientationChange = ref => {
    Dimensions.addEventListener('change', (newDimension) => {
        // console.log('Newdiamnsion==', newDimension)
        width: newDimension.screen.width;
        height = newDimension.screen.height;

        ref.setState({
            orientation: height > width ? 'portrait' : 'landscape',
        })
    });

}

const removeOrientationChanges = () => {
    Dimensions.removeEventListener('change');
}

const getDynamicStyles = () => {
    const isPortrait = height > width;
    if (isPortrait) {
        return "portraitStyle";
    } else {
        return "landscapeStyle";
    }
}

export { widthToDp, heightToDp, listenToOrientationChange, removeOrientationChanges, getDynamicStyles };
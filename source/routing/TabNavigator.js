// ./navigation/TabNavigator.js

import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MainStackNavigator, MemberAppsNavigator } from "../routing/StackNavigator";
const Tab = createBottomTabNavigator();
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import COLORS from "../constants/COLORS";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const TabTop = createMaterialTopTabNavigator();

const TabNavigator = () => {
  return (
    <TabTop.Navigator
      tabBarOptions={{
        indicatorStyle: {
          borderBottomWidth: 3,
          borderColor: "#ffc107",
        },
        activeTintColor: '#ffc107',
        inactiveTintColor: "#ffc107",

        style: { backgroundColor: COLORS.red },
      }}>

      <TabTop.Screen name="MEMBER SERVICES"
        options={{ tabBarLabel: 'MEMBER SERVICES' }}
        component={MainStackNavigator} />

      <TabTop.Screen name="MEMBER APPS" component={MemberAppsNavigator} />

    </TabTop.Navigator>
  );
};

export default TabNavigator;
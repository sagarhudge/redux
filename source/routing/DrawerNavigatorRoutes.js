
import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Documents from "../screens/Documents";
import MyTransactions from "../screens/transactions/MyTransactions";
import ChnagePaymentMethod from "../screens/transactions/ChnagePaymentMethod";

import HealthQuestionnaire from "../screens/healthQuestionnaire/HealthQuestionnaire";
import MyNeedsScreen from "../screens/MyNeedsScreen";
import NotificationDetails from "../screens/NotificationDetails";
import ProgramInformation from "../screens/ProgramInformation";
import ContactCardScreen from "../screens/ContactCardScreen";
//Card Wallet 
import DigitalHealthCardNew from "../screens/cardwallet//DigitalHealthCardNew";
import HealthToolCard from "../screens/cardwallet//HealthToolCard";
import HealthyShareCard from "../screens/cardwallet/HealthyShareCard";

import Home from "../screens/Home";

import CustomSidebarMenu from './CustomSidebarMenu';
import COLORS from "../constants/COLORS"
import { widthToDp } from '../assets/StyleSheet/Responsive';
import PaymentWallet from '../screens/payment/PaymentWallet';
import PaymentCard from '../screens/payment/PaymentCard';
import RequestPaymentcard from '../screens/payment/RequestPaymentcard';
import NewEnrollMent from '../screens/NewEnrollMent';
import ChatScreen from '../screens/botchat/ChatScreen';
 import ProgressBar from '../screens/botchat/ProgrssBar';
 const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const homeScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      options={{ headerShown: false }}
      initialRouteName="Home">

      <Stack.Screen
        name="Home"
        options={optionsDrawer}
        component={Home} />

      <Stack.Screen name="Documents"
        options={optionsDrawer}
        component={Documents} />

      <Stack.Screen name="MyTransactions"
        options={optionsDrawer}
        component={MyTransactions} />

      <Stack.Screen name="HealthQuestionnaire"
        options={optionsDrawer}
        component={HealthQuestionnaire} />

      <Stack.Screen name="MyNeedsScreen"
        options={optionsDrawer}
        component={MyNeedsScreen} />

      <Stack.Screen name="ProgramInformation"
        options={optionsDrawer}
        component={ProgramInformation} />

      <Stack.Screen name="NotificationDetails"
        options={optionsDrawer}
        component={NotificationDetails} />

      <Stack.Screen name="DigitalHealthCardNew"
        options={optionsDrawer}
        component={DigitalHealthCardNew} />

      <Stack.Screen name="HealthToolCard"
        options={optionsDrawer}
        component={HealthToolCard} />

      <Stack.Screen name="HealthyShareCard"
        options={optionsDrawer}
        component={HealthyShareCard} />

      <Stack.Screen name="ChnagePaymentMethod"
        options={optionsDrawer}
        component={ChnagePaymentMethod} />

      <Stack.Screen name="ContactCardScreen"
        options={optionsDrawer}
        component={ContactCardScreen} />

      <Stack.Screen name="PaymentWallet"
        options={optionsDrawer}
        component={PaymentWallet} />

      <Stack.Screen name="RequestPaymentcard"
        options={optionsDrawer}
        component={RequestPaymentcard} />

      <Stack.Screen name="PaymentCard"
        options={optionsDrawer}
        component={PaymentCard} />

      <Stack.Screen name="NewEnrollMent"
        options={optionsDrawerHide}
        component={NewEnrollMent} />

      <Stack.Screen name="ChatScreen"
        options={optionsDrawerHide}
        component={ChatScreen} />
 
      <Stack.Screen name="ProgressBar"
        options={optionsDrawerHide}
        component={ProgressBar} />
    </Stack.Navigator>
  );
};

const optionsDrawer = {
  headerShown: true, headerBackTitle: ' '
}

const optionsDrawerHide = {
  headerShown: false, headerBackTitle: ' '

}
const DrawerNavigatorRoutes = (props) => {
  return (
    <Drawer.Navigator
      drawerContentOptions={{
        activeTintColor: COLORS.white,
      }}
      drawerStyle={{
        width: widthToDp('70%'),
      }}
      screenOptions={{ headerShown: false }}
      drawerContent={CustomSidebarMenu}>

      <Drawer.Screen
        name="homeScreenStack"
        component={homeScreenStack}
      />

    </Drawer.Navigator>
  );
};

export default DrawerNavigatorRoutes;

import React, { Component } from 'react';
import { View, Text, Image, FlatList, StyleSheet, Linking, TouchableOpacity, DeviceEventEmitter, BackHandler } from 'react-native';
import AppStrings from "../constants/AppStrings"
import COLORS from "../constants/COLORS"
import { widthToDp as wp, heightToDp as hp } from '../assets/StyleSheet/Responsive';
import AsyncStorage from '@react-native-community/async-storage';
import { draweMenuList } from '../constants/ArrayConst'
import { passwordCheck } from '../constants/utils';
import { isAndroid } from '../constants/PlatformCheck'
import { postAxiosWithToken, getCardEnable } from '../constants/CommonServices';
import BaseUrl from '../constants/BaseUrl';
import AwesomeAlert from 'react-native-awesome-alerts';
import ReleaseManagement from '../constants/Dev_Production'
import { SafeAreaView } from 'react-native';
import { FontsApp, testFont } from '../constants/FontsApp'
import SendIntentAndroid from "react-native-send-intent";
import RNExitApp from 'react-native-exit-app';

let headingCard = wp('3.5%');
let regular = FontsApp.regular
let smallText = FontsApp.smallText
export default class MenuList extends Component {
    constructor(props) {
        super(props);
        this.AllCards;
        this.email = '';
        this.accesstoken;
        this.state = {
            loading: false,
            userName: "@@",
            Connection: true,
            title: AppStrings.exitApplication,
            message: '',
            alertCancel: AppStrings.cancel,
            alertConfirm: AppStrings.ok,
            alertCLose: AppStrings.cancel,
            show: false,
            canBeClosed: true,
            showConfirmButton: true,
            Connection: null,
        }
    }

    async componentDidMount() {
        this.DeviceEventEmitter = DeviceEventEmitter.addListener('connectionCheck', this.connectionCheck.bind(this));

        await AsyncStorage.getItem('username').then(data => {
            this.setState({ userName: data })
        });
        this.AllCards = JSON.parse(await AsyncStorage.getItem('AllCards'));
        this.email = await AsyncStorage.getItem('email');
        this.accesstoken = await AsyncStorage.getItem('accesstoken');
    }

    connectionCheck = (connectionCheck) => {
        this.setState({ Connection: connectionCheck });
    }

    componentWillUnmount() {
        this.DeviceEventEmitter.remove();
    }

    menuNavigation = async (item) => {
        this.props.navigation.closeDrawer();

        switch (item.title) {
            case AppStrings.notices:
                Linking.openURL(item.link);
                break;
            case AppStrings.FAQs:
                Linking.openURL(item.link);
                break;
            case AppStrings.telemed:
                this.checkMedLifeapp(item.androidDeeplink, item.iosDeeplink);
                break;
            case AppStrings.signOut:
                //Sign out
                this.setState({ show: true, title: '', canBeClosed: true, message: "Are you sure you want to sign-out?\nThis will log you off and close the app.", alertConfirm: 'YES', alertCancel: 'NO' });
                break;
            // case AppStrings.ContactInformation:
            //     //show Contact Card
            //     this.props.navigation.navigate(AppStrings.ContactCardScreen, { SpecialistArray: this.AllCards });
            //     break;
            case AppStrings.findProvider:
                this.findProvider();
                break;

            default: {
                this.client_id_locale = await AsyncStorage.getItem('client_id_locale');

                getCardEnable(this.AllCards, item.cardType).then(
                    enable => {
                        console.log("MENU CARD IS ENABLE ", item.cardType, enable);
                        if (enable && item.cardType == 'MembershipId') {

                            if (this.client_id_locale == "6548" || this.client_id_locale == 6548 ||
                                this.client_id_locale == "4367" || this.client_id_locale == 4367
                                || this.client_id_locale == "4376" || this.client_id_locale == 4376
                                || this.client_id_locale == "5540" || this.client_id_locale == 5540
                            ) {
                                this.props.navigation.navigate(AppStrings.HealthyShareCard);
                            }
                            else
                                this.props.navigation.navigate(AppStrings.DigitalHealthCardNew);

                        } else {
                            if (enable) {
                                if (item.cardType == AppStrings.NotificationDetails) {
                                    this.props.navigation.navigate(AppStrings.NotificationDetails, { notification: true });
                                } else if (item.cardType == AppStrings.announcementsNotices_type) {
                                    this.props.navigation.navigate(AppStrings.NotificationDetails, { notification: false });
                                } else
                                    this.props.navigation.navigate(item.navigate, this.state.Connection);
                            } else {
                                console.log("-------------------" + item.cardType);
                                if (item.cardType == AppStrings.NotificationDetails) {
                                    this.props.navigation.navigate(AppStrings.NotificationDetails, { notification: true });
                                } else if (item.cardType == AppStrings.announcementsNotices_type) {
                                    this.props.navigation.navigate(AppStrings.NotificationDetails, { notification: false });
                                } else {
                                    this.displayDisablePopup();
                                }
                            }
                        }

                    }
                );

            }
        }
    };

    checkMedLifeapp = (link, ioslink) => {
        console.log("link, ioslink", link, ioslink);
        if (isAndroid) {
            SendIntentAndroid.isAppInstalled('com.mdlive.mobile').then((isInstalled) => {
                if (isInstalled) {
                    SendIntentAndroid.openApp('com.mdlive.mobile').then((wasOpened) => {
                    });
                }
                else {
                    Linking.openURL(link).catch(err => {
                    })
                }
            });
        }
        else {
            Linking.openURL(ioslink).catch(err => {
            })
        }
    };

    findProvider() {

        if (this.state.Connection) {
            let url = BaseUrl.FABRIC_V1 + 'memberportal/planinfo';
            let request = {
                'email': this.email,
                'type': "findaprovider"
            }
            console.log("findProvider", url);
            console.log("findProvider--", this.accesstoken, request);

            this.setState({ loading: true });

            postAxiosWithToken(url, request, this.accesstoken).then(success => {

                this.setState({ loading: false });

                if (success && success.data && success.data.length != 0) {

                    Linking.openURL(success.data[0].fieldValue);

                } else {
                    var message = success.message;
                    let include = passwordCheck.getSessionStatus(message)
                    DeviceEventEmitter.emit('sessionOut', include)
                }

            }).catch(e => {
                console.log("MenuList", e.message);
                this.setState({ loading: false });
            });

        } else {
            console.log("write session code");
            console.log("findProvider", AppStrings.noInternet);

        }

    }

    render() {
        return (
            <SafeAreaView style={stylesSidebar.sideMenuContainer}>
                <View style={stylesSidebar.sideMenuContainer}>
                    <Image
                        resizeMode='cover'
                        source={require('../assets/images/UHS.png')}
                        style={{ width: undefined, height: hp('10%') }}
                    />

                    <View style={stylesSidebar.profileHeader}>

                        <Image
                            source={require('../assets/images/profile_user.png')}
                            style={stylesSidebar.imageStyle}
                        />

                        <Text style={stylesSidebar.profileHeaderText}>
                            {"Hello, " + this.state.userName}
                        </Text>
                    </View>
                    <View style={stylesSidebar.profileHeaderLine} />
                    {/* Design custom map list of drawer here */}

                    <FlatList
                        persistentScrollbar
                        showsVerticalScrollIndicator={true}
                        scrollEnabled={true}
                        numColumns={1}
                        data={draweMenuList}
                        ListFooterComponent={<Text style={{
                            width: '100%', fontSize: regular, ...testFont,
                            textAlign: 'center', marginTop: hp('2%'),
                            padding: wp('2%'), backgroundColor: COLORS.background
                        }}>
                            {ReleaseManagement.AppVersion}
                        </Text>}
                        renderItem={({ item, index }) => this.renderItem(item, index)}
                        keyExtractor={(item, index) => index.toString()} />
                    {this.displayAlert()}
                </View>
            </SafeAreaView>
        )
    }
    renderItem(item, index) {
        var imageNames = passwordCheck.imageofCardByName(item.title);

        return (
            <TouchableOpacity
                onPress={() => { this.menuNavigation(item); }}
                style={{
                    flexDirection: 'row', alignItems: 'center', borderRightColor: index == 0 ? COLORS.primaryColor : COLORS.white, borderRightWidth: index == 0 ? 6 : 0,
                    paddingLeft: hp('2%'), paddingVertical: hp('1%'), backgroundColor: index == 0 ? COLORS.background : "#ffffff"
                }}>
                <Image
                    source={imageNames}
                    style={stylesSidebar.imageStyle}
                />
                <Text style={{
                    fontSize: headingCard, textAlign: 'center', ...testFont,
                    paddingLeft: hp('2%'), color: COLORS.textColor
                }}>
                    {item.title}
                </Text>
            </TouchableOpacity>
        )
    }
    displayDisablePopup() {
        let message = `We’re facing some technical difficulties, due to which this feature is currently unavailable. For support, call Member Services at ${this.ContactNumber}, Monday through Friday, 8.00am to 8.00pm CST.`
        this.showAlert("", message);
    }
    showAlert(title, message) {
        this.setState({
            alertType: '', show: true,
            title: title, message: message == "Network Error" ? AppStrings.noInternet : message, alertConfirm: AppStrings.ok,
            canBeClosed: true, alertCancel: AppStrings.cancel, showConfirmButton: false
        });

    }
    displayAlert() {
        return <AwesomeAlert
            show={this.state.show}
            showProgress={false}
            title={this.state.title}
            message={this.state.message}
            closeOnTouchOutside={false}
            closeOnHardwareBackPress={false}

            titleStyle={{ color: COLORS.textColor }}
            messageStyle={{ color: COLORS.textColor }}
            showConfirmButton={this.state.showConfirmButton}
            showCancelButton={
                this.state.canBeClosed
            }
            cancelText={this.state.alertCancel}
            confirmText={this.state.alertConfirm}
            confirmButtonColor={COLORS.primaryColor}
            cancelButtonColor={COLORS.cancelButton}
            onCancelPressed={() => {
                this.setState({ show: false })
            }}
            onConfirmPressed={() => {
                this.setState({ show: false })
                AsyncStorage.setItem("isLogin", "false")
                RNExitApp.exitApp();

            }}
        />
    }
}

const stylesSidebar = StyleSheet.create({
    sideMenuContainer: {
        height: '100%',
        backgroundColor: COLORS.white,
        paddingTop: wp("1%"),
        color: COLORS.white, flex: 1,

    },
    profileHeader: {
        flexDirection: 'row',
        backgroundColor: COLORS.white, alignItems: 'center',
        paddingLeft: hp('2%'), paddingTop: "1%",
        textAlign: 'center',
    },
    profileHeaderPicCircle: {
        width: hp('3%'),
        height: hp('3%'),
        borderRadius: hp('3%') / 2,
        color: COLORS.white,
        backgroundColor: COLORS.primaryColor,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileHeaderText: {
        color: 'black', maxWidth: wp('55%'),
        paddingVertical: wp('3%'),
        fontWeight: 'bold',
        fontSize: headingCard, ...testFont, marginLeft: wp('4%')
    },
    profileHeaderLine: {
        height: 1,
        backgroundColor: COLORS.grayDark,
    },
    imageStyle: {
        width: wp('5%'), resizeMode: 'contain', height: hp('5%')

    }
});
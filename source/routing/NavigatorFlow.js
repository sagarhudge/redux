// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
import 'react-native-gesture-handler';

// Import React and Component
import React from 'react';

// Import Navigators from React Navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// Import Screens
import SplashScreen from '../authentication/SplashScreen';
import ForgotPassword from '../authentication/aws/ForgotPassword';
import SignInCognito from '../authentication/aws/SignInCognito';

import AppNavigator from '../routing/AppNavigator';
import FcmNotification from '../firebase/FcmNotification';
import ChatScreen from '../screens/botchat/ChatScreen';
import ProgressBar from '../screens/botchat/ProgrssBar';

const Stack = createStackNavigator();

const name = "LoginScreen";

const Auth = () => {
  // Stack Navigator for Login and Sign up Screen
  return (
    <Stack.Navigator initialRouteName={name}>


      <Stack.Screen
        name="SignInCognito"
        component={SignInCognito}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{ headerShown: false }}

      />
    </Stack.Navigator>
  );
};
 

const NavigationFlow = () => {
  return (
    <NavigationContainer>
      <FcmNotification />

      <Stack.Navigator initialRouteName="SplashScreen">
        {/* SplashScreen which will come once for 5 Seconds */}
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          // Hiding header for Splash Screen
          options={{ headerShown: false }}
        />
        {/* Auth Navigator: Include Login and Signup */}
        <Stack.Screen
          name="Auth"
          component={Auth}
          options={{ headerShown: false }}
        />
        {/* Navigation Drawer as a landing page */}
        <Stack.Screen
          name="AppNavigator"
          component={AppNavigator}
          // Hiding header for Navigation Drawer
          options={{ headerShown: false }}
        />
       
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default NavigationFlow;

import React from 'react';

import MenuList from "./MenuList"


export default CustomSidebarMenu = (props) => {

  return (
    <MenuList navigation={props.navigation} />
  );


};



import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../screens/Home";
import MemberApps from "../screens/MemberApps";
import Documents from "../screens/Documents";
const Stack = createStackNavigator();
 
const screenOptionStyle = {
  headerShown: false
};

const MainStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  );
};

const MemberAppsNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="MemberApps" component={MemberApps} />
    </Stack.Navigator>
  );
};
const DocumentsNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Documents" component={Documents} />
    </Stack.Navigator>
  );
};

export { MainStackNavigator, MemberAppsNavigator,DocumentsNavigator };